# 实名认证API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [认证](#认证)
	* [重新认证](#重新认证)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:       |:------------:                               |:-------:       |
| realName        | string     | 是              | 张三三                                      | 姓名             |
| positivePhoto   | array      | 是              | array("name"=>"正面", "identify"=>"1.jpg")  | 身份证正面照片     |
| reversePhoto    | array      | 是              | array("name"=>"反面", "identify"=>"1.jpg")  | 身份证反面照片     |
| handheldPhoto   | array      | 是              | array("name"=>"正面", "identify"=>"1.jpg")  | 手持身份证正面照片 |
| cardId          | string     | 是              | 412825199090983456                         | 身份证号          |
| updateTime      | int        |                | 1535444931                                  | 更新时间         |
| creditTime      | int        |                | 1535444931                                  | 创建时间         |
| applyStatus     | int        |                | 0                            				  | 审核状态  (0待审核 2审核通过 -2审核驳回)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
NATURALPERSON_FETCH_ONE
```
**请求示例**

```
$naturalPerson = $this->getRepository()->scenario(NaturalPersonepository::NATURALPERSON_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
naturalPerson: NaturalPerson
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
NATURALPERSON_LIST
```
**请求示例**

```
$naturalPersonList = $this->getRepository()->scenario(NaturalPersonRepository::NATURALPERSON_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
naturalPersonList: {
	0 => [
		NaturalPerson //object
	],
	1 => [
		NaturalPerson //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
NATURALPERSON_LIST
```
**请求示例**

```
list($count, $naturalPersonList) = $this->getRepository()->scenario(NaturalPersonRepository::NATURALPERSON_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
naturalPersonList: {
	0 => [
		NaturalPerson //object
	],
	1 => [
		NaturalPerson //object
	],
}
```
### <a name="认证">认证</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"naturalPersons",
		"attributes"=>array(
			"realName"=>"张三三",
			"cardId"=>"412825199809804323",
			"positivePhoto"=>array(
				'name' => '身份证正面照片name', 'identify' => '身份证正面照片identify'
			),
			"reversePhoto"=>array(
				'name' => '身份证反面照片name', 'identify' => '身份证反面照片identify'
			),
			"handheldPhoto"=>array(
				'name' => '手持身份证正面照片name', 'identify' => '手持身份证正面照片identify'
			),
		),
		"relationships"=>array(
			"member"=>array(
				"data"=>array(
					array("type"=>"members","id"=>2)
				)
			)
		)
	)
);
```
**调用示例**

```
$naturalPerson = $this->getNaturalPerson();

$naturalPerson->setRealName($command->realName);
$naturalPerson->setIdentityInfo(
    new IdentityInfo(
        $command->cardId,
        $command->positivePhoto,
        $command->reversePhoto,
        $command->handheldPhoto
    )
);

$naturalPerson->add();

```
### <a name="重新认证">重新认证</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"naturalPersons",
        "attributes"=>array(
        	"realName"=>"姓名",
        	"cardId"=>"身份证号",
            "positivePhoto"=>array(
                'name' => '身份证正面照片name', 'identify' => '身份证正面照片identify'
            ),
            "reversePhoto"=>array(
                'name' => '身份证反面照片name', 'identify' => '身份证反面照片identify'
            ),
            "handheldPhoto"=>array(
                'name' => '手持身份证正面照片name', 'identify' => '手持身份证正面照片identify'
            )
        )
    )
);
```
**调用示例**

```
$naturalPerson = $this->getRepository()->fetchOne(int $id);

$naturalPerson->setRealName($command->realName);
$naturalPerson->setIdentityInfo(
    new IdentityInfo(
        $command->cardId,
        $command->positivePhoto,
        $command->reversePhoto,
        $command->handheldPhoto
    )
);

$naturalPerson->edit();

```
### <a name="审核通过"> 审核通过 </a>

**调用示例**

```
$naturalPerson = $this->getRepository()->fetchOne(int $id);

$naturalPerson->approve();

```
### <a name="审核驳回"> 审核驳回 </a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"naturalPersons",
        "attributes"=>array(
        	"rejectReason"=>"审核驳回原因"
        )
    )
);
```

**调用示例**

```
$naturalPerson = $this->getRepository()->fetchOne(int $id);

$naturalPerson->setRejectReason($command->rejectReason);

$naturalPerson->reject();

```