# 充值 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
  * [获取单条数据场景](#获取单条数据场景)
  * [获取多条数据场景](#获取多条数据场景)
  * [根据检索条件查询数据](#根据检索条件查询数据)	
  * [充值](#充值)
  * [确认充值完成](#确认充值完成)
  * [充值失败](#充值失败)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例              | 描述       |
| :---:       | :----:     | :----:            | :----:           |:----:     |
| transactionNumber | string  |                | 1                | 交易流水号   |
| number      | string        |                | 1                | 交易单号   |
| memberAccount  | MemberAccount |  是         |                   | 所属账户  |
| amount      | string     | 是                | 100               | 充值金额  |
| status      | int        |                   |                   | 状态     |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$deposit = $this->getRepository()->scenario(DepositRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
deposit: Deposit
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
$depositList = $this->getRepository()->scenario(DepositRepository::LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
depositList: {
	0 => [
		Deposit //object
	],
	1 => [
		Deposit //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
list($count, $depositList) = $this->getRepository()->scenario(AppointmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
depositList: {
	0 => [
		Deposit //object
	],
	1 => [
		Deposit //object
	],
}
```

### <a name="充值">充值</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"deposits",
        "attributes"=>array(
            "amount"=>"1000"
        ),
        "relationships"=>array(
            "memberAccount"=>array(
                "data"=>array(
                    array("type"=>"memberAccounts","id"=>7)
                )
            ),
        )
    )
);
```

**调用示例**

```
$deposit = $this->getDeposit();
$deposit->setMemberAccount($memberAccount);
$deposit->setAmount($command->amount);

$deposit->deposit();
```

### <a name="确认充值完成">确认充值完成</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"deposits",
        "attributes"=>array(
            "paymentType" => 1,
            "transactionNumber" => '2020011922001423581412762014',
            "transactionInfo" => array(),
        )
    )
);
```

### <a name="充值失败">充值失败</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"deposits",
        "attributes"=>array(
            "paymentType" => 1,
            "failureReason" => array(),
        )
    )
);
```