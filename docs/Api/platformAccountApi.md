# 平台账户 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
    * [获取单条数据场景](#获取单条数据场景)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| accountBalance | string     |                |   200           | 账户余额    |
| incomeAmount | string     |                |   200           | 收入余额    |
| expenditureAmount | string     |                |   200           | 支出余额    |
| profitAmount | string     |                |   200           | 收益余额    |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$platformAccount = $this->getRepository()->scenario(PlatformAccountRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
platformAccount: PlatformAccount
```