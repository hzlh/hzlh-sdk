# 金融机构认证 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [认证](#认证)
	* [编辑认证信息](#编辑认证信息)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述             |
| :---:           | :----:     | :------:      |:------------:                                |:-------:        |
| enterpriseName  | string     | 是            | 信息传媒有限公司                                | 企业名称          |
| code            | string     | 是            | 923456091291023456                           | 机构编码           |
| licence         | array      | 是            | array("name"=>"licence", "identify"=>"1.jpg")| 金融许可证         |
| organizationsCategory        | int        |  是             | 1                                 | 机构类型        |
| organizationCode       | string        |  是             | 9234560912910                               | 机构代码       |
| organizationsCategoryParent        | int        |  是             | 1                                 | 机构类型父级        |
| organizationCodeCertificate       | array        |  是             |  array("name"=>"organizationCodeCertificate", "identify"=>"1.jpg")                                | 金融机构代码证      |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                            | 状态 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$financeAuthentication = $this->getRepository()->scenario(FinanceAuthenticationRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
financeAuthentication: FinanceAuthentication
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
$financeAuthenticationList = $this->getRepository()->scenario(FinanceAuthenticationRepository::LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
financeAuthenticationList: {
	0 => [
		FinanceAuthentication //object
	],
	1 => [
		FinanceAuthentication //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
list($count, $financeAuthenticationList) = $this->getRepository()->scenario(FinanceAuthenticationRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
financeAuthenticationList: {
	0 => [
		FinanceAuthentication //object
	],
	1 => [
		FinanceAuthentication //object
	],
}
```

### <a name="认证">认证</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"financeAuthentications",
        "attributes"=>array(
            "code"=>"机构编码",
            "licence"=>array(
                'name' => '金融许可证', 'identify' => '金融许可证.jpg'
            ),
            "organizationsCategory"=>1,
            "organizationCode"=>'机构代码',
            "organizationsCategoryParent"=>1,
            "organizationCodeCertificate"=>array(
                'name' => '金融机构代码证书', 'identify' => '金融机构代码证书.jpg'
            ),
        ),
        "relationships"=>array(
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>1)
                )
            )
        )
    )
);
```

**调用示例**

```
$authentication = $this->getAuthentication();

$authentication = $this->getCommonFinanceAuthentication();
$enterprise = $this->fetchEnterprise();
$authentication->setEnterprise();

$authentication->add();
```

### <a name="编辑认证信息">编辑认证信息</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"financeAuthentications",
        "attributes"=>array(
            "code"=>"机构编码",
            "licence"=>array(
                'name' => '金融许可证', 'identify' => '金融许可证.jpg'
            ),
            "organizationsCategory"=>1,
            "organizationCode"=>'机构代码',
            "organizationsCategoryParent"=>1,
            "organizationCodeCertificate"=>array(
                'name' => '金融机构代码证书', 'identify' => '金融机构代码证书.jpg'
            ),
        )
    )
);
```

**调用示例**

```
$authentication = $this->fetchAuthentication();

$authentication = $this->getCommonFinanceAuthentication();

$authentication->edit();
```
