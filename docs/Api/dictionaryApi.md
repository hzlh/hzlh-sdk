# 字典管理API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增字典管理](#新增字典管理)
	* [编辑字典管理](#编辑字典管理)
	* [启用](#启用)
	* [禁用](#禁用)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     		| 类型		 | 请求参数是否是必填 | 示例            | 描述       |
| :---:            | :----:     | :------:      |:------------:      |:-------:       |
| name             | string     | 是            | 人力资源             | 名称            |
| remark           | string     | 否            | 人力资源             | 备注            |
| parentId         | int        | 是            | 0                  | 父级id           |
| category         | int        | 是            | 0                  | 所属分类          |
| crew             | Crew        | 是            | 2                  | 发布人           |
| status           | int        | 是            |                    | 状态             |
| updateTime       | int        |               | 1535444931         | 更新时间         |
| creditTime       | int        |               | 1535444931         | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
DICTIONARY_FETCH_ONE
```
**请求示例**

```
$dictionary = $this->getRepository()->scenario(DictionaryRepository::DICTIONARY_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
dictionary: Dictionary
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
DICTIONARY_LIST
```
**请求示例**

```
$dictionaryList = $this->getRepository()->scenario(DictionaryRepository::DICTIONARY_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
dictionaryList: {
	0 => [
		Dictionary //object
	],
	1 => [
		Dictionary //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
DICTIONARY_LIST
```
**请求示例**

```
list($count, $dictionaryList) = $this->getRepository()->scenario(DictionaryRepository::DICTIONARY_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
dictionaryList: {
	0 => [
		Dictionary //object
	],
	1 => [
		Dictionary //object
	],
}
```
### <a name="新增字典管理">新增字典管理</a>

**参数**

```
$data = array(
	"data"=>array(
        "type"=>"dictionaries",
        "attributes"=>array(
            "name"=>"分类图片测试",
            "remark"=>"备注",
            "parentId"=>1,
            "category"=>1
        ),
        "relationships"=> array(
            "crew"=> array(
                "data"=> array(
                    array(
                        "type"=> "crews",
                        "id"=> 1
                    )
                )
            )
        )   
    ),
);
```
**调用示例**

```
$dictionary = $this->getDictionary();

$dictionary->setParentId($command->parentId);
$dictionary->setName($command->name);
$dictionary->setRemark($command->remark);
$dictionary->setCategory($command->category);
$dictionary->setCrew($crew);

$dictionary->add();

```
### <a name="编辑字典管理">编辑字典管理</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"dictionaries",
		"attributes"=>array(
			"name"=>"分类名称",
			"remark"=>"备注",
            "parentId"=>2
		)
	)
);
```
**调用示例**

```
$dictionary = $this->getRepository()->fetchOne(int $id);

$dictionary->setName($command->name);
$dictionary->setRemark($command->remark);
$dictionary->setParentId($command->parentId);

$dictionary->edit();

```
### <a name="启用"> 启用 </a>

**调用示例**

```
$dictionary = $this->getRepository()->fetchOne(int $id);

$dictionary->enable();

```
### <a name="禁用"> 禁用 </a>

**调用示例**

```
$dictionary = $this->getRepository()->fetchOne(int $id);

$dictionary->disable();

```