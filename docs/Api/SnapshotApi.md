# 快照信息API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
    * [获取单条数据场景](#获取单条数据场景)
    * [获取多条数据场景](#获取多条数据场景)
    * [根据检索条件查询数据](#根据检索条件查询数据)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                       | 描述             |
| :---:           | :----:     | :------:       |:------------:              |:-------:        |
| accountBalance  | string     |                |   200                      | 主账户余额        |
| frozenAccountBalance | string |               |   200                      | 冻结账户余额      |
| updateTime      | int        |               | 1535444931                  | 更新时间          |
| creditTime      | int        |               | 1535444931                  | 创建时间          |
| status          | int        |               | 0                           | 状态 0正常 -2 冻结 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
SNAPSHOT_FETCH_ONE
```
**请求示例**

```
$snapshot = $this->getRepository()->scenario(SnapshotRepository::SNAPSHOT_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
snapshot: Snapshot
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
SNAPSHOT_LIST
```
**请求示例**

```
$snapshotList = $this->getRepository()->scenario(SnapshotRepository::SNAPSHOT_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
snapshotList: {
    0 => [
        Snapshot //object
    ],
    1 => [
        Snapshot //object
    ],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
SNAPSHOT_LIST
```
**请求示例**

```
list($count, $snapshotList) = $this->getRepository()->scenario(SnapshotRepository::SNAPSHOT_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
snapshotList: {
    0 => [
        Snapshot //object
    ],
    1 => [
        Snapshot //object
    ],
}
```