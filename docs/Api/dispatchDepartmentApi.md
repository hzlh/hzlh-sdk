# 发文部门API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [编辑](#编辑)
	* [启用](#启用)
	* [禁用](#禁用)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 中央财经领导小组办公室副主任韩俊                   | 名称            |
| remark          | string     | 否            |                                              | 备注            |
| crew            | array      | 是             | array('id'=>1, 'realName'=>'张三')            | 发布员工        |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 2                                            | 状态  (2启用 -2禁用)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(DispatchDepartmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(DispatchDepartmentRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(DispatchDepartmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"dispatchDepartments",
        "attributes"=>array(
            "name"=>"名称",
            "remark"=>"备注",
            "status"=>0,
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>员工id)
                )
            )
        )
    )
);
```
**调用示例**

```
$crew = $this->getCrewRepository()->fetchOne(int $id);

$dispatchDepartment = new Sdk\DispatchDepartment\Model\DispatchDepartment();
$dispatchDepartment->setName('名称');
$dispatchDepartment->setRemark('备注');
$dispatchDepartment->setStatus(0);
$dispatchDepartment->setCrew($crew);

$dispatchDepartment->add();
```
### <a name="编辑">编辑</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"dispatchDepartments",
        "attributes"=>array(
            "name"=>"名称",
            "remark"=>"备注",
            "status"=>0,
        )
    )
);
```
**调用示例**

```
$dispatchDepartment = $this->getRepository()->fetchOne(int $id);
$dispatchDepartment->setName('名称');
$dispatchDepartment->setRemark('备注');
$dispatchDepartment->setStatus(0);

$dispatchDepartment->edit();

```
### <a name="启用">启用</a>

**调用示例**

```
$dispatchDepartment = $this->getRepository()->fetchOne(int $id);

$dispatchDepartment->enable();

```
### <a name="禁用">禁用</a>

**调用示例**

```
$dispatchDepartment = $this->getRepository()->fetchOne(int $id);

$dispatchDepartment->disable();

```