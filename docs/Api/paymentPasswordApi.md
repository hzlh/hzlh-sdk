# 支付密码API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [设置支付密码](#设置支付密码)
	* [忘记支付密码](#忘记支付密码)
	* [修改支付密码](#修改支付密码)
	* [验证支付密码](#验证支付密码)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| password        | string     | 是            |                                              | 密码             |
| oldPassword     | string     | 是            |                                              | 旧密码           |
| memberAccount   | MemberAccount | 是         |                                              | 账户             |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态             |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
PAYMENT_PASSWORD_FETCH_ONE
```
**请求示例**

```
$paymentPassword = $this->getRepository()->scenario(PaymentPasswordepository::PAYMENT_PASSWORD_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
paymentPassword: PaymentPassword
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PAYMENT_PASSWORD_LIST
```
**请求示例**

```
$paymentPasswordList = $this->getRepository()->scenario(PaymentPasswordRepository::PAYMENT_PASSWORD_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
paymentPasswordList: {
	0 => [
		PaymentPassword //object
	],
	1 => [
		PaymentPassword //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PAYMENT_PASSWORD_LIST
```
**请求示例**

```
list($count, $paymentPasswordList) = $this->getRepository()->scenario(PaymentPasswordRepository::PAYMENT_PASSWORD_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
paymentPasswordList: {
	0 => [
		PaymentPassword //object
	],
	1 => [
		PaymentPassword //object
	],
}
```
### <a name="设置支付密码">设置支付密码</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"paymentPasswords",
		"attributes"=>array(
			"password"=>"123456",
		),
		"relationships"=>array(
			"memberAccount"=>array(
				"data"=>array(
					array("type"=>"memberAccounts","id"=>2)
				)
			)
		)
	)
);
```
**调用示例**

```
$paymentPassword= $this->getPaymentPassword();

$paymentPassword->setPassword($command->paymentPassword);
$paymentPassword->setMemberAccount($memberAccount);

$paymentPassword->add();

```

### <a name="忘记支付密码">忘记支付密码</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"paymentPasswords",
		"attributes"=>array(
			"cellphone"=>"13720406329",
			"password"=>"333456",
		)
	)
);
```
**调用示例**

```
$paymentPassword = $this->getPaymentPassword();

$paymentPassword->setCellphone($command->cellphone);
$paymentPassword->setPassword($command->paymentPassword);

$paymentPassword->resetPaymentPassword();

```
### <a name="修改支付密码">修改支付密码</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"paymentPasswords",
		"attributes"=>array(
			"oldPassword"=>"456732",
			"password"=>"123567",
		)
	)
);
```
**调用示例**

```
$paymentPassword = $this->getRepository()->fetchOne(int $id);

$paymentPassword->setOldPassword($command->oldPaymentPassword);
$paymentPassword->setPassword($command->paymentPassword);

$paymentPassword->edit();

```
### <a name="验证支付密码"> 验证支付密码 </a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"paymentPasswords",
		"attributes"=>array(
			"paymentPassword"=>"123456",
		),
		"relationships"=>array(
			"memberAccount"=>array(
				"data"=>array(
					array("type"=>"memberAccounts","id"=>2)
				)
			)
		)
	)
);
```

**调用示例**

```
$paymentPassword= $this->getPaymentPassword();

$paymentPassword->setPassword($command->paymentPassword);
$paymentPassword->setMemberAccount($memberAccount);

$paymentPassword->validate();

```