# 企业员工 API 文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增员工](#新增员工)
	* [编辑员工](#编辑员工)
	* [删除员工](#删除员工)
	* [推荐至店铺主页](#推荐至店铺主页)
	* [取消推荐至店铺主页](#取消推荐至店铺主页)

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| cellphone       | string     | 是            | 13720406329                                  | 手机号           |
| userName        | string     | 是            | 13720406329                                  | 用户名           |
| realName        | string     | 是            | 张三三                                        | 姓名             |
| avatar          | array      | 是            | array("name"=>"用户头像", "identify"=>"1.jpg")| 用户头像          |
| gender          | int        | 是            | 1                                            | 性别 (1 男 2 女)  |
| password        | string     | 是            |                                              | 密码             |
| oldPassword     | string     | 是            |                                              | 旧密码           |
| birthday        | string     | 是            | 1994-09-03                                   | 出生日期          |
| education       | int        | 是            | 0 未知, 1 小学, 2 初中, 3 中专, 4 专科, 5 本科, 6 硕士研究生, 7 博士研究生  | 学历       |
| cardId          | string     | 是            | 412825100909094356                            | 身份证号    |
| briefIntroduction| string    | 是            | 简介                                          | 简介             |
| professionalTitle| array     |               | array('会计')                                 | 职称            |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态  (0启用 -2禁用, -4 删除)|
| isRecommendHomePage | int    |               | 0                                            | 是否推荐至主页  (0 否 2 是)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
STAFF_FETCH_ONE
```
**请求示例**

```
$staff = $this->getRepository()->scenario(StaffRepository::STAFF_FETCH_ONE)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
staff: Staff
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
STAFF_LIST
```
**请求示例**

```
$staffList = $this->getRepository()->scenario(StaffRepository::STAFF_LIST)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
staffList: {
	0 => [
		Staff //object
	],
	1 => [
		Staff //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
STAFF_LIST
```
**请求示例**

```
list($count, $staffList) = $this->getRepository()->scenario(StaffRepository::STAFF_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
staffList: {
	0 => [
		Staff //object
	],
	1 => [
		Staff //object
	],
}
```

### <a name="新增员工">新增员工</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"staffs",
        "attributes"=>array(
            "avatar"=>array(
                'name'=>'图片名称',
                'identify'=>'图片地址'
            ),
            "realName"=>"姓名",
            "cellphone"=>"手机号",
            "cardId"=>'身份证号',
            "birthday"=>'出身日期',
            "briefIntroduction"=>'简介',
            "gender"=>1,
            "education"=>1
        ),
        "relationships"=>array(
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>企业id)
                )
            )
        )
    )
);
```

**调用示例**

```
$staff = $this->getStaff();

$staff->setAvatar($command->avatar);
$staff->setRealName($command->realName);
$staff->setCellphone($command->cellphone);
$staff->setCardId($command->cardId);
$staff->setBirthday($command->birthday);
$staff->setBriefIntroduction($command->briefIntroduction);
$staff->setGender($command->gender);
$staff->setEducation($command->education);
$staff->setEnterprise($enterprise);

$staff->add();
```

### <a name="编辑员工">编辑员工</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"staffs",
        "attributes"=>array(
            "avatar"=>array(
                'name'=>'图片名称',
                'identify'=>'图片地址'
            ),
            "realName"=>"姓名",
            "cellphone"=>"手机号",
            "cardId"=>'身份证号',
            "birthday"=>'出身日期',
            "briefIntroduction"=>'简介',
            "gender"=>1,
            "education"=>1
        )
    )
);
```

**调用示例**

```
$staff = $this->getStaff()->fetchOne(int $id);

$staff->setAvatar($command->avatar);
$staff->setRealName($command->realName);
$staff->setCellphone($command->cellphone);
$staff->setCardId($command->cardId);
$staff->setBirthday($command->birthday);
$staff->setBriefIntroduction($command->briefIntroduction);
$staff->setGender($command->gender);
$staff->setEducation($command->education);

$staff->edit();
```

### <a name="删除员工">删除员工</a>

**调用示例**

```
$staff = $this->getRepository()->fetchOne(int $id);

$staff->deletes();

```

### <a name="推荐至店铺主页">推荐至店铺主页</a>

**调用示例**

```
$staff = $this->getRepository()->fetchOne(int $id);

$staff->recommendHomePage();

```

### <a name="取消推荐至店铺主页">取消推荐至店铺主页</a>

**调用示例**

```
$staff = $this->getRepository()->fetchOne(int $id);

$staff->cancelRecommendHomePage();

```