# 服务API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [编辑](#编辑)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
    * [关闭](#关闭)
	* [撤销](#撤销)
	* [删除](#删除)
	* [上架](#上架)
	* [下架](#下架)
---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| title       | string     | 是                | 出具信用等级证书    | 服务标题   |
| serviceCategory    | ServiceCategory| 是            |                  | 服务分类   |
| serviceObjects| array     | 否               |                  | 服务对象   |
| price        | array      | 是               | array(array('name'=>'规格', 'value'=>'12'))| sku价格  |
| minPrice     | string     |                  | 12                | 最小价格  |
| maxPrice     | string     |                  | 12                | 最大价格  |
| cover        | array      |  是    | array("name"=>"服务封面","identify"=>"1.jpg")   | 服务封面  |
| contract     | array      |  否    | array("name"=>"服务合同","identify"=>"1.pdf")   | 服务合同  |
| detail      | array      |   是  | array(array("type"=>"image","value"=>"1.jpg"))  | 服务详情  |
| enterprise   | Enterprise |                 |                     | 所属企业  |
| volume       | int        |                  |                     | 成交量    |
| attentionDegree| int      |                  |                     | 关注度    |
| applyStatus     | int         |                   | 0                         | 审核状态  (0待审核 2审核通过 -2审核驳回)|
| status          | int         |                   |                          | 状态 (0上架 -2下架 -4撤销 -6关闭 -8删除)     |
| rejectReason    | string      | 是                |                                              | 驳回原因         |
| updateTime      | int         |                  | 1535444931                                    | 更新时间         |
| creditTime      | int         |                  | 1535444931                                    | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(ServiceRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
	"data"=>array(
        "type"=>"services",
        "attributes"=>array(
            "title"=>"维修洗衣机服务",
            "detail"=>array(
                array("type"=>"text", "value"=>"维修洗衣机服务")
            ),
            "price"=>array(
                array("name"=>"天", "value"=>"1"),
                array("name"=>"月", "value"=>"30"),
                array("name"=>"年", "value"=>"300"),
            ),
            "cover"=>array("name"=>"维修洗衣机服务", "identify"=>"3.jpg"),
            "contract"=>array("name"=>"维修洗衣机服务合同", "identify"=>"3.pdf"),
            "serviceObjects"=>array(1,2)
        ),
        "relationships"=>array(
            "serviceCategory"=>array(
                "data"=>array(
                    array("type"=>"serviceCategories","id"=>5)
                )
            ),
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>2)
                )
            ),
        )
    )
);
```
**调用示例**

```
$enterpriseRepository = new Sdk\Enterprise\Repository\EnterpriseRepository();
$serviceCategoryRepository = new Sdk\ServiceCategory\Repository\ServiceCategoryRepository();

$service->setTitle('ISO9001');
$service->setDetail(array(array("type"=>"text", "value"=>"绿盾征信立信认证是对企业整体信用状况的客观展现。")));
$service->setPrice(array(
    array("name"=>"天", "value"=>"1"),
    array("name"=>"月", "value"=>"30"),
    array("name"=>"年", "value"=>"300"),
));
$service->setCover(array("name"=>"维修洗衣机服务", "identify"=>"维修洗衣机服务.jpg"));
$service->setContract(array("name"=>"维修洗衣机服务合同", "identify"=>"维修洗衣机服务合同.pdf"));
$service->setServiceObjects(array(1,3));
$service->setEnterprise($enterpriseRepository->fetchOne(1));
$service->setServiceCategory($serviceCategoryRepository->fetchOne(35));

$service->add();

```
### <a name="编辑">编辑</a>

**参数**

```
$data = array(
	"data"=>array(
        "type"=>"services",
        "attributes"=>array(
            "title"=>"维修洗衣机服务",
            "detail"=>array(
                array("type"=>"text", "value"=>"维修洗衣机服务")
            ),
            "price"=>array(
                array("name"=>"天", "value"=>"1"),
                array("name"=>"月", "value"=>"30"),
                array("name"=>"年", "value"=>"300"),
            ),
            "cover"=>array("name"=>"维修洗衣机服务", "identify"=>"3.jpg"),
            "contract"=>array("name"=>"维修洗衣机服务合同", "identify"=>"3.pdf"),
            "serviceObjects"=>array(1,2)
        ),
        "relationships"=>array(
            "serviceCategory"=>array(
                "data"=>array(
                    array("type"=>"serviceCategories","id"=>5)
                )
            )
        )
    )
);
```
**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$serviceCategoryRepository = new Sdk\ServiceCategory\Repository\ServiceCategoryRepository();

$service->setTitle('ISO9001');
$service->setDetail(array(array("type"=>"text", "value"=>"绿盾征信立信认证是对企业整体信用状况的客观展现。")));
$service->setPrice(array(
    array("name"=>"天", "value"=>"1"),
    array("name"=>"月", "value"=>"30"),
    array("name"=>"年", "value"=>"300"),
));
$service->setCover(array("name"=>"维修洗衣机服务", "identify"=>"维修洗衣机服务.jpg"));
$service->setContract(array("name"=>"维修洗衣机服务合同", "identify"=>"维修洗衣机服务合同.pdf"));
$service->setServiceObjects(array(1,3));
$service->setServiceCategory($serviceCategoryRepository->fetchOne(35));

$service->edit();

```

### <a name="重新编辑">重新编辑</a>

**参数**

```
$data = array(
	"data"=>array(
        "type"=>"services",
        "attributes"=>array(
            "title"=>"维修洗衣机服务",
            "detail"=>array(
                array("type"=>"text", "value"=>"维修洗衣机服务")
            ),
            "price"=>array(
                array("name"=>"天", "value"=>"1"),
                array("name"=>"月", "value"=>"30"),
                array("name"=>"年", "value"=>"300"),
            ),
            "cover"=>array("name"=>"维修洗衣机服务", "identify"=>"3.jpg"),
            "contract"=>array("name"=>"维修洗衣机服务合同", "identify"=>"3.pdf"),
            "serviceObjects"=>array(1,2)
        ),
        "relationships"=>array(
            "serviceCategory"=>array(
                "data"=>array(
                    array("type"=>"serviceCategories","id"=>5)
                )
            )
        )
    )
);
```
**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$serviceCategoryRepository = new Sdk\ServiceCategory\Repository\ServiceCategoryRepository();

$service->setTitle('ISO9001');
$service->setDetail(array(array("type"=>"text", "value"=>"绿盾征信立信认证是对企业整体信用状况的客观展现。")));
$service->setPrice(array(
    array("name"=>"天", "value"=>"1"),
    array("name"=>"月", "value"=>"30"),
    array("name"=>"年", "value"=>"300"),
));
$service->setCover(array("name"=>"维修洗衣机服务", "identify"=>"维修洗衣机服务.jpg"));
$service->setContract(array("name"=>"维修洗衣机服务合同", "identify"=>"维修洗衣机服务合同.pdf"));
$service->setServiceObjects(array(1,3));
$service->setServiceCategory($serviceCategoryRepository->fetchOne(35));

$service->resubmit();

```

### <a name="审核通过">审核通过</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->approve();

```
### <a name="审核驳回">审核驳回</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"services",
		"attributes"=>array(
			"rejectReason"=>'编号重复'
		)
	)
);
```
**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->setRejectReason();

$service->reject();

```
### <a name="关闭">关闭</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->close();

```
### <a name="撤销">撤销</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->revoke();

```
### <a name="删除">删除</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->deletes();

```
### <a name="上架">上架</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->onShelf();

```
### <a name="下架">下架</a>

**调用示例**

```
$service = $this->getRepository()->fetchOne(int $id);

$service->offStock();

```