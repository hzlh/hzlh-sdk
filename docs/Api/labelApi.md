# 标签API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增标签](#新增标签)
	* [编辑标签](#编辑标签)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     		| 类型		 | 请求参数是否是必填 | 示例            | 描述       |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 发改委                                         | 名称            |
| remark          | string     | 否            |                                               | 备注            |
| icon            | array      | 是            | array("name"=>"标签图片", "identify"=>"1.jpg") | 标签图片        |
| category        | int        | 是            | 1                                            | 分类  (0 公共 1政策 2服务 3 贷款产品)|
| crew            | array      | 是             | array('id'=>1, 'realName'=>'张三')            | 发布员工        |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
LABEL_FETCH_ONE
```
**请求示例**

```
$label = $this->getRepository()->scenario(LabelRepository::LABEL_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
label: Label
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LABEL_LIST
```
**请求示例**

```
$labelList = $this->getRepository()->scenario(LabelRepository::LABEL_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
labelList: {
	0 => [
		Label //object
	],
	1 => [
		Label //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LABEL_LIST
```
**请求示例**

```
list($count, $labelList) = $this->getRepository()->scenario(LabelRepository::LABEL_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
labelList: {
	0 => [
		Label //object
	],
	1 => [
		Label //object
	],
}
```
### <a name="新增标签">新增标签</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"labels",
		"attributes"=>array(
			"name"=>"名称",
			"remark"=>"备注",
			"icon"=>array('name'=>'图片名称', 'identify'=>'图片地址'),
			"category"=>2,
		),
		"relationships"=>array(
			"crew"=>array(
				"data"=>array(
					array("type"=>"crews","id"=>员工id)
				)
			)
		)
	)
);
```
**调用示例**

```
$label = $this->getLabel();

$label->setName($parentCategory);
$label->setRemark($command->name);
$label->setIcon($command->qualificationName);
$label->setCategory($command->isQualification);
$crew->setCrew($crew);

$label->add();

```
### <a name="编辑标签">编辑标签</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"labels",
		"attributes"=>array(
			"name"=>"名称",
			"remark"=>"备注",
			"icon"=>array('name'=>'图片名称', 'identify'=>'图片地址'),
			"category"=>2,
		)
	)
);
```
**调用示例**

```
$label = $this->getRepository()->fetchOne(int $id);

$label->setName($parentCategory);
$label->setRemark($command->name);
$label->setIcon($command->qualificationName);
$label->setCategory($command->isQualification);

$label->edit();

```