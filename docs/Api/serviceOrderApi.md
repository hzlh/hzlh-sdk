# 服务订单API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [下单](#下单)
	* [买家取消订单](#买家取消订单)
	* [卖家取消订单](#卖家取消订单)
	* [付款](#付款)
	* [付款失败](#付款失败)
	* [履约开始](#履约开始)
	* [履约结束](#履约结束)
	* [买家确认](#买家确认)
	* [买家删除订单](#买家删除订单)
	* [卖家删除订单](#卖家删除订单)
	* [买家永久删除订单](#买家永久删除订单)
	* [卖家永久删除订单](#卖家永久删除订单)
	* [商家修改订单金额](#商家修改订单金额)
	* [修改收货地址](#修改收货地址)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| orderno     | string     |                   | 201908021       | 订单编号    |
| buyerMemberAccount| MemberAccount |  是            |                  | 买家账户   |
| sellerEnterprise| Enterprise |               |                  | 卖家企业   |
| memberCoupon | MemberCoupon  |               |                  | 用户优惠劵  |
| totalPrice   | string     |                   |       200        | 订单总价   |
| paidAmount   | string     |                   |       200        | 实付金额   |
| collectedAmount| string   |                   |       200        | 实收金额   |
| platformPreferentialAmount| string |          |       200        | 平台优惠金额   |
| businessPreferentialAmount | string     |     |       200        | 商家优惠金额   |
| remark | string     |     |               | 备注   |
| payment      | Payment    |                   |                  | 支付信息   |
| payment[paymentType]| int |      是           |        1          | 支付类型   |
| payment[transactionNumber]| string     |  是           | 201908155256256345624563789654123 | 交易流水号   |
| payment[transactionInfo]| array     |  是           |               | 交易信息   |
| payment[paymentTime]| int |                  |                   | 支付时间   |
| category     | int        |                  |                   | 订单类型   |
| orderAddress | OrderAddress | 是             |                   | 订单收货信息|
| orderAddress[snapshot] | Snapshot |          |                  |收货地址快照信息 |
| orderAddress[cellphone] | string | 是       |   13720406329    | 联系手机号 |
| orderCommodities | OrderCommodity |            |                  |订单商品信息 |
| OrderCommodity[snapshot] | Snapshot |          |                  |商品快照信息 |
| OrderCommodity[number]   | int |               |         1         |商品数量   |
| OrderCommodity[skuIndex]   | int |             |        1        |商品价格sku索引|
| OrderCommodity[commodity]   | ITradeAble |      |                 |商品信息    |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
SERVICE_ORDER_FETCH_ONE
```
**请求示例**

```
$serviceOrder = $this->getRepository()->scenario(ServiceOrderepository::SERVICE_ORDER_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
serviceOrder: ServiceOrder
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
OA_SERVICE_ORDER_LIST
PORTAL_SERVICE_ORDER_LIST
```
**请求示例**

```
$serviceOrderList = $this->getRepository()->scenario(ServiceOrderRepository::PORTAL_SERVICE_ORDER_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
serviceOrderList: {
	0 => [
		ServiceOrder //object
	],
	1 => [
		ServiceOrder //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
OA_SERVICE_ORDER_LIST
PORTAL_SERVICE_ORDER_LIST
```
**请求示例**

```
list($count, $serviceOrderList) = $this->getRepository()->scenario(ServiceOrderRepository::OA_SERVICE_ORDER_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
serviceOrderList: {
	0 => [
		ServiceOrder //object
	],
	1 => [
		ServiceOrder //object
	],
}
```
### <a name="下单">下单</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			"remark" => '订单好难'
		),
		"relationships"=>array(
			"buyerMemberAccount"=>array(
				"data"=>array(
					array("type"=>"memberAccounts","id"=>2),
				)
			),
			"memberCoupon"=>array(
				"data"=>array(
					array("type"=>"memberCoupons","id"=>1),
					array("type"=>"memberCoupons","id"=>2),
				)
			),
			"orderAddress"=>array(
				"data"=>array(
					array(
						"type"=>"orderAddress",
						"attributes"=>array(
							"cellphone" => '13720408908'
						),
						"relationships"=>array(
							"snapshot"=>array(
								"data"=>array(
									array("type"=>"snapshots","id"=>3)
								)
							),
						)
					),
				)
			),
			"orderCommodities"=>array(
				"data"=>array(
					array(
						"type"=>"orderCommodities",
						"attributes"=>array(
							"number" => 2,
							"skuIndex" => 0,
						),
						"relationships"=>array(
							"snapshot"=>array(
								"data"=>array(
									array("type"=>"snapshots","id"=>3)
								)
							),
						)
					),
					array(
						"type"=>"orderCommodities",
						"attributes"=>array(
							"number" => 10,
							"skuIndex" => 1,
						),
						"relationships"=>array(
							"snapshot"=>array(
								"data"=>array(
									array("type"=>"snapshots","id"=>4)
								)
							),
						)
					),
				),
			),
		)
	)
);
```
**调用示例**

```
$serviceOrder = $this->getServiceOrder();

$serviceOrder->setremark();
$serviceOrder->setbuyerMemberAccount();
$serviceOrder->setmemberCoupon();
$serviceOrder->setorderAddress();
$serviceOrder->setorderCommodities();

$serviceOrder->placeOrder();

```
### <a name="买家取消订单"> 买家取消订单 </a>

**参数**

```
$data = array("data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			'cancelReason'=>1
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setCancelReason($command->cancelReason);

$serviceOrder->buyerCancel();

```
### <a name="卖家取消订单"> 卖家取消订单 </a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			'cancelReason'=>1
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setCancelReason($command->cancelReason);

$serviceOrder->sellerCancel();

```
### <a name="付款"> 付款 </a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			'paymentType'=>1,
            "transactionNumber" => '4452883283gggggg',
            "transactionInfo" => array(),
			'paymentPassword' => 0
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setPaymentType($command->paymentType);
$serviceOrder->setTransactionNumber($command->transactionNumber);
$serviceOrder->setTransactionInfo($command->transactionInfo);
$serviceOrder->setPaymentPassword($command->paymentPassword);

$serviceOrder->pay();

```
### <a name="付款失败"> 付款失败 </a>

**参数**

```
$data = array("data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			'paymentType'=>1,
            "failureReason" => array(),
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setFailureReason($command->failureReason);

$serviceOrder->paymentFailure();

```
### <a name="履约开始"> 履约开始 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->performanceBegin();

```
### <a name="履约结束"> 履约结束 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->performanceEnd();

```
### <a name="买家确认"> 买家确认 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->buyerConfirmation();

```
### <a name="买家删除订单"> 买家删除订单 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->buyerDelete();

```
### <a name="卖家删除订单"> 卖家删除订单 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->sellerDelete();

```
### <a name="买家永久删除订单"> 买家永久删除订单 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->buyerPermanentDelete();

```
### <a name="卖家永久删除订单"> 卖家永久删除订单 </a>

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->sellerPermanentDelete();

```
### <a name="商家修改订单金额"> 商家修改订单金额 </a>

**参数**

```
$data = array("data"=>array(
		"type"=>"serviceOrders",
		"attributes"=>array(
			'amount'=>100,
			'remark'=>'修改价格备注'
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setAmount($command->amount);
$serviceOrder->setRemark($command->remark);

$serviceOrder->updateOrderAmount();

```
### <a name="修改收货地址"> 修改收货地址 </a>

**参数**

```
$data = array("data"=>array(
		"type"=>"serviceOrders",
		"relationships"=>array(
			"orderAddress"=>array(
				"data"=>array(
					array(
						"type"=>"orderAddress",
						"attributes"=>array(
							"cellphone" => '13720408908'
						),
						"relationships"=>array(
							"snapshot"=>array(
								"data"=>array(
									array("type"=>"snapshots","id"=>3)
								)
							),
						)
					),
				)
			),
		)
	)
);
```

**调用示例**

```
$serviceOrder = $this->getRepository()->fetchOne(int $id);

$serviceOrder->setOrderAddress();

$serviceOrder->updateOrderAddress();

```