# 政策解读API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [重新编辑](#重新编辑)
	* [上架](#上架)
	* [下架](#下架)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填   |  示例                                        | 描述            |
| :---:           | :----:     | :------:       |:------------:                                |:-------:       |
| policy          | Policy     | 是             |                                               | 关联的政策      |
| cover			  |	array	   | 否	      	    |  array('name'=>'封面图', 'identify'=>'封面.JPG') |政策解读封面图|
| title           | string     | 是             | 宜昌自贸片区进一步完善企业简易注销登记改革试点工作实施方案| 政策解读标题            |
| detail          | array      | 是             | 文本内容，图片地址          | 政策解读详情|
| description     | string     | 是             |    政策解读描述               | 政策解读描述|
| attachments     |	array	   | 否			    |  array(array('name'=>'附件', 'identify'=>'附件.zip')) |政策附属文件|
| status          | int        |                | 0       | 状态  (0上架 -2下架)|
| pageviews       | int        |                | 0       | 浏览量    |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(PolicyInterpretationRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(PolicyInterpretationRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(PolicyInterpretationRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
	$data=array(
		"data"=>array(
			"type"=>"policyInterpretations",
			"attributes"=>array(
			    "title"=>"政策解读标题---title",
			    "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
			    "cover"=>array("name"=>"封面名称", "identify"=>"封面名称.jpg"),
			    "description"=>"政策解读描述",
			    "attachments"=>array(array("name"=>"政策解读附件","identify"=>"政策解读附件.zip"))
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews", "id"=>1)
					)
				),
				"policy"=>array(
					"data"=>array(
						array("type"=>"policies", "id"=>1)
					)
				)
			)
		)
	);
```
**调用示例**

```
$crew = $this->getCrewRepository()->fetchOne(int $id);

$policyInterpretation = new Sdk\PolicyInterpretation\Model\PolicyInterpretation();
$policyInterpretation->setStatus('-2');
$policyInterpretation->setPolicy($policy);
$policyInterpretation->setCover(array("name"=>"封面图","identify"=>"name.jpg"));
$policyInterpretation->setTitle('政策解读标题');
$policyInterpretation->setDetail(array(array("type"=>"text", "value"=>"文本内容")));
$policyInterpretation->setDescription('政策描述');
$policyInterpretation->setAttachments(array(array("name"=>"政策附件","identify"=>"政策附件.zip")));
$policyInterpretation->setCrew($crew);

```
### <a name="重新编辑">重新编辑</a>

**参数**

```
	$data = array(
		"data"=>array(
			"type"=>"policyInterpretations",
			"attributes"=>array(
			"title"=>"政策解读标题",
			"detail"=>array(array("type"=>"text", "value"=>"文本内容")),
			"cover"=>array("name"=>"封面名称", "identify"=>"封面名称.jpg"),
			"description"=>"政策解读描述",
			"attachments"=>array(array("name"=>"政策解读附件","identify"=>"identify.zip")))
		)
	);
```
**调用示例**

```
$policyInterpretation = $this->getRepository()->fetchOne(int $id);
$policyInterpretation->setCover(array("name"=>"封面图","identify"=>"name.jpg"));
$policyInterpretation->setTitle('政策解读标题');
$policyInterpretation->setDetail(array(array("type"=>"text", "value"=>"文本内容")));
$policyInterpretation->setDescription('政策描述');
$policyInterpretation->setAttachments(array(array("name"=>"政策附件","identify"=>"政策附件.zip")));

$policy->edit();

```

### <a name="上架">上架</a>

**调用示例**

```
$policyInterpretation = $this->getRepository()->fetchOne(int $id);

$policyInterpretation->onShelf();

```
### <a name="下架">下架</a>

**调用示例**

```
$policyInterpretation = $this->getRepository()->fetchOne(int $id);

$policyInterpretation->offStock();

```