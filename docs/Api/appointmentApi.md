# 预约申请API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [撤销](#撤销)
	* [删除](#删除)
---

## <a name="所需参数说明">所需参数说明</a>
| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| number       | string     | 是                | SQ202004131    | 申请编号    |
| loanProductSnapshot | Snapshot |             |             | 金融产品快照信息  |
| loanProduct  | LoanProduct |                 |             | 金融产品信息  |
| loanProductTitle  | string     | 是                | 出具信用等级证书    | 金融产品标题   |
| enterpriseName  | string     | 是                | 出具信用等级证书    | 企业名称   |
| sellerEnterprise   | Enterprise |                 |                     | 卖家企业信息 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(AppointmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(AppointmentRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(AppointmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"contactsName"=>"联系人",
			"contactsCellphone"=>"联系电话",
			"contactsArea"=>"联系地区",
			"contactsAddress"=>"联系详细地址",
			"loanAmount"=>"100",
			"loanTerm"=>12,
			"loanObject"=>1,
			"attachments"=>array(
				"name"=>"申请材料附件", "identify"=>"申请材料附件.zip"
			)
		),
		"relationships"=>array(
			"snapshot"=>array(
				"data"=>array(
					array("type"=>"snapshots","id"=>5)
				)
			),
			"member"=>array(
				"data"=>array(
					array("type"=>"members","id"=>2)
				)
			),
		)
	)
);
```
**调用示例**

```
$appointment = $this->getAppointment();

$appointment->setLoanAmount();
$appointment->setLoanTerm();
$appointment->setLoanObject();
$appointment->setLoanAmount();
$appointment->setAttachments();
$appointment->setContactsInfo(); //用户信息值对象
$appointment->setSnapshot();

$appointment->add();

```
### <a name="重新编辑">重新编辑</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"contactsName"=>"联系人",
			"contactsCellphone"=>"联系电话",
			"contactsArea"=>"联系地区",
			"contactsAddress"=>"联系详细地址",
			"loanAmount"=>"100",
			"loanTerm"=>12,
			"loanObject"=>1,
			"attachments"=>array(
				"name"=>"申请材料附件", "identify"=>"申请材料附件.zip"
			)
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setLoanAmount();
$appointment->setLoanTerm();
$appointment->setLoanObject();
$appointment->setLoanAmount();
$appointment->setAttachments();
$appointment->setContactsInfo(); //用户信息值对象

$appointment->resubmit();

```
### <a name="审核通过">审核通过</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"remark"=>"商家备注"
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setRemark();

$appointment->approve();

```
### <a name="审核驳回">审核驳回</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"rejectReason"=>'编号重复'
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setRejectReason();

$appointment->reject();

```
### <a name="完成">完成</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"loanFailReason"=>"贷款失败原因",
			"loanContractNumber"=>"贷款合同编号",
			"loanStatus"=>2,
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setLoanFailReason();
$appointment->setLoanContractNumber();
$appointment->setLoanStatus();

$appointment->completed();

```
### <a name="撤销">撤销</a>

**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->revoke();

```
### <a name="删除">删除</a>

**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->deletes();

```