# 企业认证API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [认证](#认证)
	* [编辑](#编辑)
	* [重新认证](#重新认证)
---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 中央财经领导小组办公室副主任韩俊                   | 名称            |
| remark          | string     | 否            |                                              | 备注            |
| crew            | array      | 是             | array('id'=>1, 'realName'=>'张三')            | 发布员工        |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 2                                            | 状态  (2启用 -2禁用)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(DispatchDepartmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(DispatchDepartmentRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(DispatchDepartmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="认证">认证</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"enterprises",
        "attributes"=>array(
            "name"=>"企业名称",
            "unifiedSocialCreditCode"=>"统一社会信用代码",
            "logo"=>array(
                'name' => '企业logo', 'identify' => '企业LOGOidentify'
            ),
            "businessLicense"=>array(
                'name' => '营业执照name', 'identify' => '营业执照identify'
            ),
            "powerAttorney"=>array(
                'name' => '授权委托书name', 'identify' => '授权委托书identify'
            ),
            "contactsName"=>"联系人姓名",
            "contactsCellphone"=>"联系人手机号",
            "contactsArea"=>"联系地区",
            "contactsAddress"=>"联系地址",
            "legalPersonName"=>"张三三",
            "legalPersonCardId"=>"412825199809804323",
            "legalPersonPositivePhoto"=>array(
                'name' => '身份证正面照片name', 'identify' => '身份证正面照片identify'
            ),
            "legalPersonReversePhoto"=>array(
                'name' => '身份证反面照片name', 'identify' => '身份证反面照片identify'
            ),
            "legalPersonHandheldPhoto"=>array(
                'name' => '手持身份证正面照片name', 'identify' => '手持身份证正面照片identify'
            ),
        ),
        "relationships"=>array(
            "member"=>array(
                "data"=>array(
                    array("type"=>"members","id"=>1)
                )
            )
        )
    )
);
```
**调用示例**

```
$member = $this->getMemberRepository()->fetchOne(int $id);

$enterprise = new Sdk\Enterprise\Model\Enterprise();
$enterprise->setName('企信云科技');
$enterprise->setUnifiedSocialCreditCode('1324948736747');
$enterprise->setLogo(array('name' => '企业logo', 'identify' => '企业logo.jpg'));
$enterprise->setBusinessLicense(array('name' => '营业执照name', 'identify' => '营业执照.jpg'));
$enterprise->setPowerAttorney(array('name' => '授权委托书name', 'identify' => '授权委托书.jpg'));
$enterprise->setContactsInfo(
	new Sdk\Enterprise\Model\ContactsInfo(
		'联系人姓名',
		'13202938747',
		'联系地区',
		'联系地址打算打算的大大帅'
	)
);
$enterprise->setLegalPersonInfo(
	new Sdk\Enterprise\Model\LegalPersonInfo(
		'张三三',
		'61052719980821009X',
		array('name' => 'name', 'identify' => '1.jpg'),
		array('name' => 'name', 'identify' => '2.jpg'),
		array('name' => 'name', 'identify' => '3.jpg')
	)
);
$enterprise->setMember($member);

$enterprise->add();
```
### <a name="编辑">编辑</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"enterprises",
        "attributes"=>array(
            "name"=>"企业名称",
            "unifiedSocialCreditCode"=>"统一社会信用代码",
            "logo"=>array(
                'name' => '企业logo', 'identify' => '企业logo'
            ),
            "businessLicense"=>array(
                'name' => '营业执照name', 'identify' => '营业执照name'
            ),
            "powerAttorney"=>array(
                'name' => '授权委托书name', 'identify' => '授权委托书identify'
            ),
            "contactsName"=>"联系人姓名",
            "contactsCellphone"=>"联系人手机号",
            "contactsArea"=>"联系地区",
            "contactsAddress"=>"联系地址",
            "legalPersonName"=>"张三三",
            "legalPersonCardId"=>"412825199809804323",
            "legalPersonPositivePhoto"=>array(
                'name' => '身份证正面照片name', 'identify' => '身份证正面照片identify'
            ),
            "legalPersonReversePhoto"=>array(
                'name' => '身份证反面照片name', 'identify' => '身份证反面照片identify'
            ),
            "legalPersonHandheldPhoto"=>array(
                'name' => '手持身份证正面照片name', 'identify' => '手持身份证正面照片identify'
            ),
        )
    )
);
```
**调用示例**

```
$enterprise = $this->getRepository()->fetchOne(int $id);
$enterprise->setName('企信云科技');
$enterprise->setUnifiedSocialCreditCode('1324948736747');
$enterprise->setLogo(array('name' => '企业logo', 'identify' => '企业logo.jpg'));
$enterprise->setBusinessLicense(array('name' => '营业执照name', 'identify' => '营业执照.jpg'));
$enterprise->setPowerAttorney(array('name' => '授权委托书name', 'identify' => '授权委托书.jpg'));
$enterprise->setContactsInfo(
	new Sdk\Enterprise\Model\ContactsInfo(
		'联系人姓名',
		'13202938747',
		'联系地区',
		'联系地址打算打算的大大帅'
	)
);
$enterprise->setLegalPersonInfo(
	new Sdk\Enterprise\Model\LegalPersonInfo(
		'张三三',
		'61052719980821009X',
		array('name' => 'name', 'identify' => '1.jpg'),
		array('name' => 'name', 'identify' => '2.jpg'),
		array('name' => 'name', 'identify' => '3.jpg')
	)
);

$enterprise->edit();

```

### <a name="重新认证">重新认证</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"unAuditedEnterprises",
        "attributes"=>array(
            "name"=>"企业名称",
            "unifiedSocialCreditCode"=>"统一社会信用代码",
            "logo"=>array(
                'name' => '企业logo', 'identify' => '企业LOGOidentify'
            ),
            "businessLicense"=>array(
                'name' => '营业执照name', 'identify' => '营业执照identify'
            ),
            "powerAttorney"=>array(
                'name' => '授权委托书name', 'identify' => '授权委托书identify'
            ),
            "contactsName"=>"联系人姓名",
            "contactsCellphone"=>"联系人手机号",
            "contactsArea"=>"联系地区",
            "contactsAddress"=>"联系地址",
            "legalPersonName"=>"张三三",
            "legalPersonCardId"=>"412825199809804323",
            "legalPersonPositivePhoto"=>array(
                'name' => '身份证正面照片name', 'identify' => '身份证正面照片identify'
            ),
            "legalPersonReversePhoto"=>array(
                'name' => '身份证反面照片name', 'identify' => '身份证反面照片identify'
            ),
            "legalPersonHandheldPhoto"=>array(
                'name' => '手持身份证正面照片name', 'identify' => '手持身份证正面照片identify'
            ),
        )
    )
);
```
**调用示例**

```
$unAuditedEnterprise = $this->getUnAuditedEnterpriseRepository()->fetchOne(int $id);

$unAuditedEnterprise->setName('企信云');
$unAuditedEnterprise->setUnifiedSocialCreditCode('13249487367XX');
$unAuditedEnterprise->setLogo(array('name' => '企业logo', 'identify' => '企业logo.jpg'));
$unAuditedEnterprise->setBusinessLicense(array('name' => '营业执照name', 'identify' => '营业执照identify.jpg'));
$unAuditedEnterprise->setPowerAttorney(array('name' => '授权委托书name', 'identify' => '授权委托书identify.jpg'));
$unAuditedEnterprise->setContactsInfo(
	new Sdk\Enterprise\Model\ContactsInfo(
		'联系人姓名',
		'13202938747',
		'联系地区哈哈',
		'联系地址哈哈哈'
	)
);
$unAuditedEnterprise->setLegalPersonInfo(
	new Sdk\Enterprise\Model\LegalPersonInfo(
		'张三三',
		'61052719980821009X',
		array('name' => 'name', 'identify' => '1.jpg'),
		array('name' => 'name', 'identify' => '2.jpg'),
		array('name' => 'name', 'identify' => '3.jpg')
	)
);

$unAuditedEnterprise->resubmit();

```