# 银行 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
  * [获取单条数据场景](#获取单条数据场景)
  * [获取多条数据场景](#获取多条数据场景)
  * [根据检索条件查询数据](#根据检索条件查询数据)	

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例              | 描述       |
| :---:       | :----:     | :----:            | :----:           |:----:      |
| name        | string     | 是                | 中国银行           | 银行名称    |
| logo        | array      | 是                |                  | 银行logo    |
| image       | array      | 是                |                  | 银行卡背景图  |
| status      | int        |                   |                  | 状态        |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$bank = $this->getRepository()->scenario(BankRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
bank: Bank
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
$bankList = $this->getRepository()->scenario(BankRepository::LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
bankList: {
	0 => [
		Bank //object
	],
	1 => [
		Bank //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```

**请求示例**

```
list($count, $bankList) = $this->getRepository()->scenario(BankRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
bankList: {
	0 => [
		Bank //object
	],
	1 => [
		Bank //object
	],
}
```