# 收货地址API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [编辑](编辑)
	* [删除](#删除)
	* [设为默认](#设为默认)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     		| 类型		 | 请求参数是否是必填 | 示例            | 描述       |
| :---:       		| :----:     | :----:			| :----:         |:----:     |
| area        		| string     | 是               | 陕西省，西安市   | 地区    |
| address 	  		| string 	 | 是          		|旺座国际B座      | 详细地址  |
| postalCode  		| string 	 | 是               | 705600         | 邮政编码  |
| realName    		| string     | 是               | weeks          | 联系人姓名   |
| cellphone   		| string     | 是               | 13201918373    | 联系人电话   |
| isDefaultAddress  | int 		 | 是               | 2              | 是否设为默认地址（0:否，2:是） |
| member      		| Member 	 | 是               |                | 关联用户 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
DELIVERY_ADDRESS_FETCH_ONE
```
**请求示例**

```
$deliveryAddress = $this->getRepository()->scenario(DeliveryAddressRepository::DELIVERY_ADDRESS_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
deliveryAddress: DeliveryAddress
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
DELIVERY_ADDRESS_LIST
```
**请求示例**

```
$deliveryAddressList = $this->getRepository()->scenario(DeliveryAddressRepository::DELIVERY_ADDRESS_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
deliveryAddressList: {
	0 => [
		DeliveryAddress //object
	],
	1 => [
		DeliveryAddress //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
DELIVERY_ADDRESS_LIST
```
**请求示例**

```
list($count, $deliveryAddressList) = $this->getRepository()->scenario(DeliveryAddressRepository::DELIVERY_ADDRESS_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
deliveryAddressList: {
	0 => [
		DeliveryAddress //object
	],
	1 => [
		DeliveryAddress //object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"deliveryAddress",
		"attributes"=>array(
		    "realName"=>"张三",
		    "cellphone"=>"13720406789",
		    "area"=>"陕西省,西安市,雁塔区",
		    "address"=>"长延堡街道华城泊郡",
			"postalCode"=>"100000",
		    "isDefaultAddress"=>0,
		),
		"relationships"=>array(
			"member"=>array(
				"data"=>array(
					array("type"=>"members", "id"=>10)
				)
			)
		)
    )
);
```
**调用示例**

```
$deliveryAddress = $this->getDeliveryAddress();

$deliveryAddress->setRealName();
$deliveryAddress->setCellphone();
$deliveryAddress->setArea();
$deliveryAddress->setAddress();
$deliveryAddress->setPostalCode();
$deliveryAddress->setIsDefaultAddress();

$deliveryAddress->add();

```
### <a name="编辑">编辑</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"deliveryAddress",
		"attributes"=>array(
			"realName"=>"张三",
		    "cellphone"=>"13720406789",
		    "area"=>"陕西省,西安市,雁塔区",
		    "address"=>"长延堡街道华城泊郡",
			"postalCode"=>"100000",
		    "isDefaultAddress"=>0,
		)
	)
);
```
**调用示例**

```
$deliveryAddress = $this->getRepository()->fetchOne(int $id);

$deliveryAddress->setRealName();
$deliveryAddress->setCellphone();
$deliveryAddress->setArea();
$deliveryAddress->setAddress();
$deliveryAddress->setPostalCode();
$deliveryAddress->setIsDefaultAddress();

$deliveryAddress->edit();

```
### <a name="删除"> 删除 </a>

**调用示例**

```
$deliveryAddress = $this->getRepository()->fetchOne(int $id);

$deliveryAddress->deletes();

```
### <a name="设为默认"> 设为默认 </a>

**调用示例**

```
$deliveryAddress = $this->getRepository()->fetchOne(int $id);

$deliveryAddress->setAsDefaultAddress();

```