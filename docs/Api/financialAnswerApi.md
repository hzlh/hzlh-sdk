# 财务答案API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [本人删除](#本人删除)
	* [平台删除](#平台删除)
    * [提问者删除](#提问者删除)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称              | 类型        |请求参数是否必填  |  示例                                                          | 描述            |
| :---:                | :----:     | :------:       |:------------:                                                 |:-------:       |
| content              | string     | 是             | 宜昌高新区（自贸片区）政务服务局 关于调整建设项目中介服务机构备选库的通知 | 问题内容         |
| replyCount           | int        |                 | 0                                                            | 回复数          |
| likesCount           | int        |                 | 0                                                            | 点赞数          |
| member               | Member     | 是              | 1                                                             | 用户           |
| financialQuestion    | FinancialQuestion |          | 1                                                             | 问题         |
| status               | int        |                 | 0                                                             | 状态  (0正常 -2删除)|
| deleteType           | int        |                 | 0 正常, 1 本人删除, 2 平台删除                                   | 删除类型        |
| deleteReason         | string     |                 | 不符合要求                                                      | 删除原因        |
| deletedBy            | Member/Crew| 是               | 1                                                             | 删除人          |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$financialAnswer = $this->getRepository()->scenario(FinancialAnswerRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
financialAnswer: FinancialAnswer
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
$financialAnswerList = $this->getRepository()->scenario(FinancialAnswerRepository::LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
financialAnswerList: {
	0 => [
		FinancialAnswer //object
	],
	1 => [
		FinancialAnswer //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
list($count, $financialAnswerList) = $this->getRepository()->scenario(FinancialAnswerRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
financialAnswerList: {
	0 => [
		FinancialAnswer //object
	],
	1 => [
		FinancialAnswer //object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialAnswers",
        "attributes"=>array(
            "content"=>"答案内容"
        ),
        "relationships"=>array(
            "financialQuestion"=>array(
                "data"=>array(
                    array("type"=>"financialQuestions", "id"=>1)
                )
            ),
            "member"=>array(
                "data"=>array(
                    array("type"=>"members", "id"=>1)
                )
            )
        )
    )
);
```
**调用示例**

```
$financialAnswer = $this->getFinancialAnswer();

$financialAnswer->setContent('宜昌高新区（自贸片区）政务服务局 关于调整建设项目中介服务机构备选库的通知');
$financialAnswer->setQuestion(new FinancialQuestion());
$financialAnswer->setMember(new Member());

$financialAnswer->add();

```
### <a name="本人删除"> 本人删除 </a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialAnswers",
        "attributes"=>array(
            "deleteReason"=>"",
            "deleteType"=>1
        )
        "relationships"=>array(
            "deleteBy"=>array(
                "data"=>array(
                    array("type"=>"deleteBys", "id"=>1)
                )
            ),
        )
    )
);
```
**调用示例**

```
$financialAnswer = $this->getRepository()->fetchOne(int $id);

$financialAnswer->setDeleteInfo(
	new Sdk\FinancialQA\Model\DeleteInfo(
		1,
		'',
		new Member()
	)
);

$financialAnswer->deletes()

```
### <a name="平台删除"> 平台删除 </a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialAnswers",
        "attributes"=>array(
            "deleteReason"=>"删除原因",
            "deleteType"=>2
        )
        "relationships"=>array(
            "deleteBy"=>array(
                "data"=>array(
                    array("type"=>"deleteBys", "id"=>1)
                )
            ),
        )
    )
);
```
**调用示例**

```
$financialAnswer = $this->getRepository()->fetchOne(int $id);

$financialAnswer->setDeleteInfo(
	new Sdk\FinancialQA\Model\DeleteInfo(
		2,
		'删除原因',
		new Crew()
	)
);

$financialAnswer->platformDelete()

```
### <a name="提问者删除"> 提问者删除 </a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialAnswers",
        "attributes"=>array(
            "deleteReason"=>"删除原因",
            "deleteType"=>3
        )
        "relationships"=>array(
            "deleteBy"=>array(
                "data"=>array(
                    array("type"=>"deleteBys", "id"=>1)
                )
            ),
        )
    )
);
```
**调用示例**

```
$financialAnswer = $this->getRepository()->fetchOne(int $id);

$financialAnswer->setDeleteInfo(
    new Sdk\FinancialQA\Model\DeleteInfo(
        3,
        '删除原因',
        new Member()
    )
);

$financialAnswer->questionerDelete()

```