# 成为服务商认证API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [认证](#认证)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
---

## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| enterpriseName  | string     |               | 陕西传媒网络有限公司                            | 企业名称         |
| qualificationImage| array    | 是            | array("name"=>"资质认证图片", "identify"=>"1.jpg")| 资质认证图片     |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status           | int       |               |                    | 状态             |
| applyStatus     | int        |               | 0                            | 审核状态  (0待审核 2审核通过 -2审核驳回)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(AppointmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(AppointmentRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(AppointmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="认证">认证</a>

**参数**

```
$data = array("data"=>array(
        "type"=>"authentications",
        "attributes"=>array(),
        "relationships"=>array(
            "qualifications"=>array(
                "data"=>array(
                    array(
                        "type"=>"qualifications",
                        "attributes"=>array(
                            "image" => array("name"=>"认证服务44", "identify"=>"4.jpg")
                        ),
                        "relationships"=>array(
                            "serviceCategory"=>array(
                                "data"=>array(
                                    array(
                                        "type"=>'serviceCategories',
                                        "id"=>1
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "type"=>"qualifications",
                        "attributes"=>array(
                            "image" => array("name"=>"认证服务222", "identify"=>"2.jpg")
                        ),
                        "relationships"=>array(
                            "serviceCategory"=>array(
                                "data"=>array(
                                    array(
                                        "type"=>'serviceCategories',
                                        "id"=>2
                                    )
                                )
                            )
                        )
                    ),
                )
            ),
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>2)
                )
            ),
        )
    )
);
```
**调用示例**

```
$enterpriseRepository = new Sdk\Enterprise\Repository\EnterpriseRepository();
$serviceCategoryRepository = new Sdk\ServiceCategory\Repository\ServiceCategoryRepository();

$serviceCategoryId = [1,2,3];
$serviceCategory = $serviceCategoryRepository->fetchList($serviceCategoryId);

foreach ($serviceCategoryId as $value) {
	$qualification = new Sdk\Authentication\Model\Qualification();
	$qualification->setImage(array('name' => '图片', 'identify' => '1.png'));
	$qualification->setServiceCategory($serviceCategoryRepository->fetchOne($value));
	$authentication->addQualification($qualification);
}
$authentication->setEnterprise($enterpriseRepository->fetchOne(1));
$authentication->setQualificationImage(array('name' => '图片', 'identify' => '图片.png'));

$authentication->add(); 

```
### <a name="重新认证">重新认证</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"authentications",
        "attributes"=>array(
            "qualificationImage"=>array(
                'name' => '资质认证图片名称', 'identify' => '资质认证图片地址.jpg'
            )
        )
    )
);
```
**调用示例**

```
$authentication = $this->getRepository()->fetchOne(int $id);

$authentication->setQualificationImage(array('name' => '图片', 'identify' => '图片.png'));

$authentication->resubmit();

```
### <a name="审核通过">审核通过</a>

**调用示例**

```
$authentication = $this->getRepository()->fetchOne(int $id);

$authentication->approve();

```
### <a name="审核驳回">审核驳回</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"authentications",
		"attributes"=>array(
			"rejectReason"=>'编号重复'
		)
	)
);
```
**调用示例**

```
$authentication = $this->getRepository()->fetchOne(int $id);

$authentication->setRejectReason();

$authentication->reject();

```