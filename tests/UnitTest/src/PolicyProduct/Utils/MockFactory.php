<?php
namespace Sdk\PolicyProduct\Utils;

use Sdk\PolicyProduct\Model\PolicyProduct;

class MockFactory
{
    /**
     * [generatePolicyArray 生成政策信息数组]
     * @return [array] [政策数组]
     */
    public static function generatePolicyProductArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $policyProduct = array();

        $policyProduct = array(
            'data'=>array(
                'type'=>'policyProducts',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;

        return $policyProduct;
    }
    /**
     * [generatePolicyObject 生成政策对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [政策对象]
     */
    public static function generatePolicyProductObject(int $id = 0, int $seed = 0, array $value = array()) : PolicyProduct //phpcs:ignore
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $policyProduct = new PolicyProduct($id);

        //title
        $title = self::generateTitle($faker, $value);
        $policyProduct->setTitle($title);

        return $policyProduct;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return $title = isset($value['title']) ?
            $value['title'] : $faker->title;
    }
}
