<?php
namespace Sdk\Dictionary\Model;

use PHPUnit\Framework\TestCase;

use Sdk\Crew\Model\Crew;

use Sdk\Dictionary\Repository\DictionaryRepository;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

class DictionaryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = new Dictionary();

        $this->childStub = new Class extends Dictionary{
            public function getRepository() : DictionaryRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIEnableAbleAdapter() : IEnableAbleAdapter
            {
                return parent::getIEnableAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Repository\DictionaryRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IEnableAbleAdapter',
            $this->childStub->getIEnableAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Dictionary setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(2);
        $this->assertEquals(2, $this->stub->getId());
    }

    /**
     * 设置 Dictionary setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('2');
        $this->assertEquals(2, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置 Dictionary setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 Dictionary setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end
    
    //remark 测试 ---------------------------------------------------------- start
    /**
     * 设置 Dictionary setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('string');
        $this->assertEquals('string', $this->stub->getRemark());
    }

    /**
     * 设置 Dictionary setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array(1, 2, 3));
    }
    //remark 测试 ----------------------------------------------------------   end
    
    //crew 测试 ---------------------------------------------------------- start
    /**
     * 设置 Dictionary setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $crew = new crew();
        $this->stub->setCrew($crew);
        $this->assertEquals($crew, $this->stub->getCrew());
    }

    /**
     * 设置 Dictionary setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->stub->setCrew(array(1, 2, 3));
    }
    //crew 测试 ----------------------------------------------------------   end
    
    //parentId 测试 ---------------------------------------------------------- start
    /**
     * 设置 Dictionary setParentId() 正确的传参类型,期望传值正确
     */
    public function testSetParentIdCorrectType()
    {
        $this->stub->setParentId(2);
        $this->assertEquals(2, $this->stub->getParentId());
    }

    /**
     * 设置 Dictionary setParentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetParentIdWrongType()
    {
        $this->stub->setParentId('string');
    }
    //parentId 测试 ----------------------------------------------------------   end
    
    //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->stub->setCategory($actual);
        $this->assertEquals($expected, $this->stub->getCategory());
    }
    /**
     * 循环测试 Dictionary setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(Dictionary::CATEGORY['NULL'], Dictionary::CATEGORY['NULL']),
            array(Dictionary::CATEGORY['NEWS'], Dictionary::CATEGORY['NEWS']),
            array(Dictionary::CATEGORY['QA'], Dictionary::CATEGORY['QA']),
            array(Dictionary::CATEGORY['PROFESSIONAL_TITLE'], Dictionary::CATEGORY['PROFESSIONAL_TITLE']),
            array(999, Dictionary::CATEGORY['NULL']),
        );
    }
    /**
     * 设置 Dictionary setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end
}
