<?php
namespace Sdk\Staff\Model;

use Sdk\Staff\Repository\StaffRepository;

use Sdk\Common\Model\IEnableAble;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class StaffTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Staff{
            public function getRepository() : StaffRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Repository\StaffRepository',
            $this->childStub->getRepository()
        );
    }

    public function testExtendsUser()
    {
        $this->assertInstanceOf('Sdk\User\Model\User', $this->stub);
    }
}
