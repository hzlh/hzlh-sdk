<?php
namespace Sdk\Staff\Utils;

use Sdk\Staff\Model\Staff;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateStaffArray 生成员工信息数组]
     * @return [array] [员工数组]
     */
    public static function generateStaffArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $staff = array();

        $staff = array(
            'data'=>array(
                'type'=>'staffs',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //birthday
        $birthday = self::generateBirthday($faker, $value);
        $attributes['birthday'] = $birthday;
        //education
        $education = self::generateEducation($faker, $value);
        $attributes['education'] = $education;
        //cardId
        $cardId = self::generateCardId($faker, $value);
        $attributes['cardId'] = $cardId;
        //briefIntroduction
        $briefIntroduction = self::generateBriefIntroduction($faker, $value);
        $attributes['briefIntroduction'] = $briefIntroduction;
        //professionalTitle
        $professionalTitle = self::generateProfessionalTitle($faker, $value);
        $attributes['professionalTitle'] = $professionalTitle;

        $user = \Sdk\User\Utils\MockFactory::generateUserArray();

        $staff['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);
        
        return $staff;
    }
    /**
     * [generateStaffObject 生成员工对象]
     * @param  int|integer $id    [员工Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [员工对象]
     */
    public static function generateStaffObject(int $id = 0, int $seed = 0, array $value = array()) : Staff
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $staff = new Staff($id);

        $staff = \Sdk\User\Utils\MockFactory::generateUserObject($staff);

        //birthday
        $birthday = self::generateBirthday($faker, $value);
        $staff->setBirthday($birthday);
        //education
        $education = self::generateEducation($faker, $value);
        $staff->setEducation($education);
        //cardId
        $cardId = self::generateCardId($faker, $value);
        $staff->setCardId($cardId);
        //briefIntroduction
        $briefIntroduction = self::generateBriefIntroduction($faker, $value);
        $staff->setBriefIntroduction($briefIntroduction);
        //professionalTitle
        $professionalTitle = self::generateProfessionalTitle($faker, $value);
        $staff->setProfessionalTitle($professionalTitle);

        return $staff;
    }

    private static function generateBirthday($faker, array $value = array())
    {
        unset($faker);
        return $birthday = isset($value['birthday']) ?
        $value['birthday'] : '2020-05-16';
    }

    private static function generateEducation($faker, array $value = array())
    {
        return $education = isset($value['education']) ?
        $value['education'] : $faker->numberBetween($min = 0, $max = 7);
    }

    private static function generateCardId($faker, array $value = array())
    {
        unset($faker);
        return $cardId = isset($value['cardId']) ?
        $value['cardId'] : '140729199403230987';
    }

    private static function generateBriefIntroduction($faker, array $value = array())
    {
        return $briefIntroduction = isset($value['briefIntroduction']) ?
        $value['briefIntroduction'] : $faker->text($maxNbChars = 200);
    }

    private static function generateProfessionalTitle($faker, array $value = array())
    {
        return $professionalTitle = isset($value['professionalTitle']) ?
        $value['professionalTitle'] : array($faker->name);
    }
}
