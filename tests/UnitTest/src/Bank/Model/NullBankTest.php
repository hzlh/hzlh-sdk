<?php
namespace Sdk\Bank\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullBankTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullBank::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsBank()
    {
        $this->assertInstanceof('Sdk\Bank\Model\Bank', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
