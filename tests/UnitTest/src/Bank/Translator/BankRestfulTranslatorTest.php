<?php
namespace Sdk\Bank\Translator;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Model\NullBank;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class BankRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankRestfulTranslator::class)
             ->setMethods(['getCrewRestfulTranslator'])
             ->getMock();
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Bank());
        $this->assertInstanceOf('Sdk\Bank\Model\NullBank', $result);
    }

    public function setMethods(Bank $expectObject, array $attributes)
    {
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['logo'])) {
            $expectObject->setLogo($attributes['logo']);
        }
        if (isset($attributes['image'])) {
            $expectObject->setImage($attributes['image']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $bank = \Sdk\Bank\Utils\MockFactory::generateBankArray();

        $data =  $bank['data'];

        $actual = $this->stub->arrayToObject($bank);

        $expectObject = new Bank();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $bank = \Sdk\Bank\Utils\MockFactory::generateBankArray();
        $data =  $bank['data'];

        $actual = $this->stub->arrayToObjects($bank);

        $expectArray = array();

        $expectObject = new Bank();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $bank = \Sdk\Bank\Utils\MockFactory::generateBankObject(1, 1);

        $actual = $this->stub->objectToArray($bank);

        $expectedArray = array(
            'data'=>array(
                'type'=>'banks',
                'id'=>$bank->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$bank->getName(),
            'logo'=>$bank->getLogo(),
            'image'=>$bank->getImage()
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
