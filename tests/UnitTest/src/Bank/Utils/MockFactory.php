<?php
namespace Sdk\Bank\Utils;

use Sdk\Bank\Model\Bank;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateBankArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateBankArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $bank = array();

        $bank = array(
            'data'=>array(
                'type'=>'banks',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //logo
        $logo = self::generateLogo($faker, $value);
        $attributes['logo'] = $logo;
        //image
        $image = self::generateImage($faker, $value);
        $attributes['image'] = $image;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $bank['data']['attributes'] = $attributes;

        return $bank;
    }
    /**
     * [generateBankObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateBankObject(int $id = 0, int $seed = 0, array $value = array()) : Bank
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bank = new Bank($id);

        //name
        $name = self::generateName($faker, $value);
        $bank->setName($name);
        //logo
        $logo = self::generateLogo($faker, $value);
        $bank->setLogo($logo);
        //image
        $image = self::generateImage($faker, $value);
        $bank->setImage($image);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $bank->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $bank->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $bank->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $bank->setStatus($status);

        return $bank;
    }

    private static function generateName($faker, array $value = array())
    {
        return $name = isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateLogo($faker, array $value = array())
    {
        return $logo = isset($value['logo']) ?
        $value['logo'] : array('name' => 'logo', 'identify' => 'logo.jpg');
    }

    private static function generateImage($faker, array $value = array())
    {
        return $image = isset($value['image']) ?
        $value['image'] : array('name' => 'image', 'identify' => 'image.jpg');
    }
}
