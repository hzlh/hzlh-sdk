<?php
namespace Sdk\PlatformAccount\Utils;

use Sdk\PlatformAccount\Model\PlatformAccount;

class MockFactory
{
    /**
     * [generatePlatformAccountArray 生成账户信息数组]
     * @return [array] [账户数组]
     */
    public static function generatePlatformAccountArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $platformAccount = array();

        $platformAccount = array(
            'data'=>array(
                'type'=>'platformAccounts',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //accountBalance
        $accountBalance = self::generateAccountBalance($faker, $value);
        $attributes['accountBalance'] = $accountBalance;

        //incomeAmount
        $incomeAmount = self::generateIncomeAmount($faker, $value);
        $attributes['incomeAmount'] = $incomeAmount;

        //expenditureAmount
        $expenditureAmount = self::generateExpenditureAmount($faker, $value);
        $attributes['expenditureAmount'] = $expenditureAmount;

        //profitAmount
        $profitAmount = self::generateProfitAmount($faker, $value);
        $attributes['profitAmount'] = $profitAmount;

        $platformAccount['data']['attributes'] = $attributes;

        return $platformAccount;
    }

    /**
     * [generatePlatformAccount 生成账户对象对象]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [账户对象]
     */
    public static function generatePlatformAccount(
        int $seed = 0,
        array $value = array()
    ) : PlatformAccount {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $platformAccount = new PlatformAccount();

        //accountBalance
        $accountBalance = self::generateAccountBalance($faker, $value);
        $platformAccount->setAccountBalance($accountBalance);

        //incomeAmount
        $incomeAmount = self::generateIncomeAmount($faker, $value);
        $platformAccount->setIncomeAmount($incomeAmount);

        //expenditureAmount
        $expenditureAmount = self::generateExpenditureAmount($faker, $value);
        $platformAccount->setExpenditureAmount($expenditureAmount);

        //profitAmount
        $profitAmount = self::generateProfitAmount($faker, $value);
        $platformAccount->setProfitAmount($profitAmount);

        return $platformAccount;
    }

    private static function generateAccountBalance($faker, array $value = array())
    {
        return isset($value['accountBalance']) ? $value['accountBalance'] : $faker->randomFloat();
    }

    private static function generateIncomeAmount($faker, array $value = array())
    {
        return isset($value['incomeAmount']) ? $value['incomeAmount'] : $faker->randomFloat();
    }

    private static function generateExpenditureAmount($faker, array $value = array())
    {
        return isset($value['expenditureAmount']) ? $value['expenditureAmount'] : $faker->randomFloat();
    }

    private static function generateProfitAmount($faker, array $value = array())
    {
        return isset($value['profitAmount']) ? $value['profitAmount'] : $faker->randomFloat();
    }
}
