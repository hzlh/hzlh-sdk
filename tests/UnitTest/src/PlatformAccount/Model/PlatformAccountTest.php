<?php
namespace Sdk\PlatformAccount\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class PlatformAccountTest extends TestCase
{
    private $platformAccountStub;

    private $faker;

    public function setUp()
    {
        $this->platformAccountStub = new PlatformAccount();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->platformAccountStub);
        unset($this->faker);
    }

    public function testCorrentConstruct()
    {
        $this->assertEmpty($this->platformAccountStub->getAccountBalance());
        $this->assertEmpty($this->platformAccountStub->getIncomeAmount());
        $this->assertEmpty($this->platformAccountStub->getExpenditureAmount());
        $this->assertEmpty($this->platformAccountStub->getProfitAmount());
    }
    
    //accountBalance 测试 ---------------------------------------------------- start
    /**
     * 设置 setAccountBalance() 正确的传参类型,期望传值正确
     */
    public function testSetAccountBalanceCorrectType()
    {
        $accountBalance = $this->faker->numerify();

        $this->platformAccountStub->setAccountBalance($accountBalance);
        $this->assertEquals($accountBalance, $this->platformAccountStub->getAccountBalance());
    }

    /**
     * 设置 setAccountBalance() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAccountBalanceWrongType()
    {
        $accountBalance = array($this->faker->randomNumber());

        $this->platformAccountStub->setAccountBalance($accountBalance);
    }
    //accountBalance 测试 ----------------------------------------------------   end
    
    //incomeAmount 测试 ---------------------------------------------------- start
    /**
     * 设置 setIncomeAmount() 正确的传参类型,期望传值正确
     */
    public function testSetIncomeAmountCorrectType()
    {
        $incomeAmount = $this->faker->numerify();

        $this->platformAccountStub->setIncomeAmount($incomeAmount);
        $this->assertEquals($incomeAmount, $this->platformAccountStub->getIncomeAmount());
    }

    /**
     * 设置 setIncomeAmount() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIncomeAmountWrongType()
    {
        $incomeAmount = array($this->faker->randomNumber());

        $this->platformAccountStub->setIncomeAmount($incomeAmount);
    }
    //incomeAmount 测试 ----------------------------------------------------   end
    
    //expenditureAmount 测试 ---------------------------------------------------- start
    /**
     * 设置 setExpenditureAmount() 正确的传参类型,期望传值正确
     */
    public function testSetExpenditureAmountCorrectType()
    {
        $expenditureAmount = $this->faker->numerify();

        $this->platformAccountStub->setExpenditureAmount($expenditureAmount);
        $this->assertEquals($expenditureAmount, $this->platformAccountStub->getExpenditureAmount());
    }

    /**
     * 设置 setExpenditureAmount() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpenditureAmountWrongType()
    {
        $expenditureAmount = array($this->faker->randomNumber());

        $this->platformAccountStub->setExpenditureAmount($expenditureAmount);
    }
    //expenditureAmount 测试 ----------------------------------------------------   end
    
    //profitAmount 测试 ---------------------------------------------------- start
    /**
     * 设置 setProfitAmount() 正确的传参类型,期望传值正确
     */
    public function testSetProfitAmountCorrectType()
    {
        $profitAmount = $this->faker->numerify();

        $this->platformAccountStub->setProfitAmount($profitAmount);
        $this->assertEquals($profitAmount, $this->platformAccountStub->getProfitAmount());
    }

    /**
     * 设置 setProfitAmount() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetProfitAmountWrongType()
    {
        $profitAmount = array($this->faker->randomNumber());

        $this->platformAccountStub->setProfitAmount($profitAmount);
    }
    //profitAmount 测试 ----------------------------------------------------   end
}
