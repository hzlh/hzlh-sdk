<?php
namespace Sdk\FinancialQA\Translator;

use Sdk\FinancialQA\Model\NullFinancialAnswer;
use Sdk\FinancialQA\Model\FinancialAnswer;
use Sdk\FinancialQA\Model\FinancialQuestion;
use Sdk\FinancialQA\Model\DeleteInfo;

use Sdk\Member\Model\Member;
use Sdk\Label\Model\Label;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Label\Translator\LabelRestfulTranslator;

use Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory;

class FinancialAnswerRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            FinancialAnswerRestfulTranslator::class
        )
            ->setMethods([
                'getMemberRestfulTranslator',
                'getFinancialQuestionRestfulTranslator',

            ])->getMock();

        $this->childStub =
        new class extends FinancialAnswerRestfulTranslator {
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
            public function getFinancialQuestionRestfulTranslator() : FinancialQuestionRestfulTranslator
            {
                return parent::getFinancialQuestionRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testGetQuestionRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Translator\FinancialQuestionRestfulTranslator',
            $this->childStub->getFinancialQuestionRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new FinancialAnswer());
        $this->assertInstanceOf('Sdk\FinancialQA\Model\NullFinancialAnswer', $result);
    }

    public function setMethods(FinancialAnswer $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['content'])) {
            $expectObject->setContent($attributes['content']);
        }
        if (isset($attributes['replyCount'])) {
            $expectObject->setReplyCount($attributes['replyCount']);
        }
        if (isset($attributes['likesCount'])) {
            $expectObject->setLikesCount($attributes['likesCount']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        $deleteType = isset($attributes['deleteType'])
        ? $attributes['deleteType']
        : 0;
        $deleteReason = isset($attributes['deleteReason'])
        ? $attributes['deleteReason']
        : '';
        $deleteBy = isset($attributes['deleteBy'])
        ? $attributes['deleteBy']
        : null;
        $expectObject->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }
        if (isset($relationships['financialQuestion']['data'])) {
            $expectObject->setQuestion(
                new FinancialQuestion($relationships['financialQuestion']['data']['id'])
            );
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $financialAnswer = MockFactory::generateAnswerArray();

        $data =  $financialAnswer['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $question = new FinancialQuestion($relationships['financialQuestion']['data']['id']);
        $financialQuestionRestfulTranslator = $this->prophesize(FinancialAnswerRestfulTranslator::class);
        $financialQuestionRestfulTranslator->arrayToObject(Argument::exact($relationships['financialQuestion']))
            ->shouldBeCalledTimes(1)->willReturn($question);
        $this->stub->expects($this->exactly(1))
            ->method('getFinancialQuestionRestfulTranslator')
            ->willReturn($financialQuestionRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($financialAnswer);

        $expectObject = new FinancialAnswer();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $financialAnswer = MockFactory::generateAnswerArray();
        $data =  $financialAnswer['data'];
        $relationships = $data['relationships'];

        $question = new FinancialQuestion($relationships['financialQuestion']['data']['id']);
        $financialQuestionRestfulTranslator = $this->prophesize(FinancialAnswerRestfulTranslator::class);
        $financialQuestionRestfulTranslator->arrayToObject(Argument::exact($relationships['financialQuestion']))
            ->shouldBeCalledTimes(1)->willReturn($question);
        $this->stub->expects($this->exactly(1))
            ->method('getFinancialQuestionRestfulTranslator')
            ->willReturn($financialQuestionRestfulTranslator->reveal());

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($financialAnswer);
        $expectArray = array();

        $expectObject = new FinancialAnswer();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1, 1);

        $actual = $this->stub->objectToArray(
            $financialAnswer,
            array(
                'id',
                'question',
                'member',
                'content',
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'financialAnswers',
                'id'=>$financialAnswer->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'content'=>$financialAnswer->getContent(),
            'deleteType'=>$financialAnswer->getDeleteInfo()->getType(),
            'deleteReason'=>$financialAnswer->getDeleteInfo()->getReason()
        );

        $expectedArray['data']['relationships']['financialQuestion']['data'] = array(
            array(
                'type' => 'financialQuestions',
                'id' => $financialAnswer->getQuestion()->getId()
            )
        );
        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $financialAnswer->getMember()->getId()
            )
        );
        $expectedArray['data']['relationships']['deleteBy']['data'] = array(
            array(
                'type' => 'deleteBys',
                'id' => $financialAnswer->getDeleteInfo()->getDeleteBy()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
