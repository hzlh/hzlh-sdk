<?php
namespace Sdk\FinancialQA\Translator;

use Sdk\FinancialQA\Model\NullFinancialQuestion;
use Sdk\FinancialQA\Model\FinancialQuestion;
use Sdk\FinancialQA\Model\Answer;
use Sdk\FinancialQA\Model\DeleteInfo;

use Sdk\Member\Model\Member;
use Sdk\Label\Model\Label;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Label\Translator\LabelRestfulTranslator;

use Sdk\FinancialQA\Utils\FinancialQuestion\MockFactory;

class FinancialQuestionRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            FinancialQuestionRestfulTranslator::class
        )
            ->setMethods([
                'getMemberRestfulTranslator',
                'getLabelRestfulTranslator',
                'getAnswerRestfulTranslator',

            ])->getMock();

        $this->childStub =
        new class extends FinancialQuestionRestfulTranslator {
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
            public function getLabelRestfulTranslator() : LabelRestfulTranslator
            {
                return parent::getLabelRestfulTranslator();
            }
            public function getAnswerRestfulTranslator() : AnswerRestfulTranslator
            {
                return parent::getAnswerRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testGetLabelRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Label\Translator\LabelRestfulTranslator',
            $this->childStub->getLabelRestfulTranslator()
        );
    }

    public function testGetAnswerRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Translator\AnswerRestfulTranslator',
            $this->childStub->getAnswerRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new FinancialQuestion());
        $this->assertInstanceOf('Sdk\FinancialQA\Model\NullFinancialQuestion', $result);
    }

    public function setMethods(FinancialQuestion $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['content'])) {
            $expectObject->setContent($attributes['content']);
        }
        if (isset($attributes['pageViews'])) {
            $expectObject->setPageViews($attributes['pageViews']);
        }
        if (isset($attributes['commentCount'])) {
            $expectObject->setCommentCount($attributes['commentCount']);
        }
        if (isset($attributes['attentionDegree'])) {
            $expectObject->setAttentionDegree($attributes['attentionDegree']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        $deleteType = isset($attributes['deleteType'])
        ? $attributes['deleteType']
        : 0;
        $deleteReason = isset($attributes['deleteReason'])
        ? $attributes['deleteReason']
        : '';
        $deleteBy = isset($attributes['deleteBy'])
        ? $attributes['deleteBy']
        : null;
        $expectObject->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );
        if (isset($relationships['category']['data'])) {
            $expectObject->setCategory(new Label($relationships['category']['data']['id']));
        }
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }
        if (isset($relationships['answer']['data'])) {
            $expectObject->setAnswer(
                new Answer($relationships['answer']['data']['id'])
            );
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $financialQuestion = MockFactory::generateQuestionArray();

        $data =  $financialQuestion['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $label = new Label($relationships['category']['data']['id']);
        $labelRestfulTranslator = $this->prophesize(LabelRestfulTranslator::class);
        $labelRestfulTranslator->arrayToObject(Argument::exact($relationships['category']))
            ->shouldBeCalledTimes(1)->willReturn($label);
        $this->stub->expects($this->exactly(1))
            ->method('getLabelRestfulTranslator')
            ->willReturn($labelRestfulTranslator->reveal());

        $answer = new Answer($relationships['answer']['data']['id']);
        $answerRestfulTranslator = $this->prophesize(AnswerRestfulTranslator::class);
        $answerRestfulTranslator->arrayToObject(Argument::exact($relationships['answer']))
            ->shouldBeCalledTimes(1)->willReturn($answer);
        $this->stub->expects($this->exactly(1))
            ->method('getAnswerRestfulTranslator')
            ->willReturn($answerRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($financialQuestion);

        $expectObject = new FinancialQuestion();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $financialQuestion = MockFactory::generateQuestionArray();
        $data =  $financialQuestion['data'];
        $relationships = $data['relationships'];

        $label = new Label($relationships['category']['data']['id']);
        $labelRestfulTranslator = $this->prophesize(LabelRestfulTranslator::class);
        $labelRestfulTranslator->arrayToObject(Argument::exact($relationships['category']))
            ->shouldBeCalledTimes(1)->willReturn($label);
        $this->stub->expects($this->exactly(1))
            ->method('getLabelRestfulTranslator')
            ->willReturn($labelRestfulTranslator->reveal());

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $answer = new Answer($relationships['answer']['data']['id']);
        $answerRestfulTranslator = $this->prophesize(AnswerRestfulTranslator::class);
        $answerRestfulTranslator->arrayToObject(Argument::exact($relationships['answer']))
            ->shouldBeCalledTimes(1)->willReturn($answer);
        $this->stub->expects($this->exactly(1))
            ->method('getAnswerRestfulTranslator')
            ->willReturn($answerRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($financialQuestion);
        $expectArray = array();

        $expectObject = new FinancialQuestion();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1, 1);

        $actual = $this->stub->objectToArray(
            $financialQuestion,
            array(
                'id',
                'category',
                'content',
                'member',
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'financialQuestions',
                'id'=>$financialQuestion->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'content'=>$financialQuestion->getContent(),
            'deleteType'=>$financialQuestion->getDeleteInfo()->getType(),
            'deleteReason'=>$financialQuestion->getDeleteInfo()->getReason()
        );

        $expectedArray['data']['relationships']['category']['data'] = array(
            array(
                'type' => 'labels',
                'id' => $financialQuestion->getCategory()->getId()
            )
        );
        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $financialQuestion->getMember()->getId()
            )
        );
        $expectedArray['data']['relationships']['deleteBy']['data'] = array(
            array(
                'type' => 'deleteBys',
                'id' => $financialQuestion->getDeleteInfo()->getDeleteBy()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
