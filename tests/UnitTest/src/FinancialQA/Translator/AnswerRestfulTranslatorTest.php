<?php
namespace Sdk\FinancialQA\Translator;

use Sdk\FinancialQA\Model\NullAnswer;
use Sdk\FinancialQA\Model\Answer;
use Sdk\FinancialQA\Model\DeleteInfo;

use Sdk\Member\Model\Member;
use Sdk\Label\Model\Label;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Label\Translator\LabelRestfulTranslator;

use Sdk\FinancialQA\Utils\FinancialAnswer\MockAnswerFactory;

class AnswerRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            AnswerRestfulTranslator::class
        )
            ->setMethods([
                'getMemberRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends AnswerRestfulTranslator {
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Answer());
        $this->assertInstanceOf('Sdk\FinancialQA\Model\NullAnswer', $result);
    }

    public function setMethods(Answer $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['content'])) {
            $expectObject->setContent($attributes['content']);
        }
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $answer = MockAnswerFactory::generateAnswerArray();

        $data =  $answer['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($answer);

        $expectObject = new Answer();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $answer = MockAnswerFactory::generateAnswerArray();
        $data =  $answer['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);
        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($answer);
        $expectArray = array();

        $expectObject = new Answer();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $answer = MockAnswerFactory::generateAnswerObject(1, 1);

        $actual = $this->stub->objectToArray(
            $answer,
            array(
                'id',
                'member',
                'content'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'answers',
                'id'=>$answer->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'content'=>$answer->getContent()
        );

        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $answer->getMember()->getId()
            )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
