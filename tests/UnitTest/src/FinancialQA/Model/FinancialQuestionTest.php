<?php
namespace Sdk\FinancialQA\Model;

use Sdk\FinancialQA\Repository\FinancialQuestionRepository;

use Sdk\Label\Model\Label;
use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class FinancialQuestionTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestion::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends FinancialQuestion{
            public function getRepository() : FinancialQuestionRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Repository\FinancialQuestionRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetQuestionIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetQuestionIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }

    /**
     * 设置setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(1, 2, 3));
    }
    //number 测试 ----------------------------------------------------------   end

    //content 测试 ---------------------------------------------------------- start
    /**
     * 设置setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent('string');
        $this->assertEquals('string', $this->stub->getContent());
    }

    /**
     * 设置setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent(array(1, 2, 3));
    }
    //content 测试 ----------------------------------------------------------   end

    //pageViews 测试 ---------------------------------------------------------- start
    /**
     * 设置setPageViews() 正确的传参类型,期望传值正确
     */
    public function testSetPageViewsCorrectType()
    {
        $this->stub->setPageViews(1);
        $this->assertEquals(1, $this->stub->getPageViews());
    }

    /**
     * 设置setPageViews() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetPageViewsWrongTypeButNumeric()
    {
        $this->stub->setPageViews('1');
        $this->assertTrue(is_int($this->stub->getPageViews()));
        $this->assertEquals(1, $this->stub->getPageViews());
    }
    //pageViews 测试 ----------------------------------------------------------   end

    //commentCount 测试 ---------------------------------------------------------- start
    /**
     * 设置setCommentCount() 正确的传参类型,期望传值正确
     */
    public function testSetCommentCountCorrectType()
    {
        $this->stub->setCommentCount(1);
        $this->assertEquals(1, $this->stub->getCommentCount());
    }

    /**
     * 设置setPageViews() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetCommentCountWrongTypeButNumeric()
    {
        $this->stub->setCommentCount('1');
        $this->assertTrue(is_int($this->stub->getCommentCount()));
        $this->assertEquals(1, $this->stub->getCommentCount());
    }
    //commentCount 测试 ----------------------------------------------------------   end
    
    //attentionDegree 测试 ---------------------------------------------------------- start
    /**
     * 设置setAttentionDegree() 正确的传参类型,期望传值正确
     */
    public function testSetAttentionDegreeCorrectType()
    {
        $this->stub->setAttentionDegree(1);
        $this->assertEquals(1, $this->stub->getAttentionDegree());
    }

    /**
     * 设置setPageViews() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetAttentionDegreeWrongTypeButNumeric()
    {
        $this->stub->setAttentionDegree('1');
        $this->assertTrue(is_int($this->stub->getAttentionDegree()));
        $this->assertEquals(1, $this->stub->getAttentionDegree());
    }
    //attentionDegree 测试 ----------------------------------------------------------   end
    
    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $object = new Label();
        $this->stub->setCategory($object);
        $this->assertSame($object, $this->stub->getCategory());
    }

    /**
     * 设置 setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryType()
    {
        $this->stub->setCategory(array(1,2,3));
    }
    //category 测试 -------------------------------------------------------- end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $object = new Member();
        $this->stub->setMember($object);
        $this->assertSame($object, $this->stub->getMember());
    }

    /**
     * 设置 setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberType()
    {
        $this->stub->setMember(array(1,2,3));
    }
    //member 测试 -------------------------------------------------------- end

    //answer 测试 -------------------------------------------------------- start
    /**
     * 设置 setAnswer() 正确的传参类型,期望传值正确
     */
    public function testSetAnswerCorrectType()
    {
        $object = new Answer();
        $this->stub->setAnswer($object);
        $this->assertSame($object, $this->stub->getAnswer());
    }

    /**
     * 设置 setAnswer() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAnswerType()
    {
        $this->stub->setAnswer(array(1,2,3));
    }
    //answer 测试 -------------------------------------------------------- end

    //deleteInfo 测试 -------------------------------------------------------- start
    /**
     * 设置 setDeleteInfo() 正确的传参类型,期望传值正确
     */
    public function testSetDeleteInfoCorrectType()
    {
        $object = new DeleteInfo();
        $this->stub->setDeleteInfo($object);
        $this->assertSame($object, $this->stub->getDeleteInfo());
    }

    /**
     * 设置 setDeleteInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeleteInfoType()
    {
        $this->stub->setDeleteInfo(array(1,2,3));
    }
    //deleteInfo 测试 -------------------------------------------------------- end
    
    //平台删除
    public function testPlatformDeleteSuccess()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestion::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(true);

        $repository = $this->prophesize(FinancialQuestionRepository::class);
        $repository->platformDelete(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->platformDelete();
        $this->assertTrue($result);
    }

    public function testPlatformDeleteFailure()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestion::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(false);

        $repository = $this->prophesize(FinancialQuestionRepository::class);
        $repository->platformDelete(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->platformDelete();
        $this->assertFalse($result);
    }
    //本人删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestion::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(true);

        $repository = $this->prophesize(FinancialQuestionRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestion::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(false);

        $repository = $this->prophesize(FinancialQuestionRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }

    public function testIsNormal()
    {
        $this->stub->setStatus(FinancialQuestion::STATUS['NORMAL']);

        $result = $this->stub->isNormal();

        $this->assertTrue($result);
    }
}
