<?php
namespace Sdk\FinancialQA\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullFinancialQuestionTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullFinancialQuestion::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsFinancialQuestion()
    {
        $this->assertInstanceof('Sdk\FinancialQA\Model\FinancialQuestion', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testPlatformDelete()
    {
        $result = $this->stub->platformDelete();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }


    public function testDeletes()
    {
        $result = $this->stub->deletes();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
