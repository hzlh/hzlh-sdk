<?php
namespace Sdk\FinancialQA\Model;

use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;

class DeleteInfoTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DeleteInfo(
            0,
            '',
            new Member()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetType()
    {
        $this->assertEquals(0, $this->stub->getType());
    }

    public function testGetReason()
    {
        $this->assertEquals('', $this->stub->getReason());
    }

    public function testGetDeleteBy()
    {
        $this->assertEquals(new Member(), $this->stub->getDeleteBy());
    }
}
