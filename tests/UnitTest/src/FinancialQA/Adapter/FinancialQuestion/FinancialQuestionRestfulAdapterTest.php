<?php
namespace Sdk\FinancialQA\Adapter\FinancialQuestion;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\FinancialQA\Model\FinancialQuestion;
use Sdk\FinancialQA\Model\NullFinancialQuestion;
use Sdk\FinancialQA\Utils\FinancialQuestion\MockFactory;
use Sdk\FinancialQA\Translator\FinancialQuestionRestfulTranslator;

class FinancialQuestionRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestionRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends FinancialQuestionRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIFinancialQuestionAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialQuestion\IFinancialQuestionAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('financialQuestions', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Translator\FinancialQuestionRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'FINANCIAL_QUESTION_LIST',
                FinancialQuestionRestfulAdapter::SCENARIOS['FINANCIAL_QUESTION_LIST']
            ],
            [
                'FINANCIAL_QUESTION_FETCH_ONE',
                FinancialQuestionRestfulAdapter::SCENARIOS['FINANCIAL_QUESTION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $financialQuestion = MockFactory::generateQuestionObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullFinancialQuestion())
            ->willReturn($financialQuestion);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($financialQuestion, $result);
    }
    /**
     * 为FinancialQuestionRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$financialQuestion$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareFinancialQuestionTranslator(
        FinancialQuestion $financialQuestion,
        array $keys,
        array $financialQuestionArray
    ) {
        $translator = $this->prophesize(FinancialQuestionRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($financialQuestion),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($financialQuestionArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(FinancialQuestion $financialQuestion)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($financialQuestion);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'category',
                'content',
                'member'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financialQuestions', $financialQuestionArray);

        $this->success($financialQuestion);

        $result = $this->stub->add($financialQuestion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'category',
                'content',
                'member'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financialQuestions', $financialQuestionArray);

        $this->failure($financialQuestion);
        $result = $this->stub->add($financialQuestion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行platformDelete（）
     * 判断 result 是否为true
     */
    public function testPlatformDeleteSuccess()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialQuestions/'.$financialQuestion->getId().'/delete',
                $financialQuestionArray
            );

        $this->success($financialQuestion);

        $result = $this->stub->platformDelete($financialQuestion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行platformDelete（）
     * 判断 result 是否为false
     */
    public function testPlatformDeleteFailure()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialQuestions/'.$financialQuestion->getId().'/delete',
                $financialQuestionArray
            );

        $this->failure($financialQuestion);
        $result = $this->stub->platformDelete($financialQuestion);
        $this->assertFalse($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialQuestions/'.$financialQuestion->getId().'/delete',
                $financialQuestionArray
            );

        $this->success($financialQuestion);

        $result = $this->stub->deletes($financialQuestion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialQuestionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $financialQuestion = MockFactory::generateQuestionObject(1);
        $financialQuestionArray = array();

        $this->prepareFinancialQuestionTranslator(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialQuestionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialQuestions/'.$financialQuestion->getId().'/delete',
                $financialQuestionArray
            );

        $this->failure($financialQuestion);
        $result = $this->stub->deletes($financialQuestion);
        $this->assertFalse($result);
    }
}
