<?php
namespace Sdk\FinancialQA\Adapter\FinancialAnswer;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\FinancialQA\Model\FinancialAnswer;
use Sdk\FinancialQA\Model\NullFinancialAnswer;
use Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory;
use Sdk\FinancialQA\Translator\FinancialAnswerRestfulTranslator;

class FinancialAnswerRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswerRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends FinancialAnswerRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIFinancialAnswerAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialAnswer\IFinancialAnswerAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('financialAnswers', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Translator\FinancialAnswerRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'FINANCIAL_ANSWER_LIST',
                FinancialAnswerRestfulAdapter::SCENARIOS['FINANCIAL_ANSWER_LIST']
            ],
            [
                'FINANCIAL_ANSWER_FETCH_ONE',
                FinancialAnswerRestfulAdapter::SCENARIOS['FINANCIAL_ANSWER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $financialAnswer = MockFactory::generateAnswerObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullFinancialAnswer())
            ->willReturn($financialAnswer);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($financialAnswer, $result);
    }
    /**
     * 为FinancialAnswerRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$financialAnswer$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareFinancialAnswerTranslator(
        FinancialAnswer $financialAnswer,
        array $keys,
        array $financialAnswerArray
    ) {
        $translator = $this->prophesize(FinancialAnswerRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($financialAnswer),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($financialAnswerArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(FinancialAnswer $financialAnswer)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($financialAnswer);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'question',
                'member',
                'content'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financialAnswers', $financialAnswerArray);

        $this->success($financialAnswer);

        $result = $this->stub->add($financialAnswer);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'question',
                'member',
                'content'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financialAnswers', $financialAnswerArray);

        $this->failure($financialAnswer);
        $result = $this->stub->add($financialAnswer);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行platformDelete（）
     * 判断 result 是否为true
     */
    public function testPlatformDeleteSuccess()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->success($financialAnswer);

        $result = $this->stub->platformDelete($financialAnswer);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行platformDelete（）
     * 判断 result 是否为false
     */
    public function testPlatformDeleteFailure()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->failure($financialAnswer);
        $result = $this->stub->platformDelete($financialAnswer);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行questionerDelete（）
     * 判断 result 是否为true
     */
    public function testQuestionerDeleteSuccess()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->success($financialAnswer);

        $result = $this->stub->questionerDelete($financialAnswer);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行questionerDelete（）
     * 判断 result 是否为false
     */
    public function testQuestionerDeleteFailure()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->failure($financialAnswer);
        $result = $this->stub->questionerDelete($financialAnswer);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->success($financialAnswer);

        $result = $this->stub->deletes($financialAnswer);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinancialAnswerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $financialAnswer = MockFactory::generateAnswerObject(1);
        $financialAnswerArray = array();

        $this->prepareFinancialAnswerTranslator(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            ),
            $financialAnswerArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'financialAnswers/'.$financialAnswer->getId().'/delete',
                $financialAnswerArray
            );

        $this->failure($financialAnswer);
        $result = $this->stub->deletes($financialAnswer);
        $this->assertFalse($result);
    }
}
