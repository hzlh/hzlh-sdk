<?php
namespace Sdk\FinancialQA\Utils\FinancialQuestion;

use Sdk\FinancialQA\Model\FinancialQuestion;
use Sdk\FinancialQA\Model\DeleteInfo;

class MockFactory
{
    /**
     * [generateFinancialQuestionArray 生成问题数组]
     * @return [array] [问题数组]
     */
    public static function generateQuestionArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $financialQuestion = array();

        $financialQuestion = array(
            'data'=>array(
                'type'=>'financialQuestions',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //number
        $number = self::generateNumber($faker, $value);
        $attributes['number'] = $number;
        //content
        $content = self::generateContent($faker, $value);
        $attributes['content'] = $content;
        //pageViews
        $pageViews = self::generatePageViews($faker, $value);
        $attributes['pageViews'] = $pageViews;
        //commentCount
        $commentCount = self::generateCommentCount($faker, $value);
        $attributes['commentCount'] = $commentCount;
        //attentionDegree
        $attentionDegree = self::generateAttentionDegree($faker, $value);
        $attributes['attentionDegree'] = $attentionDegree;
        //deleteType
        $deleteType = self::generateDeleteType($faker, $value);
        $attributes['deleteType'] = $deleteType;
        //deleteReason
        $deleteReason = self::generateDeleteReason($faker, $value);
        $attributes['deleteReason'] = $deleteReason;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $financialQuestion['data']['attributes'] = $attributes;
        //category
        $financialQuestion['data']['relationships']['category']['data'] = array(
            'type' => 'labels',
            'id' => $faker->randomNumber(1)
        );
        //member
        $financialQuestion['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );
        //answer
        $financialQuestion['data']['relationships']['answer']['data'] = array(
            'type' => 'financialAnswers',
            'id' => $faker->randomNumber(1)
        );

        return $financialQuestion;
    }

    /**
     * [generateFinancialQuestionObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateQuestionObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : FinancialQuestion {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $financialQuestion = new FinancialQuestion($id);

        //content
        $content = self::generateContent($faker, $value);
        $financialQuestion->setContent($content);
        //category
        $category = self::generateLabel($faker, $value);
        $financialQuestion->setCategory($category);
        //member
        $member = self::generateMember($faker, $value);
        $financialQuestion->setMember($member);
        //deleteReason
        $deleteReason = self::generateDeleteReason($faker, $value);
        //deleteType
        $deleteType = self::generateDeleteType($faker, $value);
        //deleteBy
        $deleteBy = self::generateMember($faker, $value);
        $financialQuestion->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $financialQuestion->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $financialQuestion->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $financialQuestion->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $financialQuestion->setStatus($status);

        return $financialQuestion;
    }

    private static function generateNumber($faker, array $value = array())
    {
        return $number = isset($value['number']) ?
        $value['number'] : 'Q201903021';
    }
    private static function generateContent($faker, array $value = array())
    {
        return $content = isset($value['content']) ?
        $value['content'] : $faker->word;
    }
    private static function generatePageViews($faker, array $value = array())
    {
        return $pageViews = isset($value['pageViews']) ?
        $value['pageViews'] : $faker->numerify();
    }
    private static function generateCommentCount($faker, array $value = array())
    {
        return $commentCount = isset($value['commentCount']) ?
        $value['commentCount'] : $faker->numerify();
    }
    private static function generateAttentionDegree($faker, array $value = array())
    {
        return $attentionDegree = isset($value['attentionDegree']) ?
        $value['attentionDegree'] : $faker->numerify();
    }
    private static function generateDeleteType($faker, array $value = array())
    {
        return $deleteType = isset($value['deleteType']) ?
        $value['deleteType'] : $faker->randomElement(
            $array = array(
                DeleteInfo::TYPE['NULL'],
                DeleteInfo::TYPE['DELETED'],
                DeleteInfo::TYPE['PLATFORM_DELETED'],
                DeleteInfo::TYPE['QUESTIONER_DELETED']
            )
        );
    }
    private static function generateDeleteReason($faker, array $value = array())
    {
        return $deleteReason = isset($value['deleteReason']) ?
        $value['deleteReason'] : $faker->word;
    }
    private static function generateLabel($faker, array $value = array())
    {
        return $label = isset($value['label']) ?
            $value['label'] : \Sdk\Label\Utils\MockFactory::generateLabelObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
