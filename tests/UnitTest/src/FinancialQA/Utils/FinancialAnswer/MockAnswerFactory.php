<?php
namespace Sdk\FinancialQA\Utils\FinancialAnswer;

use Sdk\FinancialQA\Model\Answer;

class MockAnswerFactory
{
    /**
     * [generateAnswerArray 生成问题数组]
     * @return [array] [问题数组]
     */
    public static function generateAnswerArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $answer = array();

        $answer = array(
            'data'=>array(
                'type'=>'answers',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //content
        $content = self::generateContent($faker, $value);
        $attributes['content'] = $content;

        $answer['data']['attributes'] = $attributes;
        //member
        $answer['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );

        return $answer;
    }

    /**
     * [generateAnswerObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateAnswerObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Answer {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $answer = new Answer($id);

        //content
        $content = self::generateContent($faker, $value);
        $answer->setContent($content);
        //member
        $member = self::generateMember($faker, $value);
        $answer->setMember($member);

        return $answer;
    }

    private static function generateContent($faker, array $value = array())
    {
        return $content = isset($value['content']) ?
        $value['content'] : $faker->word;
    }
    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
