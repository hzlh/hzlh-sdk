<?php
namespace Sdk\FinancialQA\Repository;

use Sdk\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionRestfulAdapter;
use Sdk\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class FinancialQuestionRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialQuestionRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends FinancialQuestionRepository {
            public function getAdapter() : FinancialQuestionRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : FinancialQuestionMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为FinancialQuestionRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以FinancialQuestionRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(FinancialQuestionRestfulAdapter::class);
        $adapter->scenario(Argument::exact(FinancialQuestionRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(FinancialQuestionRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为FinancialQuestionRestfulAdapter建立预言
     * 建立预期状况：platformDelete() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testPlatformDelete()
    {
        $financialQuestion = \Sdk\FinancialQA\Utils\FinancialQuestion\MockFactory::generateQuestionObject(1);

        $adapter = $this->prophesize(FinancialQuestionRestfulAdapter::class);
        $adapter->platformDelete(
            Argument::exact($financialQuestion)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->platformDelete($financialQuestion);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为FinancialQuestionRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $financialQuestion = \Sdk\FinancialQA\Utils\FinancialQuestion\MockFactory::generateQuestionObject(1);

        $adapter = $this->prophesize(FinancialQuestionRestfulAdapter::class);
        $adapter->deletes(
            Argument::exact($financialQuestion)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($financialQuestion);
        $this->assertTrue($result);
    }
}
