<?php
namespace Sdk\Deposit\Repository;

use Sdk\Deposit\Adapter\Deposit\DepositRestfulAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class DepositRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(DepositRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends DepositRepository {
            public function getAdapter() : DepositRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Deposit\Adapter\Deposit\DepositRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为DepositRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以DepositRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(DepositRestfulAdapter::class);
        $adapter->scenario(Argument::exact(DepositRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(DepositRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为DepositRestfulAdapter建立预言
     * 建立预期状况：deposit() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeposit()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositObject(1);

        $adapter = $this->prophesize(DepositRestfulAdapter::class);
        $adapter->deposit(Argument::exact($deposit))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deposit($deposit);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为DepositRestfulAdapter建立预言
     * 建立预期状况：pay() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testPay()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositObject(1);

        $adapter = $this->prophesize(DepositRestfulAdapter::class);
        $adapter->pay(Argument::exact($deposit))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->pay($deposit);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为DepositRestfulAdapter建立预言
     * 建立预期状况：paymentFailure() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testPaymentFailure()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositObject(1);

        $adapter = $this->prophesize(DepositRestfulAdapter::class);
        $adapter->paymentFailure(Argument::exact($deposit))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->paymentFailure($deposit);
        $this->assertTrue($result);
    }
}
