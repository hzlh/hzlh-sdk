<?php
namespace Sdk\MerchantCoupon\Adapter\MerchantCoupon;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\MerchantCoupon\Model\NullMerchantCoupon;
use Sdk\MerchantCoupon\Utils\MockFactory;
use Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator;

class MerchantCouponRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MerchantCouponRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends MerchantCouponRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIMerchantCouponAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Adapter\MerchantCoupon\IMerchantCouponAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('releaseCoupons', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PORTAL_COUPON_LIST',
                MerchantCouponRestfulAdapter::SCENARIOS['PORTAL_COUPON_LIST']
            ],
            [
                'OA_COUPON_LIST',
                MerchantCouponRestfulAdapter::SCENARIOS['OA_COUPON_LIST']
            ],
            [
                'COUPON_FETCH_ONE',
                MerchantCouponRestfulAdapter::SCENARIOS['COUPON_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $merchantCoupon = MockFactory::generateMerchantCouponObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullMerchantCoupon())
            ->willReturn($merchantCoupon);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($merchantCoupon, $result);
    }
    /**
     * 为MerchantCouponRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$merchantCoupon，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareMerchantCouponTranslator(
        MerchantCoupon $merchantCoupon,
        array $keys,
        array $merchantCouponArray
    ) {
        $translator = $this->prophesize(MerchantCouponRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($merchantCoupon),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($merchantCouponArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(MerchantCoupon $merchantCoupon)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($merchantCoupon);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add()）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);
        $merchantCouponArray = array();

        $this->prepareMerchantCouponTranslator(
            $merchantCoupon,
            array(
                'name',
                'couponType',
                'denomination',
                'discount',
                'useStandard',
                'validityStartTime',
                'validityEndTime',
                'issueTotal',
                'distributeTotal',
                'perQuota',
                'useScenario',
                'receivingMode',
                'receivingUsers',
                'isSuperposition',
                'applyScope',
                'applySituation',
                'releaseType',
                'reference'
            ),
            $merchantCouponArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('releaseCoupons', $merchantCouponArray);

        $this->success($merchantCoupon);

        $result = $this->stub->add($merchantCoupon);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add()
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);
        $merchantCouponArray = array();

        $this->prepareMerchantCouponTranslator(
            $merchantCoupon,
            array(
                'name',
                'couponType',
                'denomination',
                'discount',
                'useStandard',
                'validityStartTime',
                'validityEndTime',
                'issueTotal',
                'distributeTotal',
                'perQuota',
                'useScenario',
                'receivingMode',
                'receivingUsers',
                'isSuperposition',
                'applyScope',
                'applySituation',
                'releaseType',
                'reference'
            ),
            $merchantCouponArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('releaseCoupons', $merchantCouponArray);

        $this->failure($merchantCoupon);
        $result = $this->stub->add($merchantCoupon);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行delete（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'releaseCoupons/'.$merchantCoupon->getId().'/delete'
            );

        $this->success($merchantCoupon);

        $result = $this->stub->deletes($merchantCoupon);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行delete（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'releaseCoupons/'.$merchantCoupon->getId().'/delete'
            );

        $this->failure($merchantCoupon);
        $result = $this->stub->deletes($merchantCoupon);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行discontinue（）
     * 判断 result 是否为true
     */
    public function testDiscontinueSuccess()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'releaseCoupons/'.$merchantCoupon->getId().'/discontinue'
            );

        $this->success($merchantCoupon);

        $result = $this->stub->discontinue($merchantCoupon);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMerchantCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行discontinue（）
     * 判断 result 是否为false
     */
    public function testDiscontinueFailure()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'releaseCoupons/'.$merchantCoupon->getId().'/discontinue'
            );

        $this->failure($merchantCoupon);
        $result = $this->stub->discontinue($merchantCoupon);
        $this->assertFalse($result);
    }
}
