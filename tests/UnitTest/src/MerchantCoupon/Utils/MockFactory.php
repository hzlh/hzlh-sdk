<?php
namespace Sdk\MerchantCoupon\Utils;

use Sdk\MerchantCoupon\Model\MerchantCoupon;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateMerchantCouponArray 生成优惠劵信息数组]
     * @return [array] [优惠劵数组]
     */
    public static function generateMerchantCouponArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $merchantCoupon = array();

        $merchantCoupon = array(
            'data'=>array(
                'type'=>'releaseCoupons',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //releaseType
        $releaseType = self::generateReleaseType($faker, $value);
        $attributes['releaseType'] = $releaseType;

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;

        //type
        $type = self::generateType($faker, $value);
        $attributes['couponType'] = $type;

        //denomination
        $denomination = self::generateDenomination($faker, $value);
        $attributes['denomination'] = $denomination;

        //discount
        $discount = self::generateDiscount($faker, $value);
        $attributes['discount'] = $discount;

        //useStandard
        $useStandard = self::generateUseStandard($faker, $value);
        $attributes['useStandard'] = $useStandard;

        //validityStartTime
        $validityStartTime = self::generateValidityStartTime($faker, $value);
        $attributes['validityStartTime'] = $validityStartTime;

        //validityEndTime
        $validityEndTime = self::generateValidityEndTime($faker, $value);
        $attributes['validityEndTime'] = $validityEndTime;

        //issueTotal
        $issueTotal = self::generateIssueTotal($faker, $value);
        $attributes['issueTotal'] = $issueTotal;

        //distributeTotal
        $distributeTotal = self::generateDistributeTotal($faker, $value);
        $attributes['distributeTotal'] = $distributeTotal;

        //perQuota
        $perQuota = self::generatePerQuota($faker, $value);
        $attributes['perQuota'] = $perQuota;
        
        //useScenario
        $useScenario = self::generateUseScenario($faker, $value);
        $attributes['useScenario'] = $useScenario;

        //receivingMode
        $receivingMode = self::generateReceivingMode($faker, $value);
        $attributes['receivingMode'] = $receivingMode;

        //receivingUsers
        $receivingUsers = self::geneRateReceivingUsers($faker, $value);
        $attributes['receivingUsers'] = $receivingUsers;

        //isSuperposition
        $isSuperposition = self::geneRateIsSuperposition($faker, $value);
        $attributes['isSuperposition'] = $isSuperposition;

        //applyScope
        $applyScope = self::geneRateApplyScope($faker, $value);
        $attributes['applyScope'] = $applyScope;

        //applySituation
        $applySituation = self::geneRateApplySituation($faker, $value);
        $attributes['applySituation'] = $applySituation;

        //number
        $number = self::geneRateNumber($faker, $value);
        $attributes['number'] = $number;

        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;

        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;

        $merchantCoupon['data']['attributes'] = $attributes;

        return $merchantCoupon;
    }
    /**
     * [generateMerchantCouponObject 生成优惠劵对象对象]
     * @param  int|integer $id    [优惠劵id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [优惠劵对象]
     */
    public static function generateMerchantCouponObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : MerchantCoupon {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $merchantCoupon = new MerchantCoupon($id);

        //releaseType
        $releaseType = self::generateReleaseType($faker, $value);
        $merchantCoupon->setReleaseType($releaseType);

        //name
        $name = self::generateName($faker, $value);
        $merchantCoupon->setName($name);

        //type
        $type = self::generateType($faker, $value);
        $merchantCoupon->setCouponType($type);

        //denomination
        $denomination = self::generateDenomination($faker, $value);
        $merchantCoupon->setDenomination($denomination);

        //discount
        $discount = self::generateDiscount($faker, $value);
        $merchantCoupon->setDiscount($discount);

        //useStandard
        $useStandard = self::generateUseStandard($faker, $value);
        $merchantCoupon->setUseStandard($useStandard);

        //validityStartTime
        $validityStartTime = self::generateValidityStartTime($faker, $value);
        $merchantCoupon->setValidityStartTime($validityStartTime);

        //validityEndTime
        $validityEndTime = self::generateValidityEndTime($faker, $value);
        $merchantCoupon->setValidityEndTime($validityEndTime);

        //issueTotal
        $issueTotal = self::generateIssueTotal($faker, $value);
        $merchantCoupon->setIssueTotal($issueTotal);

        //distributeTotal
        $distributeTotal = self::generateDistributeTotal($faker, $value);
        $merchantCoupon->setDistributeTotal($distributeTotal);

        //perQuota
        $perQuota = self::generatePerQuota($faker, $value);
        $merchantCoupon->setPerQuota($perQuota);

        //useScenario
        $useScenario = self::generateUseScenario($faker, $value);
        $merchantCoupon->setUseScenario($useScenario);

        //receivingMode
        $receivingMode = self::generateReceivingMode($faker, $value);
        $merchantCoupon->setReceivingMode($receivingMode);

        //receivingUsers
        $receivingUsers = self::geneRateReceivingUsers($faker, $value);
        $merchantCoupon->setReceivingUsers($receivingUsers);

        //isSuperposition
        $isSuperposition = self::geneRateIsSuperposition($faker, $value);
        $merchantCoupon->setIsSuperposition($isSuperposition);

        //applyScope
        $applyScope = self::geneRateApplyScope($faker, $value);
        $merchantCoupon->setApplyScope($applyScope);

        //applySituation
        $applySituation = self::geneRateApplySituation($faker, $value);
        $merchantCoupon->setApplySituation($applySituation);

        //number
        $number = self::geneRateNumber($faker, $value);
        $merchantCoupon->setNumber($number);

        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $merchantCoupon->setStatus($status);

        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $merchantCoupon->setStatusTime($statusTime);

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $merchantCoupon->setCreateTime($createTime);

        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $merchantCoupon->setUpdateTime($updateTime);
        
        return $merchantCoupon;
    }

    private static function generateReleaseType($faker, array $value = array())
    {
        return isset($value['releaseType'])
        ? $value['releaseType']
        : $faker->randomElement(MerchantCoupon::RELEASE_TYPE);
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name'])
        ? $value['name'] : $faker->name();
    }

    private static function generateType($faker, array $value = array())
    {
        return isset($value['type'])
        ? $value['type']
        : $faker->randomElement(MerchantCoupon::COUPON_TYPE);
    }

    private static function generateDenomination($faker, array $value = array())
    {
        return isset($value['denomination'])
        ? $value['denomination'] : $faker->numerify();
    }

    private static function generateDiscount($faker, array $value = array())
    {
        return isset($value['discount'])
        ? $value['discount'] : $faker->numerify();
    }

    private static function generateUseStandard($faker, array $value = array())
    {
        return isset($value['useStandard'])
        ? $value['useStandard'] : $faker->numerify();
    }

    private static function generateValidityStartTime($faker, array $value = array())
    {
        return isset($value['validityStartTime'])
        ? $value['validityStartTime'] : $faker->unixTime();
    }

    private static function generateValidityEndTime($faker, array $value = array())
    {
        return isset($value['validityEndTime'])
         ? $value['validityEndTime'] : $faker->unixTime();
    }

    private static function generateIssueTotal($faker, array $value = array())
    {
        return isset($value['issueTotal'])
        ? $value['issueTotal'] : $faker->numerify();
    }

    private static function generateDistributeTotal($faker, array $value = array())
    {
        return isset($value['distributeTotal'])
         ? $value['distributeTotal'] : $faker->numerify();
    }

    private static function generatePerQuota($faker, array $value = array())
    {
        return isset($value['perQuota'])
        ? $value['perQuota'] : $faker->numerify();
    }

    private static function generateUseScenario($faker, array $value = array())
    {
        return isset($value['useScenario'])
         ? $value['useScenario'] : $faker->randomElement(MerchantCoupon::USE_SCENARIO);
    }

    private static function generateReceivingMode($faker, array $value = array())
    {
        return isset($value['receivingMode'])
         ? $value['receivingMode'] : $faker->randomElement(MerchantCoupon::RECEIVING_MODE);
    }

    private static function geneRateReceivingUsers($faker, array $value = array())
    {
        return isset($value['receivingUsers'])
        ? $value['receivingUsers'] : $faker->randomElement(MerchantCoupon::RECEIVING_USERS);
    }

    private static function geneRateIsSuperposition($faker, array $value = array())
    {
        return isset($value['isSuperposition'])
        ? $value['isSuperposition'] : $faker->randomElement(MerchantCoupon::IS_SUPERPOSITION);
    }

    private static function geneRateApplyScope($faker, array $value = array())
    {
        return isset($value['applyScope'])
         ? $value['applyScope'] : $faker->randomElement(MerchantCoupon::APPLY_SCOPE);
    }

    private static function geneRateApplySituation($faker, array $value = array())
    {
        return isset($value['applySituation'])
        ? $value['applySituation'] : array(1);
    }

    private static function geneRateNumber($faker, array $value = array())
    {
        return isset($value['number'])
         ? $value['number'] : $faker->name();
    }
}
