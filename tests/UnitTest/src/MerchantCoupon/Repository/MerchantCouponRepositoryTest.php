<?php
namespace Sdk\MerchantCoupon\Repository;

use Sdk\MerchantCoupon\Adapter\MerchantCoupon\MerchantCouponRestfulAdapter;

use Sdk\MerchantCoupon\Utils\MockFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class MerchantCouponRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MerchantCouponRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends MerchantCouponRepository {
            public function getAdapter() : MerchantCouponRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Adapter\MerchantCoupon\MerchantCouponRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为MerchantCouponRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以MerchantCouponRepository::PORTAL_LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(MerchantCouponRestfulAdapter::class);
        $adapter->scenario(Argument::exact(MerchantCouponRepository::PORTAL_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(MerchantCouponRepository::PORTAL_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 获取mock数据
     * 为MerchantCouponRestfulAdapter建立预言
     * 建立预期状况：add() 方法将会被调用一次，并以$merchantCoupon为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testAdd()
    {
        $merchantCoupon = MockFactory::generateMerchantCouponObject(1);
        $adapter = $this->prophesize(MerchantCouponRestfulAdapter::class);
        $adapter->add(Argument::exact($merchantCoupon))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->add($merchantCoupon);
        $this->assertTrue($result);
    }

    /**
     * 获取mock数据
     * 为MerchantCouponRestfulAdapter建立预言
     * 建立预期状况：discontinue() 方法将会被调用一次，并以$merchantCoupon为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testDiscontinue()
    {
        $merchantCoupon = \Sdk\MerchantCoupon\Utils\MockFactory::generateMerchantCouponObject(1);

        $adapter = $this->prophesize(MerchantCouponRestfulAdapter::class);
        $adapter->discontinue(Argument::exact($merchantCoupon))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->discontinue($merchantCoupon);
        $this->assertTrue($result);
    }

    /**
     * 获取mock数据
     * 为MerchantCouponRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以$merchantCoupon为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testDeletes()
    {
        $merchantCoupon = \Sdk\MerchantCoupon\Utils\MockFactory::generateMerchantCouponObject(1);

        $adapter = $this->prophesize(MerchantCouponRestfulAdapter::class);
        $adapter->deletes(Argument::exact($merchantCoupon))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($merchantCoupon);
        $this->assertTrue($result);
    }
}
