<?php
namespace Sdk\MerchantCoupon\Translator;

use Sdk\MerchantCoupon\Model\NullMerchantCoupon;
use Sdk\MerchantCoupon\Model\MerchantCoupon;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Member\Model\Member;

class MerchantCouponRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            MerchantCouponRestfulTranslator::class
        )
            ->setMethods(['getTranslatorFactory'])
            ->getMock();

        $this->childStub =
        new class extends MerchantCouponRestfulTranslator {
            public function getTranslatorFactory() : TranslatorFactory
            {
                return new TranslatorFactory();
            }
        };
        parent::setUp();
    }

    public function testGetTranslatorFactory()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Translator\TranslatorFactory',
            $this->childStub->getTranslatorFactory()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new MerchantCoupon());
        $this->assertInstanceOf('Sdk\MerchantCoupon\Model\NullMerchantCoupon', $result);
    }

    public function setMethods(MerchantCoupon $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['releaseType'])) {
            $expectObject->setReleaseType($attributes['releaseType']);
        }
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['couponType'])) {
            $expectObject->setCouponType($attributes['couponType']);
        }
        if (isset($attributes['denomination'])) {
            $expectObject->setDenomination($attributes['denomination']);
        }
        if (isset($attributes['discount'])) {
            $expectObject->setDiscount($attributes['discount']);
        }
        if (isset($attributes['useStandard'])) {
            $expectObject->setUseStandard($attributes['useStandard']);
        }
        if (isset($attributes['validityStartTime'])) {
            $expectObject->setValidityStartTime($attributes['validityStartTime']);
        }
        if (isset($attributes['validityEndTime'])) {
            $expectObject->setValidityEndTime($attributes['validityEndTime']);
        }
        if (isset($attributes['issueTotal'])) {
            $expectObject->setIssueTotal($attributes['issueTotal']);
        }
        if (isset($attributes['distributeTotal'])) {
            $expectObject->setDistributeTotal($attributes['distributeTotal']);
        }
        if (isset($attributes['perQuota'])) {
            $expectObject->setPerQuota($attributes['perQuota']);
        }
        if (isset($attributes['useScenario'])) {
            $expectObject->setUseScenario($attributes['useScenario']);
        }
        if (isset($attributes['receivingMode'])) {
            $expectObject->setReceivingMode($attributes['receivingMode']);
        }
        if (isset($attributes['receivingUsers'])) {
            $expectObject->setReceivingUsers($attributes['receivingUsers']);
        }
        if (isset($attributes['isSuperposition'])) {
            $expectObject->setIsSuperposition($attributes['isSuperposition']);
        }
        if (isset($attributes['applyScope'])) {
            $expectObject->setApplyScope($attributes['applyScope']);
        }
        if (isset($attributes['applySituation'])) {
            $expectObject->setApplySituation($attributes['applySituation']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $merchantCoupon = \Sdk\MerchantCoupon\Utils\MockFactory::generateMerchantCouponArray();

        $data =  $merchantCoupon['data'];
        $relationships = [];

        $actual = $this->stub->arrayToObject($merchantCoupon);

        $expectObject = new MerchantCoupon();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $merchantCoupon = \Sdk\MerchantCoupon\Utils\MockFactory::generateMerchantCouponArray();
        $data =  $merchantCoupon['data'];
        $relationships = [];

        $actual = $this->stub->arrayToObjects($merchantCoupon);
        $expectArray = array();

        $expectObject = new MerchantCoupon();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $merchantCoupon = \Sdk\merchantCoupon\Utils\MockFactory::generateMerchantCouponObject(1, 1);

        $actual = $this->stub->objectToArray($merchantCoupon, array(
            'id',
            'number',
            'name',
            'couponType',
            'denomination',
            'discount',
            'useStandard',
            'validityStartTime',
            'validityEndTime',
            'issueTotal',
            'distributeTotal',
            'perQuota',
            'useScenario',
            'receivingMode',
            'receivingUsers',
            'isSuperposition',
            'applyScope',
            'applySituation',
            'status',
            'createTime',
            'updateTime',
            'releaseType'
        ));
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'releaseCoupons',
                'id'=>$merchantCoupon->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'number'=>$merchantCoupon->getNumber(),
            'name'=>$merchantCoupon->getName(),
            'couponType'=>$merchantCoupon->getCouponType(),
            'denomination'=>$merchantCoupon->getDenomination(),
            'discount'=>$merchantCoupon->getDiscount(),
            'useStandard'=>$merchantCoupon->getUseStandard(),
            'validityStartTime'=>$merchantCoupon->getValidityStartTime(),
            'validityEndTime'=>$merchantCoupon->getValidityEndTime(),
            'issueTotal'=>$merchantCoupon->getIssueTotal(),
            'distributeTotal'=>$merchantCoupon->getDistributeTotal(),
            'perQuota'=>$merchantCoupon->getPerQuota(),
            'useScenario'=>$merchantCoupon->getUseScenario(),
            'receivingMode'=>$merchantCoupon->getReceivingMode(),
            'receivingUsers'=>$merchantCoupon->getReceivingUsers(),
            'isSuperposition'=>$merchantCoupon->getIsSuperposition(),
            'applyScope'=>$merchantCoupon->getApplyScope(),
            'applySituation'=>$merchantCoupon->getApplySituation(),
            'status'=>$merchantCoupon->getStatus(),
            'createTime'=>$merchantCoupon->getCreateTime(),
            'updateTime'=>$merchantCoupon->getUpdateTime(),
            'releaseType'=>$merchantCoupon->getReleaseType()

        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
