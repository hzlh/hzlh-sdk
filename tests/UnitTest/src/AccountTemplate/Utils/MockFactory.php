<?php
namespace Sdk\AccountTemplate\Utils;

use Sdk\AccountTemplate\Model\AccountTemplate;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateAccountTemplateArray 生成账套信息数组]
     * @return [array] [账套数组]
     */
    public static function generateAccountTemplateArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $accountTemplate = array();

        $accountTemplate = array(
            'data'=>array(
                'type'=>'accountTemplates',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //typeVat
        $typeVat = self::generateTypeVat($faker, $value);
        $attributes['typeVat'] = $typeVat;
        //activationDate
        $activationDate = self::generateActivationDate($faker, $value);
        $attributes['activationDate'] = $activationDate;
        //accountingStandard
        $accountingStandard = self::generateAccountingStandard($faker, $value);
        $attributes['accountingStandard'] = $accountingStandard;
        //voucherApproval
        $voucherApproval = self::generateVoucherApproval($faker, $value);
        $attributes['voucherApproval'] = $voucherApproval;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $accountTemplate['data']['attributes'] = $attributes;

        //enterprise
        $accountTemplate['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprise',
            'id' => $faker->randomNumber(1)
        );
        //industry
        $accountTemplate['data']['relationships']['industry']['data'] = array(
            'type' => 'dictionaries',
            'id' => $faker->randomNumber(1)
        );
        
        return $accountTemplate;
    }
    /**
     * [generateAccountTemplateObject 生成账套对象]
     * @param  int|integer $id    [账套Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [账套对象]
     */
    public static function generateAccountTemplateObject(int $id = 0, int $seed = 0, array $value = array()) : AccountTemplate
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $accountTemplate = new AccountTemplate($id);

        //name
        $name = self::generateName($faker, $value);
        $accountTemplate->setName($name);
        //typeVat
        $typeVat = self::generateTypeVat($faker, $value);
        $accountTemplate->setTypeVat($typeVat);
        //activationDate
        $activationDate = self::generateActivationDate($faker, $value);
        $accountTemplate->setActivationDate($activationDate);
        //accountingStandard
        $accountingStandard = self::generateAccountingStandard($faker, $value);
        $accountTemplate->setAccountingStandard($accountingStandard);
        //voucherApproval
        $voucherApproval = self::generateVoucherApproval($faker, $value);
        $accountTemplate->setVoucherApproval($voucherApproval);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $accountTemplate->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $accountTemplate->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $accountTemplate->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $accountTemplate->setStatus($status);
        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $accountTemplate->setEnterprise($enterprise);
        //industry
        $industry = self::generateIndustry($faker, $value);
        $accountTemplate->setIndustry($industry);

        return $accountTemplate;
    }

    private static function generateName($faker, array $value = array())
    {
        return $name = isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateTypeVat($faker, array $value = array())
    {
        return $typeVat = isset($value['typeVat']) ?
        $value['typeVat'] : $faker->numberBetween($min = 1, $max = 2);
    }

    private static function generateAccountingStandard($faker, array $value = array())
    {
        return $accountingStandard = isset($value['accountingStandard']) ?
        $value['accountingStandard'] : $faker->numberBetween($min = 1, $max = 2);
    }

    private static function generateVoucherApproval($faker, array $value = array())
    {
        return $voucherApproval = isset($value['voucherApproval']) ?
        $value['voucherApproval'] : $faker->numberBetween($min = 1, $max = 2);
    }

    private static function generateActivationDate($faker, array $value = array())
    {
        return $activationDate = isset($value['activationDate']) ?
        $value['activationDate'] : $faker->unixTime();
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
        $value['enterprise'] : \Sdk\Enterprise\Utils\MockFactory::generateEnterpriseObject($faker->numerify(), $faker->numerify());
    }

    private static function generateIndustry($faker, array $value = array())
    {
        return $industry = isset($value['industry']) ?
        $value['industry'] : \Sdk\Dictionary\Utils\MockFactory::generateDictionaryObject($faker->numerify(), $faker->numerify());
    }
}
