<?php
namespace Sdk\Appointment\Adapter\Appointment;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Appointment\Model\Appointment;
use Sdk\Appointment\Model\NullAppointment;
use Sdk\Appointment\Utils\MockFactory;
use Sdk\Appointment\Translator\AppointmentRestfulTranslator;

class AppointmentRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AppointmentRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends AppointmentRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIAppointmentAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Adapter\Appointment\IAppointmentAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('appointments', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Translator\AppointmentRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'APPOINTMENT_LIST',
                AppointmentRestfulAdapter::SCENARIOS['APPOINTMENT_LIST']
            ],
            [
                'APPOINTMENT_FETCH_ONE',
                AppointmentRestfulAdapter::SCENARIOS['APPOINTMENT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $appointment = MockFactory::generateAppointmentObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullAppointment())
            ->willReturn($appointment);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($appointment, $result);
    }
    /**
     * 为AppointmentRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$appointment，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareAppointmentTranslator(
        Appointment $appointment,
        array $keys,
        array $appointmentArray
    ) {
        $translator = $this->prophesize(AppointmentRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($appointment),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($appointmentArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Appointment $appointment)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($appointment);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppointmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add()）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $appointmentArray = array();

        $this->prepareAppointmentTranslator(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'attachments',
                'loanProductSnapshot',
                'member'
            ),
            $appointmentArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('appointments', $appointmentArray);

        $this->success($appointment);

        $result = $this->stub->add($appointment);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $appointmentArray = array();

        $this->prepareAppointmentTranslator(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'attachments',
                'loanProductSnapshot',
                'member'
            ),
            $appointmentArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('appointments', $appointmentArray);

        $this->failure($appointment);
        $result = $this->stub->add($appointment);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppointmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行resubmit（）
     * 判断 result 是否为true
     */
    public function testResubmitSuccess()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $appointmentArray = array();

        $this->prepareAppointmentTranslator(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'attachments'
            ),
            $appointmentArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('appointments/'.$appointment->getId().'/resubmit', $appointmentArray);

        $this->success($appointment);

        $result = $this->stub->resubmit($appointment);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppointmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $appointmentArray = array();

        $this->prepareAppointmentTranslator(
            $appointment,
            array('remark'),
            $appointmentArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('appointments/'.$appointment->getId().'/approve', $appointmentArray);

        $this->success($appointment);

        $result = $this->stub->approve($appointment);
        $this->assertTrue($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppointmentTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行completed（）
     * 判断 result 是否为true
     */
    public function testCompletedSuccess()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $appointmentArray = array();

        $this->prepareAppointmentTranslator(
            $appointment,
            array(
                'loanFailReason',
                'loanContractNumber',
                'loanStatus'
            ),
            $appointmentArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('appointments/'.$appointment->getId().'/completed', $appointmentArray);

        $this->success($appointment);

        $result = $this->stub->completed($appointment);
        $this->assertTrue($result);
    }
}
