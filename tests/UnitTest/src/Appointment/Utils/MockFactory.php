<?php
namespace Sdk\Appointment\Utils;

use Sdk\Appointment\Model\Appointment;
use Sdk\Enterprise\Model\Enterprise;

use Sdk\Common\Model\IApplyAble;

class MockFactory
{
    /**
     * [generateAppointmentArray 生成贷款产品信息数组]
     * @return [array] [贷款产品数组]
     */
    public static function generateAppointmentArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $appointment = array();

        $appointment = array(
            'data'=>array(
                'type'=>'appointment',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();


        // contactsName
        $attributes['contactsName'] = self::generateContactsName($faker, $value);
        // contactsCellphone
        $attributes['contactsCellphone'] = self::generateContactsCellphone($faker, $value);
        // contactsArea
        $attributes['contactsArea'] = self::generateContactsArea($faker, $value);
        // contactsAddress
        $attributes['contactsAddress'] = self::generateContactsAddress($faker, $value);
        // loanAmount
        $loanAmount = self::generateLoanAmount($faker, $value);
        $attributes['loanAmount'] = $loanAmount;
        // loanTerm
        $loanTerm = self::generateLoanTerm($faker, $value);
        $attributes['loanTerm'] = $loanTerm;
        // loanObject
        $loanObject = self::generateLoanObject($faker, $value);
        $attributes['loanObject'] = $loanObject;
        // attachments
        $attachments = self::generateAttachments($faker, $value);
        $attributes['attachments'] = $attachments;
        // remark
        $remark = self::generateRemark($faker, $value);
        $attributes['remark'] = $remark;
        // loanStatus
        $loanStatus = self::generateLoanStatus($faker, $value);
        $attributes['loanStatus'] = $loanStatus;
        // loanFailReason
        $loanFailReason = self::generateLoanFailReason($faker, $value);
        $attributes['loanFailReason'] = $loanFailReason;
        // loanContractNumber
        $loanContractNumber = self::generateLoanContractNumber($faker, $value);
        $attributes['loanContractNumber'] = $loanContractNumber;
        // rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $attributes['rejectReason'] = $rejectReason;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $appointment['data']['attributes'] = $attributes;

        //member
        $appointment['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );

        //snapshot
        $appointment['data']['relationships']['snapshot']['data'] = array(
            'type' => 'snapshots',
            'id' => $faker->randomNumber(1)
        );

        return $appointment;
    }

    /**
     * [generateAppointmentObject 生成贷款产品对象]
     * @param  int|integer $id    [贷款产品Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [贷款产品对象]
     */
    public static function generateAppointmentObject(int $id = 0, int $seed = 0, array $value = array()) : Appointment
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $appointment = new Appointment($id);

        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $appointment->setSellerEnterprise($enterprise);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $appointment->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $appointment->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $appointment->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $appointment->setStatus($status);

        return $appointment;
    }

    private static function generateApplyStatus($faker, array $value = array())
    {
        return $applyStatus = isset($value['applyStatus']) ?
            $value['applyStatus'] : 0;
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        unset($faker);

        return $enterprise = isset($value['sellerEnterprise']) ?
            $value['sellerEnterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise(),
                0,
                0
            );
    }

    private static function generateNumber($faker, array $value = array())
    {
        return isset($value['number']) ?
        $value['number'] : $faker->creditCardNumber;
    }

    private static function generateLoanProductTitle($faker, array $value = array())
    {
        return isset($value['loanProductTitle']) ?
        $value['loanProductTitle'] : $faker->word();
    }
    private static function generateEnterpriseName($faker, array $value = array())
    {
        return isset($value['enterpriseName']) ?
        $value['enterpriseName'] : $faker->company();
    }

    private static function generateLoanObject($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanObject']) ?
        $value['loanObject'] : 1;
    }

    private static function generateLoanTerm($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanTerm']) ?
        $value['loanTerm'] : 1;
    }

    private static function generateLoanTermUnit($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanTermUnit']) ?
        $value['loanTermUnit'] : 1;
    }

    private static function generateAttachments($faker, array $value = array())
    {
        return isset($value['attachments']) ?
        $value['attachments'] : array("name"=>$faker->word, "identify"=>"3.pdf");
    }

    private static function generateLoanStatus($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanStatus']) ?
        $value['loanStatus'] : 2;
    }

    private static function generateLoanContractNumber($faker, array $value = array())
    {
        return isset($value['loanContractNumber']) ?
        $value['loanContractNumber'] : $faker->creditCardNumber();
    }

    private static function generateLoanFailReason($faker, array $value = array())
    {
        return isset($value['loanFailReason']) ?
        $value['loanFailReason'] : $faker->word();
    }

    private static function generateContactsArea($faker, array $value = array())
    {
        return isset($value['contactsArea']) ?
            $value['contactsArea'] : $faker->area();
    }

    private static function generateContactsName($faker, array $value = array())
    {
        return isset($value['contactsName']) ?
            $value['contactsName'] : $faker->name();
    }

    private static function generateContactsAddress($faker, array $value = array())
    {
        return isset($value['contactsAddress']) ?
            $value['contactsAddress'] : $faker->address();
    }

    private static function generateRemark($faker, array $value = array())
    {
        return isset($value['remark']) ?
        $value['remark'] : $faker->word();
    }

    private static function generateLoanAmount($faker, array $value = array())
    {
        return isset($value['loanAmount']) ?
            $value['loanAmount'] : $faker->randomDigit();
    }

    private static function generateRejectReason($faker, array $value = array())
    {
        return isset($value['rejectReason']) ?
        $value['rejectReason'] : $faker->word();
    }

    private static function generateContactsCellphone($faker, array $value = array())
    {
        return isset($value['contactsCellphone']) ?
            $value['contactsCellphone'] : $faker->phoneNumber();
    }

    private static function generateLoanProduct($faker, array $value = array())
    {
        return isset($value['loanProduct']) ?
            $value['loanProduct'] : \LoanProduct\Utils\MockFactory::generateLoanProductObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateMember($faker, array $value = array())
    {
        unset($faker);

        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                new Member(),
                0,
                0
            );
    }
}
