<?php
namespace Sdk\Appointment\Repository;

use Sdk\Appointment\Adapter\Appointment\AppointmentRestfulAdapter;
use Sdk\Appointment\Adapter\Appointment\IAppointmentAdapter;
use Sdk\Appointment\Utils\MockFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class AppointmentRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AppointmentRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends AppointmentRepository {
            public function getAdapter() : AppointmentRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : IAppointmentAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Adapter\Appointment\IAppointmentAdapter',
            $this->childStub->getMockAdapter()
        );
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Adapter\Appointment\AppointmentRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    /**
     * 为AppointmentRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以AppointmentRepository::PORTAL_LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(AppointmentRestfulAdapter::class);
        $adapter->scenario(Argument::exact(AppointmentRepository::PORTAL_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(AppointmentRepository::PORTAL_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 获取mock数据
     * 为AppointmentRestfulAdapter建立预言
     * 建立预期状况：completed() 方法将会被调用一次，并以$merchantCoupon为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行completed
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testCompleted()
    {
        $appointment = MockFactory::generateAppointmentObject(1);
        $adapter = $this->prophesize(AppointmentRestfulAdapter::class);
        $adapter->completed(Argument::exact($appointment))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->completed($appointment);
        $this->assertTrue($result);
    }
}
