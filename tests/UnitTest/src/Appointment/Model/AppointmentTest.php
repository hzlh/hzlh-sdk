<?php
namespace Sdk\Appointment\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Appointment\Repository\AppointmentRepository;

class AppointmentTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Appointment::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Appointment{
            public function getRepository() : AppointmentRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Repository\AppointmentRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Appointment setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //loanObject 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setLoanObject() 是否符合预定范围
     * @dataProvider loanObjectProvider
     */
    public function testSetLoanObject($actual, $expected)
    {
        $this->stub->setLoanObject($actual);
        $this->assertEquals($expected, $this->stub->getLoanObject());
    }
    /**
     * 循环测试  setLoanObject() 数据构建器
     */
    public function loanObjectProvider()
    {
        return array(
            array(Appointment::LOAN_OBJECT['ENTERPRISE'], Appointment::LOAN_OBJECT['ENTERPRISE']),
            array(Appointment::LOAN_OBJECT['NATURAL_PERSON'], Appointment::LOAN_OBJECT['NATURAL_PERSON'])
        );
    }
    /**
     * 设置 Appointment setLoanObject() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetLoanObjectWrongType()
    {
        $this->stub->setLoanObject('string');
    }
    //loanObject 测试 ------------------------------------------------------   end

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }
    /**
     * 设置 Appointment setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(3));
    }
    //number 测试 ----------------------------------------------------------   end

    //loanTermUnit 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setTypeMode() 是否符合预定范围
     * @dataProvider loanTermUnitProvider
     */
    public function testSetLoanTermUnit($actual, $expected)
    {
        $this->stub->setLoanTermUnit($actual);
        $this->assertEquals($expected, $this->stub->getLoanTermUnit());
    }
    /**
     * 循环测试 DispatchDepartment setReceivingMode() 数据构建器
     */
    public function loanTermUnitProvider()
    {
        return array(
            array(Appointment::LOAN_TERM_UNIT['MONTH'], Appointment::LOAN_TERM_UNIT['MONTH']),
            array(Appointment::LOAN_TERM_UNIT['DAY'], Appointment::LOAN_TERM_UNIT['DAY'])
        );
    }
    /**
     * 设置 Appointment setCardType() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetLoanTermUnitWrongType()
    {
        $this->stub->setLoanTermUnit('string');
    }
    //loanTermUnit 测试 ------------------------------------------------------   end

     //loanProductTitle 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanProductTitle() 正确的传参类型,期望传值正确
     */
    public function testSetLoanProductTitleCorrectType()
    {
        $this->stub->setLoanProductTitle('string');
        $this->assertEquals('string', $this->stub->getLoanProductTitle());
    }

    /**
     * 设置 Appointment setLoanProductTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanProductTitleWrongType()
    {
        $this->stub->setLoanProductTitle(array(1, 2, 3));
    }
    //loanProductTitle 测试 ----------------------------------------------------------   end
    
    //loanContractNumber 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanContractNumber() 正确的传参类型,期望传值正确
     */
    public function testSetLoanContractNumberCorrectType()
    {
        $this->stub->setLoanContractNumber('string');
        $this->assertEquals('string', $this->stub->getLoanContractNumber());
    }

    /**
     * 设置 Service setLoanContractNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanContractNumberWrongType()
    {
        $this->stub->setLoanContractNumber(array(1, 2, 3));
    }
    //loanContractNumber 测试 ----------------------------------------------------------   end

    //enterpriseName 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseNameCorrectType()
    {
        $this->stub->setEnterpriseName('string');
        $this->assertEquals('string', $this->stub->getEnterpriseName());
    }

    /**
     * 设置 Service setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseNameWrongType()
    {
        $this->stub->setEnterpriseName(array(1, 2, 3));
    }
    //enterpriseName 测试 ----------------------------------------------------------   end

    //loanAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanAmount() 正确的传参类型,期望传值正确
     */
    public function testSetLoanAmountCorrectType()
    {
        $this->stub->setLoanAmount('0.00');
        $this->assertEquals('0.00', $this->stub->getLoanAmount());
    }

    /**
     * 设置 Service setLoanAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanAmountWrongType()
    {
        $this->stub->setLoanAmount(array(1, 2, 3));
    }
    //loanAmount 测试 ----------------------------------------------------------   end

    //loanTerm 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanTerm() 正确的传参类型,期望传值正确
     */
    public function testSetLoanTermCorrectType()
    {
        $this->stub->setLoanTerm(1);
        $this->assertEquals(1, $this->stub->getLoanTerm());
    }

    /**
     * 设置 Service setLoanTerm() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanTermWrongType()
    {
        $this->stub->setLoanTerm('string');
    }
    //loanTerm 测试 ----------------------------------------------------------   end

    //remark 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('string');
        $this->assertEquals('string', $this->stub->getRemark());
    }

    /**
     * 设置 Service setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array(1));
    }
    //remark 测试 ----------------------------------------------------------   end

    //loanFailReason 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanFailReason() 正确的传参类型,期望传值正确
     */
    public function testSetLoanFailReasonCorrectType()
    {
        $this->stub->setLoanFailReason('string');
        $this->assertEquals('string', $this->stub->getLoanFailReason());
    }

    /**
     * 设置 Service setLoanFailReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanFailReasonWrongType()
    {
        $this->stub->setLoanFailReason(array(1));
    }
    //loanFailReason 测试 ----------------------------------------------------------   end

    //rejectReason 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->stub->setRejectReason('string');
        $this->assertEquals('string', $this->stub->getRejectReason());
    }

    /**
     * 设置 Service setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->stub->setRejectReason(array(1));
    }
    //rejectReason 测试 ----------------------------------------------------------   end

    //loanStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setLoanStatus() 是否符合预定范围
     * @dataProvider loanStatusProvider
     */
    public function testSetLoanStatus($actual, $expected)
    {
        $this->stub->setLoanStatus($actual);
        $this->assertEquals($expected, $this->stub->getLoanStatus());
    }
    /**
     * 循环测试  setLoanStatus() 数据构建器
     */
    public function loanStatusProvider()
    {
        return array(
            array(Appointment::LOAN_STATUS['PENDING'], Appointment::LOAN_STATUS['PENDING']),
            array(Appointment::LOAN_STATUS['SUCCESS'], Appointment::LOAN_STATUS['SUCCESS']),
            array(Appointment::LOAN_STATUS['FAIL'], Appointment::LOAN_STATUS['FAIL'])
        );
    }
    /**
     * 设置 Appointment setLoanStatus() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetLoanStatusWrongType()
    {
        $this->stub->setLoanStatus('string');
    }
    //loanStatus 测试 ------------------------------------------------------   end
}
