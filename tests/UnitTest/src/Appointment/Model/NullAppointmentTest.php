<?php
namespace Sdk\Appointment\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullAppointmentTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAppointment::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAppointment()
    {
        $this->assertInstanceof('Sdk\Appointment\Model\Appointment', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testDeletes()
    {
        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
