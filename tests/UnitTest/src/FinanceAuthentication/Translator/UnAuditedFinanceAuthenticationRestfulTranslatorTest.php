<?php
namespace Sdk\FinanceAuthentication\Translator;

use Sdk\FinanceAuthentication\Model\NullUnAuditedFinanceAuthentication;
use Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Enterprise\Model\Enterprise;

class UnAuditedFinanceAuthenticationRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            UnAuditedFinanceAuthenticationRestfulTranslator::class
        )->setMethods(['getEnterpriseRestfulTranslator'])->getMock();
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new UnAuditedFinanceAuthentication());
        $this->assertInstanceOf('Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication', $result);
    }

    public function setMethods(UnAuditedFinanceAuthentication $expectObject, array $attributes, array $relationships)
    {
        $financeAuthentication = new FinanceAuthenticationRestfulTranslatorTest();

        $expectObject = $financeAuthentication->setMethods($expectObject, $attributes, $relationships);

        if (isset($attributes['rejectReason'])) {
            $expectObject->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['applyStatus'])) {
            $expectObject->setApplyStatus($attributes['applyStatus']);
        }
        
        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $unAuditedFinanceAuthentication =
        \Sdk\FinanceAuthentication\Utils\UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationArray(); //phpcs:ignore

        $data =  $unAuditedFinanceAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($unAuditedFinanceAuthentication);

        $expectObject = new UnAuditedFinanceAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $unAuditedFinanceAuthentication =
        \Sdk\FinanceAuthentication\Utils\UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationArray(); //phpcs:ignore

        $data =  $unAuditedFinanceAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($unAuditedFinanceAuthentication);

        $expectArray = array();

        $expectObject = new UnAuditedFinanceAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $unAuditedFinanceAuthentication =
        \Sdk\FinanceAuthentication\Utils\UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationObject(1); //phpcs:ignore

        $actual = $this->stub->objectToArray(
            $unAuditedFinanceAuthentication,
            array(
                'rejectReason'
            )
        );

        $expectedArray = array(
            'data'=>array(
                'type'=>'unAuditedFinanceAuthentications'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'rejectReason' => $unAuditedFinanceAuthentication->getRejectReason()
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
