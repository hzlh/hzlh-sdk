<?php
namespace Sdk\FinanceAuthentication\Utils;

use Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication;

use Sdk\Common\Model\IApplyAble;

class UnAuditedFinanceAuthenticationMockFactory
{
    /**
     * [generateFinanceAuthenticationArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateUnAuditedFinanceAuthenticationArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $unAuditedFinanceAuthentication = array();

        $unAuditedFinanceAuthentication = array(
            'data'=>array(
                'type'=>'unAuditedFinanceAuthentications',
                'id'=>$faker->randomNumber(2)
            )
        );
        
        $value = array();
        $attributes = array();

        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $attributes['rejectReason'] = $rejectReason;

        //applyStatus
        $applyStatus = self::generateApplyStatus($faker, $value);
        $attributes['applyStatus'] = $applyStatus;

        $FinanceAuthentication = \Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory::generateFinanceAuthenticationArray(); //phpcs:ignore

        $unAuditedFinanceAuthentication['data']['attributes'] = array_merge(
            $FinanceAuthentication['data']['attributes'],
            $attributes
        );

        $unAuditedFinanceAuthentication['data']['relationships'] = $FinanceAuthentication['data']['relationships'];
        
        return $unAuditedFinanceAuthentication;
    }

    /**
     * [generateFinanceAuthenticationObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateUnAuditedFinanceAuthenticationObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedFinanceAuthentication {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedFinanceAuthentication = new UnAuditedFinanceAuthentication($id);

        $unAuditedFinanceAuthentication =
            \Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject($unAuditedFinanceAuthentication); //phpcs:ignore

        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $unAuditedFinanceAuthentication->setRejectReason($rejectReason);

        //applyStatus
        $applyStatus = self::generateApplyStatus($faker, $value);
        $unAuditedFinanceAuthentication->setApplyStatus($applyStatus);

        return $unAuditedFinanceAuthentication;
    }

    private static function generateRejectReason($faker, array $value = array())
    {
        return $rejectReason = isset($value['rejectReason']) ?
        $value['rejectReason'] : $faker->word;
    }

    public static function generateApplyStatus($faker, array $value = array())
    {
        return $applyStatus = isset($value['applyStatus']) ?
        $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
    }
}
