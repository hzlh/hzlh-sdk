<?php
namespace Sdk\FinanceAuthentication\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullUnAuditedFinanceAuthenticationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullUnAuditedFinanceAuthentication::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsUnAuditedFinanceAuthentication()
    {
        $this->assertInstanceof('Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
