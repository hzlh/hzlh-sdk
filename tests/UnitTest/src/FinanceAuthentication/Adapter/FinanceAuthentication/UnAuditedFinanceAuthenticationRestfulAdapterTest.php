<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullUnAuditedFinanceAuthentication;
use Sdk\FinanceAuthentication\Utils\UnAuditedFinanceAuthenticationMockFactory;
use Sdk\FinanceAuthentication\Translator\UnAuditedFinanceAuthenticationRestfulTranslator;

class UnAuditedFinanceAuthenticationRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UnAuditedFinanceAuthenticationRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends UnAuditedFinanceAuthenticationRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIUnAuditedFinanceAuthenticationdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\IUnAuditedFinanceAuthenticationAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedFinanceAuthentications', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinanceAuthentication\Translator\UnAuditedFinanceAuthenticationRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UNAUDITED_FINANCE_AUTHENTICATION_LIST',
                UnAuditedFinanceAuthenticationRestfulAdapter::SCENARIOS['UNAUDITED_FINANCE_AUTHENTICATION_LIST']
            ],
            [
                'UNAUDITED_FINANCE_AUTHENTICATION_FETCH_ONE',
                UnAuditedFinanceAuthenticationRestfulAdapter::SCENARIOS['UNAUDITED_FINANCE_AUTHENTICATION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $unAuditedFinanceAuthentication = UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationObject($id); //phpcs:ignore

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullUnAuditedFinanceAuthentication())
            ->willReturn($unAuditedFinanceAuthentication);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($unAuditedFinanceAuthentication, $result);
    }

    /**
     * 为UnAuditedFinanceAuthenticationRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$UnAuditedFinanceAuthentication，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditedFinanceAuthenticationTranslator(
        UnAuditedFinanceAuthentication $unAuditedFinanceAuthentication,
        array $keys,
        array $unAuditedFinanceAuthenticationArray
    ) {
        $translator = $this->prophesize(UnAuditedFinanceAuthenticationRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedFinanceAuthentication),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($unAuditedFinanceAuthenticationArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditedFinanceAuthentication $unAuditedFinanceAuthentication)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditedFinanceAuthentication);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditedFinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $id = 1;

        $unAuditedFinanceAuthentication = UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationObject($id); //phpcs:ignore
        $unAuditedFinanceAuthenticationArray = array();

        $this->prepareUnAuditedFinanceAuthenticationTranslator(
            $unAuditedFinanceAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            ),
            $unAuditedFinanceAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'unAuditedFinanceAuthentications/'.$unAuditedFinanceAuthentication->getId().'/resubmit',
                $unAuditedFinanceAuthenticationArray
            );

        $this->success($unAuditedFinanceAuthentication);

        $result = $this->stub->resubmit($unAuditedFinanceAuthentication);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditedFinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $id = 1;

        $unAuditedFinanceAuthentication = UnAuditedFinanceAuthenticationMockFactory::generateUnAuditedFinanceAuthenticationObject($id); //phpcs:ignore
        $unAuditedFinanceAuthenticationArray = array();

        $this->prepareUnAuditedFinanceAuthenticationTranslator(
            $unAuditedFinanceAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            ),
            $unAuditedFinanceAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'unAuditedFinanceAuthentications/'.$unAuditedFinanceAuthentication->getId().'/resubmit',
                $unAuditedFinanceAuthenticationArray
            );

        $this->failure($unAuditedFinanceAuthentication);
        $result = $this->stub->resubmit($unAuditedFinanceAuthentication);
        $this->assertFalse($result);
    }
}
