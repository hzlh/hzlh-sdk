<?php
namespace Sdk\ProfessionalTitle\Utils;

use Sdk\ProfessionalTitle\Model\ProfessionalTitle;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateProfessionalTitleArray 生成职称信息数组]
     * @return [array] [职称数组]
     */
    public static function generateProfessionalTitleArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $professionalTitle = array();

        $professionalTitle = array(
            'data'=>array(
                'type'=>'professionalTitles',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //certificates
        $certificates = self::generateCertificates($faker, $value);
        $attributes['certificates'] = $certificates;
        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $attributes['enterpriseName'] = $enterpriseName;
        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $attributes['rejectReason'] = $rejectReason;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $professionalTitle['data']['attributes'] = $attributes;

        //parentCategory
        $professionalTitle['data']['relationships']['parentCategory']['data'] = array(
            'type' => 'parentCategories',
            'id' => $faker->randomNumber(1)
        );
        //category
        $professionalTitle['data']['relationships']['category']['data'] = array(
            'type' => 'categories',
            'id' => $faker->randomNumber(1)
        );
        //type
        $professionalTitle['data']['relationships']['type']['data'] = array(
            'type' => 'types',
            'id' => $faker->randomNumber(1)
        );
        //staff
        $professionalTitle['data']['relationships']['staff']['data'] = array(
            'type' => 'staffs',
            'id' => $faker->randomNumber(1)
        );

        return $professionalTitle;
    }

    /**
     * [generateProfessionalTitleObject 生成职称对象]
     * @param  int|integer $id    [职称Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [职称对象]
     */
    public static function generateProfessionalTitleObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ProfessionalTitle {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $professionalTitle = new ProfessionalTitle($id);

        //certificates
        $certificates = self::generateCertificates($faker, $value);
        $professionalTitle->setCertificates($certificates);
        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $professionalTitle->setEnterpriseName($enterpriseName);
        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $professionalTitle->setRejectReason($rejectReason);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $professionalTitle->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $professionalTitle->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $professionalTitle->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $professionalTitle->setStatus($status);
        //parentCategory
        $parentCategory = self::generateParentCategory($faker, $value);
        $professionalTitle->setParentCategory($parentCategory);
        //category
        $category = self::generateCategory($faker, $value);
        $professionalTitle->setCategory($category);
        //type
        $type = self::generateType($faker, $value);
        $professionalTitle->setType($type);
        //staff
        $staff = self::generateStaff($faker, $value);
        $professionalTitle->setStaff($staff);

        return $professionalTitle;
    }

    private static function generateCertificates($faker, array $value = array())
    {
        return isset($value['certificates'])
        ? $value['certificates']
        : array($faker->name());
    }

    private static function generateEnterpriseName($faker, array $value = array())
    {
        return isset($value['enterpriseName'])
        ? $value['enterpriseName']
        : $faker->name();
    }

    private static function generateRejectReason($faker, array $value = array())
    {
        return isset($value['rejectReason'])
        ? $value['rejectReason']
        : $faker->word;
    }

    private static function generateParentCategory($faker, array $value = array())
    {
        return isset($value['parentCategory']) ?
            $value['parentCategory'] :
            \Sdk\Label\Utils\MockFactory::generateLabelObject($faker->numerify(), $faker->numerify());
    }

    private static function generateCategory($faker, array $value = array())
    {
        return isset($value['category']) ?
            $value['category'] :
            \Sdk\Label\Utils\MockFactory::generateLabelObject($faker->numerify(), $faker->numerify());
    }

    private static function generateType($faker, array $value = array())
    {
        return isset($value['type']) ?
            $value['type'] :
            \Sdk\Label\Utils\MockFactory::generateLabelObject($faker->numerify(), $faker->numerify());
    }

    private static function generateStaff($faker, array $value = array())
    {
        return isset($value['staff']) ?
            $value['staff'] :
            \Sdk\Staff\Utils\MockFactory::generateStaffObject($faker->numerify(), $faker->numerify());
    }
}
