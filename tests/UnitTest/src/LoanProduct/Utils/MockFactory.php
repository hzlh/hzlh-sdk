<?php
namespace Sdk\LoanProduct\Utils;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\Enterprise\Model\Enterprise;

use Sdk\Label\Utils\MockFactory as LabelFactory;

use Sdk\Common\Model\IApplyAble;

use Sdk\LoanProduct\Model\LoanProductCategory;

class MockFactory
{
    /**
     * [generateLoanProductArray 生成贷款产品信息数组]
     * @return [array] [贷款产品数组]
     */
    public static function generateLoanProductArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $loanProduct = array();

        $loanProduct = array(
            'data'=>array(
                'type'=>'loanProduct',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //productObject
        $productObject = self::generateProductObject($faker, $value);
        $attributes['productObject'] = $productObject;
        //guarantyStyles
        $guarantyStyles = self::generateGuarantyStyles($faker, $value);
        $attributes['guarantyStyles'] = $guarantyStyles;
        //labels
        $labels = [
            [
                'id' => 1,
                'name' => '额度高'
            ],
            [
                'id' => 2,
                'name' => ' 放款快'
            ],
        ];
        $attributes['labels'] = $labels;
        //minLoanPeriod
        $minLoanPeriod = self::generateMinLoanPeriod($faker, $value);
        $attributes['minLoanPeriod'] = $minLoanPeriod;
        //maxLoanPeriod
        $maxLoanPeriod = self::generateMaxLoanPeriod($faker, $value);
        $attributes['maxLoanPeriod'] = $maxLoanPeriod;
        //supportCity
        $supportCity = self::generateSupportCity($faker, $value);
        $attributes['supportCity'] = $supportCity;
        //minLoanAmount
        $minLoanAmount = self::generateMinLoanAmount($faker, $value);
        $attributes['minLoanAmount'] = $minLoanAmount;
        //maxLoanAmount
        $maxLoanAmount = self::generateMaxLoanAmount($faker, $value);
        $attributes['maxLoanAmount'] = $maxLoanAmount;
        //minLoanTerm
        $minLoanTerm = self::generateMinLoanTerm($faker, $value);
        $attributes['minLoanTerm'] = $minLoanTerm;
        //maxLoanTerm
        $maxLoanTerm = self::generateMaxLoanTerm($faker, $value);
        $attributes['maxLoanTerm'] = $maxLoanTerm;
        //loanInterestRate
        $loanInterestRate = self::generateLoanInterestRate($faker, $value);
        $attributes['loanInterestRate'] = $loanInterestRate;
        //repaymentMethods
        $repaymentMethods = self::generateRepaymentMethods($faker, $value);
        $attributes['repaymentMethods'] = $repaymentMethods;
        //earlyRepaymentTerm
        $earlyRepaymentTerm = self::generateEarlyRepaymentTerm($faker, $value);
        $attributes['earlyRepaymentTerm'] = $earlyRepaymentTerm;
        //applicationMaterial
        $applicationMaterial = self::generateApplicationMaterial($faker, $value);
        $attributes['applicationMaterial'] = $applicationMaterial;
        //applicationCondition
        $applicationCondition = self::generateApplicationCondition($faker, $value);
        $attributes['applicationCondition'] = $applicationCondition;
        //contract
        $contract = self::generateContract($faker, $value);
        $attributes['contract'] = $contract;
        //application
        $application = self::generateApplication($faker, $value);
        $attributes['application'] = $application;
        //applyStatus
        $applyStatus = self::generateApplyStatus($faker, $value);
        $attributes['applyStatus'] = $applyStatus;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $loanProduct['data']['attributes'] = $attributes;

        //enterprise
        $loanProduct['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprises',
            'id' => $faker->randomNumber(1)
        );

        return $loanProduct;
    }

    /**
     * [generateLoanProductObject 生成贷款产品对象]
     * @param  int|integer $id    [贷款产品Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [贷款产品对象]
     */
    public static function generateLoanProductObject(int $id = 0, int $seed = 0, array $value = array()) : LoanProduct
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $loanProduct = new LoanProduct($id);

        //title
        $title = self::generateTitle($faker, $value);
        $loanProduct->setTitle($title);
        //number
        $number = self::generateNumber($faker, $value);
        $loanProduct->setNumber($number);
        //cover
        $cover = self::generateCover($faker, $value);
        $loanProduct->setCover($cover);
        //labels
        // $labels = self::generateLabels($faker, $value);
        // $loanProduct->addLabel($labels);
        //minLoanPeriod
        $minLoanPeriod = self::generateMinLoanPeriod($faker, $value);
        $loanProduct->setMinLoanPeriod($minLoanPeriod);
        //maxLoanPeriod
        $maxLoanPeriod = self::generateMaxLoanPeriod($faker, $value);
        $loanProduct->setMaxLoanPeriod($maxLoanPeriod);
        //supportCity
        $supportCity = self::generateSupportCity($faker, $value);
        $loanProduct->setSupportCity($supportCity);
        //minLoanAmount
        $minLoanAmount = self::generateMinLoanAmount($faker, $value);
        $loanProduct->setMinLoanAmount($minLoanAmount);
        //maxLoanAmount
        $maxLoanAmount = self::generateMaxLoanAmount($faker, $value);
        $loanProduct->setMaxLoanAmount($maxLoanAmount);
        //minLoanTerm
        $minLoanTerm = self::generateMinLoanTerm($faker, $value);
        $loanProduct->setMinLoanTerm($minLoanTerm);
        //maxLoanTerm
        $maxLoanTerm = self::generateMaxLoanTerm($faker, $value);
        $loanProduct->setMaxLoanTerm($maxLoanTerm);
        //loanTermUnit
        $loanTermUnit = self::generateLoanTermUnit($faker, $value);
        $loanProduct->setLoanTermUnit($loanTermUnit);
        //loanInterestRate
        $loanInterestRate = self::generateLoanInterestRate($faker, $value);
        $loanProduct->setLoanInterestRate($loanInterestRate);
        //loanInterestRateUnit
        $loanInterestRateUnit = self::generateLoanInterestRateUnit($faker, $value);
        $loanProduct->setLoanInterestRateUnit($loanInterestRateUnit);
        //isSupportEarlyRepayment
        $isSupportEarlyRepayment = self::generateIsSupportEarlyRepayment($faker, $value);
        $loanProduct->setIsSupportEarlyRepayment($isSupportEarlyRepayment);
        //earlyRepaymentTerm
        $earlyRepaymentTerm = self::generateEarlyRepaymentTerm($faker, $value);
        $loanProduct->setEarlyRepaymentTerm($earlyRepaymentTerm);
        //isExistEarlyRepaymentCost
        $isExistEarlyRepaymentCost = self::generateIsExistEarlyRepaymentCost($faker, $value);
        $loanProduct->setIsExistEarlyRepaymentCost($isExistEarlyRepaymentCost);
        //applicationMaterial
        $applicationMaterial = self::generateApplicationMaterial($faker, $value);
        $loanProduct->setApplicationMaterial($applicationMaterial);
        //applicationCondition
        $applicationCondition = self::generateApplicationCondition($faker, $value);
        $loanProduct->setApplicationCondition($applicationCondition);
        //contract
        $contract = self::generateContract($faker, $value);
        $loanProduct->setContract($contract);
        //application
        $application = self::generateApplication($faker, $value);
        $loanProduct->setApplication($application);
        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $loanProduct->setEnterprise($enterprise);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $loanProduct->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $loanProduct->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $loanProduct->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $loanProduct->setStatus($status);
        //loanMonthInterestRate
        $loanMonthInterestRate = self::generateLoanMonthInterestRate($faker, $value);
        $loanProduct->setLoanMonthInterestRate($loanMonthInterestRate);
        //volume
        $volume = self::generateVolume($faker, $value);
        $loanProduct->setVolume($volume);
        //volumeSuccess
        $volumeSuccess = self::generateVolumeSuccess($faker, $value);
        $loanProduct->setVolumeSuccess($volumeSuccess);
        //attentionDegree
        $attentionDegree = self::generateAttentionDegree($faker, $value);
        $loanProduct->setAttentionDegree($attentionDegree);

        return $loanProduct;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return $title = isset($value['title']) ?
        $value['title'] : $faker->word;
    }

    private static function generateNumber($faker, array $value = array())
    {
        return $number = isset($value['number']) ?
        $value['number'] : $faker->randomNumber();
    }

    private static function generateCover($faker, array $value = array())
    {
        return $cover = isset($value['cover']) ?
        $value['cover'] : array("name"=>$faker->word, "identify"=>"3.jpg");
    }

    private static function generateProductObject($faker, array $value = array())
    {
        return $productObject = isset($value['productObject']) ?
        $value['productObject'] : array(1, 2);
    }

    private static function generateGuarantyStyles($faker, array $value = array())
    {
        unset($faker);

        return $guarantyStyles = isset($value['guarantyStyles']) ?
        $value['guarantyStyles'] : array(1, 2);
    }

    private static function generateLabels($faker, array $value = array())
    {
        unset($faker);

        return $labels = isset($value['labels']) ?
            $value['labels'] : array(\Sdk\Label\Utils\MockFactory::generateLabelObject(0, 0));
    }

    private static function generateMinLoanPeriod($faker, array $value = array())
    {
        return $minLoanPeriod = isset($value['minLoanPeriod']) ?
        $value['minLoanPeriod'] : $faker->randomDigit;
    }

    private static function generateMaxLoanPeriod($faker, array $value = array())
    {
        return $maxLoanPeriod = isset($value['maxLoanPeriod']) ?
        $value['maxLoanPeriod'] : $faker->randomDigit;
    }

    private static function generateSupportCity($faker, array $value = array())
    {
        return $supportCity = isset($value['supportCity']) ?
        $value['supportCity'] : array($faker->city(), $faker->city());
    }

    private static function generateMinLoanAmount($faker, array $value = array())
    {
        return $minLoanAmount = isset($value['minLoanAmount']) ?
        $value['minLoanAmount'] : $faker->randomDigit;
    }

    private static function generateMaxLoanAmount($faker, array $value = array())
    {
        return $maxLoanAmount = isset($value['maxLoanAmount']) ?
        $value['maxLoanAmount'] : $faker->randomDigit;
    }

    private static function generateMinLoanTerm($faker, array $value = array())
    {
        return $minLoanTerm = isset($value['minLoanTerm']) ?
        $value['minLoanTerm'] : $faker->randomDigit;
    }

    private static function generateMaxLoanTerm($faker, array $value = array())
    {
        return $maxLoanTerm = isset($value['maxLoanTerm']) ?
        $value['maxLoanTerm'] : $faker->randomDigit;
    }

    private static function generateLoanTermUnit($faker, array $value = array())
    {
        unset($faker);

        return $loanTermUnit = isset($value['loanTermUnit']) ?
        $value['loanTermUnit'] : new LoanProductCategory(1, '');
    }

    private static function generateLoanInterestRate($faker, array $value = array())
    {
        return $loanInterestRate = isset($value['loanInterestRate']) ?
        $value['loanInterestRate'] : $faker->randomDigit;
    }

    private static function generateLoanInterestRateUnit($faker, array $value = array())
    {
        unset($faker);

        return $loanInterestRateUnit = isset($value['loanInterestRateUnit']) ?
        $value['loanInterestRateUnit'] : new LoanProductCategory(1, '');
    }

    private static function generateRepaymentMethods($faker, array $value = array())
    {
        unset($faker);

        return $repaymentMethods = isset($value['repaymentMethods']) ?
        $value['repaymentMethods'] : array(1, 2);
    }

    private static function generateIsSupportEarlyRepayment($faker, array $value = array())
    {
        unset($faker);

        return $isSupportEarlyRepayment = isset($value['isSupportEarlyRepayment']) ?
        $value['isSupportEarlyRepayment'] : new LoanProductCategory(1, '');
    }

    private static function generateEarlyRepaymentTerm($faker, array $value = array())
    {
        return $earlyRepaymentTerm = isset($value['earlyRepaymentTerm']) ?
        $value['earlyRepaymentTerm'] : $faker->randomDigit;
    }

    private static function generateIsExistEarlyRepaymentCost($faker, array $value = array())
    {
        unset($faker);

        return $isExistEarlyRepaymentCost = isset($value['isExistEarlyRepaymentCost']) ?
        $value['isExistEarlyRepaymentCost'] : new LoanProductCategory(1, '');
    }

    private static function generateApplicationMaterial($faker, array $value = array())
    {
        return $applicationMaterial = isset($value['applicationMaterial']) ?
            $value['applicationMaterial'] : $faker->text;
    }

    private static function generateApplicationCondition($faker, array $value = array())
    {
        return $applicationCondition = isset($value['applicationCondition']) ?
            $value['applicationCondition'] : $faker->text;
    }

    private static function generateContract($faker, array $value = array())
    {
        return $contract = isset($value['contract']) ?
        $value['contract'] : array("name"=>"维修洗衣机服务合同", "identify"=>"3.pdf");
    }

    private static function generateApplication($faker, array $value = array())
    {
        return $application = isset($value['application']) ?
        $value['application'] : array('name' =>'application', 'identify' =>'application.pdf');
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        unset($faker);

        return $enterprise = isset($value['enterprise']) ?
            $value['enterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise(),
                0,
                0
            );
    }

    private static function generateLoanMonthInterestRate($faker, array $value = array())
    {
        return $loanMonthInterestRate = isset($value['loanMonthInterestRate']) ?
        $value['loanMonthInterestRate'] : $faker->randomDigit;
    }

    private static function generateVolume($faker, array $value = array())
    {
        return $volume = isset($value['volume']) ?
        $value['volume'] : $faker->randomDigit;
    }

    private static function generateVolumeSuccess($faker, array $value = array())
    {
        return $volumeSuccess = isset($value['volumeSuccess']) ?
        $value['volumeSuccess'] : $faker->randomDigit;
    }

    private static function generateAttentionDegree($faker, array $value = array())
    {
        return $attentionDegree = isset($value['attentionDegree']) ?
        $value['attentionDegree'] : $faker->randomDigit;
    }

    private static function generateApplyStatus($faker, array $value = array())
    {
        return $applyStatus = isset($value['applyStatus']) ?
        $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
    }
}
