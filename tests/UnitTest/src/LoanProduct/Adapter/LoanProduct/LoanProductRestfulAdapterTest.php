<?php
namespace Sdk\LoanProduct\Adapter\LoanProduct;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\LoanProduct\Model\NullLoanProduct;
use Sdk\LoanProduct\Utils\MockFactory;
use Sdk\LoanProduct\Translator\LoanProductRestfulTranslator;

class LoanProductRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProductRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends LoanProductRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsILoanProductAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProduct\Adapter\LoanProduct\ILoanProductAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('loanProducts', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProduct\Translator\LoanProductRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_LOAN_PRODUCT_LIST',
                LoanProductRestfulAdapter::SCENARIOS['OA_LOAN_PRODUCT_LIST']
            ],
            [
                'PORTAL_LOAN_PRODUCT_LIST',
                LoanProductRestfulAdapter::SCENARIOS['PORTAL_LOAN_PRODUCT_LIST']
            ],
            [
                'LOAN_PRODUCT_FETCH_ONE',
                LoanProductRestfulAdapter::SCENARIOS['LOAN_PRODUCT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $loanProduct = MockFactory::generateLoanProductObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullLoanProduct())
            ->willReturn($loanProduct);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($loanProduct, $result);
    }
    /**
     * 为LoanProductRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$LoanProduct$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareLoanProductTranslator(
        LoanProduct $loanProduct,
        array $keys,
        array $loanProductArray
    ) {
        $translator = $this->prophesize(LoanProductRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($loanProduct),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($loanProductArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(LoanProduct $loanProduct)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($loanProduct);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('loanProducts', $loanProductArray);

        $this->success($loanProduct);

        $result = $this->stub->add($loanProduct);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('loanProducts', $loanProductArray);

        $this->failure($loanProduct);
        $result = $this->stub->add($loanProduct);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(), $loanProductArray);

        $this->success($loanProduct);

        $result = $this->stub->edit($loanProduct);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(), $loanProductArray);

        $this->failure($loanProduct);
        $result = $this->stub->edit($loanProduct);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行resubmit（）
     * 判断 result 是否为true
     */
    public function testResubmitSuccess()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/resubmit', $loanProductArray);

        $this->success($loanProduct);

        $result = $this->stub->resubmit($loanProduct);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行resubmit（）
     * 判断 result 是否为false
     */
    public function testResubmitFailure()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);
        $loanProductArray = array();

        $this->prepareLoanProductTranslator(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application'
            ),
            $loanProductArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/resubmit', $loanProductArray);

        $this->failure($loanProduct);
        $result = $this->stub->resubmit($loanProduct);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行onShelf（）
     * 判断 result 是否为true
     */
    public function testOnShelfSuccess()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/onShelf');

        $this->success($loanProduct);

        $result = $this->stub->onShelf($loanProduct);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行onShelf（）
     * 判断 result 是否为false
     */
    public function testOnShelfFailure()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/onShelf');

        $this->failure($loanProduct);
        $result = $this->stub->onShelf($loanProduct);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行offStock（）
     * 判断 result 是否为true
     */
    public function testOffStockSuccess()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/offStock');

        $this->success($loanProduct);

        $result = $this->stub->offStock($loanProduct);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行offStock（）
     * 判断 result 是否为false
     */
    public function testOffStockFailure()
    {
        $loanProduct = MockFactory::generateLoanProductObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProducts/' . $loanProduct->getId(). '/offStock');

        $this->failure($loanProduct);
        $result = $this->stub->offStock($loanProduct);
        $this->assertFalse($result);
    }
}
