<?php
namespace Sdk\LoanProduct\Model;

use Sdk\LoanProduct\Repository\LoanProductRepository;
use Sdk\LoanProduct\Repository\LoanProductSessionRepository;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IModifyStatusAbleAdapter;

use Sdk\LoanProduct\Model\LoanProductCategory;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Sdk\Member\Model\Member;
use Sdk\Snapshot\Model\Snapshot;
use Sdk\Enterprise\Model\Enterprise;

class LoanProductTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProduct::class)
            ->setMethods(
                [
                'getRepository',
                'IOperatAbleAdapter',
                'IModifyStatusAbleAdapter',
                'IApplyAbleAdapter',
                'IResubmitAbleAdapter'
                ]
            )
            ->getMock();

        $this->childStub = new class extends LoanProduct {
            public function getRepository() : LoanProductRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
            {
                return parent::getIModifyStatusAbleAdapter();
            }
            public function getIApplyAbleAdapter(): IApplyAbleAdapter
            {
                return parent::getIApplyAbleAdapter();
            }

            public function getIResubmitAbleAdapter(): IResubmitAbleAdapter
            {
                return parent::getIResubmitAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProduct\Repository\LoanProductRepository',
            $this->childStub->getRepository()
        );
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testCorrectImplementsIOperatAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IOperatAble', $this->stub);
    }

    public function testCorrectImplementsIModifyStatusAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IModifyStatusAble', $this->stub);
    }

    public function testGetIOperatAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IModifyStatusAbleAdapter',
            $this->childStub->getIModifyStatusAbleAdapter()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IApplyAbleAdapter',
            $this->childStub->getIApplyAbleAdapter()
        );
    }

    public function testGetIResubmitAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IResubmitAbleAdapter',
            $this->childStub->getIResubmitAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 LoanProduct setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 LoanProduct setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('title');
        $this->assertEquals('title', $this->stub->getTitle());
    }

    /**
     * 设置 LoanProduct setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array());
    }
    //title 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 LoanProduct setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array());
    }
    //number 测试 --------------------------------------------------------   end

    //cover 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $this->stub->setCover(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getCover());
    }

    /**
     * 设置 LoanProduct setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->stub->setCover('cover');
    }
    //cover 测试 --------------------------------------------------------   end

    //productObject 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setProductObject() 正确的传参类型,期望传值正确
     */
    public function testSetProductObjectCorrectType()
    {
        $this->stub->setProductObject(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getProductObject());
    }

    /**
     * 设置 LoanProduct setProductObject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetProductObjectWrongType()
    {
        $this->stub->setProductObject('productObject');
    }
    //productObject 测试 --------------------------------------------------------   end

    //guarantyStyles 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setGuarantyStyles() 正确的传参类型,期望传值正确
     */
    public function testSetGuarantyStylesCorrectType()
    {
        $this->stub->setGuarantyStyles(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getGuarantyStyles());
    }

    /**
     * 设置 LoanProduct setGuarantyStyles() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGuarantyStylesWrongType()
    {
        $this->stub->setGuarantyStyles('guarantyStyles');
    }
    //guarantyStyles 测试 --------------------------------------------------------   end

    //minLoanPeriod 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMinLoanPeriod() 正确的传参类型,期望传值正确
     */
    public function testSetMinLoanPeriodCorrectType()
    {
        $this->stub->setMinLoanPeriod(1);
        $this->assertEquals(1, $this->stub->getMinLoanPeriod());
    }

    /**
     * 设置 LoanProduct setMinLoanPeriod() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMinLoanPeriodWrongType()
    {
        $this->stub->setMinLoanPeriod('minLoanPeriod');
    }
    //minLoanPeriod 测试 --------------------------------------------------------   end

    //maxLoanPeriod 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMaxLoanPeriod() 正确的传参类型,期望传值正确
     */
    public function testSetMaxLoanPeriodCorrectType()
    {
        $this->stub->setMaxLoanPeriod(1);
        $this->assertEquals(1, $this->stub->getMaxLoanPeriod());
    }

    /**
     * 设置 LoanProduct setMaxLoanPeriod() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMaxLoanPeriodWrongType()
    {
        $this->stub->setMaxLoanPeriod('maxLoanPeriod');
    }
    //maxLoanPeriod 测试 --------------------------------------------------------   end

    //supportCity 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setSupportCity() 正确的传参类型,期望传值正确
     */
    public function testSetSupportCityCorrectType()
    {
        $this->stub->setSupportCity(array('陕西','西安','商洛'));
        $this->assertEquals(array('陕西','西安','商洛'), $this->stub->getSupportCity());
    }

    /**
     * 设置 LoanProduct setSupportCity() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSupportCityWrongType()
    {
        $this->stub->setSupportCity('supportCity');
    }
    //supportCity 测试 --------------------------------------------------------   end

    //minLoanAmount 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMinLoanAmount() 正确的传参类型,期望传值正确
     */
    public function testSetMinLoanAmountCorrectType()
    {
        $this->stub->setMinLoanAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getMinLoanAmount());
    }

    /**
     * 设置 LoanProduct setMinLoanAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMinLoanAmountWrongType()
    {
        $this->stub->setMinLoanAmount('minLoanAmount');
    }
    //minLoanAmount 测试 --------------------------------------------------------   end

    //maxLoanAmount 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMaxLoanAmount() 正确的传参类型,期望传值正确
     */
    public function testSetMaxLoanAmountCorrectType()
    {
        $this->stub->setMaxLoanAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getMaxLoanAmount());
    }

    /**
     * 设置 LoanProduct setMaxLoanAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMaxLoanAmountWrongType()
    {
        $this->stub->setMaxLoanAmount('maxLoanAmount');
    }
    //maxLoanAmount 测试 --------------------------------------------------------   end

    //minLoanTerm 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMinLoanTerm() 正确的传参类型,期望传值正确
     */
    public function testSetMinLoanTermCorrectType()
    {
        $this->stub->setMinLoanTerm(1);
        $this->assertEquals(1, $this->stub->getMinLoanTerm());
    }

    /**
     * 设置 LoanProduct setMinLoanTerm() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMinLoanTermWrongType()
    {
        $this->stub->setMinLoanTerm('minLoanTerm');
    }
    //minLoanTerm 测试 --------------------------------------------------------   end

    //maxLoanTerm 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setMaxLoanTerm() 正确的传参类型,期望传值正确
     */
    public function testSetMaxLoanTermCorrectType()
    {
        $this->stub->setMaxLoanTerm(1);
        $this->assertEquals(1, $this->stub->getMaxLoanTerm());
    }

    /**
     * 设置 LoanProduct setMaxLoanTerm() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMaxLoanTermWrongType()
    {
        $this->stub->setMaxLoanTerm('maxLoanTerm');
    }
    //maxLoanTerm 测试 --------------------------------------------------------   end

    //loanTermUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setLoanTermUnit() 正确的传参类型,期望传值正确
     */
    public function testSetLoanTermUnitCorrectType()
    {
        $object = new LoanProductCategory(1, '');
        $this->stub->setLoanTermUnit($object);
        $this->assertEquals($object, $this->stub->getLoanTermUnit());
    }

    /**
     * 设置 LoanProduct setLoanTermUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanTermUnitWrongType()
    {
        $this->stub->setLoanTermUnit('loanTermUnit');
    }
    //loanTermUnit 测试 --------------------------------------------------------   end

    //loanInterestRateUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setLoanInterestRateUnit() 正确的传参类型,期望传值正确
     */
    public function testSetLoanInterestRateUnitCorrectType()
    {
        $object = new LoanProductCategory(1, '');
        $this->stub->setLoanInterestRateUnit($object);
        $this->assertEquals($object, $this->stub->getLoanInterestRateUnit());
    }

    /**
     * 设置 LoanProduct setLoanInterestRateUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanInterestRateUnitWrongType()
    {
        $this->stub->setLoanInterestRateUnit('loanInterestRateUnit');
    }
    //loanInterestRateUnit 测试 --------------------------------------------------------   end

    //isSupportEarlyRepayment 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setIsSupportEarlyRepayment() 正确的传参类型,期望传值正确
     */
    public function testSetIsSupportEarlyRepaymentCorrectType()
    {
        $object = new LoanProductCategory(1, '');
        $this->stub->setIsSupportEarlyRepayment($object);
        $this->assertEquals($object, $this->stub->getIsSupportEarlyRepayment());
    }

    /**
     * 设置 LoanProduct setIsSupportEarlyRepayment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsSupportEarlyRepaymentWrongType()
    {
        $this->stub->setIsSupportEarlyRepayment('isSupportEarlyRepayment');
    }
    //isSupportEarlyRepayment 测试 --------------------------------------------------------   end

    //isExistEarlyRepaymentCost 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setIsExistEarlyRepaymentCost() 正确的传参类型,期望传值正确
     */
    public function testSetIsExistEarlyRepaymentCostCorrectType()
    {
        $object = new LoanProductCategory(1, '');
        $this->stub->setIsExistEarlyRepaymentCost($object);
        $this->assertEquals($object, $this->stub->getIsExistEarlyRepaymentCost());
    }

    /**
     * 设置 LoanProduct setIsExistEarlyRepaymentCost() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsExistEarlyRepaymentCostWrongType()
    {
        $this->stub->setIsExistEarlyRepaymentCost('isExistEarlyRepaymentCost');
    }
    //isExistEarlyRepaymentCost 测试 --------------------------------------------------------   end

    //loanInterestRate 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setLoanInterestRate() 正确的传参类型,期望传值正确
     */
    public function testSetLoanInterestRateCorrectType()
    {
        $this->stub->setLoanInterestRate(1.01);
        $this->assertEquals(1.01, $this->stub->getLoanInterestRate());
    }

    /**
     * 设置 LoanProduct setLoanInterestRate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanInterestRateWrongType()
    {
        $this->stub->setLoanInterestRate('loanInterestRate');
    }
    //loanInterestRate 测试 --------------------------------------------------------   end

    //repaymentMethods 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setRepaymentMethods() 正确的传参类型,期望传值正确
     */
    public function testSetRepaymentMethodsCorrectType()
    {
        $this->stub->setRepaymentMethods(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getRepaymentMethods());
    }

    /**
     * 设置 LoanProduct setRepaymentMethods() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRepaymentMethodsWrongType()
    {
        $this->stub->setRepaymentMethods('repaymentMethods');
    }
    //repaymentMethods 测试 --------------------------------------------------------   end

    //earlyRepaymentTerm 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setEarlyRepaymentTerm() 正确的传参类型,期望传值正确
     */
    public function testSetEarlyRepaymentTermCorrectType()
    {
        $this->stub->setEarlyRepaymentTerm(1);
        $this->assertEquals(1, $this->stub->getEarlyRepaymentTerm());
    }

    /**
     * 设置 LoanProduct setEarlyRepaymentTerm() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEarlyRepaymentTermWrongType()
    {
        $this->stub->setEarlyRepaymentTerm('earlyRepaymentTerm');
    }
    //earlyRepaymentTerm 测试 --------------------------------------------------------   end

    //applicationMaterial 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setApplicationMaterial() 正确的传参类型,期望传值正确
     */
    public function testSetApplicationMaterialCorrectType()
    {
        $this->stub->setApplicationMaterial('applicationMaterial');
        $this->assertEquals('applicationMaterial', $this->stub->getApplicationMaterial());
    }

    /**
     * 设置 LoanProduct setApplicationMaterial() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicationMaterialWrongType()
    {
        $this->stub->setApplicationMaterial(array());
    }
    //applicationMaterial 测试 --------------------------------------------------------   end

    //applicationCondition 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setApplicationCondition() 正确的传参类型,期望传值正确
     */
    public function testSetApplicationConditionCorrectType()
    {
        $this->stub->setApplicationCondition('applicationCondition');
        $this->assertEquals('applicationCondition', $this->stub->getApplicationCondition());
    }

    /**
     * 设置 LoanProduct setApplicationCondition() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicationConditionWrongType()
    {
        $this->stub->setApplicationCondition(array());
    }
    //applicationCondition 测试 --------------------------------------------------------   end

    //contract 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setContract() 正确的传参类型,期望传值正确
     */
    public function testSetContractCorrectType()
    {
        $this->stub->setContract(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getContract());
    }

    /**
     * 设置 LoanProduct setContract() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContractWrongType()
    {
        $this->stub->setContract('contract');
    }
    //contract 测试 --------------------------------------------------------   end

    //application 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setApplication() 正确的传参类型,期望传值正确
     */
    public function testSetApplicationCorrectType()
    {
        $this->stub->setApplication(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getApplication());
    }

    /**
     * 设置 LoanProduct setApplication() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicationWrongType()
    {
        $this->stub->setApplication('application');
    }
    //application 测试 --------------------------------------------------------   end

    //enterprise 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $object = new Enterprise();
        $this->stub->setEnterprise($object);
        $this->assertEquals($object, $this->stub->getEnterprise());
    }

    /**
     * 设置 LoanProduct setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseWrongType()
    {
        $this->stub->setEnterprise('enterprise');
    }
    //enterprise 测试 --------------------------------------------------------   end

    //snapshot 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setSnapshot() 正确的传参类型,期望传值正确
     */
    public function testSetSnapshotCorrectType()
    {
        $object = new Snapshot();
        $this->stub->setSnapshot($object);
        $this->assertEquals($object, $this->stub->getSnapshot());
    }

    /**
     * 设置 LoanProduct setSnapshot() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSnapshotWrongType()
    {
        $this->stub->setSnapshot('snapshot');
    }
    //snapshot 测试 --------------------------------------------------------   end

    //loanMonthInterestRate 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setLoanMonthInterestRate() 正确的传参类型,期望传值正确
     */
    public function testSetLoanMonthInterestRateCorrectType()
    {
        $this->stub->setLoanMonthInterestRate(1.01);
        $this->assertEquals(1.01, $this->stub->getLoanMonthInterestRate());
    }

    /**
     * 设置 LoanProduct setLoanMonthInterestRate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanMonthInterestRateWrongType()
    {
        $this->stub->setLoanMonthInterestRate('loanMonthInterestRate');
    }
    //loanMonthInterestRate 测试 --------------------------------------------------------   end

    //volume 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setVolume() 正确的传参类型,期望传值正确
     */
    public function testSetVolumeCorrectType()
    {
        $this->stub->setVolume(1);
        $this->assertEquals(1, $this->stub->getVolume());
    }

    /**
     * 设置 LoanProduct setVolume() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVolumeWrongType()
    {
        $this->stub->setVolume('volume');
    }
    //volume 测试 --------------------------------------------------------   end

    //volumeSuccess 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setVolumeSuccess() 正确的传参类型,期望传值正确
     */
    public function testSetVolumeSuccessCorrectType()
    {
        $this->stub->setVolumeSuccess(1);
        $this->assertEquals(1, $this->stub->getVolumeSuccess());
    }

    /**
     * 设置 LoanProduct setVolumeSuccess() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVolumeSuccessWrongType()
    {
        $this->stub->setVolumeSuccess('volumeSuccess');
    }
    //volumeSuccess 测试 --------------------------------------------------------   end

    //attentionDegree 测试 -------------------------------------------------------- start
    /**
     * 设置 LoanProduct setAttentionDegree() 正确的传参类型,期望传值正确
     */
    public function testSetAttentionDegreeCorrectType()
    {
        $this->stub->setAttentionDegree(1);
        $this->assertEquals(1, $this->stub->getAttentionDegree());
    }

    /**
     * 设置 LoanProduct setAttentionDegree() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttentionDegreeWrongType()
    {
        $this->stub->setAttentionDegree('attentionDegree');
    }
    //attentionDegree 测试 --------------------------------------------------------   end
}
