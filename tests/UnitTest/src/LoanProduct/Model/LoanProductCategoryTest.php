<?php
namespace Sdk\LoanProduct\Model;

use Sdk\Common\Model\Category;

use PHPUnit\Framework\TestCase;

class LoanProductCategoryTest extends TestCase
{
    public function setUp()
    {
        $this->stub = new LoanProductCategory(1, '');
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsCategory()
    {
        $this->assertInstanceof('Sdk\Common\Model\Category', $this->stub);
    }
}
