<?php
namespace Sdk\LoanProduct\Translator;

use Sdk\LoanProduct\Model\NullLoanProduct;
use Sdk\LoanProduct\Model\LoanProduct;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Enterprise\Model\Enterprise;

use Sdk\LoanProduct\Model\LoanProductCategoryFactory;

class LoanProductRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            LoanProductRestfulTranslator::class
        )
            ->setMethods(['getEnterpriseRestfulTranslator'])
            ->getMock();

        $this->childStub =
        new class extends LoanProductRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new LoanProduct());
        $this->assertInstanceOf('Sdk\LoanProduct\Model\NullLoanProduct', $result);
    }

    public function setMethods(LoanProduct $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['cover'])) {
            $expectObject->setCover($attributes['cover']);
        }
        if (isset($attributes['productObject'])) {
            $productObjects = array();
            foreach ($attributes['productObject'] as $productObject) {
                $productObjects[] = LoanProductCategoryFactory::create(
                    $productObject,
                    LoanProductCategoryFactory::TYPE['PRODUCT_OBJECT']
                );
            }

            $expectObject->setProductObject($productObjects);
        }
        if (isset($attributes['guarantyStyles'])) {
            $guarantyStyles = array();
            foreach ($attributes['guarantyStyles'] as $guarantyStyle) {
                $guarantyStyles[] = LoanProductCategoryFactory::create(
                    $guarantyStyle,
                    LoanProductCategoryFactory::TYPE['GUARANTY_STYLES']
                );
            }
            $expectObject->setGuarantyStyles($guarantyStyles);
        }
        if (isset($attributes['minLoanPeriod'])) {
            $expectObject->setMinLoanPeriod($attributes['minLoanPeriod']);
        }
        if (isset($attributes['maxLoanPeriod'])) {
            $expectObject->setMaxLoanPeriod($attributes['maxLoanPeriod']);
        }
        if (isset($attributes['supportCity'])) {
            $expectObject->setSupportCity($attributes['supportCity']);
        }
        if (isset($attributes['minLoanAmount'])) {
            $expectObject->setMinLoanAmount($attributes['minLoanAmount']);
        }
        if (isset($attributes['maxLoanAmount'])) {
            $expectObject->setMaxLoanAmount($attributes['maxLoanAmount']);
        }
        if (isset($attributes['minLoanTerm'])) {
            $expectObject->setMinLoanTerm($attributes['minLoanTerm']);
        }
        if (isset($attributes['maxLoanTerm'])) {
            $expectObject->setMaxLoanTerm($attributes['maxLoanTerm']);
        }
        if (isset($attributes['loanInterestRate'])) {
            $expectObject->setLoanInterestRate($attributes['loanInterestRate']);
        }
        if (isset($attributes['repaymentMethods'])) {
            $repaymentMethods = array();
            foreach ($attributes['repaymentMethods'] as $repaymentMethod) {
                $repaymentMethods[] = LoanProductCategoryFactory::create(
                    $repaymentMethod,
                    LoanProductCategoryFactory::TYPE['REPAYMENT_METHOD']
                );
            }
            $expectObject->setRepaymentMethods($repaymentMethods);
        }
        if (isset($attributes['earlyRepaymentTerm'])) {
            $expectObject->setEarlyRepaymentTerm($attributes['earlyRepaymentTerm']);
        }
        if (isset($attributes['applicationMaterial'])) {
            $expectObject->setApplicationMaterial($attributes['applicationMaterial']);
        }
        if (isset($attributes['applicationCondition'])) {
            $expectObject->setApplicationCondition($attributes['applicationCondition']);
        }
        if (isset($attributes['contract'])) {
            $expectObject->setContract($attributes['contract']);
        }
        if (isset($attributes['application'])) {
            $expectObject->setApplication($attributes['application']);
        }
        if (isset($attributes['loanMonthInterestRate'])) {
            $expectObject->setLoanMonthInterestRate($attributes['loanMonthInterestRate']);
        }
        if (isset($attributes['volume'])) {
            $expectObject->setVolume($attributes['volume']);
        }
        if (isset($attributes['volumeSuccess'])) {
            $expectObject->setVolumeSuccess($attributes['volumeSuccess']);
        }
        if (isset($attributes['attentionDegree'])) {
            $expectObject->setAttentionDegree($attributes['attentionDegree']);
        }
        if (isset($attributes['applyStatus'])) {
            $expectObject->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $expectObject->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $loanProduct = \Sdk\LoanProduct\Utils\MockFactory::generateLoanProductArray();

        $data =  $loanProduct['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($loanProduct);

        $expectObject = new LoanProduct();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $loanProduct = \Sdk\LoanProduct\Utils\MockFactory::generateLoanProductArray();
        $data =  $loanProduct['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($loanProduct);
        $expectArray = array();

        $expectObject = new LoanProduct();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $loanProduct = \Sdk\LoanProduct\Utils\MockFactory::generateLoanProductObject(1, 1);

        $actual = $this->stub->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise',
                'rejectReason'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'loanProducts'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'title'=>$loanProduct->getTitle(),
            'cover'=>$loanProduct->getCover(),
            'minLoanPeriod'=>$loanProduct->getMinLoanPeriod(),
            'maxLoanPeriod'=>$loanProduct->getMaxLoanPeriod(),
            'supportCity'=>$loanProduct->getSupportCity(),
            'minLoanAmount'=>$loanProduct->getMinLoanAmount(),
            'maxLoanAmount'=>$loanProduct->getMaxLoanAmount(),
            'minLoanTerm'=>$loanProduct->getMinLoanTerm(),
            'maxLoanTerm'=>$loanProduct->getMaxLoanTerm(),
            'loanTermUnit'=>$loanProduct->getLoanTermUnit()->getId(),
            'loanInterestRate'=>$loanProduct->getLoanInterestRate(),
            'loanInterestRateUnit'=>$loanProduct->getLoanInterestRateUnit()->getId(),
            'isSupportEarlyRepayment'=>$loanProduct->getIsSupportEarlyRepayment()->getId(),
            'earlyRepaymentTerm'=>$loanProduct->getEarlyRepaymentTerm(),
            'isExistEarlyRepaymentCost'=>$loanProduct->getIsExistEarlyRepaymentCost()->getId(),
            'applicationMaterial'=>$loanProduct->getApplicationMaterial(),
            'applicationCondition'=>$loanProduct->getApplicationCondition(),
            'contract'=>$loanProduct->getContract(),
            'application'=>$loanProduct->getApplication(),
            'rejectReason'=>$loanProduct->getRejectReason()
        );

        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $loanProduct->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
