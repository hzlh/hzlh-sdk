<?php
namespace Sdk\Withdrawal\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Translator\BankCardRestfulTranslator;

use Sdk\Withdrawal\Model\Withdrawal;

class WithdrawalRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            WithdrawalRestfulTranslator::class
        )
            ->setMethods([
                'getMemberAccountRestfulTranslator',
                'getBankCardRestfulTranslator',
            ])
            ->getMock();

        $this->childStub =
        new class extends WithdrawalRestfulTranslator
        {
            public function getMemberAccountRestfulTranslator(): MemberAccountRestfulTranslator
            {
                return parent::getMemberAccountRestfulTranslator();
            }

            public function getBankCardRestfulTranslator(): BankCardRestfulTranslator
            {
                return parent::getBankCardRestfulTranslator();
            }
        };
    }

    public function testGetMemberAccountRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator',
            $this->childStub->getMemberAccountRestfulTranslator()
        );
    }

    public function testGetBankCardRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\BankCard\Translator\BankCardRestfulTranslator',
            $this->childStub->getBankCardRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Withdrawal());
        $this->assertInstanceOf('Sdk\Withdrawal\Model\NullWithdrawal', $result);
    }

    public function setMethods(Withdrawal $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['transferVoucher'])) {
            $expectObject->setTransferVoucher($attributes['transferVoucher']);
        }
        if (isset($attributes['amount'])) {
            $expectObject->setAmount($attributes['amount']);
        }
        if (isset($attributes['paymentPassword'])) {
            $expectObject->setPaymentPassword($attributes['paymentPassword']);
        }
        if (isset($attributes['serviceCharge'])) {
            $expectObject->setServiceCharge($attributes['serviceCharge']);
        }
        if (isset($attributes['cellphone'])) {
            $expectObject->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['memberAccount']['data'])) {
            $expectObject->setMemberAccount(new MemberAccount($relationships['memberAccount']['data']['id']));
        }
        if (isset($relationships['bankCard']['data'])) {
            $expectObject->setBankCard(new BankCard($relationships['bankCard']['data']['id']));
        }

        return $expectObject;
    }

    private function establishingPrediction($withdrawal, $data, $relationships)
    {
        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());

        $bankCard = new BankCard($relationships['bankCard']['data']['id']);
        $bankCardRestfulTranslator = $this->prophesize(BankCardRestfulTranslator::class);
        $bankCardRestfulTranslator->arrayToObject(Argument::exact($relationships['bankCard']))
            ->shouldBeCalledTimes(1)->willReturn($bankCard);

        $this->stub->expects($this->exactly(1))
            ->method('getBankCardRestfulTranslator')
            ->willReturn($bankCardRestfulTranslator->reveal());
    }

    public function testArrayToObjectCorrectObject()
    {
        $withdrawal = \Sdk\Withdrawal\Utils\MockFactory::generateWithdrawalArray();

        $data = $withdrawal['data'];
        $relationships = $data['relationships'];
        $this->establishingPrediction($withdrawal, $data, $relationships);

        $actual = $this->stub->arrayToObject($withdrawal);

        $expectObject = new Withdrawal();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0, array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $withdrawal = \Sdk\Withdrawal\Utils\MockFactory::generateWithdrawalArray();

        $data = $withdrawal['data'];
        $relationships = $data['relationships'];
        $this->establishingPrediction($withdrawal, $data, $relationships);

        $actual = $this->stub->arrayToObjects($withdrawal);

        $expectArray = array();

        $expectObject = new Withdrawal();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id'] => $expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $withdrawal = \Sdk\Withdrawal\Utils\MockFactory::generateWithdrawalObject(1, 1);

        $actual = $this->stub->objectToArray($withdrawal);

        $expectedArray = array(
            'data' => array(
                'type' => 'withdrawals',
            ),
        );

        $expectedArray['data']['attributes'] = array(
            'amount' => $withdrawal->getAmount(),
            'serviceCharge' => $withdrawal->getServiceCharge(),
            'cellphone' => $withdrawal->getCellphone(),
            'transferVoucher' => $withdrawal->getTransferVoucher(),
            'paymentPassword' => $withdrawal->getPaymentPassword(),
            'number' => $withdrawal->getNumber(),
            'transactionNumber' => $withdrawal->getTransactionNumber()
        );

        $expectedArray['data']['relationships']['bankCard']['data'] = array(
            array(
                'type' => 'bankCards',
                'id' => $withdrawal->getBankCard()->getId(),
            ),
        );

        $expectedArray['data']['relationships']['memberAccount']['data'] = array(
            array(
                'type' => 'memberAccounts',
                'id' => $withdrawal->getMemberAccount()->getId(),
            ),
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
