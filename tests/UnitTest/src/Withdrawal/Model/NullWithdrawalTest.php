<?php
namespace Sdk\Withdrawal\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullWithdrawalTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullWithdrawal::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsWithdrawal()
    {
        $this->assertInstanceof('Sdk\Withdrawal\Model\Withdrawal', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testWithdraw()
    {
        $result = $this->stub->withdraw();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testTransferCompleted()
    {
        $result = $this->stub->transferCompleted();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
