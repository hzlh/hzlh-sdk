<?php
namespace Sdk\Withdrawal\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Withdrawal\Repository\WithdrawalRepository;
use Sdk\WithdrawalCategory\Model\WithdrawalCategory;

use Sdk\NaturalPerson\Repository\NaturalPersonRepository;

use Sdk\BankCard\Model\BankCard;
use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\Common\Model\IApplyAble;

class WithdrawalTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Withdrawal::class)
            ->setMethods([
                'getRepository',
                'isBindNaturalPerson'
            ])->getMock();

        $this->childStub = new Class extends Withdrawal{
            public function getRepository() : WithdrawalRepository
            {
                return parent::getRepository();
            }
            public function getNaturalPersonRepository(): NaturalPersonRepository
            {
                return parent::getNaturalPersonRepository();
            }
            public function isBindNaturalPerson(): bool
            {
                return parent::isBindNaturalPerson();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Withdrawal\Repository\WithdrawalRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetNaturalPersonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\NaturalPerson\Repository\NaturalPersonRepository',
            $this->childStub->getNaturalPersonRepository()
        );
    }
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Withdrawal setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Withdrawal setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //transferVoucher 测试 ---------------------------------------------------------- start
    /**
     * 设置 Withdrawal setTransferVoucher() 正确的传参类型,期望传值正确
     */
    public function testSetTransferVoucherCorrectType()
    {
        $this->stub->setTransferVoucher(array('transferVoucher'));
        $this->assertEquals(array('transferVoucher'), $this->stub->getTransferVoucher());
    }

    /**
     * 设置 Withdrawal setTransferVoucher() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransferVoucherWrongType()
    {
        $this->stub->setTransferVoucher('transferVoucher');
    }
    //transferVoucher 测试 ----------------------------------------------------------   end
    
    //paymentPassword 测试 ---------------------------------------------------------- start
    /**
     * 设置 Withdrawal setPaymentPassword() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentPasswordCorrectType()
    {
        $this->stub->setPaymentPassword('123456');
        $this->assertEquals('123456', $this->stub->getPaymentPassword());
    }

    /**
     * 设置 Withdrawal setPaymentPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentPasswordWrongType()
    {
        $this->stub->setPaymentPassword(array('paymentPassword'));
    }
    //paymentPassword 测试 ----------------------------------------------------------   end
    
    //amount 测试 ---------------------------------------------------------- start
    /**
     * 设置 Withdrawal setAmount() 正确的传参类型,期望传值正确
     */
    public function testSetAmountCorrectType()
    {
        $this->stub->setAmount('string');
        $this->assertEquals('string', $this->stub->getAmount());
    }

    /**
     * 设置 Withdrawal setAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmountWrongType()
    {
        $this->stub->setAmount(array(1, 2, 3));
    }
    //amount 测试 ----------------------------------------------------------   end
    
    //cellphone 测试 ---------------------------------------------------------- start
    /**
     * 设置 Withdrawal setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->stub->setCellphone('string');
        $this->assertEquals('string', $this->stub->getCellphone());
    }

    /**
     * 设置 Withdrawal setCellphone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCellphoneWrongType()
    {
        $this->stub->setCellphone(array(1, 2, 3));
    }
    //cellphone 测试 ----------------------------------------------------------   end
    
    //memberAccount 测试 -------------------------------------------------------- start
    /**
     * 设置 Withdrawal setMemberAccount() 正确的传参类型,期望传值正确
     */
    public function testSetMemberAccountCorrectType()
    {
        $object = new MemberAccount();
        $this->stub->setMemberAccount($object);
        $this->assertSame($object, $this->stub->getMemberAccount());
    }

    /**
     * 设置 Withdrawal setMemberAccount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberAccountType()
    {
        $this->stub->setMemberAccount(array(1,2,3));
    }
    //memberAccount 测试 -------------------------------------------------------- end
    
    //bankCard 测试 -------------------------------------------------------- start
    /**
     * 设置 Withdrawal setBankCard() 正确的传参类型,期望传值正确
     */
    public function testSetBankCardCorrectType()
    {
        $object = new BankCard();
        $this->stub->setBankCard($object);
        $this->assertSame($object, $this->stub->getBankCard());
    }

    /**
     * 设置 Withdrawal setBankCard() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankCardType()
    {
        $this->stub->setBankCard(array(1,2,3));
    }
    //bankCard 测试 -------------------------------------------------------- end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Withdrawal setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    /**
     * 循环测试 Withdrawal setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Withdrawal::WITHDRAWAL_STATUS['PENDING'], Withdrawal::WITHDRAWAL_STATUS['PENDING']),
            array(Withdrawal::WITHDRAWAL_STATUS['APPROVE'], Withdrawal::WITHDRAWAL_STATUS['APPROVE']),
            array(999, Withdrawal::WITHDRAWAL_STATUS['PENDING']),
        );
    }
    /**
     * 设置 Withdrawal setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    public function testWithdraw()
    {
        $this->stub = $this->getMockBuilder(Withdrawal::class)
            ->setMethods(['getRepository', 'isBindNaturalPerson'])
            ->getMock();
            
        $this->stub->expects($this->once())
                   ->method('isBindNaturalPerson')
                   ->willReturn(true);

        $repository = $this->prophesize(WithdrawalRepository::class);

        $repository->withdraw(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->stub->withdraw();

        $this->assertTrue($result);
    }

    public function testTransferCompletedFailure()
    {
        $this->stub = $this->getMockBuilder(Withdrawal::class)
            ->setMethods(['isPending'])
            ->getMock();

        $this->stub->expects($this->once())
                   ->method('isPending')
                   ->willReturn(false);

        $result = $this->stub->transferCompleted();

        $this->assertFalse($result);
        $this->assertEquals(WITHDRAWAL_STATUS_NOT_PENDING, Core::getLastError()->getId());
    }

    public function testTransferCompletedSuccess()
    {
        $this->stub = $this->getMockBuilder(Withdrawal::class)
            ->setMethods(['getRepository', 'isPending'])
            ->getMock();
            
        $this->stub->expects($this->exactly(1))
                   ->method('isPending')
                   ->willReturn(true);

        $status = Withdrawal::WITHDRAWAL_STATUS['APPROVE'];
        $this->stub->setStatus($status);
        $this->stub->setUpdateTime(Core::$container->get('time'));
        $this->stub->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(WithdrawalRepository::class);
        $repository->transferCompleted(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->stub->transferCompleted();
        
        $this->assertTrue($result);
    }

    public function testIsPending()
    {
        $this->stub->setStatus(Withdrawal::WITHDRAWAL_STATUS['PENDING']);

        $result = $this->stub->isPending();

        $this->assertTrue($result);
    }
}
