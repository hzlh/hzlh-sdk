<?php
namespace Sdk\Withdrawal\Repository;

use Sdk\Withdrawal\Adapter\Withdrawal\WithdrawalRestfulAdapter;

use Sdk\Withdrawal\Utils\MockFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class WithdrawalRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(WithdrawalRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends WithdrawalRepository {
            public function getAdapter() : WithdrawalRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Withdrawal\Adapter\Withdrawal\WithdrawalRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为WithdrawalRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以WithdrawalRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(WithdrawalRestfulAdapter::class);
        $adapter->scenario(Argument::exact(WithdrawalRepository::OA_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(WithdrawalRepository::OA_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 获取mock数据
     * 为WithdrawalRestfulAdapter建立预言
     * 建立预期状况：withdraw() 方法将会被调用一次，并以$withdrawal为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testWithdraw()
    {
        $withdrawal = \Sdk\Withdrawal\Utils\MockFactory::generateWithdrawalObject(1);

        $adapter = $this->prophesize(WithdrawalRestfulAdapter::class);
        $adapter->withdraw(Argument::exact($withdrawal))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->withdraw($withdrawal);
        $this->assertTrue($result);
    }
    /**
     * 获取mock数据
     * 为WithdrawalRestfulAdapter建立预言
     * 建立预期状况：transferCompleted() 方法将会被调用一次，并以$withdrawal为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testTransferCompleted()
    {
        $withdrawal = \Sdk\Withdrawal\Utils\MockFactory::generateWithdrawalObject(1);

        $adapter = $this->prophesize(WithdrawalRestfulAdapter::class);
        $adapter->transferCompleted(Argument::exact($withdrawal))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->transferCompleted($withdrawal);
        $this->assertTrue($result);
    }
}
