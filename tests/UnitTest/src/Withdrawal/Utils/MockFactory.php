<?php
namespace Sdk\Withdrawal\Utils;

use Sdk\Withdrawal\Model\Withdrawal;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
/**
     * [generateWithdrawalArray 生成提现信息数组]
     * @return [array] [提现数组]
     */
    public static function generateWithdrawalArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $withdrawal = array();

        $withdrawal = array(
            'data'=>array(
                'type'=>'withdrawals',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //transferVoucher
        $transferVoucher = self::generateTransferVoucher($faker, $value);
        $attributes['transferVoucher'] = $transferVoucher;
        //amount
        $amount = self::generateAmount($faker, $value);
        $attributes['amount'] = $amount;
        //serviceCharge
        $serviceCharge = self::generateServiceCharge($faker, $value);
        $attributes['serviceCharge'] = $serviceCharge;
        //paymentPassword
        $paymentPassword = self::generatePaymentPassword($faker, $value);
        $attributes['paymentPassword'] = $paymentPassword;
        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $attributes['cellphone'] = $cellphone;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $withdrawal['data']['attributes'] = $attributes;
        //memberAccount
        $withdrawal['data']['relationships']['memberAccount']['data'] = array(
            'type' => 'memberAccounts',
            'id' => $faker->randomNumber(1)
        );
        //bankCard
        $withdrawal['data']['relationships']['bankCard']['data'] = array(
            'type' => 'withdrawalCategories',
            'id' => $faker->randomNumber(1)
        );

        return $withdrawal;
    }

    /**
     * [generateWithdrawalObject 生成用户对象对象]
     * @param  int|integer $id    [提现Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [提现对象]
     */
    public static function generateWithdrawalObject(int $id = 0, int $seed = 0, array $value = array()) : Withdrawal
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $withdrawal = new Withdrawal($id);

        //transferVoucher
        $transferVoucher = self::generateTransferVoucher($faker, $value);
        $withdrawal->setTransferVoucher($transferVoucher);
        //amount
        $amount = self::generateAmount($faker, $value);
        $withdrawal->setAmount($amount);
        //serviceCharge
        $serviceCharge = self::generateServiceCharge($faker, $value);
        $withdrawal->setServiceCharge($serviceCharge);
        //paymentPassword
        $paymentPassword = self::generatePaymentPassword($faker, $value);
        $withdrawal->setPaymentPassword($paymentPassword);
        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $withdrawal->setCellphone($cellphone);
        //memberAccount
        $memberAccount = self::generateMemberAccount($faker, $value);
        $withdrawal->setMemberAccount($memberAccount);
        //bankCard
        $bankCard = self::generateBankCard($faker, $value);
        $withdrawal->setBankCard($bankCard);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $withdrawal->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $withdrawal->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $withdrawal->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $withdrawal->setStatus($status);

        return $withdrawal;
    }

    private static function generateTransferVoucher($faker, array $value = array())
    {
        unset($faker);
        return $transferVoucher = isset($value['transferVoucher']) ?
        $value['transferVoucher'] : array("name"=>"转账凭证", "identify"=>"3.jpg");
    }

    private static function generateAmount($faker, array $value = array())
    {
        return $amount = isset($value['amount']) ?
        $value['amount'] : $faker->numerify();
    }

    private static function generateServiceCharge($faker, array $value = array())
    {
        return $serviceCharge = isset($value['serviceCharge']) ?
        $value['serviceCharge'] : $faker->numerify();
    }

    private static function generatePaymentPassword($faker, array $value = array())
    {
        return $paymentPassword = isset($value['paymentPassword']) ?
        $value['paymentPassword'] : $faker->numberBetween($min = 100000, $max = 999999);
    }

    private static function generateCellphone($faker, array $value = array())
    {
        return $cellphone = isset($value['cellphone']) ?
        $value['cellphone'] : $faker->phoneNumber();
    }

    private static function generateMemberAccount($faker, array $value = array())
    {
        return $memberAccount = isset($value['memberAccount']) ?
            $value['memberAccount'] : \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateBankCard($faker, array $value = array())
    {
        return $bankCard = isset($value['bankCard']) ?
            $value['bankCard'] : \Sdk\BankCard\Utils\MockFactory::generateBankCardObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
