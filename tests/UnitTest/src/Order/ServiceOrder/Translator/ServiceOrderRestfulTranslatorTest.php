<?php
namespace Sdk\Order\ServiceOrder\Translator;

use Sdk\Order\ServiceOrder\Model\NullServiceOrder;
use Sdk\Order\ServiceOrder\Model\ServiceOrder;

use Sdk\Payment\Model\Payment;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ServiceOrderRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            ServiceOrderRestfulTranslator::class
        )
            ->setMethods()
            ->getMock();
        parent::setUp();
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new ServiceOrder());
        $this->assertInstanceOf('Sdk\Order\ServiceOrder\Model\NullServiceOrder', $result);
    }

    public function setMethods(ServiceOrder $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['platformPreferentialAmount'])) {
            $expectObject->setPlatformPreferentialAmount($attributes['platformPreferentialAmount']);
        }
        if (isset($attributes['businessPreferentialAmount'])) {
            $expectObject->setBusinessPreferentialAmount($attributes['businessPreferentialAmount']);
        }
        if (isset($attributes['cancelReason'])) {
            $expectObject->setCancelReason($attributes['cancelReason']);
        }
        $paymentType = isset($attributes['paymentType'])
        ? $attributes['paymentType']
        : '';
        $transactionNumber = isset($attributes['transactionNumber'])
        ? $attributes['transactionNumber']
        : '';
        $transactionInfo = isset($attributes['transactionInfo'])
        ? $attributes['transactionInfo']
        : array();
        $paymentTime = isset($attributes['paymentTime'])
        ? $attributes['paymentTime']
        : 0;

        $expectObject->setPayment(
            new Payment(
                $paymentType,
                $paymentTime,
                $transactionNumber,
                $transactionInfo
            )
        );
        if (isset($attributes['paymentId'])) {
            $expectObject->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderArray();

        $data =  $serviceOrder['data'];
        $relationships = [];

        $actual = $this->stub->arrayToObject($serviceOrder);

        $expectObject = new ServiceOrder();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderArray();
        $data =  $serviceOrder['data'];
        $relationships = [];

        $actual = $this->stub->arrayToObjects($serviceOrder);
        $expectArray = array();

        $expectObject = new ServiceOrder();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderObject(1, 1);

        $actual = $this->stub->objectToArray($serviceOrder, array(
            'id',
            'transactionNumber',
            'transactionInfo',
            'amount'
        ));
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'serviceOrders',
                'id'=>$serviceOrder->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'transactionNumber'=>$serviceOrder->getPayment()->getTransactionNumber(),
            'transactionInfo'=>$serviceOrder->getPayment()->getTransactionInfo(),
            'amount'=>$serviceOrder->getAmount(),
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
