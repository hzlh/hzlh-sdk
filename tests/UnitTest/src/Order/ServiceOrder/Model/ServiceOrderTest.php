<?php
namespace Sdk\Order\ServiceOrder\Model;

use Sdk\Order\ServiceOrder\Repository\ServiceOrderRepository;
use Sdk\Order\ServiceOrder\Repository\ServiceOrderSessionRepository;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Sdk\Order\CommonOrder\Model\CommonOrder;

class ServiceOrderTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ServiceOrder::class)
            ->setMethods(
                [
                'getRepository',
                'IOperatAbleAdapter'
                ]
            )
            ->getMock();

        $this->childStub = new class extends ServiceOrder {
            public function getRepository() : ServiceOrderRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Order\ServiceOrder\Repository\ServiceOrderRepository',
            $this->childStub->getRepository()
        );
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testExtendsCommonOrder()
    {
        $this->assertInstanceOf('Sdk\Order\CommonOrder\Model\CommonOrder', $this->stub);
    }

    public function testGetIOperatAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //paymentId 测试 -------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setPaymentId() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentIdCorrectType()
    {
        $this->stub->setPaymentId('paymentId');
        $this->assertEquals('paymentId', $this->stub->getPaymentId());
    }

    /**
     * 设置 ServiceOrder setPaymentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentIdWrongType()
    {
        $this->stub->setPaymentId(array());
    }
    //paymentId 测试 --------------------------------------------------------   end
    
    //amount 测试 ---------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setAmount() 正确的传参类型,期望传值正确
     */
    public function testSetAmountCorrectType()
    {
        $this->stub->setAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getAmount());
    }

    /**
     * 设置 ServiceOrder setAmount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetAmountWrongTypeButNumeric()
    {
        $this->stub->setAmount('1');
        $this->assertEquals(1, $this->stub->getAmount());
    }
    //amount 测试 ----------------------------------------------------------   end

    //platformPreferentialAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setPlatformPreferentialAmount() 正确的传参类型,期望传值正确
     */
    public function testSetPlatformPreferentialAmountCorrectType()
    {
        $this->stub->setPlatformPreferentialAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getPlatformPreferentialAmount());
    }

    /**
     * 设置 ServiceOrder setPlatformPreferentialAmount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetPlatformPreferentialAmountWrongTypeButNumeric()
    {
        $this->stub->setPlatformPreferentialAmount('1');
        $this->assertEquals(1, $this->stub->getPlatformPreferentialAmount());
    }
    //platformPreferentialAmount 测试 ----------------------------------------------------------   end

    //businessPreferentialAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setBusinessPreferentialAmount() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessPreferentialAmountCorrectType()
    {
        $this->stub->setBusinessPreferentialAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getBusinessPreferentialAmount());
    }

    /**
     * 设置 ServiceOrder setBusinessPreferentialAmount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetBusinessPreferentialAmountWrongTypeButNumeric()
    {
        $this->stub->setBusinessPreferentialAmount('1');
        $this->assertEquals(1, $this->stub->getBusinessPreferentialAmount());
    }
    //businessPreferentialAmount 测试 ----------------------------------------------------------   end

    //transactionNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setTransactionNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTransactionNumberCorrectType()
    {
        $this->stub->setTransactionNumber('TransactionNumber');
        $this->assertEquals('TransactionNumber', $this->stub->getTransactionNumber());
    }

    /**
     * 设置 ServiceOrder setTransactionNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransactionNumberWrongType()
    {
        $this->stub->setTransactionNumber(array());
    }
    //transactionNumber 测试 --------------------------------------------------------   end

    //transactionInfo 测试 -------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setTransactionInfo() 正确的传参类型,期望传值正确
     */
    public function testSetTransactionInfoCorrectType()
    {
        $this->stub->setTransactionInfo(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getTransactionInfo());
    }

    /**
     * 设置 ServiceOrder setTransactionInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransactionInfoWrongType()
    {
        $this->stub->setTransactionInfo('transactionInfo');
    }
    //transactionInfo 测试 --------------------------------------------------------   end

    //paymentTime 测试 -------------------------------------------------------- start
    /**
     * 设置 ServiceOrder setPaymentTime() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentTimeCorrectType()
    {
        $this->stub->setPaymentTime(0);
        $this->assertEquals(0, $this->stub->getPaymentTime());
    }

    /**
     * 设置 ServiceOrder setPaymentTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentTimeWrongType()
    {
        $this->stub->setPaymentTime('string');
    }
}
