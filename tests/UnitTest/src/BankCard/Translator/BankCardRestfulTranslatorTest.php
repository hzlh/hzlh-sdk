<?php
namespace Sdk\BankCard\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Model\NullBankCard;
use Sdk\BankCard\Model\BankCardModelFactory;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Translator\BankRestfulTranslator;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

class BankCardRestfulTranslatorTest extends TestCase
{

    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankCardRestfulTranslator::class)
                ->setMethods([
                    'getBankRestfulTranslator',
                    'getMemberAccountRestfulTranslator'
                ])
                ->getMock();

        $this->childStub = new class extends BankCardRestfulTranslator {
            public function getBankRestfulTranslator() : BankRestfulTranslator
            {
                return parent::getBankRestfulTranslator();
            }

            public function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
            {
                return parent::getMemberAccountRestfulTranslator();
            }
        };
    }

    public function testGetBankRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Bank\Translator\BankRestfulTranslator',
            $this->childStub->getBankRestfulTranslator()
        );
    }

    public function testGetMemberAccountRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator',
            $this->childStub->getMemberAccountRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new BankCard());
        $this->assertInstanceOf('Sdk\BankCard\Model\NullBankCard', $result);
    }

    private function setMethods(BankCard $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['accountType'])) {
            $expectObject->setAccountType($attributes['accountType']);
        }
        if (isset($attributes['cardType'])) {
            $expectObject->setCardType($attributes['cardType']);
        }
        if (isset($attributes['cardNumber'])) {
            $expectObject->setCardNumber($attributes['cardNumber']);
        }
        if (isset($attributes['image'])) {
            $expectObject->setImage($attributes['image']);
        }
        if (isset($attributes['bankBranchArea'])) {
            $expectObject->setBankBranchArea($attributes['bankBranchArea']);
        }
        if (isset($attributes['bankBranchAddress'])) {
            $expectObject->setBankBranchAddress($attributes['bankBranchAddress']);
        }
        if (isset($attributes['cellphone'])) {
            $expectObject->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['cardholderName'])) {
            $expectObject->setCardholderName($attributes['cardholderName']);
        }
        if (isset($attributes['licence'])) {
            $expectObject->setLicence($attributes['licence']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['bank']['data'])) {
            $expectObject->setBank(new Bank($relationships['bank']['data']['id']));
        }
        if (isset($relationships['memberAccount']['data'])) {
            $expectObject->setMemberAccount(new MemberAccount($relationships['memberAccount']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $bankCard = \Sdk\BankCard\Utils\MockFactory::generateBankCardArray();

        $data =  $bankCard['data'];
        $relationships = $data['relationships'];

        $bank = new Bank($relationships['bank']['data']['id']);
        $bankRestfulTranslator = $this->prophesize(BankRestfulTranslator::class);
        $bankRestfulTranslator->arrayToObject(Argument::exact($relationships['bank']))
            ->shouldBeCalledTimes(1)->willReturn($bank);

        $this->stub->expects($this->exactly(1))
            ->method('getBankRestfulTranslator')
            ->willReturn($bankRestfulTranslator->reveal());

        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($bankCard);

        $expectObject = new BankCard();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);
        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $bankCard = \Sdk\BankCard\Utils\MockFactory::generateBankCardArray();
        $data =  $bankCard['data'];
        $relationships = $data['relationships'];

        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());

        $bank = new Bank($relationships['bank']['data']['id']);
        $bankRestfulTranslator = $this->prophesize(BankRestfulTranslator::class);
        $bankRestfulTranslator->arrayToObject(Argument::exact($relationships['bank']))
            ->shouldBeCalledTimes(1)->willReturn($bank);

        $this->stub->expects($this->exactly(1))
            ->method('getBankRestfulTranslator')
            ->willReturn($bankRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($bankCard);

        $expectArray = array();

        $expectObject = new BankCard();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $bankCard = \Sdk\BankCard\Utils\MockFactory::generateBankCardObject(1, 1);

        $actual = $this->stub->objectToArray($bankCard, array(
                'id',
                'accountType',
                'cardType',
                'cardNumber',
                'bankBranchArea',
                'bankBranchAddress',
                'cellphone',
                'cardholderName',
                'licence',
                'unbindReason',
                'memberAccount',
                'bank'
            ));

        $expectedArray = array(
            'data'=>array(
                'type'=>'bankCards',
                'id'=>$bankCard->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'accountType'=>$bankCard->getAccountType(),
            'cardType'=>$bankCard->getCardType(),
            'cardNumber'=>$bankCard->getCardNumber(),
            'bankBranchArea'=>$bankCard->getBankBranchArea(),
            'bankBranchAddress'=>$bankCard->getBankBranchAddress(),
            'cellphone'=>$bankCard->getCellphone(),
            'cardholderName'=>$bankCard->getCardholderName(),
            'licence'=>$bankCard->getLicence(),
            'unbindReason'=>$bankCard->getUnbindReason()
        );

        $expectedArray['data']['relationships']['bank']['data'] = array(
            array(
                'type' => 'banks',
                'id' => $bankCard->getBank()->getId()
            )
        );
        
        $expectedArray['data']['relationships']['memberAccount']['data'] = array(
            array(
                'type' => 'memberAccounts',
                'id' => $bankCard->getMemberAccount()->getId()
            )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
