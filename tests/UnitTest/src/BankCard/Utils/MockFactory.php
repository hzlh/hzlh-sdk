<?php
namespace Sdk\BankCard\Utils;

use Sdk\BankCard\Model\BankCard;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateBankCardArray 生成政策信息数组]
     * @return [array] [政策数组]
     */
    public static function generateBankCardArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $bankCard = array();

        $bankCard = array(
            'data'=>array(
                'type'=>'bankCards',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //accountType
        $accountType = self::generateAccountType($faker, $value);
        $attributes['accountType'] = $accountType;

        //cardType
        $cardType = self::generateCardType($faker, $value);
        $attributes['cardType'] = $cardType;

        //cardNumber
        $cardNumber = self::generateCardNumber($faker, $value);
        $attributes['cardNumber'] = $cardNumber;

        //bankBranchArea
        $bankBranchArea = self::generateBankBranchArea($faker, $value);
        $attributes['bankBranchArea'] = $bankBranchArea;

        //bankBranchAddress
        $bankBranchAddress = self::generateBankBranchAddress($faker, $value);
        $attributes['bankBranchAddress'] = $bankBranchAddress;

        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $attributes['cellphone'] = $cellphone;

        //cardholderName
        $cardholderName = self::generateCardholderName($faker, $value);
        $attributes['cardholderName'] = $cardholderName;

        //licence
        $licence = self::generateLicence($faker, $value);
        $attributes['licence'] = $licence;

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;

        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;

        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;

        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $bankCard['data']['attributes'] = $attributes;

        //bank
        $bankCard['data']['relationships']['bank']['data'] = array(
            'type' => 'banks',
            'id' => $faker->randomNumber(1)
        );

        //memberAccount
        $bankCard['data']['relationships']['memberAccount']['data'] = array(
            'type' => 'memberAccounts',
            'id' => $faker->randomNumber(1)
        );

        return $bankCard;
    }
    /**
     * [generateBankCardObject 生成政策对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [政策对象]
     */
    public static function generateBankCardObject(int $id = 0, int $seed = 0, array $value = array()) : BankCard
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bankCard = new BankCard($id);

        //accountType
        $accountType = self::generateAccountType($faker, $value);
        $bankCard->setAccountType($accountType);

        //cardType
        $cardType = self::generateCardType($faker, $value);
        $bankCard->setCardType($cardType);

        //cardNumber
        $cardNumber = self::generateCardNumber($faker, $value);
        $bankCard->setCardNumber($cardNumber);

        //bankBranchArea
        $bankBranchArea = self::generateBankBranchArea($faker, $value);
        $bankCard->setBankBranchArea($bankBranchArea);

        //bankBranchAddress
        $bankBranchAddress = self::generateBankBranchAddress($faker, $value);
        $bankCard->setBankBranchAddress($bankBranchAddress);

        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $bankCard->setCellphone($cellphone);

        //cardholderName
        $cardholderName = self::generateCardholderName($faker, $value);
        $bankCard->setCardholderName($cardholderName);

        //licence
        $licence = self::generateLicence($faker, $value);
        $bankCard->setLicence($licence);

        //bank
        $bank = self::generateBank($faker, $value);
        $bankCard->setBank($bank);

        //memberAccount
        $memberAccount = self::generateMemberAccount($faker, $value);
        $bankCard->setMemberAccount($memberAccount);

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $bankCard->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $bankCard->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $bankCard->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $bankCard->setStatus($status);

        return $bankCard;
    }

    private static function generateAccountType($faker, array $value = array())
    {
        return isset($value['accountType'])
        ? $value['accountType']
        : $faker->randomElement(
            BankCard::ACCOUNT_TYPE
        );
    }

    private static function generateCardType($faker, array $value = array())
    {
        return isset($value['cardType'])
        ? $value['cardType']
        : $faker->randomElement(
            BankCard::CARD_TYPE
        );
    }

    private static function generateCardNumber($faker, array $value = array())
    {
        return isset($value['cardNumber'])
        ? $value['cardNumber']
        : $faker->creditCardNumber();
    }

    private static function generateBankBranchArea($faker, array $value = array())
    {
        return isset($value['bankBranchArea'])
        ? $value['bankBranchArea']
        : $faker->city();
    }

    private static function generateBankBranchAddress($faker, array $value = array())
    {
        return isset($value['bankBranchAddress'])
        ? $value['bankBranchAddress']
        : $faker->address();
    }

    private static function generateCellphone($faker, array $value = array())
    {
        return isset($value['cellphone'])
        ? $value['cellphone']
        : $faker->phoneNumber();
    }

    private static function generateCardholderName($faker, array $value = array())
    {
        return isset($value['cardholderName'])
        ? $value['cardholderName']
        : $faker->name();
    }

    private static function generateLicence($faker, array $value = array())
    {
        return isset($value['licence']) ? $value['licence'] :
           array('name'=>$faker->name(), 'identify'=>'licence.jpg');
    }

    private static function generateBank($faker, array $value = array())
    {
        return isset($value['bank']) ?
            $value['bank'] :
            \Sdk\Bank\Utils\MockFactory::generateBankObject($faker->numerify(), $faker->numerify());
    }

    private static function generateMemberAccount($faker, array $value = array())
    {
        return isset($value['memberAccount']) ?
            $value['memberAccount'] :
            \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject($faker->numerify(), $faker->numerify());
    }
}
