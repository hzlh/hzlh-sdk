<?php
namespace Sdk\Coupon\Repository;

use Sdk\Coupon\Adapter\Coupon\CouponRestfulAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class CouponRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CouponRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends CouponRepository {
            public function getAdapter() : CouponRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Coupon\Adapter\Coupon\CouponRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为CouponRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以CouponRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(CouponRestfulAdapter::class);
        $adapter->scenario(Argument::exact(CouponRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(CouponRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为CouponRestfulAdapter建立预言
     * 建立预期状况:receive() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testReceive()
    {
        $coupon = \Sdk\Coupon\Utils\MockFactory::generateCouponObject(1);

        $adapter = $this->prophesize(CouponRestfulAdapter::class);
        $adapter->receive(Argument::exact($coupon))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->receive($coupon);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为CouponRestfulAdapter建立预言
     * 建立预期状况:deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $coupon = \Sdk\Coupon\Utils\MockFactory::generateCouponObject(1);

        $adapter = $this->prophesize(CouponRestfulAdapter::class);
        $adapter->deletes(Argument::exact($coupon))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($coupon);
        $this->assertTrue($result);
    }
}
