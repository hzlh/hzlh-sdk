<?php
namespace Sdk\Coupon\Model;

use Sdk\Coupon\Repository\CouponRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\Member\Model\Member;

class CouponTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Coupon::class)
            ->setMethods(
                [
                'getRepository'
                ]
            )
            ->getMock();

        $this->childStub = new class extends Coupon {
            public function getRepository() : CouponRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Coupon\Repository\CouponRepository',
            $this->childStub->getRepository()
        );
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Coupon setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Coupon setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //merchantCoupon 测试 -------------------------------------------------------- start
    /**
     * 设置 Coupon setMerchantCoupon() 正确的传参类型,期望传值正确
     */
    public function testSetMerchantCouponCorrectType()
    {
        $object = new MerchantCoupon();
        $this->stub->setMerchantCoupon($object);
        $this->assertSame($object, $this->stub->getMerchantCoupon());
    }

    /**
     * 设置 Coupon setMerchantCoupon() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMerchantCouponType()
    {
        $this->stub->setMerchantCoupon(array(1,2,3));
    }
    //merchantCoupon 测试 -------------------------------------------------------- end
    
    //releaseType 测试 -------------------------------------------------------- start
    /**
     * 设置 Coupon setReleaseType() 正确的传参类型,期望传值正确
     */
    public function testSetReleaseTypeCorrectType()
    {
        $this->stub->setReleaseType(0);
        $this->assertEquals(0, $this->stub->getReleaseType());
    }

    /**
     * 设置 Coupon setReleaseType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReleaseTypeWrongType()
    {
        $this->stub->setReleaseType('string');
    }
    //releaseType 测试 --------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 Coupon setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $object = new Member();
        $this->stub->setMember($object);
        $this->assertSame($object, $this->stub->getMember());
    }

    /**
     * 设置 Coupon setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberType()
    {
        $this->stub->setMember(array(1,2,3));
    }
    //member 测试 -------------------------------------------------------- end
}
