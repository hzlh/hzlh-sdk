<?php
namespace Sdk\PaymentPassword\Utils;

use Sdk\PaymentPassword\Model\PaymentPassword;

class MockFactory
{
    /**
     * [generatePaymentPasswordArray 生成支付密码数组]
     * @return [array] [支付密码数组]
     */
    public static function generatePaymentPasswordArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $paymentPassword = array();

        $paymentPassword = array(
            'data'=>array(
                'type'=>'paymentPasswords',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $attributes['cellphone'] = $cellphone;
        //password
        $password = self::generatePassword($faker, $value);
        $attributes['password'] = $password;
        //oldPassword
        $oldPassword = self::generateOldPassword($faker, $value);
        $attributes['oldPassword'] = $oldPassword;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $paymentPassword['data']['attributes'] = $attributes;
        //memberAccount
        $paymentPassword['data']['relationships']['memberAccount']['data'] = array(
            'type' => 'memberAccounts',
            'id' => $faker->randomNumber(1)
        );

        return $paymentPassword;
    }

    /**
     * [generatePaymentPasswordObject 生成政策解读对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [政策解读对象]
     */
    public static function generatePaymentPasswordObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : PaymentPassword {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $paymentPassword = new PaymentPassword($id);

        //cellphone
        $cellphone = self::generateCellphone($faker, $value);
        $paymentPassword->setCellphone($cellphone);
        //password
        $password = self::generatePassword($faker, $value);
        $paymentPassword->setPassword($password);
        //oldPassword
        $oldPassword = self::generateOldPassword($faker, $value);
        $paymentPassword->setOldPassword($oldPassword);
        //memberAccount
        $memberAccount = self::generateMemberAccount($faker, $value);
        $paymentPassword->setMemberAccount($memberAccount);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $paymentPassword->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $paymentPassword->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $paymentPassword->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $paymentPassword->setStatus($status);

        return $paymentPassword;
    }

    private static function generateCellphone($faker, array $value = array())
    {
        return $cellphone = isset($value['cellphone']) ?
            $value['cellphone'] : 13201816773;
    }

    private static function generatePassword($faker, array $value = array())
    {
        return $password = isset($value['password']) ?
            $value['password'] : 132323;
    }

    private static function generateOldPassword($faker, array $value = array())
    {
        return $oldPassword = isset($value['oldPassword']) ?
            $value['oldPassword'] : 132324;
    }

    private static function generateMemberAccount($faker, array $value = array())
    {
        return $memberAccount = isset($value['memberAccount']) ?
            $value['memberAccount'] : \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
