<?php
namespace Sdk\PaymentPassword\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullPaymentPasswordTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPaymentPassword::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsPaymentPassword()
    {
        $this->assertInstanceof('Sdk\PaymentPassword\Model\PaymentPassword', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
