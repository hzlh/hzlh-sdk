<?php
namespace Sdk\PaymentPassword\Model;

use Sdk\PaymentPassword\Repository\PaymentPasswordRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Marmot\Core;

use Sdk\MemberAccount\Model\MemberAccount;

class PaymentPasswordTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PaymentPassword::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends PaymentPassword{
            public function getRepository() : PaymentPasswordRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\PaymentPassword\Repository\PaymentPasswordRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 PaymentPassword setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 PaymentPassword setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //cellphone 测试 ---------------------------------------------------------- start
    /**
     * 设置 PaymentPassword setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->stub->setCellphone('13201816773');
        $this->assertEquals('13201816773', $this->stub->getCellphone());
    }

    /**
     * 设置 PaymentPassword setCellphone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCellphoneWrongType()
    {
        $this->stub->setCellphone(array(1, 2, 3));
    }
    //cellphone 测试 ----------------------------------------------------------   end

    //password 测试 ---------------------------------------------------------- start
    /**
     * 设置 PaymentPassword setPassword() 正确的传参类型,期望传值正确
     */
    public function testSetPasswordCorrectType()
    {
        $this->stub->setPassword('314158');
        $this->assertEquals('314158', $this->stub->getPassword());
    }

    /**
     * 设置 PaymentPassword setPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPasswordWrongType()
    {
        $this->stub->setPassword(array(1, 2, 3));
    }
    //password 测试 ----------------------------------------------------------   end

    //oldPassword 测试 ---------------------------------------------------------- start
    /**
     * 设置 PaymentPassword setOldPassword() 正确的传参类型,期望传值正确
     */
    public function testSetOldPasswordCorrectType()
    {
        $this->stub->setOldPassword('314158');
        $this->assertEquals('314158', $this->stub->getOldPassword());
    }

    /**
     * 设置 PaymentPassword setOldPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOldPasswordWrongType()
    {
        $this->stub->setOldPassword(array(1, 2, 3));
    }
    //oldPassword 测试 ----------------------------------------------------------   end

    //memberAccount 测试 ---------------------------------------------------------- start
    /**
     * 设置 PaymentPassword setMemberAccount() 正确的传参类型,期望传值正确
     */
    public function testSetMemberAccountCorrectType()
    {
        $object = new MemberAccount();

        $this->stub->setMemberAccount($object);
        $this->assertEquals($object, $this->stub->getMemberAccount());
    }

    /**
     * 设置 PaymentPassword setMemberAccount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberAccountWrongType()
    {
        $this->stub->setMemberAccount(array(1, 2, 3));
    }
    //memberAccount 测试 ----------------------------------------------------------   end
}
