<?php
namespace Sdk\PaymentPassword\Adapter\PaymentPassword;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\PaymentPassword\Model\PaymentPassword;
use Sdk\PaymentPassword\Model\NullPaymentPassword;
use Sdk\PaymentPassword\Utils\MockFactory;
use Sdk\PaymentPassword\Translator\PaymentPasswordRestfulTranslator;

class PaymentPasswordRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PaymentPasswordRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends PaymentPasswordRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPaymentPasswordAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PaymentPassword\Adapter\PaymentPassword\IPaymentPasswordAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('paymentPasswords', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\PaymentPassword\Translator\PaymentPasswordRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PAYMENT_PASSWORD_LIST',
                PaymentPasswordRestfulAdapter::SCENARIOS['PAYMENT_PASSWORD_LIST']
            ],
            [
                'PAYMENT_PASSWORD_FETCH_ONE',
                PaymentPasswordRestfulAdapter::SCENARIOS['PAYMENT_PASSWORD_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $paymentPassword = MockFactory::generatePaymentPasswordObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullPaymentPassword())
            ->willReturn($paymentPassword);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($paymentPassword, $result);
    }

        /**
     * 为PaymentPasswordRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$paymentPassword，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function preparePaymentPasswordTranslator(
        PaymentPassword $paymentPassword,
        array $keys,
        array $paymentPasswordArray
    ) {
        $translator = $this->prophesize(PaymentPasswordRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($paymentPassword),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($paymentPasswordArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(PaymentPassword $paymentPassword)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($paymentPassword);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'password',
                'memberAccount',
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('paymentPasswords', $paymentPasswordArray);

        $this->success($paymentPassword);

        $result = $this->stub->add($paymentPassword);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'password',
                'memberAccount',
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('paymentPasswords', $paymentPasswordArray);

        $this->failure($paymentPassword);
        $result = $this->stub->add($paymentPassword);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'oldPassword',
                'password'
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'paymentPasswords/'.$paymentPassword->getId().'/updatePassword',
                $paymentPasswordArray
            );

        $this->success($paymentPassword);

        $result = $this->stub->edit($paymentPassword);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'oldPassword',
                'password'
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'paymentPasswords/'.$paymentPassword->getId().'/updatePassword',
                $paymentPasswordArray
            );

        $this->failure($paymentPassword);
        $result = $this->stub->edit($paymentPassword);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testResetSuccess()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'cellphone',
                'password'
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'paymentPasswords/resetPassword',
                $paymentPasswordArray
            );

        $this->success($paymentPassword);

        $result = $this->stub->resetPaymentPassword($paymentPassword);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePaymentPasswordTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testResetFailure()
    {
        $paymentPassword = MockFactory::generatePaymentPasswordObject(1);
        $paymentPasswordArray = array();

        $this->preparePaymentPasswordTranslator(
            $paymentPassword,
            array(
                'cellphone',
                'password'
            ),
            $paymentPasswordArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'paymentPasswords/resetPassword',
                $paymentPasswordArray
            );

        $this->failure($paymentPassword);
        $result = $this->stub->resetPaymentPassword($paymentPassword);
        $this->assertFalse($result);
    }
}
