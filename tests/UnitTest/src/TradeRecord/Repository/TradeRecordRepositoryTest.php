<?php
namespace Sdk\TradeRecord\Repository;

use Sdk\TradeRecord\Adapter\TradeRecord\TradeRecordRestfulAdapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\TradeRecord\Utils\MockFactory;

class TradeRecordRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TradeRecordRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends TradeRecordRepository {
            public function getAdapter() : TradeRecordRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\TradeRecord\Adapter\TradeRecord\TradeRecordRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为TradeRecordRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以TradeRecordRepository::OA_LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(TradeRecordRestfulAdapter::class);
        $adapter->scenario(Argument::exact(TradeRecordRepository::OA_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(TradeRecordRepository::OA_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
