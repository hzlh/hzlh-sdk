<?php
namespace Sdk\TradeRecord\Adapter\TradeRecord;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\TradeRecord\Model\TradeRecord;
use Sdk\TradeRecord\Model\NullTradeRecord;
use Sdk\TradeRecord\Utils\MockFactory;
use Sdk\TradeRecord\Translator\TradeRecordRestfulTranslator;

class TradeRecordRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TradeRecordRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction'
            ])->getMock();

        $this->childStub = new class extends TradeRecordRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsITradeRecordAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\TradeRecord\Adapter\TradeRecord\ITradeRecordAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('tradeRecords', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\TradeRecord\Translator\TradeRecordRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_TRADE_RECORD_LIST',
                TradeRecordRestfulAdapter::SCENARIOS['OA_TRADE_RECORD_LIST']
            ],
            [
                'PORTAL_TRADE_RECORD_LIST',
                TradeRecordRestfulAdapter::SCENARIOS['PORTAL_TRADE_RECORD_LIST']
            ],
            [
                'TRADE_RECORD_FETCH_ONE',
                TradeRecordRestfulAdapter::SCENARIOS['TRADE_RECORD_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $tradeRecord = MockFactory::generateTradeRecordObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullTradeRecord())
            ->willReturn($tradeRecord);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($tradeRecord, $result);
    }
}
