<?php
namespace Sdk\TradeRecord\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\MemberAccount\Model\MemberAccount;

class TradeRecordTest extends TestCase
{
    private $stub;

    private $faker;

    public function setUp()
    {
        $this->stub = new TradeRecord();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->faker);
    }

    public function testCorrentConstruct()
    {
        $this->assertEquals(0, $this->stub->getId());
        $this->assertEquals(0, $this->stub->getReferenceId());
        $this->assertInstanceOf(
            'Sdk\MemberAccount\Model\MemberAccount',
            $this->stub->getMemberAccount()
        );
        $this->assertEquals(0, $this->stub->getTradeTime());
        $this->assertEquals(TradeRecord::TRADE_RECORD_TYPES['NULL'], $this->stub->getType());
        $this->assertEquals(0.0, $this->stub->getTradeMoney());
        $this->assertEmpty($this->stub->getDebtor());
        $this->assertEmpty($this->stub->getCreditor());
        $this->assertEquals(0.0, $this->stub->getBalance());
        $this->assertEmpty($this->stub->getComment());
        $this->assertEquals(TradeRecord::STATUS_NORMAL, $this->stub->getStatus());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $id = $this->faker->randomNumber();

        $this->stub->setId($id);
        $this->assertEquals($id, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //referenceId 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setReferenceId() 正确的传参类型,期望传值正确
     */
    public function testSetReferenceIdCorrectType()
    {
        $referenceId = $this->faker->randomNumber();

        $this->stub->setReferenceId($referenceId);
        $this->assertEquals($referenceId, $this->stub->getReferenceId());
    }

    /**
     * 设置 TradeRecord setReferenceId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReferenceIdWrongType()
    {
        $this->stub->setReferenceId(array(1,2,3));
    }
    //referenceId 测试 -------------------------------------------------------   end
     
    //memberAccount 测试 ---------------------------------------------------- start
    /**
     * 设置 setMemberAccount() 正确的传参类型,期望传值正确
     */
    public function testSetMemberAccountCorrectType()
    {
        $memberAccount = new MemberAccount();

        $this->stub->setMemberAccount($memberAccount);
        $this->assertEquals($memberAccount, $this->stub->getMemberAccount());
    }

    /**
     * 设置 setMemberAccount() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberAccountWrongType()
    {
        $memberAccount = array($this->faker->randomNumber());

        $this->stub->setMemberAccount($memberAccount);
    }
    //memberAccount 测试 ----------------------------------------------------   end
    
    //tradeTime 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setTradeTime() 正确的传参类型,期望传值正确
     */
    public function testSetTradeTimeCorrectType()
    {
        $tradeTime = $this->faker->randomNumber();

        $this->stub->setTradeTime($tradeTime);
        $this->assertEquals($tradeTime, $this->stub->getTradeTime());
    }

    /**
     * 设置 TradeRecord setTradeTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTradeTimeWrongType()
    {
        $this->stub->setTradeTime(array(1,2,3));
    }
    //tradeTime 测试 -------------------------------------------------------   end
    
    //type 测试 ------------------------------------------------------ start
    /**
     * 循环测试 TradeRecord setType() 是否符合预定范围
     *
     * @dataProvider typeProvider
     */
    public function testSetType($actual, $expected)
    {
        $this->stub->setType($actual);
        $this->assertEquals($expected, $this->stub->getType());
    }
    /**
     * 循环测试 Order setType() 数据构建器
     */
    public function typeProvider()
    {
        return array(
            array(
                TradeRecord::TRADE_RECORD_TYPES['DEPOSIT'],
                TradeRecord::TRADE_RECORD_TYPES['DEPOSIT']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM'],
                TradeRecord::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'],
                TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM'],
                TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE'],
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE'],
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'],
                TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL'],
                TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL']
            ),
            array(
                TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM'],
                TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM']
            ),
            array(999,TradeRecord::TRADE_RECORD_TYPES['NULL']),
        );
    }
    /**
     * 设置 TradeRecord setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTypeWrongType()
    {
        $this->stub->setType('string');
    }
    //type 测试 ------------------------------------------------------   end
    
    //tradeMoney 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setTradeMoney() 正确的传参类型,期望传值正确
     */
    public function testSetTradeMoneyCorrectType()
    {
        $tradeMoney = $this->faker->randomNumber();

        $this->stub->setTradeMoney($tradeMoney);
        $this->assertEquals($tradeMoney, $this->stub->getTradeMoney());
    }

    /**
     * 设置 TradeRecord setTradeMoney() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTradeMoneyWrongType()
    {
        $this->stub->setTradeMoney(array(1,2,3));
    }
    //tradeMoney 测试 -------------------------------------------------------   end
    
    //debtor 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setDebtor() 正确的传参类型,期望传值正确
     */
    public function testSetDebtorCorrectType()
    {
        $debtor = $this->faker->word();

        $this->stub->setDebtor($debtor);
        $this->assertEquals($debtor, $this->stub->getDebtor());
    }

    /**
     * 设置 TradeRecord setDebtor() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDebtorWrongType()
    {
        $this->stub->setDebtor(array(1,2,3));
    }
    //debtor 测试 -------------------------------------------------------   end
    
    //creditor 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setCreditor() 正确的传参类型,期望传值正确
     */
    public function testSetCreditorCorrectType()
    {
        $creditor = $this->faker->word();

        $this->stub->setCreditor($creditor);
        $this->assertEquals($creditor, $this->stub->getCreditor());
    }

    /**
     * 设置 TradeRecord setCreditor() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditorWrongType()
    {
        $this->stub->setCreditor(array(1,2,3));
    }
    //creditor 测试 -------------------------------------------------------   end
    
    //balance 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setBalance() 正确的传参类型,期望传值正确
     */
    public function testSetBalanceCorrectType()
    {
        $balance = $this->faker->randomNumber();

        $this->stub->setBalance($balance);
        $this->assertEquals($balance, $this->stub->getBalance());
    }

    /**
     * 设置 TradeRecord setBalance() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBalanceWrongType()
    {
        $this->stub->setBalance(array(1,2,3));
    }
    //balance 测试 -------------------------------------------------------   end
    
    //comment 测试 ------------------------------------------------------- start
    /**
     * 设置 TradeRecord setComment() 正确的传参类型,期望传值正确
     */
    public function testSetCommentCorrectType()
    {
        $comment = $this->faker->word();

        $this->stub->setComment($comment);
        $this->assertEquals($comment, $this->stub->getComment());
    }

    /**
     * 设置 TradeRecord setComment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCommentWrongType()
    {
        $this->stub->setComment(array(1,2,3));
    }
    //comment 测试 -------------------------------------------------------   end
    
    //status 测试 ---------------------------------------------------------- start
    /**
     * 设置 setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $status = $this->faker->randomNumber();

        $this->stub->setStatus($status);
        $this->assertEquals($status, $this->stub->getStatus());
    }
    //status 测试 ----------------------------------------------------------   end
}
