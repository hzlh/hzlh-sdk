<?php
namespace Sdk\TradeRecord\Utils;

use Sdk\TradeRecord\Model\TradeRecord;

class MockFactory
{
    /**
     * [generateTradeRecordArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateTradeRecordArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $tradeRecord = array();

        $tradeRecord = array(
            'data'=>array(
                'type'=>'tradeRecords',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //referenceId
        $referenceId = isset($value['referenceId']) ? $value['referenceId'] : $faker->numerify();
        $attributes['referenceId'] = $referenceId;

        //type
        $attributes['type'] = self::generateType($tradeRecord, $faker, $value);

        //tradeTime
        $attributes['tradeTime'] = isset($value['tradeTime']) ? $value['tradeTime'] : $faker->unixTime();

        //tradeMoney
        $attributes['tradeMoney'] = isset($value['tradeMoney']) ? $value['tradeMoney'] : $faker->numerify();

        //debtor
        $attributes['debtor'] = isset($value['debtor']) ? $value['debtor'] : $faker->word();

        //creditor
        $attributes['creditor'] = isset($value['creditor']) ? $value['creditor'] : $faker->word();

        //balance
        $attributes['balance'] = isset($value['balance']) ? $value['balance'] : $faker->numerify();

        //comment
        $attributes['comment'] = isset($value['comment']) ? $value['comment'] : $faker->word();

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $tradeRecord['data']['attributes'] = $attributes;

        //memberAccount
        $tradeRecord['data']['relationships']['memberAccount']['data'] = array(
            'type' => 'memberAccounts',
            'id' => $faker->randomNumber(1)
        );


        return $tradeRecord;
    }
    /**
     * [generateTradeRecordObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateTradeRecordObject(int $id = 0, int $seed = 0, array $value = array()) : TradeRecord
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $tradeRecord = new TradeRecord($id);

        //referenceId
        $referenceId = isset($value['referenceId']) ? $value['referenceId'] : $faker->numerify();
        $tradeRecord->setReferenceId($referenceId);

        //tradeTime
        $tradeRecord->setTradeTime($faker->unixTime());

        //type
        self::generateType($tradeRecord, $faker, $value);

        //tradeMoney
        $tradeMoney = isset($value['tradeMoney']) ? $value['tradeMoney'] : $faker->numerify();
        $tradeRecord->setTradeMoney($tradeMoney);

        //debtor
        $debtor = isset($value['debtor']) ? $value['debtor'] : $faker->word();
        $tradeRecord->setDebtor($debtor);

        //creditor
        $creditor = isset($value['creditor']) ? $value['creditor'] : $faker->word();
        $tradeRecord->setCreditor($creditor);

        //balance
        $balance = isset($value['balance']) ? $value['balance'] : $faker->numerify();
        $tradeRecord->setBalance($balance);

        //comment
        $comment = isset($value['comment']) ? $value['comment'] : $faker->word();
        $tradeRecord->setComment($comment);

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $tradeRecord->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $tradeRecord->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $tradeRecord->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $tradeRecord->setStatus($status);

        //memberAccount
        $memberAccount = self::generateMemberAccount($faker, $value);
        $tradeRecord->setMemberAccount($memberAccount);

        return $tradeRecord;
    }

    private static function generateMemberAccount($faker, array $value = array())
    {
        return isset($value['memberAccount']) ?
            $value['memberAccount'] :
            \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject($faker->numerify(), $faker->numerify());
    }

    private static function generateType($object, $faker, $value) : void
    {
        $type = isset($value['type'])
        ? $value['type']
        : $faker->randomElement(
            TradeRecord::TRADE_RECORD_TYPES
        );
        $object->setType($type);
    }
}
