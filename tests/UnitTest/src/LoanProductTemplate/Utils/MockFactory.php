<?php
namespace Sdk\LoanProductTemplate\Utils;

use Sdk\LoanProductTemplate\Model\LoanProductTemplate;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateLoanProductTemplateArray 生成模板信息数组]
     * @return [array] [模板数组]
     */
    public static function generateLoanProductTemplateArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $loanProductTemplate = array();

        $loanProductTemplate = array(
            'data'=>array(
                'type'=>'loanProductTemplates',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //fileType
        $fileType = self::generateFileType($faker, $value);
        $attributes['fileType'] = $fileType;
        //file
        $file = self::generateFile($faker, $value);
        $attributes['file'] = $file;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $loanProductTemplate['data']['attributes'] = $attributes;

        //enterprise
        $loanProductTemplate['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprise',
            'id' => $faker->randomNumber(1)
        );
        
        return $loanProductTemplate;
    }
    /**
     * [generateLoanProductTemplateObject 生成模板对象]
     * @param  int|integer $id    [模板Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [模板对象]
     */
    public static function generateLoanProductTemplateObject(int $id = 0, int $seed = 0, array $value = array()) : LoanProductTemplate
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $loanProductTemplate = new LoanProductTemplate($id);

        //name
        $name = self::generateName($faker, $value);
        $loanProductTemplate->setName($name);
        //fileType
        $fileType = self::generateFileType($faker, $value);
        $loanProductTemplate->setFileType($fileType);
        //file
        $file = self::generateFile($faker, $value);
        $loanProductTemplate->setFile($file);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $loanProductTemplate->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $loanProductTemplate->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $loanProductTemplate->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $loanProductTemplate->setStatus($status);
        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $loanProductTemplate->setEnterprise($enterprise);

        return $loanProductTemplate;
    }

    private static function generateName($faker, array $value = array())
    {
        return $name = isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateFileType($faker, array $value = array())
    {
        return $fileType = isset($value['fileType']) ?
        $value['fileType'] : $faker->numberBetween($min = 1, $max = 4);
    }

    private static function generateFile($faker, array $value = array())
    {
        unset($faker);
        return $file = isset($value['file']) ?
            $value['file'] : array('name' =>'file', 'identify' => 'file.png');
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
        $value['enterprise'] : \Sdk\Enterprise\Utils\MockFactory::generateEnterpriseObject($faker->numerify(), $faker->numerify());
    }
}
