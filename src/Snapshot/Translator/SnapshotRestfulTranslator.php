<?php
namespace Sdk\Snapshot\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Snapshot\Model\Snapshot;
use Sdk\Snapshot\Model\NullSnapshot;

class SnapshotRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getTranslatorFactory()
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $snapshot = null)
    {
        return $this->translateToObject($expression, $snapshot);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $snapshot = null)
    {
        if (empty($expression)) {
            return NullSnapshot::getInstance();
        }

        if ($snapshot == null) {
            $snapshot = new Snapshot();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $snapshot->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['category'])) {
            $snapshot->setCategory($attributes['category']);
        }
        if (isset($attributes['createTime'])) {
            $snapshot->setCreateTime($attributes['createTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['snapshotObject']['data'])) {
            $snapshotObject = $this->changeArrayFormat($relationships['snapshotObject']['data']);

            $translator = $this->getTranslatorFactory()->getTranslator($attributes['category']);
            
            $snapshot->setSnapshotObject($translator->arrayToObject($snapshotObject));
        }

        return $snapshot;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($snapshot, array $keys = array())
    {
        if (!$snapshot instanceof Snapshot) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id'
            );
        }

        $expression = array(
                'type' => 'snapshots'
        );

        if (in_array('id', $keys)) {
            $expression['id'] = $snapshot->getId();
        }

        return $expression;
    }
}
