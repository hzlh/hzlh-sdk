<?php
namespace Sdk\Snapshot\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

use Sdk\Snapshot\Model\ISnapshotAble;

class TranslatorFactory
{
    const MAPS = array(
        ISnapshotAble::CATEGORY['SERVICE']=>
        'Sdk\Service\Translator\ServiceRestfulTranslator',
        ISnapshotAble::CATEGORY['DELIVERY_ADDRESS']=>
        'Sdk\DeliveryAddress\Translator\DeliveryAddressRestfulTranslator',
        ISnapshotAble::CATEGORY['LOAN_PRODUCT']=>
        'Sdk\LoanProduct\Translator\LoanProductRestfulTranslator'
    );

    public function getTranslator(int $category) : ITranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
