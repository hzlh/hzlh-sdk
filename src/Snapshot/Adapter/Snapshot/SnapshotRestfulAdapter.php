<?php
namespace Sdk\Snapshot\Adapter\Snapshot;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Snapshot\Model\Snapshot;
use Sdk\Snapshot\Model\NullSnapshot;
use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

class SnapshotRestfulAdapter extends GuzzleAdapter implements ISnapshotAdapter
{
    use FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'SNAPSHOT_LIST'=>[
                'fields'=>[],
                'include'=> 'snapshotObject'
            ],
            'SNAPSHOT_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'snapshotObject'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new SnapshotRestfulTranslator();
        $this->resource = 'snapshots';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullSnapshot::getInstance());
    }
}
