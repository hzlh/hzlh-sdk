<?php
namespace Sdk\Snapshot\Adapter\Snapshot;

use Sdk\Common\Adapter\IFetchAbleAdapter;

interface ISnapshotAdapter extends IFetchAbleAdapter
{
}
