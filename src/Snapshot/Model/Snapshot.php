<?php
namespace Sdk\Snapshot\Model;

use Sdk\Snapshot\Repository\SnapshotRepository;

use Sdk\Service\Model\NullService;

class Snapshot
{
    protected $id;

    protected $snapshotObject;

    protected $category;

    protected $createTime;


    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->snapshotObject = new NullService();
        $this->category = ISnapshotAble::CATEGORY['SERVICE'];
        $this->createTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->snapshotObject);
        unset($this->category);
        unset($this->createTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSnapshotObject(ISnapshotAble $snapshotObject) : void
    {
        $this->snapshotObject = $snapshotObject;
    }

    public function getSnapshotObject() : ISnapshotAble
    {
        return $this->snapshotObject;
    }
    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, ISnapshotAble::CATEGORY) ?
        $category : ISnapshotAble::CATEGORY['NULL'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setCreateTime(int $createTime) : void
    {
        $this->createTime = $createTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }
}
