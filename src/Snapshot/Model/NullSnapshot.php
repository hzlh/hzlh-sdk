<?php
namespace Sdk\Snapshot\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullSnapshot extends Snapshot implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
