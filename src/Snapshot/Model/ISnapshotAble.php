<?php
namespace Sdk\Snapshot\Model;

interface ISnapshotAble
{
    const CATEGORY = array(
        'NULL' => 0,
        'SERVICE' => 1,
        'DELIVERY_ADDRESS' => 2,
        'LOAN_PRODUCT' => 3
    );
}
