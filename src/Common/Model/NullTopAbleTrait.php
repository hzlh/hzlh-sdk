<?php
namespace Sdk\Common\Model;

trait NullTopAbleTrait
{
    public function top() : bool
    {
        return $this->resourceNotExist();
    }

    public function cancelTop() : bool
    {
        return $this->resourceNotExist();
    }

    public function isStick() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}
