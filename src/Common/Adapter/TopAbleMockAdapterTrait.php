<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\ITopAble;

trait TopAbleMockAdapterTrait
{
    public function top(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }
}
