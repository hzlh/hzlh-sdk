<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Command\CancelTopCommand;

abstract class CancelTopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(CancelTopCommand $command)
    {
        $this->topAble = $this->fetchITopObject($command->id);

        return $this->topAble->cancelTop();
    }
}
