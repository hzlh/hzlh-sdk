<?php
namespace Sdk\MoneyWiseNews\Model;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\IOnShelfAble;

class MoneyWiseNewsModelFactory
{
    const STICK_CN = array(
        ITopAble::STICK['ENABLED'] => '置顶',
        ITopAble::STICK['DISABLED'] => '取消置顶'
    );

    const STATUS_CN = array(
        IOnShelfAble::STATUS['ONSHELF'] => '上架',
        IOnShelfAble::STATUS['OFFSTOCK'] => '下架'
    );
}
