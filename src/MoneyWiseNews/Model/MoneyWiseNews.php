<?php
namespace Sdk\MoneyWiseNews\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IOnShelfAble;
use Sdk\Common\Model\TopAbleTrait;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\OnShelfAbleTrait;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IOnShelfAbleAdapter;

use Sdk\Crew\Model\Crew;

use Sdk\Dictionary\Model\Dictionary;

use Sdk\MoneyWiseNews\Repository\MoneyWiseNewsRepository;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class MoneyWiseNews implements IObject, IOperatAble, IOnShelfAble, ITopAble
{
    use Object, OperatAbleTrait, OnShelfAbleTrait, TopAbleTrait;

    /**
     * [$id id]
     * @var [int]
     */
    private $id;
    /**
     * [$title 标题]
     * @var [string]
     */
    private $title;
    /**
     * [$source 来源]
     * @var [string]
     */
    private $source;
    /**
     * [$number 编号]
     * @var [string]
     */
    private $number;
    /**
     * [$detail 详情]
     * @var [array]
     */
    private $detail;
    /**
     * [$description 描述]
     * @var [string]
     */
    private $description;
    /**
     * [$cover 封面图]
     * @var [array]
     */
    private $cover;
    /**
     * [$collection 收藏量]
     * @var [int]
     */
    private $collection;
    /**
     * [$pageViews 浏览量]
     * @var [int]
     */
    private $pageViews;
    /**
     * [$category 所属分类]
     * @var [Dictionary]
     */
    private $category;
    /**
     * [$crew 操作人员]
     * @var [Crew]
     */
    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->number = '';
        $this->source = '';
        $this->detail = array();
        $this->description = '';
        $this->cover = array();
        $this->collection = 0;
        $this->pageViews = 0;
        $this->category = new Dictionary();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IOnShelfAble::STATUS['ONSHELF'];
        $this->stick = ITopAble::STICK['DISABLED'];
        $this->statusTime = 0;
        $this->repository = new MoneyWiseNewsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->number);
        unset($this->source);
        unset($this->detail);
        unset($this->description);
        unset($this->cover);
        unset($this->collection);
        unset($this->pageViews);
        unset($this->category);
        unset($this->crew);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->stick);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setSource(string $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setCollection(int $collection) : void
    {
        $this->collection = $collection;
    }

    public function getCollection() : int
    {
        return $this->collection;
    }

    public function setPageViews(int $pageViews) : void
    {
        $this->pageViews = $pageViews;
    }

    public function getPageViews() : int
    {
        return $this->pageViews;
    }
    
    public function setCategory(Dictionary $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : Dictionary
    {
        return $this->category;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : MoneyWiseNewsRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return $this->getRepository();
    }
}
