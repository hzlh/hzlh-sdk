<?php
namespace Sdk\MoneyWiseNews\Adapter\MoneyWiseNews;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IOnShelfAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IMoneyWiseNewsAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter, ITopAbleAdapter //phpcs:ignore
{
}
