<?php
namespace Sdk\MoneyWiseNews\Adapter\MoneyWiseNews;

use Sdk\Common\Adapter\TopAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\OnShelfAbleMockAdapterTrait;

use Sdk\MoneyWiseNews\Model\MoneyWiseNews;
use Sdk\MoneyWiseNews\Utils\MockFactory;

class MoneyWiseNewsMockAdapter implements IMoneyWiseNewsAdapter
{
    use OperatAbleMockAdapterTrait, OnShelfAbleMockAdapterTrait, TopAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateMoneyWiseNewsObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $moneyWiseNewsList = array();

        foreach ($ids as $id) {
            $moneyWiseNewsList[] = MockFactory::generateMoneyWiseNewsObject($id);
        }

        return $moneyWiseNewsList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateMoneyWiseNewsObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateMoneyWiseNewsObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
