<?php
namespace Sdk\MoneyWiseNews\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\TopAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\OnShelfAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\MoneyWiseNews\Model\MoneyWiseNews;
use Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\IMoneyWiseNewsAdapter;
use Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsMockAdapter;
use Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class MoneyWiseNewsRepository extends Repository implements IMoneyWiseNewsAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        TopAbleRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_MONEY_WISE_NEWS_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_MONEY_WISE_NEWS_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'MONEY_WISE_NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MoneyWiseNewsRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IMoneyWiseNewsAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IMoneyWiseNewsAdapter
    {
        return new MoneyWiseNewsMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
