<?php
namespace Sdk\MoneyWiseNews\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\MoneyWiseNews\Model\MoneyWiseNews;
use Sdk\MoneyWiseNews\Model\NullMoneyWiseNews;
use Sdk\MoneyWiseNews\Model\MoneyWiseNewsModelFactory;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class MoneyWiseNewsRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    public function arrayToObject(array $expression, $moneyWiseNews = null)
    {
        return $this->translateToObject($expression, $moneyWiseNews);
    }

    protected function translateToObject(array $expression, $moneyWiseNews = null)
    {
        if (empty($expression)) {
            return NullMoneyWiseNews::getInstance();
        }
        if ($moneyWiseNews === null) {
            $moneyWiseNews = new MoneyWiseNews();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $moneyWiseNews->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $moneyWiseNews->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $moneyWiseNews->setNumber($attributes['number']);
        }
        if (isset($attributes['source'])) {
            $moneyWiseNews->setSource($attributes['source']);
        }
        if (isset($attributes['cover'])) {
            $moneyWiseNews->setCover($attributes['cover']);
        }
        if (isset($attributes['detail'])) {
            $moneyWiseNews->setDetail($attributes['detail']);
        }
        if (isset($attributes['description'])) {
            $moneyWiseNews->setDescription($attributes['description']);
        }
        if (isset($attributes['collection'])) {
            $moneyWiseNews->setCollection($attributes['collection']);
        }
        if (isset($attributes['pageViews'])) {
            $moneyWiseNews->setPageViews($attributes['pageViews']);
        }
        if (isset($attributes['stick'])) {
            $moneyWiseNews->setStick($attributes['stick']);
        }
        if (isset($attributes['statusTime'])) {
            $moneyWiseNews->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $moneyWiseNews->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $moneyWiseNews->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $moneyWiseNews->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $moneyWiseNews->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        if (isset($relationships['category']['data'])) {
            $category = $this->changeArrayFormat($relationships['category']['data']);
            $moneyWiseNews->setCategory($this->getDictionaryRestfulTranslator()->arrayToObject($category));
        }

        return $moneyWiseNews;
    }

    public function objectToArray($moneyWiseNews, array $keys = array())
    {
        $expression = array();

        if (!$moneyWiseNews instanceof MoneyWiseNews) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'source',
                'detail',
                'description',
                'cover',
                'crew',
                'category'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'news'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $moneyWiseNews->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $moneyWiseNews->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $moneyWiseNews->getSource();
        }
        if (in_array('detail', $keys)) {
            $attributes['detail'] = $moneyWiseNews->getDetail();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $moneyWiseNews->getDescription();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $moneyWiseNews->getCover();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $moneyWiseNews->getCrew()->getId()
                )
            );
        }

        if (in_array('category', $keys)) {
            $expression['data']['relationships']['category']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $moneyWiseNews->getCategory()->getId()
                )
            );
        }

        return $expression;
    }
}
