<?php
namespace Sdk\Staff\Adapter\Staff;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Staff\Model\Staff;

use Marmot\Interfaces\IAsyncAdapter;

interface IStaffAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{

}
