<?php
namespace Sdk\Staff\Adapter\Staff;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Staff\Model\Staff;
use Sdk\Staff\Model\NullStaff;
use Sdk\Staff\Translator\StaffRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class StaffRestfulAdapter extends GuzzleAdapter implements IStaffAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        EnableAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'STAFF_LIST'=>[
            'fields'=>[
                'staffs'=>'realName,avatar,cellphone,professionalTitles,briefIntroduction,isRecommendHomePage,createTime,updateTime,status'
            ],
            'include' => 'enterprise',
        ],
        'STAFF_FETCH_ONE'=>[
            'fields'=>[],
            'include' => 'enterprise',
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new StaffRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'staffs';
    }

    protected function getResource()
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors(): array
    {
        $mapError = [
            100 => STAFF_CELLPHONE_EXIST
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullStaff::getInstance());
    }

    protected function addAction(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'education',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    protected function editAction(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'education',
            )
        );

        $this->patch(
            $this->getResource().'/'.$staff->getId(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function deletes(Staff $staff) : bool
    {
        $this->delete(
            $this->getResource().'/'.$staff->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }
        return false;
    }

    public function recommendHomePage(Staff $staff) : bool
    {
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/recommendHomePage'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }
        return false;
    }

    public function cancelRecommendHomePage(Staff $staff) : bool
    {
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/cancelRecommendHomePage'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }
        return false;
    }
}
