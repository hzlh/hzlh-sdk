<?php
namespace Sdk\Staff\Translator;

use Sdk\Staff\Model\Staff;
use Sdk\Staff\Model\NullStaff;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\User\Translator\UserRestfulTranslator;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class StaffRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $staff = null)
    {
        return $this->translateToObject($expression, $staff);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $staff = null)
    {
        if (empty($expression)) {
            return NullStaff::getInstance();
        }

        if ($staff == null) {
            $staff = new Staff();
        }

        $staff = parent::translateToObject($expression, $staff);

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $staff->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['birthday'])) {
            $staff->setBirthday($attributes['birthday']);
        }
        if (isset($attributes['education'])) {
            $staff->setEducation($attributes['education']);
        }
        if (isset($attributes['cardId'])) {
            $staff->setCardId($attributes['cardId']);
        }
        if (isset($attributes['briefIntroduction'])) {
            $staff->setBriefIntroduction($attributes['briefIntroduction']);
        }
        if (isset($attributes['professionalTitles'])) {
            $staff->setProfessionalTitle($attributes['professionalTitles']);
        }
        if (isset($attributes['isRecommendHomePage'])) {
            $staff->setIsRecommendHomePage($attributes['isRecommendHomePage']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $staff->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $staff;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($staff, array $keys = array())
    {
        $user = parent::objectToArray($staff, $keys);

        if (!$staff instanceof Staff) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'birthday',
                'education',
                'cardId',
                'briefIntroduction',
                'professionalTitle',
                'isRecommendHomePage',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'staffs'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $staff->getId();
        }

        $attributes = array();

        if (in_array('birthday', $keys)) {
            $attributes['birthday'] = $staff->getBirthday();
        }
        if (in_array('education', $keys)) {
            $attributes['education'] = $staff->getEducation();
        }
        if (in_array('cardId', $keys)) {
            $attributes['cardId'] = $staff->getCardId();
        }
        if (in_array('briefIntroduction', $keys)) {
            $attributes['briefIntroduction'] = $staff->getBriefIntroduction();
        }
        if (in_array('professionalTitle', $keys)) {
            $attributes['professionalTitle'] = $staff->getProfessionalTitle();
        }
        if (in_array('isRecommendHomePage', $keys)) {
            $attributes['isRecommendHomePage'] = $staff->getIsRecommendHomePage();
        }

        $expression['data']['attributes'] = $attributes;

        $expression['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type'=>'enterprises',
                    'id'=>$staff->getEnterprise()->getId()
                )
            );
        }

        return $expression;
    }
}
