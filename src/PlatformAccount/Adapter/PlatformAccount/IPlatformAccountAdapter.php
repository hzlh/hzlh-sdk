<?php
namespace Sdk\PlatformAccount\Adapter\PlatformAccount;

use Sdk\PlatformAccount\Model\PlatformAccount;

interface IPlatformAccountAdapter
{
    public function fetchPlatformAccount() : PlatformAccount;
}
