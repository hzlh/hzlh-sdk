<?php
namespace Sdk\PlatformAccount\Adapter\PlatformAccount;

use Sdk\PlatformAccount\Model\PlatformAccount;
use Sdk\PlatformAccount\Utils\MockFactory;

class PlatformAccountMockAdapter implements IPlatformAccountAdapter
{
    public function fetchPlatformAccount() : PlatformAccount
    {
        return MockFactory::generatePlatformAccountObject();
    }
}
