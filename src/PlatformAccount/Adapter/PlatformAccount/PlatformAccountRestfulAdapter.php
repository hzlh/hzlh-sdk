<?php
namespace Sdk\PlatformAccount\Adapter\PlatformAccount;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\PlatformAccount\Model\PlatformAccount;
use Sdk\PlatformAccount\Model\NullPlatformAccount;

use Sdk\PlatformAccount\Translator\PlatformAccountRestfulTranslator;

class PlatformAccountRestfulAdapter extends GuzzleAdapter implements IPlatformAccountAdapter
{
    const SCENARIOS = [
        'PLATFORM_ACCOUNT_FETCH_ONE' => [
            'fields' => []
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct($uri, $authKey);
        $this->translator = new PlatformAccountRestfulTranslator();
        $this->resource = 'platformAccount';
        $this->scenario = array();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchPlatformAccount() : PlatformAccount
    {
        $this->get(
            $this->getResource()
        );

        return $this->isSuccess() ? $this->translateToObject() : NullPlatformAccount::getInstance();
    }
}
