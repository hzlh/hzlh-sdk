<?php
namespace Sdk\PlatformAccount\Model;

class PlatformAccount
{
    private $accountBalance;

    private $incomeAmount;

    private $expenditureAmount;

    private $profitAmount;

    public function __construct()
    {
        $this->accountBalance = 0.0;
        $this->incomeAmount = 0.0;
        $this->expenditureAmount = 0.0;
        $this->profitAmount = 0.0;
    }

    public function __destruct()
    {
        unset($this->accountBalance);
        unset($this->incomeAmount);
        unset($this->expenditureAmount);
        unset($this->profitAmount);
    }

    public function setAccountBalance(float $accountBalance) : void
    {
        $this->accountBalance = $accountBalance;
    }

    public function getAccountBalance() : float
    {
        return $this->accountBalance;
    }
    
    public function setIncomeAmount(float $incomeAmount) : void
    {
        $this->incomeAmount = $incomeAmount;
    }

    public function getIncomeAmount() : float
    {
        return $this->incomeAmount;
    }
    
    public function setExpenditureAmount(float $expenditureAmount) : void
    {
        $this->expenditureAmount = $expenditureAmount;
    }

    public function getExpenditureAmount() : float
    {
        return $this->expenditureAmount;
    }
    
    public function setProfitAmount(float $profitAmount) : void
    {
        $this->profitAmount = $profitAmount;
    }

    public function getProfitAmount() : float
    {
        return $this->profitAmount;
    }
}
