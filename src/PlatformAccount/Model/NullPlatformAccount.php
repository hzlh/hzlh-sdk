<?php
namespace Sdk\PlatformAccount\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullPlatformAccount extends PlatformAccount implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
