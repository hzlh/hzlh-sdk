<?php
namespace Sdk\AccountTemplate\Adapter\AccountTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IAccountTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
