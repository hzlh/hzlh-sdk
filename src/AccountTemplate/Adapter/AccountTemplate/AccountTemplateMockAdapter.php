<?php
namespace Sdk\AccountTemplate\Adapter\AccountTemplate;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

use Sdk\AccountTemplate\Model\AccountTemplate;
use Sdk\AccountTemplate\Utils\MockFactory;

class AccountTemplateMockAdapter implements IAccountTemplateAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateAccountTemplateObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $accountTemplateList = array();

        foreach ($ids as $id) {
            $accountTemplateList[] = MockFactory::generateAccountTemplateObject($id);
        }

        return $accountTemplateList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}