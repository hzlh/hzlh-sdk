<?php
namespace Sdk\AccountTemplate\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Dictionary\Model\Dictionary;

use Sdk\AccountTemplate\Repository\AccountTemplateRepository;

class AccountTemplate implements IObject,IOperatAble
{
    use Object, OperatAbleTrait;

    const TYPE_VAT = array(
        'SMALL_SCALE' => 1, //小规模纳税人
        'NORMAL' => 2 //一般纳税人
    );

    const ACCOUNTING_STANDARD = array(
        'SMALL_ENTERPRISE' => 1, //小企业会计准则
        'ENTERPRISE' => 2, //企业会计准则
        'ORGANIZATION' => 3 //民间非营利组织会计制度
    );

    const VOUCHER_APPROVAL = array(
        'NO' => 0, //不审核
        'YES' => 2 //审核
    );

    const STATUS = array(
        'NORMAL' => 0, //使用中
        'DELETED' => -2 //已删除
    );

    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 账套名]
     * @var [string]
     */
    private $name;
    /**
     * [$industry 行业]
     * @var [Dictionary]
     */
    private $industry;
    /**
     * [$typeVat 增值税种类]
     * @var [int]
     */
    private $typeVat;
    /**
     * [$activationDate 启用年月]
     * @var [date]
     */
    private $activationDate;
    /**
     * [$accountingStandard 会计准则]
     * @var [int]
     */
    private $accountingStandard;
    /**
     * [$voucherApproval 凭证审核]
     * @var [int]
     */
    private $voucherApproval;
    /**
     * [$enterprise 企业信息]
     * @var [Enterprise]
     */
    private $enterprise;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->industry = new Dictionary();
        $this->typeVat = self::TYPE_VAT['SMALL_SCALE'];
        $this->activationDate = 0;
        $this->accountingStandard = self::ACCOUNTING_STANDARD['SMALL_ENTERPRISE'];
        $this->voucherApproval = self::VOUCHER_APPROVAL['NO'];
        $this->enterprise = new Enterprise();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->repository = new AccountTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->industry);
        unset($this->typeVat);
        unset($this->activationDate);
        unset($this->accountingStandard);
        unset($this->voucherApproval);
        unset($this->enterprise);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setIndustry(Dictionary $industry) : void
    {
        $this->industry = $industry;
    }

    public function getIndustry() : Dictionary
    {
        return $this->industry;
    }

    public function setTypeVat(int $typeVat): void
    {
        $this->typeVat = $typeVat;
    }

    public function getTypeVat(): int
    {
        return $this->typeVat;
    }

    public function setActivationDate(int $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function getActivationDate(): int
    {
        return $this->activationDate;
    }

    public function setAccountingStandard(int $accountingStandard): void
    {
        $this->accountingStandard = $accountingStandard;
    }

    public function getAccountingStandard(): int
    {
        return $this->accountingStandard;
    }

    public function setVoucherApproval(int $voucherApproval): void
    {
        $this->voucherApproval = $voucherApproval;
    }

    public function getVoucherApproval(): int
    {
        return $this->voucherApproval;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::STATUS)) ?
            $status : self::STATUS['NORMAL'];
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    protected function getRepository(): AccountTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}