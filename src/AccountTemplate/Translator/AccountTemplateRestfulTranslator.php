<?php
namespace Sdk\AccountTemplate\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\AccountTemplate\Model\AccountTemplate;
use Sdk\AccountTemplate\Model\NullAccountTemplate;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

class AccountTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    public function arrayToObject(array $expression, $accountTemplate = null)
    {
        return $this->translateToObject($expression, $accountTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $accountTemplate = null)
    {
        if (empty($expression)) {
            return NullAccountTemplate::getInstance();
        }

        if ($accountTemplate == null) {
            $accountTemplate = new AccountTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $accountTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $accountTemplate->setName($attributes['name']);
        }
        if (isset($attributes['typeVat'])) {
            $accountTemplate->setTypeVat($attributes['typeVat']);
        }
        if (isset($attributes['activationDate'])) {
            $accountTemplate->setActivationDate($attributes['activationDate']);
        }
        if (isset($attributes['accountingStandard'])) {
            $accountTemplate->setAccountingStandard($attributes['accountingStandard']);
        }
        if (isset($attributes['voucherApproval'])) {
            $accountTemplate->setVoucherApproval($attributes['voucherApproval']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $accountTemplate->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }
        if (isset($relationships['industry']['data'])) {
            $industry = $this->changeArrayFormat($relationships['industry']['data']);
            $accountTemplate->setIndustry(
                $this->getDictionaryRestfulTranslator()->arrayToObject($industry)
            );
        }

        return $accountTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($accountTemplate, array $keys = array())
    {
        if (!$accountTemplate instanceof AccountTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'typeVat',
                'activationDate',
                'accountingStandard',
                'voucherApproval',
                'industry',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'accountTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $accountTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $accountTemplate->getName();
        }
        if (in_array('typeVat', $keys)) {
            $attributes['typeVat'] = $accountTemplate->getTypeVat();
        }
        if (in_array('activationDate', $keys)) {
            $attributes['activationDate'] = $accountTemplate->getActivationDate();
        }
        if (in_array('accountingStandard', $keys)) {
            $attributes['accountingStandard'] = $accountTemplate->getAccountingStandard();
        }
        if (in_array('voucherApproval', $keys)) {
            $attributes['voucherApproval'] = $accountTemplate->getVoucherApproval();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type'=>'enterprises',
                    'id'=>$accountTemplate->getEnterprise()->getId()
                )
            );
        }

        if (in_array('industry', $keys)) {
            $expression['data']['relationships']['industry']['data'] = array(
                array(
                    'type'=>'dictionaries',
                    'id'=>$accountTemplate->getIndustry()->getId()
                )
            );
        }

        return $expression;
    }
}