<?php
namespace Sdk\AccountTemplate\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\AccountTemplate\Adapter\AccountTemplate\IAccountTemplateAdapter;
use Sdk\AccountTemplate\Adapter\AccountTemplate\AccountTemplateMockAdapter;
use Sdk\AccountTemplate\Adapter\AccountTemplate\AccountTemplateRestfulAdapter;
use Sdk\AccountTemplate\Model\AccountTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class AccountTemplateRepository extends Repository implements IAccountTemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ACCOUNT_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'ACCOUNT_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AccountTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IAccountTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IAccountTemplateAdapter
    {
        return new AccountTemplateRestfulAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(AccountTemplate $accountTemplate) : bool
    {
        return $this->getAdapter()->deletes($accountTemplate);
    }
}