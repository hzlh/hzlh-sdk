<?php
/**
 * 1-99 系统错误规范
 * 100-1000 通用错误规范
 * 1001-2000 用户通用错误提示规范
 * 2001-3000 前台用户错误提示规范
 * 3001-4000 实名认证错误提示规范
 * 4001-5000 企业错误提示规范
 * 5001-6000 员工错误提示规范
 * 6001-7000 政策错误提示规范
 * 7001-8000 政策解读错误提示规范
 * 8001-9000 发文部门错误提示规范
 * 9001-10000 标签错误提示规范
 * 10001-11000 服务分类错误提示规范
 * 11001-12000 认证成为服务商错误提示规范
 * 12001-13000 服务需求错误提示规范
 * 13001-14000 服务错误提示规范
 * 14001-15000 收货地址提示规范
 * 15001-16000 支付密码提示规范
 * 16001-17000 支付密码提示规范
 * 17001-18000 提现提示规范
 * 18001-19000 充值提示规范
 * 19001-20000 支付提示规范
 * 20001-21000 优惠券提示规范
 * 21001-22000 服务订单提示规范
 * 22001-23000 金融机构身份认证提示规范
 * 23001-24000 贷款产品提示规范
 * 24001-25000 预约申请提示规范
 * 25001-26000 财务问答提示规范
 * 26001-27000 企业员工提示规范
 * 26001-27000 企业员工提示规范
 * 27001-28000 字典管理提示规范
 * 28001-29000 财智新规提示规范
 */
/**
 * csrf 验证失效
 */
define('CSRF_VERIFY_FAILURE', 15);
/**
 * 滑动验证失败
 */
define('AFS_VERIFY_FAILURE', 16);
/**
 * 用户未登录
 */
define('NEED_SIGNIN', 17);
/**
 * 短信发送太频繁
 */
define('SMS_SEND_TOO_QUICK', 18);
/**
 * 数据重复
 */
define('PARAMETER_IS_UNIQUE', 100);
/**
 * 数据格式不正确
 */
define('PARAMETER_FORMAT_INCORRECT', 101);
/**
 * 参数不能为空
 */
define('PARAMETER_IS_EMPTY', 102);
/**
 * 状态已上架
 */
define('RESOURCE_STATUS_ON_SHELF', 103);
/**
 * 状态已下架
 */
define('RESOURCE_STATUS_OFF_STOCK', 104);
/**
 * 审核状态非待审核
 */
define('RESOURCE_STATUS_NOT_PENDING', 105);
/**
 * 审核状态非已拒绝
 */
define('RESOURCE_STATUS_NOT_REJECT', 106);
/**
 * 状态非上架状态
 */
define('RESOURCE_STATUS_NOT_ON_SHELF', 107);
/**
 * 状态非正常状态
 */
define('RESOURCE_STATUS_NOT_NORMAL', 108);
/**
 * 状态已禁用
 */
define('RESOURCE_STATUS_DISABLED', 109);
/**
 * 状态已启用
 */
define('RESOURCE_STATUS_ENABLED', 110);
/**
 * 用户未进行实名认证
 */
define('USER_NOT_NATURAL_PERSON_AUTHENTICATION', 111);
/**
 * 用户未进行企业认证
 */
define('USER_NOT_ENTERPRISE_AUTHENTICATION', 112);
/**
 * 状态已置顶
 */
define('RESOURCE_STICK_ENABLED', 114);
/**
 * 状态未置顶
 */
define('RESOURCE_STICK_DISABLED', 115);
/**
 * 置顶数已达到最大限制
 */
define('RESOURCE_STICK_IS_FULL', 116);
/**
 * 图片格式不正确
 */
define('IMAGE_FORMAT_ERROR', 201);
/**
 * 附件格式不正确
 */
define('ATTACHMENT_FORMAT_ERROR', 202);
/**
 * 姓名格式不正确
 */
define('REAL_NAME_FORMAT_ERROR', 203);
/**
 * 手机号格式不正确
 */
define('CELLPHONE_FORMAT_ERROR', 204);
/**
 * 价格格式不正确
 */
define('PRICE_FORMAT_ERROR', 205);
/**
 * 身份证格式不正确
 */
define('CARDID_FORMAT_ERROR', 206);
/**
 * 日期格式不正确
 */
define('DATE_FORMAT_ERROR', 207);
/**
 * 名称格式不正确
 */
define('NAME_FORMAT_ERROR', 208);
/**
 * 详细地址格式不正确
 */
define('ADDRESS_FORMAT_ERROR', 209);
/**
 * url格式不正确
 */
define('URL_FORMAT_ERROR', 210);
/**
 * 标题格式不正确
 */
define('TITLE_FORMAT_ERROR', 211);
/**
 * 描述格式不正确
 */
define('DESCRIPTION_FORMAT_ERROR', 212);
/**
 * 驳回原因格式不正确
 */
define('REJECT_REASON_FORMAT_ERROR', 213);
/**
 * 详情格式不正确
 */
define('DETAIL_FORMAT_ERROR', 214);
/**
 * 最低值不能高于最高值
 */
define('MIN_VALUE_CANNOT_THAN_MAX_VALUE', 215);
/**
 * 不能选择之前的时间
 */
define('CANNOT_SELECT_PREVIOUS_TIME', 216);
/**
 * 状态非已驳回或已撤销
 */
define('RESOURCE_STATUS_NOT_REJECT_OR_REVOKED', 217);
/**
 * 状态非已通过
 */
define('RESOURCE_STATUS_NOT_APPROVE', 218);

// 用户通用----------------------------------------------------------------
/**
 * 性别类型不存在
 */
define('GENDER_TYPE_NOT_EXIST', 1001);
/**
 * 密码格式不正确
 */
define('PASSWORD_FORMAT_ERROR', 1002);
/**
 * 旧密码不正确
 */
define('OLD_PASSWORD_INCORRECT', 1003);
/**
 * 密码错误
 */
define('PASSWORD_INCORRECT', 1004);
/**
 * 手机号已存在
 */
define('CELLPHONE_EXIST', 1005);
/**
 * 账号不存在
 */
define('CELLPHONE_NOT_EXIST', 1006);
/**
 * 验证码错误
 */
define('CAPTCHA_ERROR', 1007);
/**
 * 密码与确认密码不一致
 */
define('INCONSISTENT_PASSWORD', 1008);
/**
 * 该账户已禁用
 */
define('STATUS_DISABLED', 1009);
/**
 * 确认密码必须和新密码一致
 */
define('INCONSISTENT_NEW_PASSWORD', 1010);
/**
 * 生日格式错误
 */
define('BIRTHDAY_FORMAT_ERROR', 1011);
// 前台用户----------------------------------------------------------------
/**
 * 昵称格式不正确
 */
define('NICK_NAME_FORMAT_ERROR', 2001);
/**
 * 简介格式不正确
 */
define('BRIEF_INTRODUCTION_FORMAT_ERROR', 2002);
// 企业----------------------------------------------------------------
/**
 * 统一社会信用代码格式不正确
 */
define('UNIFIED_SOCIAL_CREDIT_CODE_FORMAT_ERROR', 4001);
/**
 * 该企业已存在
 */
define('ENTERPRISE_EXIST', 4002);
/**
 * 联系人电话格式不正确
 */
define('CONTACTS_PPHONE_FORMAT_ERROR', 4003);
// 政策----------------------------------------------------------------
/**
 * 适用对象不存在
 */
define('APPLICABLE_OBJECT_NOT_EXIST', 6001);
/**
 * 适用行业不存在
 */
define('APPLICABLE_INDUSTRIES_NOT_EXIST', 6002);
/**
 * 政策级别不存在
 */
define('LEVEL_NOT_EXIST', 6003);
/**
 * 政策分类不存在
 */
define('CLASSIFY_NOT_EXIST', 6004);
/**
 * 受理地址格式不正确
 */
define('ADMISSIBLE_ADDRESS_FORMAT_ERROR', 6005);
/**
 * 政策标签格式不正确
 */
define('POLICY_LABEL_FORMAT_ERROR', 6006);
/**
 * 政策关联产品格式不正确
 */
define('POLICY_PRODUCTS_FORMAT_ERROR', 6007);
// 发文部门----------------------------------------------------------------
/**
 * 部门名称格式不正确
 */
define('DISPATCH_DEPARTMENT_NAME_FORMAT_ERROR', 8001);
/**
 * 备注格式不正确
 */
define('DISPATCH_DEPARTMENT_REMARK_FORMAT_ERROR', 8002);
/**
 * 发文部门名称已存在
 */
define('DISPATCH_DEPARTMENT_NAME_EXIST', 8003);
// 标签----------------------------------------------------------------
/**
 * 标签名称格式不正确
 */
define('LABEL_NAME_FORMAT_ERROR', 9001);
/**
 * 标签分类不存在
 */
define('LABEL_CATEGORY_NOT_EXIST', 9002);
/**
 * 标签备注格式不正确
 */
define('LABEL_REMARK_FORMAT_ERROR', 9003);
/**
 * 标签名称已存在
 */
define('LABEL_NAME_EXIST', 9004);
// 服务分类----------------------------------------------------------------
/**
 * 服务分类名称格式不正确
 */
define('SERVICE_CATEGORY_NAME_FORMAT_ERROR', 10001);
/**
 * 服务分类资质认证名称格式不正确
 */
define('SERVICE_CATEGORY_QUALIFICATION_NAME_FORMAT_ERROR', 10002);
/**
 * 是否需要资质认证分类不存在
 */
define('SERVICE_CATEGORY_IS_QUALIFICATION_NOT_EXIST', 10003);
/**
 * 分类已存在
 */
define('CATEGORY_IS_EXIST', 10004);
// 发布需求----------------------------------------------------------------
/**
 * 需求标题格式不正确
 */
define('SERVICE_REQUIREMENT_TITLE_FORMAT_ERROR', 12001);
/**
 * 最大价格不能低于最小价格
 */
define('MIN_PRICE_CAN_NOT_THAN_MAX_PRICE', 12002);
// 发布服务----------------------------------------------------------------
/**
 * 服务标题格式不正确
 */
define('SERVICE_TITLE_FORMAT_ERROR', 13001);

/**
 * 服务对象不存在
 */
define('SERVICE_OBJECT_NOT_EXIST', 13002);
/**
 * 服务价格格式不正确
 */
define('SERVICE_PRICE_FORMAT_ERROR', 13003);

/**
 * 服务合同格式不正确
 */
define('SERVICE_CONTRACT_FORMAT_ERROR', 13004);

/**
 * 该企业未认证该分类
 */
define('ENTERPRISE_NOT_AUTHENTICATION_SERVICE_CATEGORY', 13005);

// 收货地址
/**
 * 默认地址状态不正确
 */
define('DEFAULT_ADDRESS_STATUS_ERROR', 14001);
/**
 * 邮政编码格式不正确
 */
define('POSTAL_CODE_FORMAT_ERROR', 14002);
/**
 * 默认地址类型不存在
 */
define('IS_DEFAULT_ADDRESS_NOT_EXIST', 14003);

// 支付密码
/**
 * 支付密码格式不正确
 */
define('PAYMENT_PASSWORD_FORMAT_ERROR', 15001);
/**
 * 当前支付密码不正确
 */
define('OLD_PAYMENT_PASSWORD_INCORRECT', 15002);
/**
 * 支付密码不正确
 */
define('PAYMENT_PASSWORD_INCORRECT', 15003);
/**
 * 新密码不能与旧密码一致
 */
define('NEW_PASSWORD_CANNOT_BE_SAME_AS_OLD_PASSWORD', 15004);
/**
 * 未设置支付密码
 */
define('PAYMENT_PASSWORD_NOT_SET', 15005);

// 银行卡
/**
 * 账户类型不存在
 */
define('BANK_CARD_ACCOUNT_TYPE_NOT_EXIST', 16001);
/**
 * 银行类型不存在
 */
define('BANK_CARD_TYPE_NOT_EXIST', 16002);
/**
 * 卡号格式不正确
 */
define('BANK_CARD_NUMBER_FORMAT_ERROR', 16003);
/**
 * 解绑原因不存在
 */
define('BANK_CARD_UNBIND_REASON_NOT_EXIST', 16004);
/**
 * 银行卡已存在
 */
define('BANK_CARD_EXIST', 16005);

// 提现
/**
 * 账户余额不足
 */
define('ACCOUNT_BALANCE_NOT_ENOUGH', 17001);
/**
 * 账户未绑定银行卡
 */
define('ACCOUNT_NOT_BIND_BANK_CARD', 17002);
/**
 * 提现状态非待转账
 */
define('WITHDRAWAL_STATUS_NOT_PENDING', 17003);
/**
 * 提现金额不合理
 */
define('WITHDRAWAL_AMOUNT_UNREASONABLE', 17004);

//充值
/**
 * 充值金额不合理
 */
define('DEPOSIT_AMOUNT_UNREASONABLE', 18001);

// 优惠劵---------------------------------------------------------------
/**
 * 优惠券类型不存在
 */
define('COUPON_TYPE_NOT_EXIST', 20001);
/**
 * 折扣值格式不正确
 */
define('DISCOUNT_FORMAT_ERROR', 20002);
/**
 * 总发行量格式不正确
 */
define('ISSUE_TOTAL_FORMAT_ERROR', 20003);
/**
 * 每人限领格式不正确
 */
define('QUOTA_FORMAT_ERROR', 20004);
/**
 * 优惠券领取方式类型不存在
 */
define('RECEIVING_MODE_NOT_EXIST', 20005);
/**
 * 优惠券可领取用户类型不存在
 */
define('RECEIVING_USERS_NOT_EXIST', 20006);
/**
 * 优惠券是否和店铺优惠券叠加使用类型不存在
 */
define('IS_SUPERPOSITION_NOT_EXIST', 20007);
/**
 * 优惠券使用范围类型不存在
 */
define('APPLY_SCOPE_NOT_EXIST', 20008);
/**
 * 判断当前发布优惠券的是平台还是某一店铺类型不存在
 */
define('RELEASE_TYPE_NOT_EXIST', 20009);
/**
 * 优惠劵状态为已删除
 */
define('COUPON_STATUS_IS_DELETED', 20010);
/**
 * 优惠劵名称格式错误
 */
define('COUPON_NAME_FORMAT_ERROR', 20011);
/**
 * 已领取的优惠券数量超过卖家规定数量
 */
define('EXCEEDING_SPECIFIED_QUANTITY', 20012);
/**
 * 优惠券已经被领完了，快滚
 */
define('COUPON_TOTAL_IS_NULL', 20013);
/**
 * 每人限领不能大于总发行量
 */
define('QUOTA_NOT_GREATER_THAN_ISSUE_TOTAL', 20014);
/**
 * 使用门槛不能小于优惠券面额
 */
define('USE_STANDARD_GREATER_THAN_DENOMINATION', 20015);
/**
 * 请输入0.01-999.99的优惠券面额
 */
define('DENOMINATION_MAX_OVER_LIMIT', 20016);
/**
 * 使用门槛最高为99999.99元
 */
define('USE_STANDARD_MAX_OVER_LIMIT', 20017);
/**
 * 优惠券使用场景不存在
 */
define('USE_SCENARIO_NOT_EXIST', 20018);
/**
 * 领取方式与使用场景不符
 */
define('MODE_IS_INCONSISTENT_WITH_SCENARIO', 20019);

//订单
//商品失效
define('COMMODITY_FAILURE', 21001);
//sku价格失效
define('SKU_FAILURE', 21002);
//订单状态非待付款
define('ORDER_STATUS_NOT_PENDING', 21003);
//订单状态非已付款
define('ORDER_STATUS_NOT_PAID', 21004);
//订单状态非履约开始
define('ORDER_STATUS_NOT_PERFORMANCE_BEGIN', 21005);
//订单状态非履约结束
define('ORDER_STATUS_NOT_PERFORMANCE_END', 21006);
//订单状态非买家确认
define('ORDER_STATUS_NOT_BUYER_CONFIRMATION', 21007);
//订单状态非删除
define('ORDER_STATUS_NOT_DELETE', 21008);
//订单状态非取消状态或完成状态
define('ORDER_STATUS_NOT_CENCEL_OR_COMPLETED', 21009);
//订单金额太低,没办法使用优惠劵
define('CANNOT_USE_COUPON', 21010);
//商家优惠不能优惠到一元以下
define('CANNOT_USE_BUSINESS_PREFERNTIAL', 21011);
//买家取消订单原因不存在
define('ORDER_BUYER_CANCEL_REASON_NOT_EXIST', 21012);
//卖家取消订单原因不存在
define('ORDER_SELLER_CANCEL_REASON_NOT_EXIST', 21013);
//商家优惠金额格式不正确
define('BUSINESS_PREFERNTIAL_AMOUNT_FORMAT_ERROR', 21014);
//收货地址失效
define('DELIVERY_ADDRESS_FAILURE', 21015);
//优惠劵超出使用数量
define('MEMBER_COUPON_MORE_THAN_SPECIFIED', 21016);
//优惠劵不支持叠加使用
define('MEMBER_COUPON_NOT_SUPERPOSITION_USE', 21017);
//优惠劵状态非待使用
define('MEMBER_COUPON_STATUS_NOT_UNUSER', 21018);
//订单金额不满足优惠劵使用条件
define('MEMBER_COUPON_NOT_MEET_CONCESSION', 21019);
//订单金额不满足优惠劵使用范围
define('MEMBER_COUPON_NOT_APPLY_SCOPE', 21020);
//订单金额修改超出指定范围
define('UPDATE_ORDER_AMOUNT_OUT_OF_RANGE', 21021);
//订单支付金额不正确
define('ORDER_PAIND_AMOUNT_INCORRECT', 21022);
//修改订单价格备注不正确
define('UPDATE_ORDER_AMOUNT_REMARK_FORMAT_ERROR', 21023);
//优惠劵不在使用时间段内
define('MEMBER_COUPON_NOT_IN_USE_PERIOD', 21024);

//金融机构身份认证
//机构编码格式不正确
define('ORGANIZATIONS_CODE_FORMAT_ERROR', 22001);
//机构类型不存在
define('ORGANIZATIONS_CATEGORY_NOT_EXIST', 22002);
//机构代码格式不正确
define('ORGANIZATION_CODE_FORMAT_ERROR', 22003);

//贷款产品
//贷款产品标题格式不正确.
define('LOAN_PRODUCT_TITLE_FORMAT_ERROR', 23001);
//贷款产品对象不存在.
define('LOAN_PRODUCT_OBJECT_NOT_EXIST', 23002);
//贷款产品担保方式不存在.
define('LOAN_PRODUCT_GUARANTY_STYLES_NOT_EXIST', 23003);
//贷款产品特点格式不正确.
define('LOAN_PRODUCT_LABELS_FORMAT_ERROR', 23004);
//贷款产品支持城市格式不正确.
define('LOAN_PRODUCT_SUPPORT_CITY_FORMAT_ERROR', 23005);
//贷款产品贷款利率格式不正确.
define('LOAN_PRODUCT_LOAN_INTEREST_RATE_FORMAT_ERROR', 23006);
//贷款产品还款方式不存在.
define('LOAN_PRODUCT_REPAYMENT_METHOD_NOT_EXIST', 23007);
//贷款产品是否支持提前还款类型不存在.
define('LOAN_PRODUCT_IS_SUPPORT_EARLY_REPAYMENT_NOT_EXIST', 23008);
//贷款产品是否存在提前还款费用类型不存在.
define('LOAN_PRODUCT_IS_EXIST_EARLY_REPAYMENT_COST_NOT_EXIST', 23009);
//贷款产品所需材料格式不正确.
define('LOAN_PRODUCT_APPLICATION_MATERIAL_FORMAT_ERROR', 23010);
//贷款产品申请条件格式不正确.
define('LOAN_PRODUCT_APPLICATION_CONDITION_FORMAT_ERROR', 23011);
//贷款产品合同格式不正确.
define('LOAN_PRODUCT_CONTRACT_FORMAT_ERROR', 23012);
//贷款产品申请书格式不正确.
define('LOAN_PRODUCT_APPLICATION_FORMAT_ERROR', 23013);
//贷款产品贷款额度格式不正确
define('LOAN_PRODUCT_LOAN_AMOUNT_FORMAT_ERROR', 23014);
//贷款产品贷款期限格式不正确
define('LOAN_PRODUCT_LOAN_TERM_FORMAT_ERROR', 23015);
//贷款产品放款期限格式不正确
define('LOAN_PRODUCT_LOAN_PERIOD_FORMAT_ERROR', 23016);
//提前还款期限格式错误
define('LOAN_PRODUCT_EARLY_REPAYMENT_TERM_FORMAT_ERROR', 23017);
//贷款产品快照 ID 错误
define('LOAN_PRODUCT_SNAPSHOT_ID_ERROR', 23018);

//贷款产品对象不存在.
define('APPOINTMENT_LOAN_OBJECT_NOT_EXIST', 24001);
//申请资料附件格式不正确.
define('APPOINTMENT_ATTACHMENTS_FORMAT_ERROR', 24002);
//卖家备注格式不正确.
define('APPOINTMENT_REMARK_FORMAT_ERROR', 24003);
//贷款状态不存在.
define('APPOINTMENT_LOAN_STATUS_NOT_EXIST', 24004);
//贷款合同编号格式不正确.
define('APPOINTMENT_LOAN_CONTRACT_NUMBER_FORMAT_ERROR', 24005);
//贷款失败原因格式不正确.
define('APPOINTMENT_LOAN_FAIL_REASON_FORMAT_ERROR', 24006);
//贷款额度超出范围
define('APPOINTMENT_LOAN_AMOUNT_OUT_OF_RANGE', 24007);
//贷款期限超出范围
define('APPOINTMENT_LOAN_TERM_OUT_OF_RANGE', 24008);
//实名认证后才可以申请贷款.
define('APPOINTMENT_NEED_NATURAL_PERSON_AUTHENTICATION', 24010);
//企业认证后才可以申请贷款.
define('APPOINTMENT_NEED_ENTERPRISE_AUTHENTICATION', 24011);

//财务问答

//问题内容格式错误
define('QUESTION_CONTENT_FORMAT_ERROR', 25001);
//问答删除原因格式错误
define('QA_DELETE_REASON_FORMAT_ERROR', 25002);
//回答内容格式错误
define('ANSWER_CONTENT_FORMAT_ERROR', 25003);
//删除类型不存在
define('QA_DELETE_TYPE_NOT_EXIST', 25004);

//企业员工

//学历不存在
define('STAFF_EDUCATION_NOT_EXIST', 26001);
//简介格式不正确
define('STAFF_BRIEF_INTRODUCTION_FORMAT_ERROR', 26002);
//员工手机号已存在
define('STAFF_CELLPHONE_EXIST', 26003);
//职业资格证书格式不正确
define('CERTIFICATES_FORMAT_ERROR', 26004);
//职称数量超出限制
define('PROFESSIONAL_TITLE_OUT_OF_RANGE', 26005);
//该职称已绑定
define('PROFESSIONAL_TITLE_HAS_BEEN_BIND', 26006);
//未被推荐到主页
define('NOT_RECOMMEND_TO_HOME_PAGE', 26007);
//已被推荐到主页
define('HAVE_RECOMMEND_TO_HOME_PAGE', 26008);
//推荐到主页超出限制
define('STAFF_OUT_OF_RECOMMEND_RANGE', 26009);


//字典管理
//字典名称格式不正确
define('DICTIONARY_NAME_FORMAT_ERROR', 27001);
//字典分类不存在
define('DICTIONARY_CATEGORY_NOT_EXIST', 27002);
//字典级别不存在
define('DICTIONARY_LEVEL_NOT_EXIST', 27003);
//字典名称已经存在
define('DICTIONARY_NAME_IS_EXIST', 27004);

//财智新规管理
//新闻来源格式不正确
define('MONEY_WISE_NEWS_SOURCE_FORMAT_ERROR', 28001);
//新闻分类不存在
define('MONEY_WISE_NEWS_CATEGORY_NOT_EXIST', 28002);
//新闻置顶数已达到最大限制
define('MONEY_WISE_NEWS_STICK_IS_FULL', 28003);

//账套
//增值税种类不存在
define('TYPE_VAT_NOT_EXIST', 29001);
//会计准则种类不存在
define('ACCOUNTING_STANDARD_NOT_EXIST', 29002);
//行业格式不正确
define('INDUSTRY_FORMAT_ERROR', 29003);
//只能有一个免费的帐套
define('GRATIS_ACCOUNT_TEMPLATE_UNIQUE', 29004);
//账套名称已存在
define('ACCOUNT_TEMPLATE_NAME_EXIST', 29005);

//金融产品模板
//模板示例格式错误
define('FILE_FORMAT_ERROR', 30001);
//文件类型不存在
define('FILE_TYPE_NOT_EXIST', 30002);