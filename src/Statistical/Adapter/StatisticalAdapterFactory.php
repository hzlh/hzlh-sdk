<?php
namespace Sdk\Statistical\Adapter;

class StatisticalAdapterFactory
{
    const MAPS = array(
        'staticsActiveEnterprise'=>
        'Sdk\Statistical\Adapter\StaticsActiveEnterpriseAdapter',
        'staticsServiceAuthenticationCount'=>
        'Sdk\Statistical\Adapter\StaticsServiceAuthenticationCountAdapter',
        'staticsServiceRequirementCount'=>
        'Sdk\Statistical\Adapter\StaticsServiceRequirementCountAdapter',
        'staticsServiceCount'=>
        'Sdk\Statistical\Adapter\StaticsServiceCountAdapter',
        'staticsMemberCouponCount'=>
        'Sdk\Statistical\Adapter\StaticsMemberCouponCountAdapter',
        'staticsEnterpriseOrderVolume'=>
        'Sdk\Statistical\Adapter\StaticsEnterpriseOrderVolumeAdapter',
        'staticsLoanProductCount'=>
        'Sdk\Statistical\Adapter\StaticsLoanProductCountAdapter',
        'staticsFinanceCount'=>
        'Sdk\Statistical\Adapter\StaticsFinanceCountAdapter',
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : false;
    }
}
