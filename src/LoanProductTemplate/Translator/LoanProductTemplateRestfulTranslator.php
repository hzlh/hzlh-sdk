<?php
namespace Sdk\LoanProductTemplate\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\LoanProductTemplate\Model\LoanProductTemplate;
use Sdk\LoanProductTemplate\Model\NullLoanProductTemplate;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class LoanProductTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $loanProductTemplate = null)
    {
        return $this->translateToObject($expression, $loanProductTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $loanProductTemplate = null)
    {
        if (empty($expression)) {
            return NullLoanProductTemplate::getInstance();
        }

        if ($loanProductTemplate == null) {
            $loanProductTemplate = new LoanProductTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $loanProductTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $loanProductTemplate->setName($attributes['name']);
        }
        if (isset($attributes['fileType'])) {
            $loanProductTemplate->setFileType($attributes['fileType']);
        }
        if (isset($attributes['file'])) {
            $loanProductTemplate->setFile($attributes['file']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $loanProductTemplate->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $loanProductTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($loanProductTemplate, array $keys = array())
    {
        if (!$loanProductTemplate instanceof LoanProductTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'fileType',
                'file',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'loanProductTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $loanProductTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $loanProductTemplate->getName();
        }
        if (in_array('fileType', $keys)) {
            $attributes['fileType'] = $loanProductTemplate->getFileType();
        }
        if (in_array('file', $keys)) {
            $attributes['file'] = $loanProductTemplate->getFile();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type'=>'enterprises',
                    'id'=>$loanProductTemplate->getEnterprise()->getId()
                )
            );
        }

        return $expression;
    }
}