<?php
namespace Sdk\LoanProductTemplate\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\ILoanProductTemplateAdapter;
use Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateMockAdapter;
use Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateRestfulAdapter;
use Sdk\LoanProductTemplate\Model\LoanProductTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class LoanProductTemplateRepository extends Repository implements ILoanProductTemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'LOAN_PRODUCT_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'LOAN_PRODUCT_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LoanProductTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ILoanProductTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ILoanProductTemplateAdapter
    {
        return new LoanProductTemplateRestfulAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(LoanProductTemplate $loanProductTemplate) : bool
    {
        return $this->getAdapter()->deletes($loanProductTemplate);
    }
}