<?php
namespace Sdk\LoanProductTemplate\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\LoanProductTemplate\Repository\LoanProductTemplateRepository;

class LoanProductTemplate implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const FILE_TYPE = array(
        'IMAGE' => 1,
        'PDF' =>2,
        'XLS' => 3,
        'DOC' => 4
    );

    const FILE_TYPE_FORMAT_WIDGET = array(
        self::FILE_TYPE['IMAGE'] => array('jpg', 'png', 'jpeg'),
        self::FILE_TYPE['PDF'] => array('pdf'),
        self::FILE_TYPE['XLS'] => array('xls', 'xlsx'),
        self::FILE_TYPE['DOC'] => array('doc', 'docx')
    );

    const  FILE_TYPE_FORMAT = array(
        self::FILE_TYPE['IMAGE'] => '图片(jpg,jpeg,png)',
        self::FILE_TYPE['PDF'] => 'PDF文档',
        self::FILE_TYPE['XLS'] => 'xls/xlsx 文档',
        self::FILE_TYPE['DOC'] => 'doc/docx 文档'
    );

    const STATUS = array(
        'NORMAL' => 0, //使用中
        'DELETED' => -2 //已删除
    );

    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 模板名]
     * @var [string]
     */
    private $name;
    /**
     * [$name 文件类型]
     * @var [int]
     */
    private $fileType;
    /**
     * [$name 文件]
     * @var [array]
     */
    private $file;
    /**
     * [$enterprise 企业信息]
     * @var [Enterprise]
     */
    private $enterprise;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->fileType = self::FILE_TYPE['IMAGE'];
        $this->file = array();
        $this->enterprise = new Enterprise();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->repository = new LoanProductTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->fileType);
        unset($this->file);
        unset($this->enterprise);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setFileType(int $fileType): void
    {
        $this->fileType = $fileType;
    }

    public function getFileType(): int
    {
        return $this->fileType;
    }

    public function setFile(array $file): void
    {
        $this->file = $file;
    }

    public function getFile(): array
    {
        return $this->file;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::STATUS)) ?
            $status : self::STATUS['NORMAL'];
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    protected function getRepository(): LoanProductTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}