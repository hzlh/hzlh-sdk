<?php
namespace Sdk\LoanProductTemplate\Adapter\LoanProductTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\LoanProductTemplate\Model\LoanProductTemplate;
use Sdk\LoanProductTemplate\Model\NullLoanProductTemplate;
use Sdk\LoanProductTemplate\Translator\LoanProductTemplateRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class LoanProductTemplateRestfulAdapter extends GuzzleAdapter implements ILoanProductTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'LOAN_PRODUCT_TEMPLATE_LIST'=>[
            'fields'=>[
                'loanProductTemplates'=>'name,fileType,status,createTime,updateTime,enterprise'
            ],
            'include'=> 'enterprise'
        ],
        'LOAN_PRODUCT_TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new LoanProductTemplateRestfulTranslator();
        $this->resource = 'loanProductTemplates';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullLoanProductTemplate::getInstance());
    }

    protected function addAction(LoanProductTemplate $loanProductTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file',
                'enterprise'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProductTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(LoanProductTemplate $loanProductTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file',
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$loanProductTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProductTemplate);
            return true;
        }

        return false;
    }

    public function deletes(LoanProductTemplate $loanProductTemplate) : bool
    {
        $this->delete(
            $this->getResource().'/'.$loanProductTemplate->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProductTemplate);
            return true;
        }
        return false;
    }
}