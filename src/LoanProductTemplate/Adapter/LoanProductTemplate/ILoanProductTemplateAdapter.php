<?php
namespace Sdk\LoanProductTemplate\Adapter\LoanProductTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface ILoanProductTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
