<?php
namespace Sdk\PolicyProduct\Adapter\PolicyProduct;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\PolicyProduct\Model\NullPolicyProduct;
use Sdk\PolicyProduct\Translator\PolicyProductRestfulTranslator;

class PolicyProductRestfulAdapter extends GuzzleAdapter implements IPolicyProductAdapter
{
    use CommonMapErrorsTrait, FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_POLICY_PRODUCT_LIST'=>[
                'fields'=>[],
                'include'=> 'enterprise,product'
            ],
            'PORTAL_POLICY_PRODUCT_LIST'=>[
                'fields'=>[],
                'include'=> 'enterprise,product'
            ],
            'POLICY_PRODUCT_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'enterprise,product'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicyProductRestfulTranslator();
        $this->resource = 'policyProducts';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicyProduct::getInstance());
    }
}
