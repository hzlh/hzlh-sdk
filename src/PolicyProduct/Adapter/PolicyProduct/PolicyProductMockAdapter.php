<?php
namespace Sdk\PolicyProduct\Adapter\PolicyProduct;

use Sdk\PolicyProduct\Utils\MockFactory;

class PolicyProductMockAdapter implements IPolicyProductAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generatePolicyProductObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $policyList = array();

        foreach ($ids as $id) {
            $policyList[] = MockFactory::generatePolicyProductObject($id);
        }

        return $policyList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generatePolicyProductObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generatePolicyProductObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
