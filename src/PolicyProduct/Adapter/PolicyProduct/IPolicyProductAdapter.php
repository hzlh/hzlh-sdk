<?php
namespace Sdk\PolicyProduct\Adapter\PolicyProduct;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IPolicyProductAdapter extends IFetchAbleAdapter, IAsyncAdapter
{

}
