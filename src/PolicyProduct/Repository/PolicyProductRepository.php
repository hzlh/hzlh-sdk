<?php
namespace Sdk\PolicyProduct\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\PolicyProduct\Adapter\PolicyProduct\IPolicyProductAdapter;
use Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductMockAdapter;
use Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductRestfulAdapter;

class PolicyProductRepository extends Repository implements IPolicyProductAdapter
{
    use FetchRepositoryTrait, ErrorRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_POLICY_PRODUCT_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_POLICY_PRODUCT_LIST';
    const FETCH_ONE_MODEL_UN = 'POLICY_PRODUCT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PolicyProductRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPolicyProductAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPolicyProductAdapter
    {
        return new PolicyProductMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
