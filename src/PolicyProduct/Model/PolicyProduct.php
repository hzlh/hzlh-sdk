<?php
namespace Sdk\PolicyProduct\Model;

use Sdk\Service\Model\NullService;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\PolicyProduct\Repository\PolicyProductRepository;

class PolicyProduct
{
    /**
     *
     * @var ['SERVICE'] 服务超市
     * @var ['FINANCE'] 金融超市
     */
    const CATEGORY = array(
        'SERVICE' => 1,
        'FINANCE' => 2
    );

    private $id;

    private $product;

    private $title;

    private $cover;

    private $attribute;

    private $productPrice;

    private $enterprise;

    private $category;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->product = new NullService();
        $this->title = '';
        $this->cover = array();
        $this->attribute = '';
        $this->productPrice = 0.0;
        $this->enterprise = new Enterprise();
        $this->category = self::CATEGORY['SERVICE'];
        $this->policyProductRepository = new PolicyProductRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->product);
        unset($this->title);
        unset($this->cover);
        unset($this->attribute);
        unset($this->productPrice);
        unset($this->enterprise);
        unset($this->category);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }
    
    public function setProduct(IPolicyRelationAble $product) : void
    {
        $this->product = $product;
    }

    public function getProduct() : IPolicyRelationAble
    {
        return $this->product;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setAttribute($attribute) : void
    {
        $this->attribute = $attribute;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function setProductPrice(float $productPrice) : void
    {
        $this->productPrice = $productPrice;
    }

    public function getProductPrice() : float
    {
        return $this->productPrice;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }
    
    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, self::CATEGORY) ? $category : self::CATEGORY['SERVICE'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    protected function getRepository() : PolicyProductRepository
    {
        return $this->repository;
    }
}
