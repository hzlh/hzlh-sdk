<?php
namespace Sdk\PolicyProduct\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\PolicyProduct\Model\PolicyProduct;
use Sdk\PolicyProduct\Model\NullPolicyProduct;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class PolicyProductRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $policyProduct = null)
    {
        return $this->translateToObject($expression, $policyProduct);
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $policyProduct = null)
    {
        if (empty($expression)) {
            return NullPolicyProduct::getInstance();
        }
        if ($policyProduct === null) {
            $policyProduct = new PolicyProduct();
        }
    
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $policyProduct->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $policyProduct->setTitle($attributes['title']);
        }
        if (isset($attributes['attribute'])) {
            $policyProduct->setAttribute($attributes['attribute']);
        }
        if (isset($attributes['cover'])) {
            $policyProduct->setCover($attributes['cover']);
        }
        if (isset($attributes['productPrice'])) {
            $policyProduct->setProductPrice($attributes['productPrice']);
        }
        if (isset($attributes['category'])) {
            $policyProduct->setCategory($attributes['category']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $policyProduct->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        if (isset($relationships['product']['data'])) {
            if (isset($expression['included'])) {
                $product = $this->changeArrayFormat($relationships['product']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $product = $this->changeArrayFormat($relationships['product']['data']);
            }

            $productTranslator = $this->getTranslatorFactory()->getTranslator($attributes['category']);

            $policyProduct->setProduct($productTranslator->arrayToObject($product));
        }

        return $policyProduct;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($policyProduct, array $keys = array())
    {
        $expression = array();

        if (!$policyProduct instanceof PolicyProduct) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'product',
                'category'
            );
        }

        $expression = array(
            'type'=>'policyProducts'
        );

        if (in_array('id', $keys)) {
            $expression['id'] = $policyProduct->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $policyProduct->getTitle();
        }
        if (in_array('attribute', $keys)) {
            $attributes['attribute'] = $policyProduct->getAttribute();
        }
        if (in_array('productPrice', $keys)) {
            $attributes['productPrice'] = $policyProduct->getProductPrice();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $policyProduct->getCategory();
        }
        
        $expression['attributes'] = $attributes;

        if (in_array('product', $keys)) {
            $expression['relationships']['product']['data'] = array(
                array(
                    'type' => 'products',
                    'id' => $policyProduct->getProduct()->getId()
                )
            );
        }

        return $expression;
    }
}
