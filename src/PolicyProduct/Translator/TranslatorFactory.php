<?php
namespace Sdk\PolicyProduct\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\PolicyProduct\Model\PolicyProduct;
use Sdk\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        PolicyProduct::CATEGORY['SERVICE'] =>
        'Sdk\Service\Translator\ServiceRestfulTranslator',
        PolicyProduct::CATEGORY['FINANCE'] =>
        'Sdk\LoanProduct\Translator\LoanProductRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
