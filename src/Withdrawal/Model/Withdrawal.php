<?php
namespace Sdk\Withdrawal\Model;

use Marmot\Core;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\BankCard\Model\BankCard;

use Sdk\Withdrawal\Repository\WithdrawalRepository;

use Sdk\NaturalPerson\Repository\NaturalPersonRepository;

use Sdk\Common\Model\IApplyAble;

use Sdk\TradeRecord\Model\ITradeAble;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class Withdrawal implements IObject, ITradeAble
{
    use Object;

    const WITHDRAWAL_STATUS = array(
        'PENDING' => 0,
        'APPROVE' => 2
    );

    const NATURAL_PERSON_COUNT = 1;

    private $id;

    private $transferVoucher;

    private $memberAccount;

    private $number;

    private $transactionNumber;

    private $bankCard;

    private $amount;

    private $serviceCharge;

    private $paymentPassword;

    private $cellphone;

    private $repository;

    private $naturalPersonRepository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->transferVoucher = array();
        $this->memberAccount = new MemberAccount();
        $this->number = '';
        $this->transactionNumber = '';
        $this->bankCard = new BankCard();
        $this->amount = 0.00;
        $this->serviceCharge = 0.00;
        $this->paymentPassword = '';
        $this->cellphone = '';
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::WITHDRAWAL_STATUS['PENDING'];
        $this->statusTime = 0;
        $this->repository = new WithdrawalRepository();
        $this->naturalPersonRepository = new NaturalPersonRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->transferVoucher);
        unset($this->memberAccount);
        unset($this->number);
        unset($this->transactionNumber);
        unset($this->bankCard);
        unset($this->amount);
        unset($this->serviceCharge);
        unset($this->paymentPassword);
        unset($this->cellphone);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
        unset($this->naturalPersonRepository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTransferVoucher(array $transferVoucher): void
    {
        $this->transferVoucher = $transferVoucher;
    }

    public function getTransferVoucher(): array
    {
        return $this->transferVoucher;
    }

    public function setMemberAccount(MemberAccount $memberAccount): void
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount(): MemberAccount
    {
        return $this->memberAccount;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setTransactionNumber(string $transactionNumber) : void
    {
        $this->transactionNumber = $transactionNumber;
    }

    public function getTransactionNumber() : string
    {
        return $this->transactionNumber;
    }

    public function setBankCard(BankCard $bankCard): void
    {
        $this->bankCard = $bankCard;
    }

    public function getBankCard(): BankCard
    {
        return $this->bankCard;
    }

    public function setCellphone(string $cellphone): void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone(): string
    {
        return $this->cellphone;
    }

    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setServiceCharge(float $serviceCharge): void
    {
        $this->serviceCharge = $serviceCharge;
    }

    public function getServiceCharge(): float
    {
        return $this->serviceCharge;
    }

    public function setPaymentPassword(string $paymentPassword): void
    {
        $this->paymentPassword = $paymentPassword;
    }

    public function getPaymentPassword(): string
    {
        return $this->paymentPassword;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::WITHDRAWAL_STATUS)) ?
            $status : self::WITHDRAWAL_STATUS['PENDING'];
    }

    protected function getRepository(): WithdrawalRepository
    {
        return $this->repository;
    }

    protected function getNaturalPersonRepository(): NaturalPersonRepository
    {
        return $this->naturalPersonRepository;
    }
    
    /**
     * 申请提现
     * @return bool
     */
    public function withdraw() : bool
    {
        if (!$this->isBindNaturalPerson()) {
            Core::setLastError(USER_NOT_NATURAL_PERSON_AUTHENTICATION);
            return false;
        }

        return $this->getRepository()->withdraw($this);
    }

    protected function isBindNaturalPerson() : bool
    {
        $filter['member'] = $this->getMemberAccount()->getId();
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        list($naturalPersonList, $count) = $this->getNaturalPersonRepository()->search($filter);

        unset($naturalPersonList);
        $count = 1;
        return $count == self::NATURAL_PERSON_COUNT;
    }

    /**
     * 确认转账
     * @return bool
     */
    public function transferCompleted(): bool
    {
        if (!$this->isPending()) {
            Core::setLastError(WITHDRAWAL_STATUS_NOT_PENDING);
            return false;
        }

        return $this->getRepository()->transferCompleted($this);
    }

    public function isPending() : bool
    {
        return $this->getStatus() == self::WITHDRAWAL_STATUS['PENDING'];
    }
}
