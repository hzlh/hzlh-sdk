<?php
namespace Sdk\Withdrawal\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;

use Sdk\Withdrawal\Adapter\Withdrawal\IWithdrawalAdapter;
use Sdk\Withdrawal\Adapter\Withdrawal\WithdrawalMockAdapter;
use Sdk\Withdrawal\Adapter\Withdrawal\WithdrawalRestfulAdapter;

use Sdk\Withdrawal\Model\Withdrawal;

class WithdrawalRepository extends Repository implements IWithdrawalAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_WITHDRAWAL_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_WITHDRAWAL_LIST';
    const FETCH_ONE_MODEL_UN = 'WITHDRAWAL_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new WithdrawalRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IWithdrawalAdapter
    {
        return new WithdrawalMockAdapter();
    }

    public function getActualAdapter(): IWithdrawalAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function withdraw(Withdrawal $withdrawal): bool
    {
        return $this->getAdapter()->withdraw($withdrawal);
    }

    public function transferCompleted(Withdrawal $withdrawal): bool
    {
        return $this->getAdapter()->transferCompleted($withdrawal);
    }
}
