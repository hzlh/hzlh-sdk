<?php
namespace Sdk\Withdrawal\Adapter\Withdrawal;

use Sdk\Withdrawal\Model\Withdrawal;
use Sdk\Withdrawal\Utils\MockFactory;

class WithdrawalMockAdapter implements IWithdrawalAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateWithdrawalObject($id);
    }

    public function fetchList(array $ids): array
    {
        $withdrawalList = array();

        foreach ($ids as $id) {
            $withdrawalList[] = MockFactory::generateWithdrawalObject($id);
        }

        return $withdrawalList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateWithdrawalObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $withdrawalList = array();

        foreach ($ids as $id) {
            $withdrawalList[] = MockFactory::generateWithdrawalObject($id);
        }

        return $withdrawalList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function withdraw(Withdrawal $withdrawal): bool
    {
        unset($withdrawal);
        return true;
    }

    public function transferCompleted(Withdrawal $withdrawal): bool
    {
        unset($withdrawal);
        return true;
    }
}
