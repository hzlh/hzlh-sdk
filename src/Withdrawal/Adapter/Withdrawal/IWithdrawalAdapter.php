<?php
namespace Sdk\Withdrawal\Adapter\Withdrawal;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IWithdrawalAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
