<?php
namespace Sdk\Withdrawal\Adapter\Withdrawal;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Withdrawal\Model\Withdrawal;
use Sdk\Withdrawal\Model\NullWithdrawal;
use Sdk\Withdrawal\Translator\WithdrawalRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class WithdrawalRestfulAdapter extends GuzzleAdapter implements IWithdrawalAdapter
{
    use FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_WITHDRAWAL_LIST'=>[
                'fields'=>[
                    'withdrawals'=>'amount,password,bankCard,status,createTime,updateTime',
                ],
                'include'=> 'memberAccount,bankCard'
            ],
            'OA_WITHDRAWAL_LIST'=>[
                'fields'=>[
                    'withdrawals'=>'amount,bankCard,memberAccount,cellphone,status,createTime,updateTime',
                ],
                'include'=> 'memberAccount,bankCard'
            ],
            'WITHDRAWAL_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'memberAccount,memberAccount.member,bankCard,bankCard.bank'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new WithdrawalRestfulTranslator();
        $this->resource = 'withdrawals';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullWithdrawal::getInstance());
    }

    public function withdraw(Withdrawal $withdrawal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $withdrawal,
            array(
                'amount',
                'paymentPassword',
                'bankCard',
                'memberAccount'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($withdrawal);
            return true;
        }

        return false;
    }

    public function transferCompleted(Withdrawal $withdrawal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $withdrawal,
            array('serviceCharge', 'transferVoucher')
        );

        $this->patch(
            $this->getResource().'/'.$withdrawal->getId().'/transferCompleted',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($withdrawal);
            return true;
        }

        return false;
    }
}
