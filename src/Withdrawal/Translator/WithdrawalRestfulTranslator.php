<?php
namespace Sdk\Withdrawal\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Sdk\BankCard\Translator\BankCardRestfulTranslator;

use Sdk\Withdrawal\Model\Withdrawal;
use Sdk\Withdrawal\Model\NullWithdrawal;

class WithdrawalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    public function getBankCardRestfulTranslator()
    {
        return new BankCardRestfulTranslator();
    }

    public function arrayToObject(array $expression, $withdrawal = null)
    {
        return $this->translateToObject($expression, $withdrawal);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $withdrawal = null)
    {
        if (empty($expression)) {
            return NullWithdrawal::getInstance();
        }

        if ($withdrawal == null) {
            $withdrawal = new Withdrawal();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $withdrawal->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['transferVoucher'])) {
            $withdrawal->setTransferVoucher($attributes['transferVoucher']);
        }
        if (isset($attributes['amount'])) {
            $withdrawal->setAmount($attributes['amount']);
        }
        if (isset($attributes['serviceCharge'])) {
            $withdrawal->setServiceCharge($attributes['serviceCharge']);
        }
        if (isset($attributes['number'])) {
            $withdrawal->setNumber($attributes['number']);
        }
        if (isset($attributes['transactionNumber'])) {
            $withdrawal->setTransactionNumber($attributes['transactionNumber']);
        }
        if (isset($attributes['paymentPassword'])) {
            $withdrawal->setPaymentPassword($attributes['paymentPassword']);
        }
        if (isset($attributes['cellphone'])) {
            $withdrawal->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['createTime'])) {
            $withdrawal->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $withdrawal->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $withdrawal->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $withdrawal->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['memberAccount']['data'])) {
            if (isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat(
                    $relationships['memberAccount']['data'],
                    $expression['included']
                );
            }
            if (!isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            }
            $withdrawal->setMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount)
            );
        }

        if (isset($relationships['bankCard']['data'])) {
            if (isset($expression['included'])) {
                $bankCard = $this->changeArrayFormat(
                    $relationships['bankCard']['data'],
                    $expression['included']
                );
            }
            if (!isset($expression['included'])) {
                $bankCard = $this->changeArrayFormat($relationships['bankCard']['data']);
            }
            $withdrawal->setBankCard(
                $this->getBankCardRestfulTranslator()->arrayToObject($bankCard)
            );
        }

        return $withdrawal;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($withdrawal, array $keys = array())
    {
        if (!$withdrawal instanceof Withdrawal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'transferVoucher',
                'amount',
                'serviceCharge',
                'number',
                'transactionNumber',
                'paymentPassword',
                'cellphone',
                'memberAccount',
                'bankCard',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'withdrawals'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $withdrawal->getId();
        }

        $attributes = array();

        if (in_array('transferVoucher', $keys)) {
            $attributes['transferVoucher'] = $withdrawal->getTransferVoucher();
        }
        if (in_array('amount', $keys)) {
            $attributes['amount'] = $withdrawal->getAmount();
        }
        if (in_array('serviceCharge', $keys)) {
            $attributes['serviceCharge'] = $withdrawal->getServiceCharge();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $withdrawal->getNumber();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $withdrawal->getTransactionNumber();
        }
        if (in_array('paymentPassword', $keys)) {
            $attributes['paymentPassword'] = $withdrawal->getPaymentPassword();
        }
        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $withdrawal->getCellphone();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type'=>'memberAccounts',
                    'id'=>$withdrawal->getMemberAccount()->getId()
                )
            );
        }

        if (in_array('bankCard', $keys)) {
            $expression['data']['relationships']['bankCard']['data'] = array(
                array(
                    'type'=>'bankCards',
                    'id'=>$withdrawal->getBankCard()->getId()
                )
            );
        }

        return $expression;
    }
}
