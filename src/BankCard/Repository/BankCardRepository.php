<?php
namespace Sdk\BankCard\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Adapter\BankCard\IBankCardAdapter;
use Sdk\BankCard\Adapter\BankCard\BankCardMockAdapter;
use Sdk\BankCard\Adapter\BankCard\BankCardRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class BankCardRepository extends Repository implements IBankCardAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_BANK_CARD_LIST';
    const FETCH_ONE_MODEL_UN = 'BANK_CARD_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BankCardRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IBankCardAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBankCardAdapter
    {
        return new BankCardMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function bind(BankCard $bankCard) : bool
    {
        return $this->getAdapter()->bind($bankCard);
    }

    public function unBind(BankCard $bankCard) : bool
    {
        return $this->getAdapter()->unBind($bankCard);
    }
}
