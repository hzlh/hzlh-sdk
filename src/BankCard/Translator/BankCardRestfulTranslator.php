<?php
namespace Sdk\BankCard\Translator;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Model\NullBankCard;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Bank\Translator\BankRestfulTranslator;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class BankCardRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getBankRestfulTranslator() : BankRestfulTranslator
    {
        return new BankRestfulTranslator();
    }

    protected function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
    {
        return new MemberAccountRestfulTranslator();
    }

    public function arrayToObject(array $expression, $bankCard = null)
    {
        return $this->translateToObject($expression, $bankCard);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $bankCard = null)
    {
        if (empty($expression)) {
            return NullBankCard::getInstance();
        }

        if ($bankCard == null) {
            $bankCard = new BankCard();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $bankCard->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['accountType'])) {
            $bankCard->setAccountType($attributes['accountType']);
        }
        if (isset($attributes['cardType'])) {
            $bankCard->setCardType($attributes['cardType']);
        }
        if (isset($attributes['cardNumber'])) {
            $bankCard->setCardNumber($attributes['cardNumber']);
        }
        if (isset($attributes['bankBranchArea'])) {
            $bankCard->setBankBranchArea($attributes['bankBranchArea']);
        }
        if (isset($attributes['bankBranchAddress'])) {
            $bankCard->setBankBranchAddress($attributes['bankBranchAddress']);
        }
        if (isset($attributes['cellphone'])) {
            $bankCard->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['cardholderName'])) {
            $bankCard->setCardholderName($attributes['cardholderName']);
        }
        if (isset($attributes['licence'])) {
            $bankCard->setLicence($attributes['licence']);
        }
        if (isset($attributes['unbindReason'])) {
            $bankCard->setUnbindReason($attributes['unbindReason']);
        }
        if (isset($attributes['createTime'])) {
            $bankCard->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $bankCard->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $bankCard->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $bankCard->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['bank']['data'])) {
            $bank = $this->changeArrayFormat($relationships['bank']['data']);
            $bankCard->setBank($this->getBankRestfulTranslator()->arrayToObject($bank));
        }
        if (isset($relationships['memberAccount']['data'])) {
            $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            $bankCard->setMemberAccount($this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount));
        }

        return $bankCard;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($bankCard, array $keys = array())
    {
        $expression = array();

        if (!$bankCard instanceof BankCard) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'accountType',
                'cardType',
                'cardNumber',
                'bankBranchArea',
                'bankBranchAddress',
                'cellphone',
                'cardholderName',
                'licence',
                'unbindReason',
                'memberAccount',
                'paymentPassword',
                'bank'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'bankCards'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $bankCard->getId();
        }

        $attributes = array();

        if (in_array('accountType', $keys)) {
            $attributes['accountType'] = $bankCard->getAccountType();
        }
        if (in_array('cardType', $keys)) {
            $attributes['cardType'] = $bankCard->getCardType();
        }
        if (in_array('cardNumber', $keys)) {
            $attributes['cardNumber'] = $bankCard->getCardNumber();
        }
        if (in_array('bankBranchArea', $keys)) {
            $attributes['bankBranchArea'] = $bankCard->getBankBranchArea();
        }
        if (in_array('bankBranchAddress', $keys)) {
            $attributes['bankBranchAddress'] = $bankCard->getBankBranchAddress();
        }
        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $bankCard->getCellphone();
        }
        if (in_array('cardholderName', $keys)) {
            $attributes['cardholderName'] = $bankCard->getCardholderName();
        }
        if (in_array('licence', $keys)) {
            $attributes['licence'] = $bankCard->getLicence();
        }
        if (in_array('unbindReason', $keys)) {
            $attributes['unbindReason'] = $bankCard->getUnbindReason();
        }
        if (in_array('paymentPassword', $keys)) {
            $attributes['paymentPassword'] = $bankCard->getPaymentPassword();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('bank', $keys)) {
            $expression['data']['relationships']['bank']['data'] = array(
                array(
                    'type' => 'banks',
                    'id' => $bankCard->getBank()->getId()
                )
             );
        }

        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type' => 'memberAccounts',
                    'id' => $bankCard->getMemberAccount()->getId()
                )
             );
        }

        return $expression;
    }
}
