<?php
namespace Sdk\BankCard\Adapter\BankCard;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Model\NullBankCard;
use Sdk\BankCard\Translator\BankCardRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class BankCardRestfulAdapter extends GuzzleAdapter implements IBankCardAdapter
{
    use FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_BANK_CARD_LIST'=>[
                'fields'=>[
                    'bankCards'=>'accountType,cardNumber,cardholderName,status,createTime,updateTime,bank',
                    'banks'=>'name,logo,image'
                ],
                'include'=> 'memberAccount,bank'
            ],
            'BANK_CARD_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'memberAccount,bank'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BankCardRestfulTranslator();
        $this->resource = 'bankCards';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => BANK_CARD_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullBankCard::getInstance());
    }

    public function bind(BankCard $bankCard) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $bankCard,
            array(
                'cardholderName',
                'cardNumber',
                'bankBranchArea',
                'bankBranchAddress',
                'cellphone',
                'licence',
                'accountType',
                'cardType',
                'memberAccount',
                'paymentPassword',
                'bank',
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($bankCard);
            return true;
        }

        return false;
    }

    public function unBind(BankCard $bankCard) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $bankCard,
            array('unbindReason', 'paymentPassword')
        );

        $this->patch(
            $this->getResource().'/'.$bankCard->getId().'/unBind',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($bankCard);
            return true;
        }

        return false;
    }
}
