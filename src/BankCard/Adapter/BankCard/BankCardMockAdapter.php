<?php
namespace Sdk\BankCard\Adapter\BankCard;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Utils\MockFactory;

class BankCardMockAdapter implements IBankCardAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateBankCardObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $bankCardList = array();

        foreach ($ids as $id) {
            $bankCardList[] = MockFactory::generateBankCardObject($id);
        }

        return $bankCardList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateBankCardObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateBankCardObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function bind(BankCard $bankCard) : bool
    {
        unset($bankCard);

        return true;
    }

    public function unBind(BankCard $bankCard) : bool
    {
        unset($bankCard);

        return true;
    }
}
