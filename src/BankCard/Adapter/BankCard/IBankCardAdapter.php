<?php
namespace Sdk\BankCard\Adapter\BankCard;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\BankCard\Model\BankCard;

interface IBankCardAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function bind(BankCard $bankCard) : bool;

    public function unBind(BankCard $bankCard) : bool;
}
