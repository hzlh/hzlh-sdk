<?php
namespace Sdk\BankCard\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullBankCard extends BankCard implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function bind() : bool
    {
        return $this->resourceNotExist();
    }

    public function unBind() : bool
    {
        return $this->resourceNotExist();
    }
}
