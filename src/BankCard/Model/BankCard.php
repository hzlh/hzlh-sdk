<?php
namespace Sdk\BankCard\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Bank\Model\Bank;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\BankCard\Repository\BankCardRepository;

class BankCard implements IObject
{
    use Object;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );
    const ACCOUNT_TYPE = array(
        'PUBLIC' => 1, //对公
        'PRIVATE' => 2, //对私
    );
    const CARD_TYPE = array(
        'DEBIT_CARD' => 1, //借记卡
        'CREDIT_CARD' => 2, //贷记卡
    );
    const UNBIND_REASON = array(
        'OTHER' => 3, //其他
        'WORRY' => 1, //我担心资金安全
        'ABANDON' => 2, //我不再使用该银行卡
    );

    /**
     * @var int $id id
     */
    private $id;
    /**
     * @var int $accountType 账户类型
     */
    private $accountType;
    /**
     * @var int $cardType 银行卡类型
     */
    private $cardType;
    /**
     * @var string $cardNumber 卡号
     */
    private $cardNumber;
    /**
     * @var string $bankBranchArea 开户支行区域
     */
    private $bankBranchArea;
    /**
     * @var string $bankBranchAddress 开户支行地址
     */
    private $bankBranchAddress;
    /**
     * @var string $cellphone 预留手机号
     */
    private $cellphone;
    /**
     * @var string $cardholderName 持卡对象名称
     */
    private $cardholderName;
    /**
     * @var array $licence 开户许可证
     */
    private $licence;
    /**
     * @var MemberAccount $memberAccount 账户
     */
    private $memberAccount;
    /**
     * @var Bank $bank 银行
     */
    private $bank;
    /**
     * @var int $unbindReason 解绑原因
     */
    private $unbindReason;

    /**
     * @var string $paymentPassword 支付密码
     */
    private $paymentPassword;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->accountType = self::ACCOUNT_TYPE['PUBLIC'];
        $this->cardType = self::CARD_TYPE['DEBIT_CARD'];
        $this->cardNumber = '';
        $this->bankBranchArea = '';
        $this->bankBranchAddress = '';
        $this->cellphone = '';
        $this->cardholderName = '';
        $this->licence = array();
        $this->memberAccount = new MemberAccount();
        $this->bank = new Bank();
        $this->unbindReason = self::UNBIND_REASON['OTHER'];
        $this->paymentPassword = '';
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new BankCardRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->accountType);
        unset($this->cardType);
        unset($this->cardNumber);
        unset($this->bankBranchArea);
        unset($this->bankBranchAddress);
        unset($this->cellphone);
        unset($this->cardholderName);
        unset($this->licence);
        unset($this->memberAccount);
        unset($this->bank);
        unset($this->unbindReason);
        unset($this->paymentPassword);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAccountType(int $accountType) : void
    {
        $this->accountType = $accountType;
    }

    public function getAccountType() : int
    {
        return $this->accountType;
    }

    public function setCardType(int $cardType) : void
    {
        $this->cardType = $cardType;
    }

    public function getCardType() : int
    {
        return $this->cardType;
    }

    public function setCardNumber(string $cardNumber) : void
    {
        $this->cardNumber = $cardNumber;
    }

    public function getCardNumber() : string
    {
        return $this->cardNumber;
    }

    public function setBankBranchArea(string $bankBranchArea) : void
    {
        $this->bankBranchArea = $bankBranchArea;
    }

    public function getBankBranchArea() : string
    {
        return $this->bankBranchArea;
    }

    public function setBankBranchAddress(string $bankBranchAddress) : void
    {
        $this->bankBranchAddress = $bankBranchAddress;
    }

    public function getBankBranchAddress() : string
    {
        return $this->bankBranchAddress;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setCardholderName(string $cardholderName) : void
    {
        $this->cardholderName = $cardholderName;
    }

    public function getCardholderName() : string
    {
        return $this->cardholderName;
    }

    public function setLicence(array $licence) : void
    {
        $this->licence = $licence;
    }

    public function getLicence() : array
    {
        return $this->licence;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }

    public function setBank(Bank $bank) : void
    {
        $this->bank = $bank;
    }

    public function getBank() : Bank
    {
        return $this->bank;
    }

    public function setUnbindReason(int $unbindReason) : void
    {
        $this->unbindReason = $unbindReason;
    }

    public function getUnbindReason() : int
    {
        return $this->unbindReason;
    }

    public function setPaymentPassword(string $paymentPassword) : void
    {
        $this->paymentPassword = $paymentPassword;
    }

    public function getPaymentPassword() : string
    {
        return $this->paymentPassword;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : BankCardRepository
    {
        return $this->repository;
    }

    public function bind() : bool
    {
        if ($this->getMemberAccount() instanceof INull || $this->getBank() instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return $this->getRepository()->bind($this);
    }

    public function unBind() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getRepository()->unBind($this);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }
}
