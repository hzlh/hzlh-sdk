<?php
namespace Sdk\Coupon\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Member\Model\Member;

use Sdk\MerchantCoupon\Model\MerchantCoupon;

use Sdk\Coupon\Repository\CouponRepository;

class Coupon implements IObject
{
    use Object;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'USED' => 2, //已使用
        'CONFIRM' => 4, //已使用
        'OVERDUE' => -2, //已过期
        'DELETED' => -4, //已删除
    );

    /**
     * @var int $id id
     */
    private $id;
    /**
     * @var Member $member 用户
     */
    private $member;
    /**
     * @var MerchantCoupon $merchantCoupon 优惠劵
     */
    private $merchantCoupon;

    private $releaseType;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->merchantCoupon = new MerchantCoupon();
        $this->releaseType = 0;
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new CouponRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->merchantCoupon);
        unset($this->releaseType);
        unset($this->member);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setMerchantCoupon(MerchantCoupon $merchantCoupon) : void
    {
        $this->merchantCoupon = $merchantCoupon;
    }

    public function getMerchantCoupon() : MerchantCoupon
    {
        return $this->merchantCoupon;
    }

    public function setReleaseType(int $releaseType) : void
    {
        $this->releaseType = $releaseType;
    }

    public function getReleaseType()
    {
        return $this->releaseType;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : CouponRepository
    {
        return $this->repository;
    }

    public function receive() : bool
    {
        if ($this->getMember() instanceof INull || $this->getMerchantCoupon() instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return $this->getRepository()->receive($this);
    }

    public function deletes() : bool
    {
        if ($this->isDeleted()) {
            Core::setLastError(COUPON_STATUS_IS_DELETED);
            return false;
        }

        return $this->getRepository()->deletes($this);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    public function isDeleted() : bool
    {
        return $this->getStatus() == self::STATUS['DELETED'];
    }
}
