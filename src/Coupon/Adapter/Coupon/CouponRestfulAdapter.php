<?php
namespace Sdk\Coupon\Adapter\Coupon;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Coupon\Model\Coupon;
use Sdk\Coupon\Model\NullCoupon;
use Sdk\Coupon\Translator\CouponRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class CouponRestfulAdapter extends GuzzleAdapter implements ICouponAdapter
{
    use FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_COUPON_LIST'=>[
                'fields'=>[
                    'memberCoupons'=>'releaseType,status,createTime,updateTime,statusTime,releaseCoupon,member'
                ],
                'include'=> 'releaseCoupon,member,releaseCoupon.reference'
            ],
            'COUPON_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'releaseCoupon,member'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CouponRestfulTranslator();
        $this->resource = 'memberCoupons';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCoupon::getInstance());
    }

    public function receive(Coupon $coupon) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $coupon,
            array(
                'merchantCoupon',
                'member'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($coupon);
            return true;
        }

        return false;
    }

    public function deletes(Coupon $coupon) : bool
    {
        $this->patch(
            $this->getResource().'/'.$coupon->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($coupon);
            return true;
        }

        return false;
    }
}
