<?php
namespace Sdk\Coupon\Adapter\Coupon;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Coupon\Model\Coupon;

interface ICouponAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function receive(Coupon $coupon) : bool;

    public function deletes(Coupon $coupon) : bool;
}
