<?php
namespace Sdk\Coupon\Adapter\Coupon;

use Sdk\Coupon\Model\Coupon;
use Sdk\Coupon\Utils\MockFactory;

class CouponMockAdapter implements ICouponAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateCouponObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $couponList = array();

        foreach ($ids as $id) {
            $couponList[] = MockFactory::generateCouponObject($id);
        }

        return $couponList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateCouponObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateCouponObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function receive(Coupon $coupon) : bool
    {
        unset($coupon);

        return true;
    }

    public function deletes(Coupon $coupon) : bool
    {
        unset($coupon);

        return true;
    }
}
