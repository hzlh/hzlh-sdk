<?php
namespace Sdk\Coupon\Translator;

use Sdk\Coupon\Model\Coupon;
use Sdk\Coupon\Model\NullCoupon;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Member\Translator\MemberRestfulTranslator;

use Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class CouponRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    protected function getMerchantCouponRestfulTranslator() : MerchantCouponRestfulTranslator
    {
        return new MerchantCouponRestfulTranslator();
    }

    public function arrayToObject(array $expression, $coupon = null)
    {
        return $this->translateToObject($expression, $coupon);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $coupon = null)
    {
        if (empty($expression)) {
            return NullCoupon::getInstance();
        }

        if ($coupon == null) {
            $coupon = new Coupon();
        }
        
        $data =  isset($expression['data']) ? $expression['data'] : $expression;

        if (isset($data['id'])) {
            $id = $data['id'];
            $coupon->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['releaseType'])) {
            $coupon->setReleaseType($attributes['releaseType']);
        }
        if (isset($attributes['status'])) {
            $coupon->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $coupon->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $coupon->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $coupon->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $coupon->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        if (isset($relationships['releaseCoupon']['data'])) {
            if (isset($expression['included'])) {
                $releaseCoupon = $this->changeArrayFormat(
                    $relationships['releaseCoupon']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $releaseCoupon = $this->changeArrayFormat($relationships['releaseCoupon']['data']);
            }
            
            $coupon->setMerchantCoupon($this->getMerchantCouponRestfulTranslator()->arrayToObject($releaseCoupon));
        }

        return $coupon;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($coupon, array $keys = array())
    {
        $expression = array();

        if (!$coupon instanceof Coupon) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'merchantCoupon',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'memberCoupons'
            )
        );

        $attributes = array();

        $expression['data']['attributes'] = $attributes;

        if (in_array('merchantCoupon', $keys)) {
            $expression['data']['relationships']['releaseCoupon']['data'] = array(
                array(
                    'type' => 'releaseCoupons',
                    'id' => $coupon->getMerchantCoupon()->getId()
                )
            );
        }

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $coupon->getMember()->getId()
                )
             );
        }

        return $expression;
    }
}
