<?php
namespace Sdk\Order\ServiceOrder\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\Order\ServiceOrder\Model\NullServiceOrder;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Sdk\Order\CommonOrder\Translator\OrderAddressRestfulTranslator;
use Sdk\Order\CommonOrder\Translator\OrderUpdateAmountRecordRestfulTranslator;
use Sdk\Order\CommonOrder\Translator\OrderCommoditiesRestfulTranslator;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Coupon\Translator\CouponRestfulTranslator;

use Sdk\Payment\Model\Payment;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class ServiceOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    /**
     * @codeCoverageIgnore
     */
    protected function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
    {
        return new MemberAccountRestfulTranslator();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function getOrderAddressRestfulTranslator() : OrderAddressRestfulTranslator
    {
        return new OrderAddressRestfulTranslator();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function getOrderCommoditiesRestfulTranslator() : OrderCommoditiesRestfulTranslator
    {
        return new OrderCommoditiesRestfulTranslator();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function getCouponRestfulTranslator() : CouponRestfulTranslator
    {
        return new CouponRestfulTranslator();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function getOrderUpdateAmountRecordTranslator() : OrderUpdateAmountRecordRestfulTranslator
    {
        return new OrderUpdateAmountRecordRestfulTranslator();
    }

    public function arrayToObject(array $expression, $serviceOrder = null)
    {
        return $this->translateToObject($expression, $serviceOrder);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $serviceOrder = null)
    {
        if (empty($expression)) {
            return NullServiceOrder::getInstance();
        }

        if ($serviceOrder == null) {
            $serviceOrder = new ServiceOrder();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $serviceOrder->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['orderno'])) {
            $serviceOrder->setOrderno($attributes['orderno']);
        }
        if (isset($attributes['paymentId'])) {
            $serviceOrder->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['totalPrice'])) {
            $serviceOrder->setTotalPrice($attributes['totalPrice']);
        }
        if (isset($attributes['paidAmount'])) {
            $serviceOrder->setPaidAmount($attributes['paidAmount']);
        }
        if (isset($attributes['collectedAmount'])) {
            $serviceOrder->setCollectedAmount($attributes['collectedAmount']);
        }
        if (isset($attributes['platformPreferentialAmount'])) {
            $serviceOrder->setPlatformPreferentialAmount($attributes['platformPreferentialAmount']);
        }
        if (isset($attributes['businessPreferentialAmount'])) {
            $serviceOrder->setBusinessPreferentialAmount($attributes['businessPreferentialAmount']);
        }
        if (isset($attributes['buyerOrderStatus'])) {
            $serviceOrder->setBuyerOrderStatus($attributes['buyerOrderStatus']);
        }
        if (isset($attributes['sellerOrderStatus'])) {
            $serviceOrder->setSellerOrderStatus($attributes['sellerOrderStatus']);
        }
        if (isset($attributes['timeRecord'])) {
            $serviceOrder->setTimeRecord($attributes['timeRecord']);
        }
        if (isset($attributes['failureReason'])) {
            $serviceOrder->setFailureReason($attributes['failureReason']);
        }
        if (isset($attributes['remark'])) {
            $serviceOrder->setRemark($attributes['remark']);
        }
        if (isset($attributes['cancelReason'])) {
            $serviceOrder->setCancelReason($attributes['cancelReason']);
        }
        if (isset($attributes['status'])) {
            $serviceOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $serviceOrder->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $serviceOrder->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $serviceOrder->setStatusTime($attributes['statusTime']);
        }
        $paymentType = isset($attributes['paymentType'])
        ? $attributes['paymentType']
        : '';
        $transactionNumber = isset($attributes['transactionNumber'])
        ? $attributes['transactionNumber']
        : '';
        $transactionInfo = isset($attributes['transactionInfo'])
        ? $attributes['transactionInfo']
        : array();
        $paymentTime = isset($attributes['paymentTime'])
        ? $attributes['paymentTime']
        : 0;
        $serviceOrder->setPayment(
            new Payment(
                $paymentType,
                $paymentTime,
                $transactionNumber,
                $transactionInfo
            )
        );

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['buyerMemberAccount']['data'])) {
            if (isset($expression['included'])) {
                $buyerMemberAccount = $this->changeArrayFormat(
                    $relationships['buyerMemberAccount']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $buyerMemberAccount = $this->changeArrayFormat($relationships['buyerMemberAccount']['data']);
            }

            $serviceOrder->setBuyerMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($buyerMemberAccount)
            );
        }
        if (isset($relationships['orderUpdateAmountRecord']['data'])) {
            if (isset($expression['included'])) {
                $orderUpdateAmountRecord = $this->changeArrayFormat(
                    $relationships['orderUpdateAmountRecord']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $orderUpdateAmountRecord = $this->changeArrayFormat($relationships['orderUpdateAmountRecord']['data']);
            }

            $serviceOrder->setOrderUpdateAmountRecord(
                $this->getOrderUpdateAmountRecordTranslator()->arrayToObject($orderUpdateAmountRecord)
            );
        }
        if (isset($relationships['sellerEnterprise']['data'])) {
            $sellerEnterprise = $this->changeArrayFormat($relationships['sellerEnterprise']['data']);
            $serviceOrder->setSellerEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($sellerEnterprise)
            );
        }
        if (isset($relationships['orderAddress']['data'])) {
            if (isset($expression['included'])) {
                $orderAddress = $this->changeArrayFormat(
                    $relationships['orderAddress']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $orderAddress = $this->changeArrayFormat($relationships['orderAddress']['data']);
            }
            $serviceOrder->setOrderAddress(
                $this->getOrderAddressRestfulTranslator()->arrayToObject($orderAddress)
            );
        }
        if (isset($relationships['memberCoupons']['data'])) {
            foreach ($relationships['memberCoupons']['data'] as $memberCouponArray) {
                if (isset($expression['included'])) {
                    $memberCoupon = $this->changeArrayFormat(
                        $memberCouponArray,
                        $expression['included']
                    );
                }

                if (!isset($expression['included'])) {
                    $memberCoupon = $this->changeArrayFormat($memberCouponArray);
                }

                $memberCouponObject = $this->getCouponRestfulTranslator()->arrayToObject($memberCoupon);
                $serviceOrder->addMemberCoupon($memberCouponObject);
            }
        }
        if (isset($relationships['orderCommodities']['data'])) {
            foreach ($relationships['orderCommodities']['data'] as $orderCommodityArray) {
                if (isset($expression['included'])) {
                    $orderCommodity = $this->changeArrayFormat(
                        $orderCommodityArray,
                        $expression['included']
                    );
                }

                if (!isset($expression['included'])) {
                    $orderCommodity = $this->changeArrayFormat($orderCommodityArray);
                }

                $orderCommodityObject = $this->getOrderCommoditiesRestfulTranslator()->arrayToObject($orderCommodity);
                $serviceOrder->addOrderCommodity($orderCommodityObject);
            }
        }

        return $serviceOrder;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceOrder, array $keys = array())
    {
        $expression = array();

        if (!$serviceOrder instanceof ServiceOrder) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'amount',
                'remark',
                'cancelReason',
                'transactionNumber',
                'transactionInfo',
                'paymentPassword',
                'paymentType',
                'buyerMemberAccount',
                'memberCoupon',
                'orderAddress',
                'orderCommodities'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'serviceOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $serviceOrder->getId();
        }

        $attributes = array();

        if (in_array('remark', $keys)) {
            $attributes['remark'] = $serviceOrder->getRemark();
        }

        if (in_array('amount', $keys)) {
            $attributes['amount'] = $serviceOrder->getAmount();
        }

        if (in_array('cancelReason', $keys)) {
            $attributes['cancelReason'] = $serviceOrder->getCancelReason();
        }

        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $serviceOrder->getPayment()->getTransactionNumber();
        }

        if (in_array('transactionInfo', $keys)) {
            $attributes['transactionInfo'] = $serviceOrder->getPayment()->getTransactionInfo();
        }

        if (in_array('paymentPassword', $keys)) {
            $attributes['paymentPassword'] = $serviceOrder->getPayment()->getPaymentPassword();
        }

        if (in_array('paymentType', $keys)) {
            $attributes['paymentType'] = $serviceOrder->getPayment()->getType();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('buyerMemberAccount', $keys)) {
            $expression['data']['relationships']['buyerMemberAccount']['data'] = array(
                array(
                    'type' => 'memberAccount',
                    'id' => $serviceOrder->getBuyerMemberAccount()->getId()
                )
             );
        }

        if (in_array('memberCoupon', $keys)) {
            $memberCouponArray = $this->setUpMemberCouponArray($serviceOrder);
            $expression['data']['relationships']['memberCoupon']['data'] = $memberCouponArray;
        }

        if (in_array('orderAddress', $keys)) {
            $orderAddress = $this->getOrderAddressRestfulTranslator()->objectToArray(
                $serviceOrder->getOrderAddress()
            );
            $expression['data']['relationships']['orderAddress']['data'][] = $orderAddress;
        }

        if (in_array('orderCommodities', $keys)) {
            $orderCommodities = $this->setUpOrderCommoditiesArray($serviceOrder);
            $expression['data']['relationships']['orderCommodities']['data'] = $orderCommodities;
        }

        return $expression;
    }

    /**
     * @codeCoverageIgnore
     */
    private function setUpMemberCouponArray(ServiceOrder $serviceOrder)
    {
        $memberCouponArray = [];

        $memberCoupon = $serviceOrder->getMemberCoupons();
        foreach ($memberCoupon as $memberCouponKey) {
            $memberCouponArray[] = array(
                    'type' => 'memberCoupons',
                    'id' => $memberCouponKey->getId()
                );
        }

        return $memberCouponArray;
    }
    /**
     * @codeCoverageIgnore
     */
    private function setUpOrderCommoditiesArray(ServiceOrder $serviceOrder)
    {
        $orderCommoditiesArray = [];

        $orderCommodities = $serviceOrder->getOrderCommodities();
        foreach ($orderCommodities as $orderCommoditiesKey) {
            $orderCommoditiesArray[] = $this->getOrderCommoditiesRestfulTranslator()
                                            ->objectToArray($orderCommoditiesKey);
        }

        return $orderCommoditiesArray;
    }
}
