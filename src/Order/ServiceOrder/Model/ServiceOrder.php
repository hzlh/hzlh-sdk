<?php
namespace Sdk\Order\ServiceOrder\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Order\CommonOrder\Model\CommonOrder;

use Sdk\Payment\Model\IPayAble;

use Sdk\Member\Model\Member;
use Sdk\Coupon\Model\Coupon;
use Sdk\MerchantCoupon\Model\MerchantCoupon;

use Sdk\Coupon\Repository\CouponRepository;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\Order\ServiceOrder\Repository\ServiceOrderRepository;

use Sdk\TradeRecord\Model\ITradeAble;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class ServiceOrder extends CommonOrder implements IPayAble, ITradeAble, IOperatAble
{
    const PROBAILITY = 100;
    const MIN_AMOUNT = 0.01;
    const CAN_USE_COUPON_AMOUNT = 20;
    const UPDATE_ORDER_AMOUNT_LOWER_LIMINT = 0.01;
    const ORDER_AMOUNT_LOWER_LIMINT = 0;
    const MAX_COUPON_AMOUNT = 200;

    const MEMBER_COUPON_USE_MAX_COUNT = 2;
    const MEMBER_COUPON_NOT_SUPERPOSITION_COUNT = 1;

    use OperatAbleTrait;

    private $repository;

    private $platformPreferentialAmount;

    private $businessPreferentialAmount;

    private $transactionNumber;

    private $transactionInfo;

    private $paymentTime;

    private $paymentId;

    private $amount;

    public function __construct()
    {
        parent::__construct();
        $this->platformPreferentialAmount = 0;
        $this->businessPreferentialAmount = 0;
        $this->transactionNumber = '';
        $this->transactionInfo = array();
        $this->paymentTime = 0;
        $this->paymentId = '';
        $this->amount = 0;
        $this->repository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->platformPreferentialAmount);
        unset($this->businessPreferentialAmount);
        unset($this->transactionNumber);
        unset($this->transactionInfo);
        unset($this->paymentTime);
        unset($this->paymentId);
        unset($this->amount);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setPaymentId(string $paymentId) : void
    {
        $this->paymentId = $paymentId;
    }

    public function getPaymentId() : string
    {
        return $this->paymentId;
    }

    public function setAmount(float $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function setPlatformPreferentialAmount(float $platformPreferentialAmount) : void
    {
        $this->platformPreferentialAmount = $platformPreferentialAmount;
    }

    public function getPlatformPreferentialAmount() : float
    {
        return $this->platformPreferentialAmount;
    }

    public function setBusinessPreferentialAmount(float $businessPreferentialAmount) : void
    {
        $this->businessPreferentialAmount = $businessPreferentialAmount;
    }

    public function getBusinessPreferentialAmount() : float
    {
        return $this->businessPreferentialAmount;
    }

    public function setTransactionNumber(string $transactionNumber) : void
    {
        $this->transactionNumber = $transactionNumber;
    }

    public function getTransactionNumber() : string
    {
        return $this->transactionNumber;
    }

    public function setTransactionInfo(array $transactionInfo) : void
    {
        $this->transactionInfo = $transactionInfo;
    }

    public function getTransactionInfo() : array
    {
        return $this->transactionInfo;
    }

    public function setPaymentTime(int $paymentTime) : void
    {
        $this->paymentTime = $paymentTime;
    }

    public function getPaymentTime() : int
    {
        return $this->paymentTime;
    }

    protected function getEnterpriseRepository()
    {
        return new EnterpriseRepository();
    }


    /**
     * [placeOrderAction 下单]
     * @return [type] [bool]
     *
     * @codeCoverageIgnore
     */
    protected function placeOrderAction() : bool
    {
        $this->calculateTotalPriceAndSellerEnterprise();
        $this->calculateAmount();

        if ($this->getSellerEnterprise() instanceof INull || $this->getBuyerMemberAccount() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY);
            return false;
        }

        if (!$this->isCanUseCoupon()) {
            return false;
        }

        return $this->getRepository()->add($this);
    }

    private function isCanUseCoupon() : bool
    {
        if (!$this->isSuperposition()) {
            return false;
        }

        if (!$this->isUsed()) {
            return false;
        }

        if (!$this->isExpired()) {
            return false;
        }

        if (!$this->isMeetConcession()) {
            return false;
        }
        
        if (!$this->isApplyScope()) {
            return false;
        }

        if ($this->getPaidAmount() < self::ORDER_AMOUNT_LOWER_LIMINT) {
            Core::setLastError(ORDER_PAIND_AMOUNT_INCORRECT);
            return false;
        }
        
        return true;
    }

    private function isSuperposition() : bool
    {
        $memberCoupons = $this->getMemberCoupons();
        $count = count($memberCoupons);

        if ($count > self::MEMBER_COUPON_USE_MAX_COUNT) {
            Core::setLastError(MEMBER_COUPON_MORE_THAN_SPECIFIED);
            return false;
        }

        if ($count > self::MEMBER_COUPON_NOT_SUPERPOSITION_COUNT) {
            $isSuperpositions = array();
            foreach ($memberCoupons as $memberCoupon) {
                $isSuperpositions[] = $memberCoupon->getMerchantCoupon()->getIsSuperposition();
            }

            if (in_array(MerchantCoupon::IS_SUPERPOSITION['NO'], $isSuperpositions)) {
                Core::setLastError(MEMBER_COUPON_NOT_SUPERPOSITION_USE);
                return false;
            }
        }

        return true;
    }

    private function isUsed() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($memberCoupon->getStatus() != Coupon::STATUS['NORMAL']) {
                Core::setLastError(MEMBER_COUPON_STATUS_NOT_UNUSER);
                return false;
            }
        }

        return true;
    }

    private function isExpired() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            $validityStartTime = $memberCoupon->getMerchantCoupon()->getValidityStartTime();
            $validityEndTime = $memberCoupon->getMerchantCoupon()->getValidityEndTime();
            $currentTime = Core::$container->get('time');

            if ($validityStartTime > $currentTime) {
                Core::setLastError(MEMBER_COUPON_NOT_IN_USE_PERIOD);
                return false;
            }

            if ($validityEndTime!= 0 && $validityEndTime < $currentTime) {
                Core::setLastError(MEMBER_COUPON_NOT_IN_USE_PERIOD);
                return false;
            }
        }

        return true;
    }

    private function isMeetConcession() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($this->getTotalPrice() < $memberCoupon->getMerchantCoupon()->getUseStandard()) {
                Core::setLastError(MEMBER_COUPON_NOT_MEET_CONCESSION);
                return false;
            }
        }

        return true;
    }

    private function isApplyScope() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($memberCoupon->getMerchantCoupon()->getApplyScope() == MerchantCoupon::APPLY_SCOPE['STORE_CURRENCY']) {
                $sellerEnterpriseId = $this->getSellerEnterprise()->getId();

                if (!in_array($sellerEnterpriseId, $memberCoupon->getMerchantCoupon()->getApplySituation())) {
                    Core::setLastError(MEMBER_COUPON_NOT_APPLY_SCOPE);
                    return false;
                }
            }
        }

        return true;
    }

    private function calculateTotalPriceAndSellerEnterprise() : void
    {
        $totalPrice = 0;
        $sellerEnterpriseId = 0;

        $orderCommodities = $this->getOrderCommodities();

        foreach ($orderCommodities as $orderCommodity) {
            $priceList = $orderCommodity->getSnapshot()->getSnapshotObject()->getPrice();
            $unitPprice = $priceList[$orderCommodity->getSkuIndex()]['value'];
            $totalPrice+=$unitPprice*$orderCommodity->getNumber();
            $sellerEnterpriseId = $orderCommodity->getSnapshot()->getSnapshotObject()->getEnterprise()->getId();
        }

        $sellerEnterprise = $this->getEnterpriseRepository()->fetchOne($sellerEnterpriseId);

        $this->setTotalPrice($totalPrice);
        $this->setSellerEnterprise($sellerEnterprise);
    }

    private function calculateAmount() : void
    {
        $totalPrice = $this->getTotalPrice();

        $businessPreferentialAmount = $this->calculateBusinessPreferentialAmount();
        $platformPreferentialAmount = $this->calculatePlatformPreferentialAmount();

        $paidAmount = $totalPrice-$businessPreferentialAmount-$platformPreferentialAmount;
        $paidAmount = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMINT : $paidAmount;
        $collectedAmount = $paidAmount+$platformPreferentialAmount;

        $this->setPaidAmount($paidAmount);
        $this->setCollectedAmount($collectedAmount);
    }

    private function calculateBusinessPreferentialAmount()
    {
        $totalPrice = $this->getTotalPrice();

        $memberCoupons = $this->getMemberCoupons();
        $businessPreferentialAmount = 0;

        foreach ($memberCoupons as $memberCoupon) {
            if ($memberCoupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['MERCHANT']) {
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) { //phpcs:ignore
                    $businessPreferentialAmount = $memberCoupon->getMerchantCoupon()->getDenomination();
                }
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getMerchantCoupon()->getDiscount();
                    $businessPreferentialAmount = $totalPrice*((self::PROBAILITY-$discount)/100);
                }
            }
        }
        
        return $businessPreferentialAmount;
    }

    private function calculatePlatformPreferentialAmount()
    {
        $totalPrice = $this->getTotalPrice();

        $memberCoupons = $this->getMemberCoupons();
        $platformPreferentialAmount = 0;

        foreach ($memberCoupons as $memberCoupon) {
            if ($memberCoupon->getReleaseType() == MerchantCoupon::RELEASE_TYPE['PLATFORM']) {
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']) { //phpcs:ignore
                    $platformPreferentialAmount = $memberCoupon->getMerchantCoupon()->getDenomination();
                }
                if ($memberCoupon->getMerchantCoupon()->getCouponType() == MerchantCoupon::COUPON_TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getMerchantCoupon()->getDiscount();
                    $platformPreferentialAmount = $totalPrice*((self::PROBAILITY-$discount)/100);
                    $platformPreferentialAmount = $platformPreferentialAmount > self::MAX_COUPON_AMOUNT ?
                                                  self::MAX_COUPON_AMOUNT :
                                                  $platformPreferentialAmount;
                }
            }
        }
        
        return $platformPreferentialAmount;
    }

    /**
     * [payAction 付款]
     * @return [type] [bool]
     * @codeCoverageIgnore
     *
     */
    protected function payAction() : bool
    {
        return $this->getRepository()->edit($this);
    }
    /**
     * [updateOrderAmount 修改订单金额]
     * @return [type] [bool]
     */
    public function updateOrderAmount() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->getRepository()->updateOrderAmount($this);
    }
    /**
     * [updateOrderAddress 修改订单地址]
     * @return [type] [bool]
     * @codeCoverageIgnore
     */
    public function updateOrderAddress() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->getRepository()->updateOrderAddress($this);
    }

    public function paymentFailure() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }
        
        return $this->getRepository()->paymentFailure($this);
    }
}
