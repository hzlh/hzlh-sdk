<?php
namespace Sdk\Order\ServiceOrder\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Order\CommonOrder\Model\NullOrderTrait;

class NullServiceOrder extends ServiceOrder implements INull
{
    use NullOrderTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function placeOrderAction() : bool
    {
        return  $this->resourceNotExist();
    }

    public function payAction() : bool
    {
        return  $this->resourceNotExist();
    }

    public function paymentFailure() : bool
    {
        return  $this->resourceNotExist();
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
