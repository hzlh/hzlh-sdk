<?php
namespace Sdk\Order\ServiceOrder\Adapter\ServiceOrder;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;

interface IServiceOrderAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
    public function paymentFailure(ServiceOrder $serviceOrder) : bool;
}
