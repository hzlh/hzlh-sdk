<?php
namespace Sdk\Order\ServiceOrder\Adapter\ServiceOrder;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\Order\ServiceOrder\Model\NullServiceOrder;
use Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class ServiceOrderRestfulAdapter extends GuzzleAdapter implements IServiceOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;
    
    const SCENARIOS = [
            'OA_SERVICE_ORDER_LIST'=>[
                'fields'=>[],
                'include'=> 'buyerMemberAccount,buyerMemberAccount.member,orderUpdateAmountRecord,sellerEnterprise,orderAddress,orderCommodities,memberCoupons,orderAddress.deliveryAddress,orderCommodities.commodity,memberCoupons.releaseCoupon'//phpcs:ignore
            ],
            'PORTAL_SERVICE_ORDER_LIST'=>[
                'fields'=>[],
                'include'=> 'buyerMemberAccount,buyerMemberAccount.member,orderUpdateAmountRecord,sellerEnterprise,orderAddress,orderCommodities,memberCoupons,orderAddress.deliveryAddress,orderCommodities.commodity,memberCoupons.releaseCoupon'//phpcs:ignore
            ],
            'SERVICE_ORDER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'buyerMemberAccount,buyerMemberAccount.member,orderUpdateAmountRecord,sellerEnterprise,orderAddress,orderCommodities,memberCoupons,orderAddress.deliveryAddress,orderCommodities.commodity,memberCoupons.releaseCoupon'//phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ServiceOrderRestfulTranslator();
        $this->resource = 'serviceOrders';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullServiceOrder::getInstance());
    }
    //下单
    protected function addAction(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'remark',
                'buyerMemberAccount',
                'memberCoupon',
                'orderAddress',
                'orderCommodities'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }

        return false;
    }
    //付款
    protected function editAction(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentPassword'
            )
        );

        $this->post(
            'payments/'.$serviceOrder->getPaymentId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }

        return false;
    }
    //修改订单金额
    public function updateOrderAmount(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'amount',
                'remark'
            )
        );

        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/updateOrderAmount',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //修改订单地址
    public function updateOrderAddress(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'orderAddress'
            )
        );

        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/updateOrderAddress',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //买家取消订单
    public function buyerCancel(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'cancelReason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/buyerCancel',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //卖家取消订单
    public function sellerCancel(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'cancelReason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/sellerCancel',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //履约开始
    public function performanceBegin(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/performanceBegin'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //履约结束
    public function performanceEnd(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/performanceEnd'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //买家确认
    public function buyerConfirmation(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/buyerConfirmation'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //买家删除订单
    public function buyerDelete(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/buyerDelete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //买家永久删除订单
    public function buyerPermanentDelete(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/buyerPermanentDelete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //卖家删除订单
    public function sellerDelete(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/sellerDelete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }
    //卖家永久删除订单
    public function sellerPermanentDelete(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/sellerPermanentDelete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }
        return false;
    }

    public function paymentFailure(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array(
                'failureReason'
            )
        );
        
        $this->patch(
            'payments/'.$serviceOrder->getPaymentId().'/paymentFailure',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }

        return false;
    }
}
