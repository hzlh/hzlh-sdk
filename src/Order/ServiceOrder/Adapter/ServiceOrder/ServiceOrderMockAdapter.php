<?php
namespace Sdk\Order\ServiceOrder\Adapter\ServiceOrder;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\ModifyStatusAbleMockAdapterTrait;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\Order\ServiceOrder\Utils\MockFactory;

class ServiceOrderMockAdapter implements IServiceOrderAdapter
{
    use OperatAbleMockAdapterTrait, ModifyStatusAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateServiceOrderObject($id);
    }

    public function fetchList(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generateServiceOrderObject($id);
        }

        return $serviceList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateServiceOrderObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generateServiceOrderObject($id);
        }

        return $serviceList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function paymentFailure(ServiceOrder $serviceOrder) : bool
    {
        unset($serviceOrder);
        return true;
    }
}
