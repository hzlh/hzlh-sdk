<?php
namespace Sdk\Order\ServiceOrder\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\Order\ServiceOrder\Adapter\ServiceOrder\IServiceOrderAdapter;
use Sdk\Order\ServiceOrder\Adapter\ServiceOrder\ServiceOrderMockAdapter;
use Sdk\Order\ServiceOrder\Adapter\ServiceOrder\ServiceOrderRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;

use Sdk\Order\CommonOrder\Repository\CommonOrderRepositoryTrait;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class ServiceOrderRepository extends Repository implements IServiceOrderAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait,
        CommonOrderRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_SERVICE_ORDER_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_SERVICE_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICE_ORDER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ServiceOrderRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IServiceOrderAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IServiceOrderAdapter
    {
        return new ServiceOrderMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    //修改订单金额
    public function updateOrderAmount(ServiceOrder $serviceOrder) : bool
    {
        return $this->getAdapter()->updateOrderAmount($serviceOrder);
    }
    //修改订单地址
    public function updateOrderAddress(ServiceOrder $serviceOrder) : bool
    {
        return $this->getAdapter()->updateOrderAddress($serviceOrder);
    }

    public function paymentFailure(ServiceOrder $serviceOrder): bool
    {
        return $this->getAdapter()->paymentFailure($serviceOrder);
    }
}
