<?php
namespace Sdk\Order\CommonOrder\Model;

use Sdk\DeliveryAddress\Model\DeliveryAddress;

use Sdk\Snapshot\Model\Snapshot;

class OrderAddress
{
    private $id;

    private $snapshot;

    private $cellphone;

    private $deliveryAddress;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->snapshot = new Snapshot();
        $this->deliveryAddress = new DeliveryAddress();
        $this->cellphone = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->snapshot);
        unset($this->deliveryAddress);
        unset($this->cellphone);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSnapshot(Snapshot $snapshot)
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : Snapshot
    {
        return $this->snapshot;
    }
    
    public function setDeliveryAddress(DeliveryAddress $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryAddress() : DeliveryAddress
    {
        return $this->deliveryAddress;
    }
    
    public function setCellphone(string $cellphone)
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }
}
