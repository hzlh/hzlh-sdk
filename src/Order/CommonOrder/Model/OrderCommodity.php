<?php
namespace Sdk\Order\CommonOrder\Model;

use Marmot\Core;

use Sdk\Service\Model\NullService;

use Sdk\Snapshot\Model\Snapshot;

class OrderCommodity
{
    private $id;

    private $number;

    private $snapshot;

    private $skuIndex;

    private $commodity;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = 0;
        $this->snapshot = new Snapshot();
        $this->skuIndex = 0;
        $this->commodity = new NullService();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->snapshot);
        unset($this->skuIndex);
        unset($this->commodity);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setNumber(int $number)
    {
        $this->number = $number;
    }

    public function getNumber() : int
    {
        return $this->number;
    }

    public function setSnapshot(Snapshot $snapshot)
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : Snapshot
    {
        return $this->snapshot;
    }
    
    public function setSkuIndex(int $skuIndex)
    {
        $this->skuIndex = $skuIndex;
    }

    public function getSkuIndex() : int
    {
        return $this->skuIndex;
    }
    
    public function setCommodity(ITradeAble $commodity)
    {
        $this->commodity = $commodity;
    }

    public function getCommodity() : ITradeAble
    {
        return $this->commodity;
    }
}
