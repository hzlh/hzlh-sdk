<?php
namespace Sdk\Order\CommonOrder\Model;

use Marmot\Core;

class OrderUpdateAmountRecord
{
    private $id;

    private $amount;

    private $remark;

    private $createTime;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->amount = 0.0;
        $this->remark = '';
        $this->createTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->amount);
        unset($this->remark);
        unset($this->createTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAmount(float $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }
    
    public function setCreateTime(int $createTime) : void
    {
        $this->createTime = $createTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }
}
