<?php
namespace Sdk\Order\CommonOrder\Model;

trait NullOrderTrait
{
    public function placeOrder() : bool
    {
        return $this->resourceNotExist();
    }

    public function pay() : bool
    {
        return $this->resourceNotExist();
    }

    public function updateStatus(int $status) : bool
    {
        unset($status);
        return $this->resourceNotExist();
    }

    public function performanceBegin() : bool
    {
        return $this->resourceNotExist();
    }

    public function performanceEnd() : bool
    {
        return $this->resourceNotExist();
    }

    public function buyerConfirmation() : bool
    {
        return $this->resourceNotExist();
    }

    public function completion() : bool
    {
        return $this->resourceNotExist();
    }

    public function cancelOrder() : bool
    {
        return $this->resourceNotExist();
    }
}
