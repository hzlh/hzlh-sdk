<?php
namespace Sdk\Order\CommonOrder\Repository;

use Sdk\Common\Model\IOperatAble;

trait CommonOrderRepositoryTrait
{
    //买家取消订单
    public function buyerCancel(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->buyerCancel($operatAbleObject);
    }
    //卖家取消订单
    public function sellerCancel(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->sellerCancel($operatAbleObject);
    }
    //履约开始
    public function performanceBegin(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->performanceBegin($operatAbleObject);
    }
    //履约结束
    public function performanceEnd(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->performanceEnd($operatAbleObject);
    }
    //买家确认
    public function buyerConfirmation(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->buyerConfirmation($operatAbleObject);
    }
    //买家删除订单
    public function buyerDelete(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->buyerDelete($operatAbleObject);
    }
    //买家永久删除订单
    public function buyerPermanentDelete(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->buyerPermanentDelete($operatAbleObject);
    }
    //卖家删除订单
    public function sellerDelete(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->sellerDelete($operatAbleObject);
    }
    //卖家永久删除订单
    public function sellerPermanentDelete(IOperatAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->sellerPermanentDelete($operatAbleObject);
    }
}
