<?php
namespace Sdk\Order\CommonOrder\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\CommonOrder\Model\OrderCommodity;
use Sdk\Order\CommonOrder\Model\NullOrderCommodity;

use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;
use Sdk\Service\Translator\ServiceRestfulTranslator;

class OrderCommoditiesRestfulTranslator implements IRestfulTranslator
{
    const NUMBER_PREFIX = 'FW';

    use RestfulTranslatorTrait;

    public function getSnapshotRestfulTranslator()
    {
        return new SnapshotRestfulTranslator();
    }

    public function getServiceRestfulTranslator()
    {
        return new ServiceRestfulTranslator();
    }

    public function arrayToObject(array $expression, $orderCommodities = null)
    {
        return $this->translateToObject($expression, $orderCommodities);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $orderCommodities = null)
    {
        if (empty($expression)) {
            return NullOrderCommodity::getInstance();
        }

        if ($orderCommodities == null) {
            $orderCommodities = new OrderCommodity();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $orderCommodities->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $orderCommodities->setNumber($attributes['number']);
        }
        if (isset($attributes['skuIndex'])) {
            $orderCommodities->setSkuIndex($attributes['skuIndex']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['snapshot']['data'])) {
            $snapshot = $this->changeArrayFormat($relationships['snapshot']['data']);
            $orderCommodities->setSnapshot(
                $this->getSnapshotRestfulTranslator()->arrayToObject($snapshot)
            );
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['commodity']['data'])) {
            if (isset($expression['included'])) {
                $commodity = $this->changeArrayFormat(
                    $relationships['commodity']['data'],
                    $expression['included']
                );
            }
            
            if (!isset($expression['included'])) {
                $commodity = $this->changeArrayFormat($relationships['commodity']['data']);
            }
            if (strpos($commodity['data']['id'], '_')) {
                $ids = explode('_', $commodity['data']['id']);
                
                $commodity['data']['id'] = $ids[1];
                $commodity['data']['attributes']['number'] = self::NUMBER_PREFIX.date('Ymd', $commodity['data']['attributes']['createTime']).$ids[1]; //phpcs:ignore
            }

            $orderCommodities->setCommodity(
                $this->getServiceRestfulTranslator()->arrayToObject($commodity)
            );
        }
        
        return $orderCommodities;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderCommodities, array $keys = array())
    {
        $expression = array();

        if (!$orderCommodities instanceof OrderCommodity) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'number',
                'skuIndex',
                'snapshot'
            );
        }

        $expression = array(
            'type'=>'orderCommodities'
        );

        $attributes = array();

        if (in_array('number', $keys)) {
            $attributes['number'] = $orderCommodities->getNumber();
        }

        if (in_array('skuIndex', $keys)) {
            $attributes['skuIndex'] = $orderCommodities->getSkuIndex();
        }

        $expression['attributes'] = $attributes;

        if (in_array('snapshot', $keys)) {
            $expression['relationships']['snapshot']['data'] = array(
                array(
                    'type' => 'snapshot',
                    'id' => $orderCommodities->getSnapshot()->getId()
                )
             );
        }

        return $expression;
    }
}
