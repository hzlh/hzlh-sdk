<?php
namespace Sdk\Order\CommonOrder\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\CommonOrder\Model\OrderAddress;
use Sdk\Order\CommonOrder\Model\NullOrderAddress;

use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;

use Sdk\DeliveryAddress\Translator\DeliveryAddressRestfulTranslator;

class OrderAddressRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getSnapshotRestfulTranslator()
    {
        return new SnapshotRestfulTranslator();
    }

    public function getDeliveryAddressRestfulTranslator()
    {
        return new DeliveryAddressRestfulTranslator();
    }

    public function arrayToObject(array $expression, $orderAddress = null)
    {
        return $this->translateToObject($expression, $orderAddress);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $orderAddress = null)
    {
        if (empty($expression)) {
            return NullOrderAddress::getInstance();
        }

        if ($orderAddress == null) {
            $orderAddress = new OrderAddress();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $orderAddress->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $orderAddress->setCellphone($attributes['cellphone']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['snapshot']['data'])) {
            $snapshot = $this->changeArrayFormat($relationships['snapshot']['data']);
            $orderAddress->setSnapshot(
                $this->getSnapshotRestfulTranslator()->arrayToObject($snapshot)
            );
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['deliveryAddress']['data'])) {
            if (isset($expression['included'])) {
                $deliveryAddress = $this->changeArrayFormat(
                    $relationships['deliveryAddress']['data'],
                    $expression['included']
                );
            }
            
            if (!isset($expression['included'])) {
                $deliveryAddress = $this->changeArrayFormat($relationships['deliveryAddress']['data']);
            }

            $orderAddress->setDeliveryAddress(
                $this->getDeliveryAddressRestfulTranslator()->arrayToObject($deliveryAddress)
            );
        }
        
        return $orderAddress;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderAddress, array $keys = array())
    {
        $expression = array();

        if (!$orderAddress instanceof OrderAddress) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'cellphone',
                'snapshot',
            );
        }

        $expression = array(
                'type'=>'orderAddress'
        );

        $attributes = array();

        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $orderAddress->getCellphone();
        }

        $expression['attributes'] = $attributes;

        if (in_array('snapshot', $keys)) {
            $expression['relationships']['snapshot']['data'] = array(
                array(
                    'type' => 'snapshot',
                    'id' => $orderAddress->getSnapshot()->getId()
                )
             );
        }

        return $expression;
    }
}
