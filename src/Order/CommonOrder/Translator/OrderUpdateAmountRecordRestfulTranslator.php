<?php
namespace Sdk\Order\CommonOrder\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\CommonOrder\Model\OrderUpdateAmountRecord;
use Sdk\Order\CommonOrder\Model\NullOrderUpdateAmountRecord;


class OrderUpdateAmountRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $orderUpdateAmountRecord = null)
    {
        return $this->translateToObject($expression, $orderUpdateAmountRecord);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $orderUpdateAmountRecord = null)
    {
        if (empty($expression)) {
            return NullOrderUpdateAmountRecord::getInstance();
        }

        if ($orderUpdateAmountRecord == null) {
            $orderUpdateAmountRecord = new OrderUpdateAmountRecord();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $orderUpdateAmountRecord->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['amount'])) {
            $orderUpdateAmountRecord->setAmount($attributes['amount']);
        }
        if (isset($attributes['remark'])) {
            $orderUpdateAmountRecord->setRemark($attributes['remark']);
        }
        if (isset($attributes['createTime'])) {
            $orderUpdateAmountRecord->setCreateTime($attributes['createTime']);
        }

        return $orderUpdateAmountRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderUpdateAmountRecord, array $keys = array())
    {
        $expression = array();

        return $expression;
    }
}
