<?php
namespace Sdk\MerchantCoupon\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        MerchantCoupon::RELEASE_TYPE['PLATFORM'] =>
        'Sdk\Crew\Translator\CrewRestfulTranslator',
        MerchantCoupon::RELEASE_TYPE['MERCHANT'] =>
        'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
