<?php
namespace Sdk\Payment\Model;

use Sdk\Common\Repository\NullRepository;

class PaymentFactory
{
    const MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Sdk\Order\ServiceOrder\Repository\ServiceOrderRepository',
        IPayAble::TYPE['DEPOSIT']=>'\Sdk\Deposit\Repository\DepositRepository',
    );
    
    const VIEW_MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Service\Order\ServiceOrder\View\ServiceOrderView',
        IPayAble::TYPE['DEPOSIT']=>'\Member\Deposit\View\DepositView',
    );
    
    const SERVICE_MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Service\Order\ServiceOrder\Service\ServiceOrderService',
        IPayAble::TYPE['DEPOSIT']=>'\Member\Deposit\Service\DepositService',
    );

    public function getModel(string $paymentId) : IPayAble
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];
        $id = $temp[1];

        $repository = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        $repository = class_exists($repository) ? new $repository : NullRepository::getInstance();

        return $repository->scenario($repository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    public function getView(string $paymentId) : string
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];

        return isset(self::VIEW_MAPS[$type]) ? self::VIEW_MAPS[$type] : '';
    }

    public function getService(string $paymentId) : string
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];

        return isset(self::SERVICE_MAPS[$type]) ? self::SERVICE_MAPS[$type] : '';
    }
}
