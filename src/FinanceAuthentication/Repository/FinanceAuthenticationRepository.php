<?php
namespace Sdk\FinanceAuthentication\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\FinanceAuthenticationMockAdapter;
use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\FinanceAuthenticationRestfulAdapter;
use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\IFinanceAuthenticationAdapter;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

class FinanceAuthenticationRepository extends Repository implements IFinanceAuthenticationAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'FINANCIAL_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'FINANCIAL_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new FinanceAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IFinanceAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IFinanceAuthenticationAdapter
    {
        return new FinanceAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
