<?php
namespace Sdk\FinanceAuthentication\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\IUnAuditedFinanceAuthenticationAdapter;
use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\UnAuditedFinanceAuthenticationMockAdapter;
use Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\UnAuditedFinanceAuthenticationRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class UnAuditedFinanceAuthenticationRepository extends Repository implements IUnAuditedFinanceAuthenticationAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UNAUDITED_FINANCE_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'UNAUDITED_FINANCE_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditedFinanceAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IUnAuditedFinanceAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IUnAuditedFinanceAuthenticationAdapter
    {
        return new UnAuditedFinanceAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
