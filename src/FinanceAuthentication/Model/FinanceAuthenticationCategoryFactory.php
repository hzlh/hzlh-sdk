<?php
namespace Sdk\FinanceAuthentication\Model;

class FinanceAuthenticationCategoryFactory
{
    const TYPE = array(
        'ORGANIZATIONS_FIRST_CATEGORY' => 1,
        'ORGANIZATIONS_SECOND_CATEGORY' => 2,
        'CERTIFICATION_FIRST_AUTHORITY' => 3,
        'CERTIFICATION_SECOND_AUTHORITY' => 4
    );

    const ORGANIZATIONS_FIRST_CATEGORY = array(
        1 => '政策性银行',
        2 => '商业银行',
        3 => '农村合作银行',
        4 => '城市信用社',
        5 => '农村信用社',
        6 => '资金互助社',
        7 => '开发性金融机构',
        8 => '邮政储蓄网点',
        9 => '金融资产管理公司',
        10 => '信托公司',
        11 => '财务公司',
        12 => '金融租赁公司',
        13 => '汽车金融公司',
        14 => '货币经纪公司',
        15 => '贷款公司',
        16 => '村镇银行',
        17 => '消费金融公司',
        18 => '其他金融机构'
    );

    const ORGANIZATIONS_SECOND_CATEGORY = array(
        1 => '总行',
        2 => '总行营业部',
        3 => '一级分行',
        4 => '一级分行营业部',
        5 => '二级分行',
        6 => '二级分行营业部',
        7 => '支行',
        8 => '其他分支机构',
        9 => '总行',
        10 => '总行营业部、专营机构',
        11 => '一级分行',
        12 => '一级分行营业部',
        13 => '二级分行',
        14 => '一级分行直属支行',
        15 => '二级分行营业部',
        16 => '支行',
        17 => '分理处、办事处、营业所',
        18 => '储蓄所',
        19 => '其他分支机构',
        20 => '邮储代理营业所',
        21 => '总行',
        22 => '支行',
        23 => '分理处',
        24 => '储蓄所',
        25 => '其他分支机构',
        26 => '法人',
        27 => '分社、营业部',
        28 => '其他分支机构',
        29 => '省级联合社',
        30 => '地级联合社',
        31 => '县级联合社',
        32 => '信用合作社',
        33 => '信用社',
        34 => '储蓄所',
        35 => '其他分支机构',
        36 => '法人',
        37 => '总行',
        38 => '总行营业部、专营机构',
        39 => '一级分行',
        40 => '其他分支机构',
        41 => '二级分行',
        42 => '二级分行营业部',
        43 => '支行',
        44 => '一级分行营业部',
        45 => '储蓄网点',
        46 => '总公司',
        47 => '分公司',
        48 => '其他分支机构',
        49 => '总公司',
        50 => '其他分支机构',
        51 => '总公司',
        52 => '其他分支机构',
        53 => '分公司',
        54 => '总公司',
        55 => '分公司',
        56 => '其他分支机构',
        57 => '总公司',
        58 => '总公司',
        59 => '分公司',
        60 => '其他分支机构',
        61 => '总公司',
        62 => '总行',
        63 => '其他分支机构',
        64 => '分理处',
        65 => '储蓄所',
        66 => '支行',
        67 => '总公司',
        68 => '分公司',
        69 => '总公司',
        70 => '分公司',
        71 => '其他分支机构',
    );

    public static function create(int $id, $type) : FinanceAuthenticationCategory
    {
        switch ($type) {
            case self::TYPE['ORGANIZATIONS_FIRST_CATEGORY']:
                $type = self::ORGANIZATIONS_FIRST_CATEGORY;
                break;
            case self::TYPE['ORGANIZATIONS_SECOND_CATEGORY']:
                $type = self::ORGANIZATIONS_SECOND_CATEGORY;
                break;
        }

        return in_array($id, array_keys($type))
            ? new FinanceAuthenticationCategory($id, $type[$id])
            : new NullFinanceAuthenticationCategory(0, '');
    }
}
