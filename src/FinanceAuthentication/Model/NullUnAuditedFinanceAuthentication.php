<?php
namespace Sdk\FinanceAuthentication\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\NullResubmitAbleTrait;
use Sdk\Common\Model\NullApplyAbleTrait;

class NullUnAuditedFinanceAuthentication extends UnAuditedFinanceAuthentication implements INull
{
    use NullResubmitAbleTrait,NullApplyAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
