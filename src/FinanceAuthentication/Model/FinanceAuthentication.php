<?php
namespace Sdk\FinanceAuthentication\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\FinanceAuthentication\Repository\FinanceAuthenticationRepository;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Common\Model\IOperatAble;

use Sdk\Common\Model\OperatAbleTrait;

class FinanceAuthentication implements IOperatAble, IObject
{
    use Object, OperatAbleTrait;

    private $id;

    private $enterpriseName;

    private $organizationsCategory;

    private $organizationsCategoryParent;

    private $code;

    private $licence;

    private $organizationCode;

    private $organizationCodeCertificate;

    private $enterprise;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->enterpriseName = '';
        $this->organizationsCategory = 0;
        $this->organizationsCategoryParent = 0;
        $this->code = '';
        $this->licence = array();
        $this->organizationCode = '';
        $this->organizationCodeCertificate = array();
        $this->enterprise = new Enterprise();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->repository = new FinanceAuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->enterpriseName);
        unset($this->organizationsCategory);
        unset($this->organizationsCategoryParent);
        unset($this->code);
        unset($this->licence);
        unset($this->organizationCode);
        unset($this->organizationCodeCertificate);
        unset($this->enterprise);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setOrganizationsCategory(FinanceAuthenticationCategory $organizationsCategory) : void
    {
        $this->organizationsCategory = $organizationsCategory;
    }

    public function getOrganizationsCategory() : FinanceAuthenticationCategory
    {
        return $this->organizationsCategory;
    }

    public function setOrganizationsCategoryParent(FinanceAuthenticationCategory $organizationsCategoryParent) : void
    {
        $this->organizationsCategoryParent = $organizationsCategoryParent;
    }

    public function getOrganizationsCategoryParent() : FinanceAuthenticationCategory
    {
        return $this->organizationsCategoryParent;
    }

    public function setCode(string $code) : void
    {
        $this->code = $code;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function setLicence(array $licence) : void
    {
        $this->licence = $licence;
    }

    public function getLicence() : array
    {
        return $this->licence;
    }

    public function setOrganizationCode(string $organizationCode) : void
    {
        $this->organizationCode = $organizationCode;
    }

    public function getOrganizationCode() : string
    {
        return $this->organizationCode;
    }

    public function setOrganizationCodeCertificate(array $organizationCodeCertificate) : void
    {
        $this->organizationCodeCertificate = $organizationCodeCertificate;
    }

    public function getOrganizationCodeCertificate() : array
    {
        return $this->organizationCodeCertificate;
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
