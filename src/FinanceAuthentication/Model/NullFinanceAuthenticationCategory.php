<?php
namespace Sdk\FinanceAuthentication\Model;

use Marmot\Interfaces\INull;

class NullFinanceAuthenticationCategory extends FinanceAuthenticationCategory implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self(0, '');
        }
        return self::$instance;
    }
}
