<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

use Sdk\FinanceAuthentication\Utils\MockFactory;

class FinanceAuthenticationMockAdapter implements IFinanceAuthenticationAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateFinanceAuthenticationObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $financeAuthenticationList = array();

        foreach ($ids as $id) {
            $financeAuthenticationList[] = MockFactory::generateFinanceAuthenticationObject($id);
        }

        return $financeAuthenticationList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateFinanceAuthenticationObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $financeAuthenticationList = array();

        foreach ($ids as $id) {
            $financeAuthenticationList[] = MockFactory::generateFinanceAuthenticationObject($id);
        }

        return $financeAuthenticationList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
