<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Marmot\Interfaces\IAsyncAdapter;

interface IUnAuditedFinanceAuthenticationAdapter extends IResubmitAbleAdapter, IApplyAbleAdapter, IFetchAbleAdapter, IAsyncAdapter //phpcs:ignore
{
}
