<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Sdk\Common\Adapter\ResubmitAbleMockAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;

use Sdk\FinanceAuthentication\Utils\MockFactory;

class UnAuditedFinanceAuthenticationMockAdapter implements IUnAuditedFinanceAuthenticationAdapter
{
    use ResubmitAbleMockAdapterTrait, ApplyAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateUnAuditedFinanceAuthenticationObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedFinanceList = array();

        foreach ($ids as $id) {
            $unAuditedFinanceList[] = MockFactory::generateUnAuditedFinanceAuthenticationObject($id);
        }

        return $unAuditedFinanceList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateUnAuditedFinanceAuthenticationObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $unAuditedFinanceList = array();

        foreach ($ids as $id) {
            $unAuditedFinanceList[] = MockFactory::generateUnAuditedFinanceAuthenticationObject($id);
        }

        return $unAuditedFinanceList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
