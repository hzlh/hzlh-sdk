<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\FinanceAuthentication\Model\FinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullFinanceAuthentication;

use Sdk\FinanceAuthentication\Translator\FinanceAuthenticationRestfulTranslator;

class FinanceAuthenticationRestfulAdapter extends GuzzleAdapter implements IFinanceAuthenticationAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
            'FINANCIAL_AUTHENTICATION_LIST'=>[
                'fields'=>[
                    'financeAuthentication' => []
                ],
                'include'=>'enterprise'
            ],
            'FINANCIAL_AUTHENTICATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=>'enterprise'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new FinanceAuthenticationRestfulTranslator();
        $this->resource = 'financeAuthentications';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullFinanceAuthentication::getInstance());
    }

    protected function addAction(FinanceAuthentication $financeAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financeAuthentication);
            return true;
        }

        return false;
    }

    protected function editAction(FinanceAuthentication $financeAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            )
        );

        $this->patch(
            $this->getResource().'/'.$financeAuthentication->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financeAuthentication);
            return true;
        }

        return false;
    }
}
