<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IFinanceAuthenticationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
