<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullUnAuditedFinanceAuthentication;

use Sdk\FinanceAuthentication\Translator\UnAuditedFinanceAuthenticationRestfulTranslator;

class UnAuditedFinanceAuthenticationRestfulAdapter extends GuzzleAdapter implements IUnAuditedFinanceAuthenticationAdapter //phpcs:ignore
{
    use FetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
            'UNAUDITED_FINANCE_AUTHENTICATION_LIST'=>[
                'fields'=>[
                    'unAuditedFinanceAuthentication' => []
                ],
                'include'=>'relation'
            ],
            'UNAUDITED_FINANCE_AUTHENTICATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=>'relation'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new UnAuditedFinanceAuthenticationRestfulTranslator();
        $this->resource = 'unAuditedFinanceAuthentications';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullUnAuditedFinanceAuthentication::getInstance());
    }

    protected function resubmitAction(UnAuditedFinanceAuthentication $unAuditedFinanceAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditedFinanceAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            )
        );

        $this->patch(
            $this->getResource().'/'.$unAuditedFinanceAuthentication->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditedFinanceAuthentication);
            return true;
        }

        return false;
    }
}
