<?php
namespace Sdk\FinanceAuthentication\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\FinanceAuthentication\Model\FinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullFinanceAuthentication;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\FinanceAuthentication\Model\FinanceAuthenticationCategoryFactory;

class FinanceAuthenticationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $financeAuthentication = null)
    {
        return $this->translateToObject($expression, $financeAuthentication);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $financeAuthentication = null)
    {
        if (empty($expression)) {
            return NullFinanceAuthentication::getInstance();
        }

        if ($financeAuthentication == null) {
            $financeAuthentication = new FinanceAuthentication();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $financeAuthentication->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['enterpriseName'])) {
            $financeAuthentication->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['organizationsCategory'])) {
            $financeAuthentication->setOrganizationsCategory(FinanceAuthenticationCategoryFactory::create(
                $attributes['organizationsCategory'],
                FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']
            ));
        }
        if (isset($attributes['organizationsCategoryParent'])) {
            $financeAuthentication->setOrganizationsCategoryParent(FinanceAuthenticationCategoryFactory::create(
                $attributes['organizationsCategoryParent'],
                FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']
            ));
        }
        if (isset($attributes['code'])) {
            $financeAuthentication->setCode($attributes['code']);
        }
        if (isset($attributes['licence'])) {
            $financeAuthentication->setLicence($attributes['licence']);
        }
        if (isset($attributes['organizationCode'])) {
            $financeAuthentication->setOrganizationCode($attributes['organizationCode']);
        }
        if (isset($attributes['organizationCodeCertificate'])) {
            $financeAuthentication->setOrganizationCodeCertificate($attributes['organizationCodeCertificate']);
        }
        if (isset($attributes['createTime'])) {
            $financeAuthentication->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $financeAuthentication->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $financeAuthentication->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $financeAuthentication->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $financeAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $financeAuthentication;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($financeAuthentication, array $keys = array())
    {
        if (!$financeAuthentication instanceof FinanceAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'code',
                'organizationsCategory',
                'organizationsCategoryParent',
                'licence',
                'enterprise'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'financeAuthentications',
            ),
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $financeAuthentication->getId();
        }

        $attributes = array();

        if (in_array('organizationsCategory', $keys)) {
            $attributes['organizationsCategory'] = $financeAuthentication->getOrganizationsCategory()->getId();
        }

        if (in_array('organizationsCategoryParent', $keys)) {
            $attributes['organizationsCategoryParent'] = $financeAuthentication
                        ->getOrganizationsCategoryParent()->getId();
        }

        if (in_array('code', $keys)) {
            $attributes['code'] = $financeAuthentication->getCode();
        }

        if (in_array('licence', $keys)) {
            $attributes['licence'] = $financeAuthentication->getLicence();
        }

        if (in_array('organizationCode', $keys)) {
            $attributes['organizationCode'] = $financeAuthentication->getOrganizationCode();
        }

        if (in_array('organizationCodeCertificate', $keys)) {
            $attributes['organizationCodeCertificate'] = $financeAuthentication->getOrganizationCodeCertificate();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $financeAuthentication->getEnterprise()->getId(),
                ),
            );
        }

        return $expression;
    }
}
