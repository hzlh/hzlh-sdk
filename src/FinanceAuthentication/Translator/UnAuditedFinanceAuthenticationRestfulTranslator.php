<?php
namespace Sdk\FinanceAuthentication\Translator;

use Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullUnAuditedFinanceAuthentication;

class UnAuditedFinanceAuthenticationRestfulTranslator extends FinanceAuthenticationRestfulTranslator
{
    public function arrayToObject(array $expression, $unAuditedFinanceAuthentication = null)
    {
        return $this->translateToObject($expression, $unAuditedFinanceAuthentication);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $unAuditedFinanceAuthentication = null)
    {
        if (empty($expression)) {
            return NullUnAuditedFinanceAuthentication::getInstance();
        }

        if ($unAuditedFinanceAuthentication == null) {
            $unAuditedFinanceAuthentication = new UnAuditedFinanceAuthentication();
        }

        $unAuditedFinanceAuthentication = parent::translateToObject($expression, $unAuditedFinanceAuthentication);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['rejectReason'])) {
            $unAuditedFinanceAuthentication->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['applyStatus'])) {
            $unAuditedFinanceAuthentication->setApplyStatus($attributes['applyStatus']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['relation']['data'])) {
            $relation = $this->changeArrayFormat($relationships['relation']['data']);
            $unAuditedFinanceAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($relation)
            );
        }

        return $unAuditedFinanceAuthentication;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($unAuditedFinanceAuthentication, array $keys = array())
    {
        $financeAuthentication = parent::objectToArray($unAuditedFinanceAuthentication, $keys);
        
        if (!$unAuditedFinanceAuthentication instanceof UnAuditedFinanceAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'rejectReason'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'unAuditedFinanceAuthentications'
            )
        );

        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditedFinanceAuthentication->getRejectReason();
        }

        $expression['data']['attributes'] = array_merge($financeAuthentication['data']['attributes'], $attributes);

        return $expression;
    }
}
