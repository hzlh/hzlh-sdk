<?php
namespace Sdk\LoanProduct\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Label\Translator\LabelRestfulTranslator;
use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\LoanProduct\Model\NullLoanProduct;

use Sdk\LoanProduct\Model\LoanProductCategoryFactory;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class LoanProductRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getLabelRestfulTranslator()
    {
        return new LabelRestfulTranslator();
    }

    public function getSnapshotRestfulTranslator()
    {
        return new SnapshotRestfulTranslator();
    }

    public function arrayToObject(array $expression, $loanProduct = null)
    {
        return $this->translateToObject($expression, $loanProduct);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $loanProduct = null)
    {
        if (empty($expression)) {
            return NullLoanProduct::getInstance();
        }

        if ($loanProduct == null) {
            $loanProduct = new LoanProduct();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $loanProduct->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $loanProduct->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $loanProduct->setNumber($attributes['number']);
        }
        if (isset($attributes['enterpriseName'])) {
            $loanProduct->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['cover'])) {
            $loanProduct->setCover($attributes['cover']);
        }
        if (isset($attributes['productObject'])) {
            $productObjects = array();
            foreach ($attributes['productObject'] as $productObject) {
                $productObjects[] = LoanProductCategoryFactory::create(
                    $productObject,
                    LoanProductCategoryFactory::TYPE['PRODUCT_OBJECT']
                );
            }

            $loanProduct->setProductObject($productObjects);
        }
        if (isset($attributes['guarantyStyles'])) {
            $guarantyStyles = array();
            foreach ($attributes['guarantyStyles'] as $guarantyStyle) {
                $guarantyStyles[] = LoanProductCategoryFactory::create(
                    $guarantyStyle,
                    LoanProductCategoryFactory::TYPE['GUARANTY_STYLES']
                );
            }
            $loanProduct->setGuarantyStyles($guarantyStyles);
        }
        if (isset($attributes['minLoanPeriod'])) {
            $loanProduct->setMinLoanPeriod($attributes['minLoanPeriod']);
        }
        if (isset($attributes['maxLoanPeriod'])) {
            $loanProduct->setMaxLoanPeriod($attributes['maxLoanPeriod']);
        }
        if (isset($attributes['supportCity'])) {
            $loanProduct->setSupportCity($attributes['supportCity']);
        }
        if (isset($attributes['minLoanAmount'])) {
            $loanProduct->setMinLoanAmount($attributes['minLoanAmount']);
        }
        if (isset($attributes['maxLoanAmount'])) {
            $loanProduct->setMaxLoanAmount($attributes['maxLoanAmount']);
        }
        if (isset($attributes['minLoanTerm'])) {
            $loanProduct->setMinLoanTerm($attributes['minLoanTerm']);
        }
        if (isset($attributes['maxLoanTerm'])) {
            $loanProduct->setMaxLoanTerm($attributes['maxLoanTerm']);
        }
        if (isset($attributes['loanTermUnit'])) {
            $loanProduct->setLoanTermUnit(LoanProductCategoryFactory::create(
                $attributes['loanTermUnit'],
                LoanProductCategoryFactory::TYPE['LOAN_TERM_UNIT']
            ));
        }
        if (isset($attributes['loanInterestRate'])) {
            $loanProduct->setLoanInterestRate($attributes['loanInterestRate']);
        }
        if (isset($attributes['loanInterestRateUnit'])) {
            $loanProduct->setLoanInterestRateUnit(LoanProductCategoryFactory::create(
                $attributes['loanInterestRateUnit'],
                LoanProductCategoryFactory::TYPE['LOAN_INTEREST_RATE_UNIT']
            ));
        }
        if (isset($attributes['repaymentMethods'])) {
            $repaymentMethods = array();
            foreach ($attributes['repaymentMethods'] as $repaymentMethod) {
                $repaymentMethods[] = LoanProductCategoryFactory::create(
                    $repaymentMethod,
                    LoanProductCategoryFactory::TYPE['REPAYMENT_METHOD']
                );
            }
            $loanProduct->setRepaymentMethods($repaymentMethods);
        }
        if (isset($attributes['isSupportEarlyRepayment'])) {
            $loanProduct->setIsSupportEarlyRepayment(LoanProductCategoryFactory::create(
                $attributes['isSupportEarlyRepayment'],
                LoanProductCategoryFactory::TYPE['IS_SUPPORT_EARLY_REPAYMENT']
            ));
        }
        if (isset($attributes['earlyRepaymentTerm'])) {
            $loanProduct->setEarlyRepaymentTerm($attributes['earlyRepaymentTerm']);
        }
        if (isset($attributes['isExistEarlyRepaymentCost'])) {
            $loanProduct->setIsExistEarlyRepaymentCost(LoanProductCategoryFactory::create(
                $attributes['isExistEarlyRepaymentCost'],
                LoanProductCategoryFactory::TYPE['IS_EXIST_EARLY_REPAYMENT_COST']
            ));
        }
        if (isset($attributes['applicationMaterial'])) {
            $loanProduct->setApplicationMaterial($attributes['applicationMaterial']);
        }
        if (isset($attributes['applicationCondition'])) {
            $loanProduct->setApplicationCondition($attributes['applicationCondition']);
        }
        if (isset($attributes['contract'])) {
            $loanProduct->setContract($attributes['contract']);
        }
        if (isset($attributes['application'])) {
            $loanProduct->setApplication($attributes['application']);
        }
        if (isset($attributes['loanMonthInterestRate'])) {
            $loanProduct->setLoanMonthInterestRate($attributes['loanMonthInterestRate']);
        }
        if (isset($attributes['volume'])) {
            $loanProduct->setVolume($attributes['volume']);
        }
        if (isset($attributes['volumeSuccess'])) {
            $loanProduct->setVolumeSuccess($attributes['volumeSuccess']);
        }
        if (isset($attributes['attentionDegree'])) {
            $loanProduct->setAttentionDegree($attributes['attentionDegree']);
        }
        if (isset($attributes['rejectReason'])) {
            $loanProduct->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $loanProduct->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $loanProduct->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $loanProduct->setStatus($attributes['status']);
        }
        if (isset($attributes['applyStatus'])) {
            $loanProduct->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['statusTime'])) {
            $loanProduct->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['labels']['data'])) {
            $this->setUpLabels($relationships['labels']['data'], $loanProduct);
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $loanProduct->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }
        if (isset($relationships['snapshot']['data'])) {
            $snapshot = $this->changeArrayFormat($relationships['snapshot']['data']);
            $loanProduct->setSnapshot(
                $this->getSnapshotRestfulTranslator()->arrayToObject($snapshot)
            );
        }

        return $loanProduct;
    }

    protected function setUpLabels(array $labels, LoanProduct $loanProduct)
    {
        foreach ($labels as $labelsArray) {
            $labels = $this->changeArrayFormat($labelsArray);
            $labelsObject = $this->getLabelRestfulTranslator()->arrayToObject($labels);
            $loanProduct->addLabel($labelsObject);
        }
    }
    
    protected function setUpLabelsArray(LoanProduct $loanProduct)
    {
        $labelsArray = [];

        $labels = $loanProduct->getLabels();
        foreach ($labels as $labelsKey) {
            $labelsArray[] = array(
                'type' => 'labels',
                'id' => $labelsKey->getId()
            );
        }

        return $labelsArray;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($loanProduct, array $keys = array())
    {
        if (!$loanProduct instanceof LoanProduct) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise',
                'rejectReason'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'loanProducts'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $loanProduct->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $loanProduct->getTitle();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $loanProduct->getCover();
        }
        if (in_array('productObject', $keys)) {
            foreach ($loanProduct->getProductObject() as $key => $productObject) {
                $attributes['productObject'][$key] = $productObject->getId();
            }
        }
        if (in_array('guarantyStyles', $keys)) {
            foreach ($loanProduct->getGuarantyStyles() as $key => $guarantyStyles) {
                $attributes['guarantyStyles'][$key] = $guarantyStyles->getId();
            }
        }
        if (in_array('minLoanPeriod', $keys)) {
            $attributes['minLoanPeriod'] = $loanProduct->getMinLoanPeriod();
        }
        if (in_array('maxLoanPeriod', $keys)) {
            $attributes['maxLoanPeriod'] = $loanProduct->getMaxLoanPeriod();
        }
        if (in_array('supportCity', $keys)) {
            $attributes['supportCity'] = $loanProduct->getSupportCity();
        }
        if (in_array('minLoanAmount', $keys)) {
            $attributes['minLoanAmount'] = $loanProduct->getMinLoanAmount();
        }
        if (in_array('maxLoanAmount', $keys)) {
            $attributes['maxLoanAmount'] = $loanProduct->getMaxLoanAmount();
        }
        if (in_array('minLoanTerm', $keys)) {
            $attributes['minLoanTerm'] = $loanProduct->getMinLoanTerm();
        }
        if (in_array('maxLoanTerm', $keys)) {
            $attributes['maxLoanTerm'] = $loanProduct->getMaxLoanTerm();
        }
        if (in_array('loanTermUnit', $keys)) {
            $attributes['loanTermUnit'] = $loanProduct->getLoanTermUnit()->getId();
        }
        if (in_array('loanInterestRate', $keys)) {
            $attributes['loanInterestRate'] = $loanProduct->getLoanInterestRate();
        }
        if (in_array('loanInterestRateUnit', $keys)) {
            $attributes['loanInterestRateUnit'] = $loanProduct->getLoanInterestRateUnit()->getId();
        }
        if (in_array('repaymentMethods', $keys)) {
            foreach ($loanProduct->getRepaymentMethods() as $key => $repaymentMethods) {
                $attributes['repaymentMethods'][$key] = $repaymentMethods->getId();
            }
        }
        if (in_array('isSupportEarlyRepayment', $keys)) {
            $attributes['isSupportEarlyRepayment'] = $loanProduct->getIsSupportEarlyRepayment()->getId();
        }
        if (in_array('earlyRepaymentTerm', $keys)) {
            $attributes['earlyRepaymentTerm'] = $loanProduct->getEarlyRepaymentTerm();
        }
        if (in_array('isExistEarlyRepaymentCost', $keys)) {
            $attributes['isExistEarlyRepaymentCost'] = $loanProduct->getIsExistEarlyRepaymentCost()->getId();
        }
        if (in_array('applicationMaterial', $keys)) {
            $attributes['applicationMaterial'] = $loanProduct->getApplicationMaterial();
        }
        if (in_array('applicationCondition', $keys)) {
            $attributes['applicationCondition'] = $loanProduct->getApplicationCondition();
        }
        if (in_array('contract', $keys)) {
            $attributes['contract'] = $loanProduct->getContract();
        }
        if (in_array('application', $keys)) {
            $attributes['application'] = $loanProduct->getApplication();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $loanProduct->getRejectReason();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type'=>'enterprises',
                    'id'=>$loanProduct->getEnterprise()->getId()
                )
            );
        }

        if (in_array('labels', $keys)) {
            $labelsArray = $this->setUpLabelsArray($loanProduct);
            $expression['data']['relationships']['labels']['data'] = $labelsArray;
        }

        return $expression;
    }
}
