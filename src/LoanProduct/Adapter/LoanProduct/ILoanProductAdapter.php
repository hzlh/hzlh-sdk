<?php
namespace Sdk\LoanProduct\Adapter\LoanProduct;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface ILoanProductAdapter extends IAsyncAdapter, IFetchAbleAdapter, ILoanProductOperatAdapter, IModifyStatusAbleAdapter//phpcs:ignore
{
}
