<?php
namespace Sdk\LoanProduct\Adapter\LoanProduct;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\ModifyStatusAbleMockAdapterTrait;
use Sdk\Common\Adapter\ResubmitAbleMockAdapterTrait;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\LoanProduct\Utils\MockFactory;

class LoanProductMockAdapter implements ILoanProductAdapter
{
    use OperatAbleMockAdapterTrait,
        ApplyAbleMockAdapterTrait,
        LoanProductMockAdapterTrait,
        ModifyStatusAbleMockAdapterTrait,
        ResubmitAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateLoanProductObject($id);
    }

    public function fetchList(array $ids): array
    {
        $loanProductList = array();

        foreach ($ids as $id) {
            $loanProductList[] = MockFactory::generateLoanProductObject($id);
        }

        return $loanProductList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateLoanProductObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $loanProductList = array();

        foreach ($ids as $id) {
            $loanProductList[] = MockFactory::generateLoanProductObject($id);
        }

        return $loanProductList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
