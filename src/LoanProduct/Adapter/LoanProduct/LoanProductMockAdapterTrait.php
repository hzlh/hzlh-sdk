<?php
namespace Sdk\LoanProduct\Adapter\LoanProduct;

trait LoanProductMockAdapterTrait
{
    public function onShelf(LoanProduct $loanProduct): bool
    {
        unset($loanProduct);
        return true;
    }

    public function offStock(LoanProduct $loanProduct): bool
    {
        unset($loanProduct);
        return true;
    }
}
