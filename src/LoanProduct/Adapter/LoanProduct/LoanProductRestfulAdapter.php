<?php
namespace Sdk\LoanProduct\Adapter\LoanProduct;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\LoanProduct\Model\NullLoanProduct;

use Sdk\LoanProduct\Translator\LoanProductRestfulTranslator;

class LoanProductRestfulAdapter extends GuzzleAdapter implements ILoanProductAdapter
{
    use CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'OA_LOAN_PRODUCT_LIST' => [
            'fields' => [],
            // 'fields' => [
            //     'loanProducts'=>'number,title,enterpriseName,productObject,minLoanAmount,maxLoanAmount,minLoanPeriod,maxLoanPeriod,enterprise,applyStatus,updateTime,status'//phpcs:ignore
            // ],
            'include' => 'enterprise,labels',
        ],
        'PORTAL_LOAN_PRODUCT_LIST' => [
            'fields' => [],
            'include' => 'enterprise,labels',
        ],
        'LOAN_PRODUCT_FETCH_ONE' => [
            'fields' => [],
            'include' => 'enterprise,snapshot,labels',
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new LoanProductRestfulTranslator();
        $this->resource = 'loanProducts';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullLoanProduct::getInstance());
    }

    /**
     * [addAction 发布贷款产品]
     * @param LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    protected function addAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
     * [editAction 编辑]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    protected function editAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
    * [resubmitAction 重新认证]
    * @param  LoanProduct $loanProduct [object]
    * @return [type]           [bool]
    */
    protected function resubmitAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
     * [onShelf 上架贷款产品]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    public function onShelf(LoanProduct $loanProduct): bool
    {
        return $this->onShelfAction($loanProduct);
    }

    protected function onShelfAction(LoanProduct $loanProduct): bool
    {
        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/onShelf'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }
        return false;
    }

    /**
     * [offStock 下架贷款产品]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    public function offStock(LoanProduct $loanProduct): bool
    {
        return $this->offStockAction($loanProduct);
    }

    protected function offStockAction(LoanProduct $loanProduct): bool
    {
        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/offStock'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }
        return false;
    }
}
