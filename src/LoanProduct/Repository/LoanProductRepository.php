<?php
namespace Sdk\LoanProduct\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Sdk\LoanProduct\Adapter\LoanProduct\ILoanProductAdapter;
use Sdk\LoanProduct\Adapter\LoanProduct\LoanProductMockAdapter;
use Sdk\LoanProduct\Adapter\LoanProduct\LoanProductRestfulAdapter;

use Sdk\LoanProduct\Model\LoanProduct;

class LoanProductRepository extends Repository implements ILoanProductAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_LOAN_PRODUCT_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_LOAN_PRODUCT_LIST';
    const FETCH_ONE_MODEL_UN = 'LOAN_PRODUCT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LoanProductRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): ILoanProductAdapter
    {
        return new LoanProductMockAdapter();
    }

    public function getActualAdapter(): ILoanProductAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function onShelf(LoanProduct $loanProduct): bool
    {
        return $this->getAdapter()->onShelf($loanProduct);
    }

    public function offStock(LoanProduct $loanProduct): bool
    {
        return $this->getAdapter()->offStock($loanProduct);
    }
}
