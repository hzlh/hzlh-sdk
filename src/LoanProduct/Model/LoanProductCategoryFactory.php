<?php
namespace Sdk\LoanProduct\Model;

use Sdk\Common\Model\IApplyAble;

class LoanProductCategoryFactory
{
    const PRODUCT_OBJECT_CN = array(
        LoanProduct::PRODUCT_OBJECT['ENTERPRISE'] => "企业",
        LoanProduct::PRODUCT_OBJECT['NATURAL_PERSON'] => "个人",
    );

    const GUARANTY_STYLES_CN = array(
        LoanProduct::GUARANTY_STYLES['CREDIT'] => "信用",
        LoanProduct::GUARANTY_STYLES['MORTGAGE'] => "抵押",
        LoanProduct::GUARANTY_STYLES['ENSURE'] => "保证",
        LoanProduct::GUARANTY_STYLES['PLEDGE'] => "质押",
    );

    const LOAN_TERM_UNIT_CN = array(
        LoanProduct::LOAN_TERM_UNIT['MONTH'] => "月",
        LoanProduct::LOAN_TERM_UNIT['DAY'] => "天",
    );

    const LOAN_INTEREST_RATE_UNIT_CN = array(
        LoanProduct::LOAN_INTEREST_RATE_UNIT['MONTH'] => "月",
        LoanProduct::LOAN_INTEREST_RATE_UNIT['YEAR'] => "年",
        LoanProduct::LOAN_INTEREST_RATE_UNIT['DAY'] => "日",
    );

    const REPAYMENT_METHOD_CN = array(
        LoanProduct::REPAYMENT_METHOD['AVERAGE_CAPITAL_PLUS_INTEREST'] => "等额本息",
        LoanProduct::REPAYMENT_METHOD['AVERAGE_CAPITAL'] => "等额本金",
        LoanProduct::REPAYMENT_METHOD['BEFORE_INTEREST_AFTER_PRINCIPAL_PAYMENT'] => "先息后本",
        LoanProduct::REPAYMENT_METHOD['BORROW_AND_RETURN'] => "随借随还",
    );

    const IS_SUPPORT_EARLY_REPAYMENT_CN = array(
        LoanProduct::IS_SUPPORT_EARLY_REPAYMENT['NO'] => "否",
        LoanProduct::IS_SUPPORT_EARLY_REPAYMENT['YES'] => "是",
    );

    const IS_EXIST_EARLY_REPAYMENT_COST_CN = array(
        LoanProduct::IS_EXIST_EARLY_REPAYMENT_COST['YES'] => "有",
        LoanProduct::IS_EXIST_EARLY_REPAYMENT_COST['NO'] => "无",
    );

    const TYPE = array(
        'PRODUCT_OBJECT' => 1,
        'GUARANTY_STYLES' => 2,
        'LOAN_TERM_UNIT' => 3,
        'LOAN_INTEREST_RATE_UNIT' => 4,
        'REPAYMENT_METHOD' => 5,
        'IS_SUPPORT_EARLY_REPAYMENT' => 6,
        'IS_EXIST_EARLY_REPAYMENT_COST' => 7,
    );

    public static function create(int $id, $type) : LoanProductCategory
    {
        switch ($type) {
            case self::TYPE['PRODUCT_OBJECT']:
                $type = self::PRODUCT_OBJECT_CN;
                break;
            case self::TYPE['GUARANTY_STYLES']:
                $type = self::GUARANTY_STYLES_CN;
                break;
            case self::TYPE['LOAN_TERM_UNIT']:
                $type = self::LOAN_TERM_UNIT_CN;
                break;
            case self::TYPE['LOAN_INTEREST_RATE_UNIT']:
                $type = self::LOAN_INTEREST_RATE_UNIT_CN;
                break;
            case self::TYPE['REPAYMENT_METHOD']:
                $type = self::REPAYMENT_METHOD_CN;
                break;
            case self::TYPE['IS_SUPPORT_EARLY_REPAYMENT']:
                $type = self::IS_SUPPORT_EARLY_REPAYMENT_CN;
                break;
            case self::TYPE['IS_EXIST_EARLY_REPAYMENT_COST']:
                $type = self::IS_EXIST_EARLY_REPAYMENT_COST_CN;
                break;
        }

        return in_array($id, array_keys($type))
            ? new LoanProductCategory($id, $type[$id])
            : new NullLoanProductCategory(0, '');
    }
}
