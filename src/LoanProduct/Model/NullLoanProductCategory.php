<?php
namespace Sdk\LoanProduct\Model;

use Marmot\Interfaces\INull;

class NullLoanProductCategory extends LoanProductCategory implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self(0, '');
        }
        return self::$instance;
    }
}
