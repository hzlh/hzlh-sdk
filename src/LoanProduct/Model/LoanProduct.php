<?php
namespace Sdk\LoanProduct\Model;

use Marmot\Core;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IResubmitAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\ResubmitAbleTrait;
use Sdk\Common\Model\ModifyStatusAbleTrait;

use Sdk\PolicyProduct\Model\IPolicyRelationAble;

use Sdk\Snapshot\Model\Snapshot;
use Sdk\Snapshot\Model\ISnapshotAble;

use Sdk\Label\Model\Label;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\LoanProduct\Repository\LoanProductRepository;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class LoanProduct implements IObject, IApplyAble, IOperatAble, IResubmitAble, IModifyStatusAble, ISnapshotAble, IPolicyRelationAble //phpcs:ignore
{
    use Object, ApplyAbleTrait, OperatAbleTrait, ResubmitAbleTrait, ModifyStatusAbleTrait;

    const LOAN_PRODUCT_STATUS = array(
        'ON_SHELF' => 0,
        'OFF_STOCK' => -2,
        'REVOKED' => -4,
        'CLOSED' => -6,
        'DELETED' => -8,
    );

    const PRODUCT_OBJECT = array(
        'ENTERPRISE' => 1,
        'NATURAL_PERSON' => 2
    );

    const GUARANTY_STYLES = array(
        'CREDIT' => 1,
        'MORTGAGE' => 2,
        'ENSURE' => 3,
        'PLEDGE' => 4
    );

    const LOAN_TERM_UNIT = array(
        'MONTH' => 1,
        'DAY' => 2
    );

    const LOAN_INTEREST_RATE_UNIT = array(
        'MONTH' => 1,
        'YEAR' => 2,
        'DAY' => 3
    );

    const REPAYMENT_METHOD = array(
        'AVERAGE_CAPITAL_PLUS_INTEREST' => 1,
        'AVERAGE_CAPITAL' => 2,
        'BEFORE_INTEREST_AFTER_PRINCIPAL_PAYMENT' => 3,
        'BORROW_AND_RETURN' => 4
    );

    const IS_SUPPORT_EARLY_REPAYMENT = array(
        'NO' => 0,
        'YES' => 1
    );

    const IS_EXIST_EARLY_REPAYMENT_COST = array(
        'YES' => 0,
        'NO' => 1
    );

    private $id;
    //产品标题
    private $title;
    //产品编号
    private $number;
    //产品封面
    private $cover;
    //贷款对象
    private $productObject;
    //担保方式
    private $guarantyStyles;
    //产品特点
    private $labels;
    //最短放款期限
    private $minLoanPeriod;
    //最长放款期限
    private $maxLoanPeriod;
    //支持城市
    private $supportCity;
    //最小可贷额度
    private $minLoanAmount;
    //最大可贷额度
    private $maxLoanAmount;
    //最小贷款期限
    private $minLoanTerm;
    //最大贷款期限
    private $maxLoanTerm;
    //贷款期限单位
    private $loanTermUnit;
    //贷款利率
    private $loanInterestRate;
    //贷款利率单位
    private $loanInterestRateUnit;
    //还款方式
    private $repaymentMethods;
    //是否支持提前还款
    private $isSupportEarlyRepayment;
    //提前还款期限
    private $earlyRepaymentTerm;
    //是否存在提前还款费用
    private $isExistEarlyRepaymentCost;
    //申请材料
    private $applicationMaterial;
    //申请条件
    private $applicationCondition;
    //合同
    private $contract;
    //贷款申请书
    private $application;
    //所属企业
    private $enterprise;
    //快照信息
    private $snapshot;
    //贷款月利率
    private $loanMonthInterestRate;
    //申请量
    private $volume;
    //申请成功量
    private $volumeSuccess;
    //关注度
    private $attentionDegree;
    //驳回原因
    private $rejectReason;
    //企业名称
    private $enterpriseName;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->number = '';
        $this->cover = array();
        $this->productObject = array();
        $this->guarantyStyles = array();
        $this->labels = array();
        $this->minLoanPeriod = 0;
        $this->maxLoanPeriod = 0;
        $this->supportCity = array();
        $this->minLoanAmount = 0.00;
        $this->maxLoanAmount = 0.00;
        $this->minLoanTerm = 0;
        $this->maxLoanTerm = 0;
        $this->loanTermUnit = NullLoanProductCategory::getInstance();
        $this->loanInterestRate = 0.0000;
        $this->loanInterestRateUnit = NullLoanProductCategory::getInstance();
        $this->repaymentMethods = array();
        $this->isSupportEarlyRepayment = NullLoanProductCategory::getInstance();
        $this->earlyRepaymentTerm = 0;
        $this->isExistEarlyRepaymentCost = NullLoanProductCategory::getInstance();
        $this->applicationMaterial = '';
        $this->applicationCondition = '';
        $this->contract = array();
        $this->application = array();
        $this->enterprise = new Enterprise();
        $this->snapshot = new Snapshot();
        $this->applyStatus = IApplyAble::APPLY_STATUS['PENDING'];
        $this->status = self::LOAN_PRODUCT_STATUS['DELETED'];
        $this->loanMonthInterestRate = 0.0000;
        $this->volume = 0;
        $this->volumeSuccess = 0;
        $this->attentionDegree = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->rejectReason = '';
        $this->enterpriseName = '';
        $this->repository = new LoanProductRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->number);
        unset($this->cover);
        unset($this->productObject);
        unset($this->guarantyStyles);
        unset($this->labels);
        unset($this->minLoanPeriod);
        unset($this->maxLoanPeriod);
        unset($this->supportCity);
        unset($this->minLoanAmount);
        unset($this->maxLoanAmount);
        unset($this->minLoanTerm);
        unset($this->maxLoanTerm);
        unset($this->loanTermUnit);
        unset($this->loanInterestRate);
        unset($this->loanInterestRateUnit);
        unset($this->repaymentMethods);
        unset($this->isSupportEarlyRepayment);
        unset($this->earlyRepaymentTerm);
        unset($this->isExistEarlyRepaymentCost);
        unset($this->applicationMaterial);
        unset($this->applicationCondition);
        unset($this->contract);
        unset($this->application);
        unset($this->enterprise);
        unset($this->snapshot);
        unset($this->applyStatus);
        unset($this->status);
        unset($this->loanMonthInterestRate);
        unset($this->volume);
        unset($this->volumeSuccess);
        unset($this->attentionDegree);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->rejectReason);
        unset($this->enterpriseName);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }
    
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setCover(array $cover): void
    {
        $this->cover = $cover;
    }

    public function getCover(): array
    {
        return $this->cover;
    }

    public function setProductObject(array $productObject): void
    {
        $this->productObject = $productObject;
    }

    public function getProductObject(): array
    {
        return $this->productObject;
    }
    
    public function setGuarantyStyles(array $guarantyStyles): void
    {
        $this->guarantyStyles = $guarantyStyles;
    }

    public function getGuarantyStyles(): array
    {
        return $this->guarantyStyles;
    }

    public function addLabel(Label $label) : void
    {
        $this->labels[] = $label;
    }

    public function clearLabel()
    {
        $this->labels = [];
    }

    public function getLabels() : array
    {
        return $this->labels;
    }

    public function setMinLoanPeriod(int $minLoanPeriod): void
    {
        $this->minLoanPeriod = $minLoanPeriod;
    }

    public function getMinLoanPeriod(): int
    {
        return $this->minLoanPeriod;
    }

    public function setMaxLoanPeriod(int $maxLoanPeriod): void
    {
        $this->maxLoanPeriod = $maxLoanPeriod;
    }

    public function getMaxLoanPeriod(): int
    {
        return $this->maxLoanPeriod;
    }

    public function setSupportCity(array $supportCity): void
    {
        $this->supportCity = $supportCity;
    }

    public function getSupportCity(): array
    {
        return $this->supportCity;
    }

    public function setMinLoanAmount(float $minLoanAmount): void
    {
        $this->minLoanAmount = $minLoanAmount;
    }

    public function getMinLoanAmount(): float
    {
        return $this->minLoanAmount;
    }

    public function setMaxLoanAmount(float $maxLoanAmount): void
    {
        $this->maxLoanAmount = $maxLoanAmount;
    }

    public function getMaxLoanAmount(): float
    {
        return $this->maxLoanAmount;
    }

    public function setMinLoanTerm(int $minLoanTerm): void
    {
        $this->minLoanTerm = $minLoanTerm;
    }

    public function getMinLoanTerm(): int
    {
        return $this->minLoanTerm;
    }

    public function setMaxLoanTerm(int $maxLoanTerm): void
    {
        $this->maxLoanTerm = $maxLoanTerm;
    }

    public function getMaxLoanTerm(): int
    {
        return $this->maxLoanTerm;
    }

    public function setLoanTermUnit(LoanProductCategory $loanTermUnit): void
    {
        $this->loanTermUnit = $loanTermUnit;
    }

    public function getLoanTermUnit(): LoanProductCategory
    {
        return $this->loanTermUnit;
    }

    public function setLoanInterestRate(float $loanInterestRate): void
    {
        $this->loanInterestRate = $loanInterestRate;
    }

    public function getLoanInterestRate(): float
    {
        return $this->loanInterestRate;
    }

    public function setLoanInterestRateUnit(LoanProductCategory $loanInterestRateUnit): void
    {
        $this->loanInterestRateUnit = $loanInterestRateUnit;
    }

    public function getLoanInterestRateUnit(): LoanProductCategory
    {
        return $this->loanInterestRateUnit;
    }

    public function setRepaymentMethods(array $repaymentMethods): void
    {
        $this->repaymentMethods = $repaymentMethods;
    }

    public function getRepaymentMethods(): array
    {
        return $this->repaymentMethods;
    }

    public function setIsSupportEarlyRepayment(LoanProductCategory $isSupportEarlyRepayment): void
    {
        $this->isSupportEarlyRepayment = $isSupportEarlyRepayment;
    }

    public function getIsSupportEarlyRepayment(): LoanProductCategory
    {
        return $this->isSupportEarlyRepayment;
    }

    public function setEarlyRepaymentTerm(int $earlyRepaymentTerm): void
    {
        $this->earlyRepaymentTerm = $earlyRepaymentTerm;
    }

    public function getEarlyRepaymentTerm(): int
    {
        return $this->earlyRepaymentTerm;
    }

    public function setIsExistEarlyRepaymentCost(LoanProductCategory $isExistEarlyRepaymentCost): void
    {
        $this->isExistEarlyRepaymentCost = $isExistEarlyRepaymentCost;
    }

    public function getIsExistEarlyRepaymentCost(): LoanProductCategory
    {
        return $this->isExistEarlyRepaymentCost;
    }

    public function setApplicationMaterial(string $applicationMaterial): void
    {
        $this->applicationMaterial = $applicationMaterial;
    }

    public function getApplicationMaterial(): string
    {
        return $this->applicationMaterial;
    }

    public function setApplicationCondition(string $applicationCondition): void
    {
        $this->applicationCondition = $applicationCondition;
    }

    public function getApplicationCondition(): string
    {
        return $this->applicationCondition;
    }

    public function setContract(array $contract): void
    {
        $this->contract = $contract;
    }

    public function getContract(): array
    {
        return $this->contract;
    }

    public function setApplication(array $application): void
    {
        $this->application = $application;
    }

    public function getApplication(): array
    {
        return $this->application;
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::LOAN_PRODUCT_STATUS)) ?
            $status : self::LOAN_PRODUCT_STATUS['DELETED'];
    }

    public function setSnapshot(Snapshot $snapshot): void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot(): Snapshot
    {
        return $this->snapshot;
    }

    public function setLoanMonthInterestRate(float $loanMonthInterestRate): void
    {
        $this->loanMonthInterestRate = $loanMonthInterestRate;
    }

    public function getLoanMonthInterestRate(): float
    {
        return $this->loanMonthInterestRate;
    }

    public function setVolume(int $volume): void
    {
        $this->volume = $volume;
    }

    public function getVolume(): int
    {
        return $this->volume;
    }

    public function setVolumeSuccess(int $volumeSuccess): void
    {
        $this->volumeSuccess = $volumeSuccess;
    }

    public function getVolumeSuccess(): int
    {
        return $this->volumeSuccess;
    }

    public function setAttentionDegree(int $attentionDegree): void
    {
        $this->attentionDegree = $attentionDegree;
    }

    public function getAttentionDegree(): int
    {
        return $this->attentionDegree;
    }

    protected function getRepository(): LoanProductRepository
    {
        return $this->repository;
    }

    protected function getIResubmitAbleAdapter(): IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter(): IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIModifyStatusAbleAdapter(): IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 下架
     * @return bool 是否下架成功
     */
    public function offStock(): bool
    {
        if (!$this->isOnShelf()) {
            return false;
        }

        return $this->getRepository()->offStock($this);
    }

    /**
     * 上架
     * @return bool 是否上架成功
     */
    public function onShelf(): bool
    {
        if (!$this->isOffStock()) {
            return false;
        }

        return $this->getRepository()->onShelf($this);
    }

    /**
     * 关闭
     * @return bool 是否关闭成功
     */
    public function close() : bool
    {
        if (!$this->isApprove() || !$this->isOffStock()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }
        $modifyStatusAdapter = $this->getIModifyStatusAbleAdapter();
        return $modifyStatusAdapter->close($this);
    }

    public function isOnShelf(): bool
    {
        return $this->getStatus() == self::LOAN_PRODUCT_STATUS['ON_SHELF'];
    }

    public function isOffStock(): bool
    {
        return $this->getStatus() == self::LOAN_PRODUCT_STATUS['OFF_STOCK'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::LOAN_PRODUCT_STATUS['ON_SHELF'];
    }

    public function isRevoked() : bool
    {
        return $this->getStatus() == self::LOAN_PRODUCT_STATUS['REVOKED'];
    }

    public function isClosed() : bool
    {
        return $this->getStatus() == self::LOAN_PRODUCT_STATUS['CLOSED'];
    }
}
