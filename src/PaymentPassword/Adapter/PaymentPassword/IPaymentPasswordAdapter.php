<?php
namespace Sdk\PaymentPassword\Adapter\PaymentPassword;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IPaymentPasswordAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
