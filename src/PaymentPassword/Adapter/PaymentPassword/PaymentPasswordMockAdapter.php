<?php
namespace Sdk\PaymentPassword\Adapter\PaymentPassword;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

use Sdk\PaymentPassword\Model\PaymentPassword;
use Sdk\PaymentPassword\Utils\MockFactory;

class PaymentPasswordMockAdapter implements IPaymentPasswordAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generatePaymentPasswordObject($id);
    }

    public function fetchList(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generatePaymentPasswordObject($id);
        }

        return $serviceList;
    }

    public function search(array $filter = array(), array $sort = array(), int $offset = 0, int $size = 20): array
    {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generatePaymentPasswordObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generatePaymentPasswordObject($id);
        }

        return $serviceList;
    }

    public function searchAsync(array $filter = array(), array $sort = array(), int $offset = 0, int $size = 20): array
    {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
