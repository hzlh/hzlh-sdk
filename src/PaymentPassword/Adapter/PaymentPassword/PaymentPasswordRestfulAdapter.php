<?php
namespace Sdk\PaymentPassword\Adapter\PaymentPassword;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\PaymentPassword\Model\PaymentPassword;
use Sdk\PaymentPassword\Model\NullPaymentPassword;
use Sdk\PaymentPassword\Translator\PaymentPasswordRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PaymentPasswordRestfulAdapter extends GuzzleAdapter implements IPaymentPasswordAdapter
{
    use CommonMapErrorsTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'PAYMENT_PASSWORD_LIST' => [
            'fields' => [],
            'include' => 'memberAccount',
        ],

        'PAYMENT_PASSWORD_FETCH_ONE' => [
            'fields' => [],
            'include' => 'memberAccount',
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PaymentPasswordRestfulTranslator();
        $this->resource = 'paymentPasswords';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            10 => PAYMENT_PASSWORD_NOT_SET,
            1004 => PAYMENT_PASSWORD_INCORRECT
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPaymentPassword::getInstance());
    }

    protected function addAction(PaymentPassword $paymentPassword): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $paymentPassword,
            array(
                'password',
                'memberAccount'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($paymentPassword);
            return true;
        }

        return false;
    }

    protected function editAction(PaymentPassword $paymentPassword): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $paymentPassword,
            array(
                'oldPassword',
                'password'
            )
        );

        $this->patch(
            $this->getResource().'/'.$paymentPassword->getId().'/updatePassword',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($paymentPassword);
            return true;
        }

        return false;
    }

    public function resetPaymentPassword(PaymentPassword $paymentPassword) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $paymentPassword,
            array(
                'cellphone',
                'password'
            )
        );

        $this->patch(
            $this->getResource().'/resetPassword',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($paymentPassword);
            return true;
        }

        return false;
    }

    public function validate(PaymentPassword $paymentPassword) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $paymentPassword,
            array(
                'password',
                'memberAccount'
            )
        );
        
        $this->patch(
            $this->getResource().'/validate',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($paymentPassword);
            return true;
        }

        return false;
    }
}
