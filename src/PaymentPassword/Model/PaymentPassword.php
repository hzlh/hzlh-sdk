<?php
namespace Sdk\PaymentPassword\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\PaymentPassword\Repository\PaymentPasswordRepository;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class PaymentPassword implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    private $cellphone;

    private $password;

    private $oldPassword;

    private $memberAccount;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->cellphone = '';
        $this->password = '';
        $this->oldPassword = '';
        $this->memberAccount = new MemberAccount();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->repository = new PaymentPasswordRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->cellphone);
        unset($this->password);
        unset($this->oldPassword);
        unset($this->memberAccount);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setOldPassword(string $oldPassword) : void
    {
        $this->oldPassword = $oldPassword;
    }

    public function getOldPassword() : string
    {
        return $this->oldPassword;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }

    protected function getRepository(): PaymentPasswordRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * [resetPaymentPassword 重置支付密码]
     * @return [type] [bool]
     */
    public function resetPaymentPassword()
    {
        return $this->getRepository()->resetPaymentPassword($this);
    }
    /**
     * [validate 验证旧支付密码]
     * @return [type] [bool]
     */
    public function validate()
    {
        return $this->getRepository()->validate($this);
    }
}
