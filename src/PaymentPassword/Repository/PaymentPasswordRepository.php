<?php
namespace Sdk\PaymentPassword\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\PaymentPassword\Adapter\PaymentPassword\IPaymentPasswordAdapter;
use Sdk\PaymentPassword\Adapter\PaymentPassword\PaymentPasswordMockAdapter;
use Sdk\PaymentPassword\Adapter\PaymentPassword\PaymentPasswordRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\PaymentPassword\Model\PaymentPassword;

class PaymentPasswordRepository extends Repository implements IPaymentPasswordAdapter
{
    use AsyncRepositoryTrait, FetchRepositoryTrait, OperatAbleRepositoryTrait;

    const LIST_MODEL_UN = 'PAYMENT_PASSWORD_LIST';
    const FETCH_ONE_MODEL_UN = 'PAYMENT_PASSWORD_FETCH_ONE';

    private $adapter;

    private $mockAdapter;

    public function __construct()
    {
        $this->adapter = new PaymentPasswordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
        $this->mockAdapter = new PaymentPasswordMockAdapter();
    }

    public function getMockAdapter(): IPaymentPasswordAdapter
    {
        return $this->mockAdapter;
    }

    public function getActualAdapter(): IPaymentPasswordAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    /**
     * [resetPaymentPassword 重置支付密码]
     * @param  PaymentPassword $paymentPassword [PaymentPassword]
     * @return [type]                            [bool]
     */
    public function resetPaymentPassword(PaymentPassword $paymentPassword)
    {
        return $this->getAdapter()->resetPaymentPassword($paymentPassword);
    }
    /**
     * [validate 验证支付密码]
     * @param  PaymentPassword $paymentPassword [PaymentPassword]
     * @return [type]                            [bool]
     */
    public function validate(PaymentPassword $paymentPassword)
    {
        return $this->getAdapter()->validate($paymentPassword);
    }
}
