<?php
namespace Sdk\PaymentPassword\Translator;

use Sdk\PaymentPassword\Model\PaymentPassword;
use Sdk\PaymentPassword\Model\NullPaymentPassword;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class PaymentPasswordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    public function arrayToObject(array $expression, $paymentPassword = null)
    {
        return $this->translateToObject($expression, $paymentPassword);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $paymentPassword = null)
    {
        if (empty($expression)) {
            return NullPaymentPassword::getInstance();
        }
        if ($paymentPassword === null) {
            $paymentPassword = new PaymentPassword();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $paymentPassword->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $paymentPassword->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['password'])) {
            $paymentPassword->setPassword($attributes['password']);
        }
        if (isset($attributes['oldPassword'])) {
            $paymentPassword->setOldPassword($attributes['oldPassword']);
        }
        if (isset($attributes['statusTime'])) {
            $paymentPassword->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $paymentPassword->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $paymentPassword->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $paymentPassword->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['memberAccount']['data'])) {
            $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            $paymentPassword->setMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount)
            );
        }

        return $paymentPassword;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($paymentPassword, array $keys = array())
    {
        $expression = array();

        if (!$paymentPassword instanceof PaymentPassword) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'cellphone',
                'password',
                'oldPassword',
                'memberAccount'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'paymentPasswords'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $paymentPassword->getId();
        }

        $attributes = array();

        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $paymentPassword->getCellphone();
        }
        if (in_array('password', $keys)) {
            $attributes['password'] = $paymentPassword->getPassword();
        }
        if (in_array('oldPassword', $keys)) {
            $attributes['oldPassword'] = $paymentPassword->getOldPassword();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type' => 'memberAccounts',
                    'id' => $paymentPassword->getMemberAccount()->getId()
                )
            );
        }

        return $expression;
    }
}
