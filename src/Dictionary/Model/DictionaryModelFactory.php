<?php
namespace Sdk\Dictionary\Model;

class DictionaryModelFactory
{
    /**
     * 所属分类
     */
    const CATEGORY = array(
            1 => array(
                'name' => '新闻类',
                'level' => 1
            ),
            2 => array(
                'name' => '问答类',
                'level' => 1
            ),
            3 => array(
                'name' => '职称类',
                'level' => 3
            ),
            4 => array(
                'name' => '政策适用对象',
                'level' => 1
            ),
            5 => array(
                'name' => '政策适用行业',
                'level' => 1
            ),
            6 => array(
                'name' => '政策级别',
                'level' => 1
            ),
            7 => array(
                'name' => '政策分类',
                'level' => 1
            ),
            8 => array(
                'name' => '帐套行业',
                'level' => 1
            ),
        );
}
