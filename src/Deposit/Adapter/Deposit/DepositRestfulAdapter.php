<?php
namespace Sdk\Deposit\Adapter\Deposit;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Model\NullDeposit;
use Sdk\Deposit\Translator\DepositRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class DepositRestfulAdapter extends GuzzleAdapter implements IDepositAdapter
{
    use FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'DEPOSIT_LIST'=>[
            'fields'=>[
                'deposits'=>'amount,memberAccount,status,createTime,updateTime',
            ],
            'include'=> 'memberAccount'
        ],
        'DEPOSIT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'memberAccount'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new DepositRestfulTranslator();
        $this->resource = 'deposits';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullDeposit::getInstance());
    }

    public function deposit(Deposit $deposit) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array('amount', 'memberAccount')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }

    public function pay(Deposit $deposit) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo'
            )
        );
        
        $this->post(
            'payments/'.$deposit->getPaymentId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }

    public function paymentFailure(Deposit $deposit) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array(
                'failureReason'
            )
        );
        
        $this->patch(
            'payments/'.$deposit->getPaymentId().'/paymentFailure',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }
}
