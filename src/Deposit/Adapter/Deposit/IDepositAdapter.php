<?php
namespace Sdk\Deposit\Adapter\Deposit;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Sdk\Deposit\Model\Deposit;

interface IDepositAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    public function deposit(Deposit $deposit) : bool;

    public function pay(Deposit $deposit) : bool;

    public function paymentFailure(Deposit $deposit) : bool;
}
