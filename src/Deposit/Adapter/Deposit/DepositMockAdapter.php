<?php
namespace Sdk\Deposit\Adapter\Deposit;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Utils\MockFactory;

class DepositMockAdapter implements IDepositAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateDepositObject($id);
    }

    public function fetchList(array $ids): array
    {
        $depositList = array();

        foreach ($ids as $id) {
            $depositList[] = MockFactory::generateDepositObject($id);
        }

        return $depositList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateDepositObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $depositList = array();

        foreach ($ids as $id) {
            $depositList[] = MockFactory::generateDepositObject($id);
        }

        return $depositList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function deposit(Deposit $deposit): bool
    {
        unset($deposit);
        return true;
    }

    public function pay(Deposit $deposit) : bool
    {
        unset($deposit);
        return true;
    }

    public function paymentFailure(Deposit $deposit) : bool
    {
        unset($deposit);
        return true;
    }
}
