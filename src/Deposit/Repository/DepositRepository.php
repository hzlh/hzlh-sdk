<?php
namespace Sdk\Deposit\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;

use Sdk\Deposit\Adapter\Deposit\IDepositAdapter;
use Sdk\Deposit\Adapter\Deposit\DepositMockAdapter;
use Sdk\Deposit\Adapter\Deposit\DepositRestfulAdapter;

use Sdk\Deposit\Model\Deposit;

class DepositRepository extends Repository implements IDepositAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'DEPOSIT_LIST_MODEL';
    const FETCH_ONE_MODEL_UN = 'DEPOSIT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new DepositRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IDepositAdapter
    {
        return new DepositMockAdapter();
    }

    public function getActualAdapter(): IDepositAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deposit(Deposit $deposit): bool
    {
        return $this->getAdapter()->deposit($deposit);
    }

    public function pay(Deposit $deposit): bool
    {
        return $this->getAdapter()->pay($deposit);
    }

    public function paymentFailure(Deposit $deposit): bool
    {
        return $this->getAdapter()->paymentFailure($deposit);
    }
}
