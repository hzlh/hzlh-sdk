<?php
namespace Sdk\Deposit\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Deposit\Repository\DepositRepository;

use Sdk\Payment\Model\IPayAble;
use Sdk\Payment\Model\Payment;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\TradeRecord\Model\ITradeAble;

class Deposit implements IObject, IPayAble, ITradeAble
{
    const DEPOSIT_STATUS = array(
        "PENDING" => 0, //未充值
        "APPROVE" => 2 //已充值
    );

    use Object;

    private $id;

    private $number;

    private $paymentId;
    
    private $memberAccount;

    private $payment;

    private $amount;

    private $failureReason;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->paymentId = '';
        $this->memberAccount = new MemberAccount();
        $this->payment = new Payment();
        $this->amount = 0.0;
        $this->failureReason = array();
        $this->status = self::DEPOSIT_STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new DepositRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->paymentId);
        unset($this->memberAccount);
        unset($this->payment);
        unset($this->amount);
        unset($this->failureReason);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setPaymentId(string $paymentId) : void
    {
        $this->paymentId = $paymentId;
    }

    public function getPaymentId() : string
    {
        return $this->paymentId;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }
    
    public function setPayment(Payment $payment) : void
    {
        $this->payment = $payment;
    }

    public function getPayment() : Payment
    {
        return $this->payment;
    }

    public function setAmount(float $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function setFailureReason(array $failureReason) : void
    {
        $this->failureReason = $failureReason;
    }

    public function getFailureReason() : array
    {
        return $this->failureReason;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::DEPOSIT_STATUS) ?
                        $status :
                        self::DEPOSIT_STATUS['PENDING'];
    }

    protected function getRepository() : DepositRepository
    {
        return $this->repository;
    }
    
    public function deposit() : bool
    {
        return $this->getRepository()->deposit($this);
    }
    
    public function pay() : bool
    {
        return $this->getRepository()->pay($this);
    }

    public function paymentFailure() : bool
    {
        return $this->getRepository()->paymentFailure($this);
    }
    
    public function isBelongCurrentMember($memberAccountId) : bool
    {
        if ($memberAccountId != $this->getMemberAccount()->getId()) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return true;
    }
}
