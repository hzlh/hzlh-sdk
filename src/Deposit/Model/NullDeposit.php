<?php
namespace Sdk\Deposit\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullDeposit extends Deposit implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist(): bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function deposit(): bool
    {
        return $this->resourceNotExist();
    }

    public function pay(): bool
    {
        return $this->resourceNotExist();
    }

    public function paymentFailure(): bool
    {
        return $this->resourceNotExist();
    }
}
