<?php
namespace Sdk\Deposit\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Model\NullDeposit;

use Sdk\Payment\Model\Payment;

class DepositRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    public function arrayToObject(array $expression, $deposit = null)
    {
        return $this->translateToObject($expression, $deposit);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $deposit = null)
    {
        if (empty($expression)) {
            return NullDeposit::getInstance();
        }

        if ($deposit == null) {
            $deposit = new Deposit();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $deposit->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['amount'])) {
            $deposit->setAmount($attributes['amount']);
        }

        $type = isset($attributes['paymentType'])? $attributes['paymentType'] : 0;
        $transactionNumber = isset($attributes['transactionNumber'])? $attributes['transactionNumber'] : '';
        $time = isset($attributes['paymentTime'])? $attributes['paymentTime'] : 0;
        $transactionInfo = isset($attributes['transactionInfo'])? $attributes['transactionInfo'] : array();
        $deposit->setPayment(new Payment($type, $time, $transactionNumber, $transactionInfo));

        if (isset($attributes['number'])) {
            $deposit->setNumber($attributes['number']);
        }
        if (isset($attributes['failureReason'])) {
            $deposit->setFailureReason($attributes['failureReason']);
        }
        if (isset($attributes['paymentId'])) {
            $deposit->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['createTime'])) {
            $deposit->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $deposit->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $deposit->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $deposit->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['memberAccount']['data'])) {
            if (isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat(
                    $relationships['memberAccount']['data'],
                    $expression['included']
                );
            }
            if (!isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            }
            $deposit->setMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount)
            );
        }

        return $deposit;
    }

     /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($deposit, array $keys = array())
    {
        if (!$deposit instanceof Deposit) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'amount',
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'memberAccount',
                'failureReason'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'deposits'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $deposit->getId();
        }

        $attributes = array();

        if (in_array('amount', $keys)) {
            $attributes['amount'] = $deposit->getAmount();
        }
        if (in_array('paymentType', $keys)) {
            $attributes['paymentType'] = $deposit->getPayment()->getType();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $deposit->getPayment()->getTransactionNumber();
        }
        if (in_array('transactionInfo', $keys)) {
            $attributes['transactionInfo'] = $deposit->getPayment()->getTransactionInfo();
        }
        if (in_array('failureReason', $keys)) {
            $attributes['failureReason'] = $deposit->getFailureReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type'=>'memberAccounts',
                    'id'=>$deposit->getMemberAccount()->getId()
                )
            );
        }

        return $expression;
    }
}
