<?php
namespace Sdk\Deposit\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Model\NullDeposit;

use Sdk\MemberAccount\Translator\MemberAccountTranslator;

class DepositTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $deposit = null)
    {
        unset($deposit);
        unset($expression);
        return NullDeposit::getInstance();
    }

    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return new MemberAccountTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($deposit, array $keys = array())
    {
        if (!$deposit instanceof Deposit) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'paymentId',
                'memberAccount'=>[],
                'amount',
                'number',
                'failureReason',
                'paymentType',
                'paymentTime',
                'transactionNumber',
                'transactionInfo',
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($deposit->getId());
        }
        if (in_array('paymentId', $keys)) {
            $expression['paymentId'] = marmot_encode($deposit->getPaymentId());
        }
        if (in_array('amount', $keys)) {
            $expression['amount'] = $deposit->getAmount();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $deposit->getNumber();
        }
        if (in_array('failureReason', $keys)) {
            $expression['failureReason'] = $deposit->getFailureReason();
        }
        if (in_array('paymentType', $keys)) {
            $expression['paymentType'] = $deposit->getPayment()->getType();
        }
        if (in_array('paymentTime', $keys)) {
            $expression['paymentTime'] = $deposit->getPayment()->getTime();
        }
        if (in_array('transactionNumber', $keys)) {
            $expression['transactionNumber'] = $deposit->getPayment()->getTransactionNumber();
        }
        if (in_array('transactionInfo', $keys)) {
            $expression['transactionInfo'] = $deposit->getPayment()->getTransactionInfo();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $deposit->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $deposit->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $deposit->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $deposit->getStatus();
        }
        if (isset($keys['memberAccount'])) {
            $expression['memberAccount'] = $this->getMemberAccountTranslator()->objectToArray(
                $deposit->getMemberAccount(),
                $keys['memberAccount']
            );
        }

        return $expression;
    }
}
