<?php
namespace Sdk\ProfessionalTitle\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ProfessionalTitle\Model\ProfessionalTitle;
use Sdk\ProfessionalTitle\Model\NullProfessionalTitle;

use Sdk\Staff\Translator\StaffRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

class ProfessionalTitleRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }

    protected function getDictionaryRestfulTranslator() : DictionaryRestfulTranslator
    {
        return new DictionaryRestfulTranslator();
    }

    public function arrayToObject(array $expression, $professionalTitle = null)
    {
        return $this->translateToObject($expression, $professionalTitle);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $professionalTitle = null)
    {
        if (empty($expression)) {
            return NullProfessionalTitle::getInstance();
        }

        if ($professionalTitle == null) {
            $professionalTitle = new ProfessionalTitle();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $professionalTitle->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['certificates'])) {
            $professionalTitle->setCertificates($attributes['certificates']);
        }
        if (isset($attributes['staff'])) {
            $professionalTitle->setStaff($attributes['staff']);
        }
        if (isset($attributes['enterpriseName'])) {
            $professionalTitle->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['rejectReason'])) {
            $professionalTitle->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $professionalTitle->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $professionalTitle->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $professionalTitle->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $professionalTitle->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['applyStatus'])) {
            $professionalTitle->setApplyStatus($attributes['applyStatus']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['parentCategory']['data'])) {
            $parentCategory = $this->changeArrayFormat($relationships['parentCategory']['data']);
            $professionalTitle->setParentCategory(
                $this->getDictionaryRestfulTranslator()->arrayToObject($parentCategory)
            );
        }
        if (isset($relationships['category']['data'])) {
            $category = $this->changeArrayFormat($relationships['category']['data']);
            $professionalTitle->setCategory(
                $this->getDictionaryRestfulTranslator()->arrayToObject($category)
            );
        }
        if (isset($relationships['type']['data'])) {
            $type = $this->changeArrayFormat($relationships['type']['data']);
            $professionalTitle->setType(
                $this->getDictionaryRestfulTranslator()->arrayToObject($type)
            );
        }
        if (isset($relationships['staff']['data'])) {
            if (isset($expression['included'])) {
                $staff = $this->changeArrayFormat($relationships['staff']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $staff = $this->changeArrayFormat($relationships['staff']['data']);
            }

            $professionalTitle->setStaff(
                $this->getStaffRestfulTranslator()->arrayToObject($staff)
            );
        }

        return $professionalTitle;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($professionalTitle, array $keys = array())
    {
        if (!$professionalTitle instanceof ProfessionalTitle) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'certificates',
                'enterpriseName',
                'rejectReason',
                'staff',
                'parentCategory',
                'category',
                'type',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'professionalTitles'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $professionalTitle->getId();
        }

        $attributes = array();

        if (in_array('certificates', $keys)) {
            $attributes['certificates'] = $professionalTitle->getCertificates();
        }
        if (in_array('enterpriseName', $keys)) {
            $attributes['enterpriseName'] = $professionalTitle->getEnterpriseName();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $professionalTitle->getRejectReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('parentCategory', $keys)) {
            $expression['data']['relationships']['parentCategory']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $professionalTitle->getParentCategory()->getId()
                )
             );
        }
        if (in_array('category', $keys)) {
            $expression['data']['relationships']['category']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $professionalTitle->getCategory()->getId()
                )
             );
        }
        if (in_array('type', $keys)) {
            $expression['data']['relationships']['type']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $professionalTitle->getType()->getId()
                )
             );
        }
        if (in_array('staff', $keys)) {
            $expression['data']['relationships']['staff']['data'] = array(
                array(
                    'type' => 'staffs',
                    'id' => $professionalTitle->getStaff()->getId()
                )
             );
        }

        return $expression;
    }
}
