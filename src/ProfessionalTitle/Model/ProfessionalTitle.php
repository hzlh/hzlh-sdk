<?php
namespace Sdk\ProfessionalTitle\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\ProfessionalTitle\Repository\ProfessionalTitleRepository;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Staff\Model\Staff;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IResubmitAble;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\ResubmitAbleTrait;
use Sdk\Common\Model\ApplyAbleTrait;

class ProfessionalTitle implements IOperatAble, IObject, IResubmitAble, IApplyAble
{
    use Object, OperatAbleTrait, ResubmitAbleTrait, ApplyAbleTrait;

    private $id;

    private $parentCategory;

    private $category;

    private $type;

    private $certificates;

    private $staff;

    private $enterpriseName;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->parentCategory = new Dictionary();
        $this->category = new Dictionary();
        $this->type = new Dictionary();
        $this->certificates = array();
        $this->staff = new Staff();
        $this->enterpriseName = '';
        $this->rejectReason = '';
        $this->applyStatus = IApplyAble::APPLY_STATUS['PENDING'];
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new ProfessionalTitleRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->parentCategory);
        unset($this->category);
        unset($this->type);
        unset($this->certificates);
        unset($this->staff);
        unset($this->enterpriseName);
        unset($this->rejectReason);
        unset($this->applyStatus);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setParentCategory(Dictionary $parentCategory)
    {
        $this->parentCategory = $parentCategory;
    }

    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    public function setCategory(Dictionary $category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setType(Dictionary $type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setCertificates(array $certificates) : void
    {
        $this->certificates = $certificates;
    }

    public function getCertificates() : array
    {
        return $this->certificates;
    }

    public function setStaff(Staff $staff)
    {
        $this->staff = $staff;
    }

    public function getStaff()
    {
        return $this->staff;
    }

    public function setEnterpriseName(string $enterpriseName)
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    protected function getRepository(): ProfessionalTitleRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIResubmitAbleAdapter(): IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter(): IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    public function batchAdd(array $professionalTitleArray) : bool
    {
        return $this->getRepository()->batchAdd($professionalTitleArray);
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}
