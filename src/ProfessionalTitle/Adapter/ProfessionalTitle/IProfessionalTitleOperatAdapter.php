<?php
namespace Sdk\ProfessionalTitle\Adapter\ProfessionalTitle;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;

interface IProfessionalTitleOperatAdapter extends IOperatAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter
{
}
