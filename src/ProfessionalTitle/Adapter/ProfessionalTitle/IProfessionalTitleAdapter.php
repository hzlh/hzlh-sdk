<?php
namespace Sdk\ProfessionalTitle\Adapter\ProfessionalTitle;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IProfessionalTitleAdapter extends IFetchAbleAdapter, IProfessionalTitleOperatAdapter, IAsyncAdapter
{
}
