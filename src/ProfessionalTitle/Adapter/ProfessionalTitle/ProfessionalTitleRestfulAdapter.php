<?php
namespace Sdk\ProfessionalTitle\Adapter\ProfessionalTitle;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\ProfessionalTitle\Model\ProfessionalTitle;
use Sdk\ProfessionalTitle\Model\NullProfessionalTitle;

use Sdk\ProfessionalTitle\Translator\ProfessionalTitleRestfulTranslator;

class ProfessionalTitleRestfulAdapter extends GuzzleAdapter implements IProfessionalTitleAdapter
{
    use ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'OA_PROFESSIONAL_TITLE_LIST' => [
            'fields' => [
                'professionalTitles'=>
                'type,staff,enterpriseName,applyStatus,updateTime,status'
            ],
            'include' => 'staff,parentCategory,category,type',
        ],
        'PORTAL_PROFESSIONAL_TITLE_LIST' => [
            'fields' => [],
            'include' => 'staff,parentCategory,category,type',
        ],
        'PROFESSIONAL_TITLE_FETCH_ONE' => [
            'fields' => [],
            'include' => 'staff,staff.enterprise,parentCategory,category,type',
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct($uri, $authKey);
        $this->translator = new ProfessionalTitleRestfulTranslator();
        $this->resource = 'professionalTitles';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullProfessionalTitle::getInstance());
    }

    protected function addAction(): bool
    {
        return false;
    }

    public function batchAdd(array $professionalTitleArray): bool
    {
        $data = array();

        foreach ($professionalTitleArray as $professionalTitle) {
            $data['data'][] = $this->getTranslator()->objectToArray(
                $professionalTitle,
                array(
                    'type',
                    'certificates',
                    'staff'
                )
            );
        }
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($professionalTitle);
            return true;
        }

        return false;
    }

    protected function editAction(ProfessionalTitle $professionalTitle) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $professionalTitle,
            array(
                'type',
                'certificates',
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$professionalTitle->getId(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($professionalTitle);
            return true;
        }

        return false;
    }

    protected function resubmitAction(ProfessionalTitle $professionalTitle) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $professionalTitle,
            array(
                'type',
                'certificates',
            )
        );

        $this->patch(
            $this->getResource().'/'.$professionalTitle->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($professionalTitle);
            return true;
        }

        return false;
    }

    public function deletes(ProfessionalTitle $professionalTitle) : bool
    {
        $this->delete(
            $this->getResource().'/'.$professionalTitle->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($professionalTitle);
            return true;
        }
        return false;
    }
}
