<?php
namespace Sdk\ProfessionalTitle\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\ProfessionalTitle\Model\ProfessionalTitle;
use Sdk\ProfessionalTitle\Adapter\ProfessionalTitle\ProfessionalTitleMockAdapter;
use Sdk\ProfessionalTitle\Adapter\ProfessionalTitle\ProfessionalTitleRestfulAdapter;
use Sdk\ProfessionalTitle\Adapter\ProfessionalTitle\IProfessionalTitleAdapter;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

class ProfessionalTitleRepository extends Repository implements IProfessionalTitleAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    const OA_LIST_MODEL_UN = 'OA_PROFESSIONAL_TITLE_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_PROFESSIONAL_TITLE_LIST';
    const FETCH_ONE_MODEL_UN = 'PROFESSIONAL_TITLE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ProfessionalTitleRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IProfessionalTitleAdapter
    {
        return new ProfessionalTitleMockAdapter();
    }

    public function getActualAdapter(): IProfessionalTitleAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function batchAdd(array $professionalTitleArray) : bool
    {
        return $this->getAdapter()->batchAdd($professionalTitleArray);
    }

    public function deletes(ProfessionalTitle $professionalTitle) : bool
    {
        return $this->getAdapter()->deletes($professionalTitle);
    }
}
