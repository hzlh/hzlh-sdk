<?php
namespace Sdk\Bank\Repository;

use Sdk\Bank\Adapter\Bank\IBankAdapter;
use Sdk\Bank\Adapter\Bank\BankMockAdapter;
use Sdk\Bank\Adapter\Bank\BankRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;

class BankRepository extends Repository implements IBankAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BANK_LIST';
    const FETCH_ONE_MODEL_UN = 'BANK_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BankRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IBankAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBankAdapter
    {
        return new BankMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
