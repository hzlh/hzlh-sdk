<?php
namespace Sdk\Bank\Adapter\Bank;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Model\NullBank;
use Sdk\Bank\Translator\BankRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class BankRestfulAdapter extends GuzzleAdapter implements IBankAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'BANK_LIST'=>[
                'fields'=>['banks'=>'name,logo,image']
            ],
            'BANK_FETCH_ONE'=>[
                'fields'=>[]
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BankRestfulTranslator();
        $this->resource = 'banks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullBank::getInstance());
    }
}
