<?php
namespace Sdk\Bank\Adapter\Bank;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Utils\MockFactory;

class BankMockAdapter implements IBankAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateBankObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $bankList = array();

        foreach ($ids as $id) {
            $bankList[] = MockFactory::generateBankObject($id);
        }

        return $bankList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateBankObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $bankList = array();

        foreach ($ids as $id) {
            $bankList[] = MockFactory::generateBankObject($id);
        }

        return $bankList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
