<?php
namespace Sdk\Bank\Adapter\Bank;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IBankAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
