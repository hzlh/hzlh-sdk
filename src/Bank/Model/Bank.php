<?php
namespace Sdk\Bank\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class Bank implements IObject
{
    use Object;

    const STATUS_NORMAL = 0; //正常

    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 银行名称]
     * @var [string]
     */
    private $name;
    /**
     * [$logo logo]
     * @var [array]
     */
    private $logo;
    /**
     * [$image 银行卡背景图]
     * @var [array]
     */
    private $image;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->logo = array();
        $this->image = array();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->logo);
        unset($this->image);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setLogo(array $logo) : void
    {
        $this->logo = $logo;
    }

    public function getLogo() : array
    {
        return $this->logo;
    }

    public function setImage(array $image) : void
    {
        $this->image = $image;
    }

    public function getImage() : array
    {
        return $this->image;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
