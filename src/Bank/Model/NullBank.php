<?php
namespace Sdk\Bank\Model;

use Marmot\Interfaces\INull;

class NullBank extends Bank implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
