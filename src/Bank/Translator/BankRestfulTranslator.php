<?php
namespace Sdk\Bank\Translator;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Model\NullBank;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Marmot\Interfaces\IRestfulTranslator;

class BankRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $bank = null)
    {
        return $this->translateToObject($expression, $bank);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $bank = null)
    {
        if (empty($expression)) {
            return NullBank::getInstance();
        }

        if ($bank == null) {
            $bank = new Bank();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $bank->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $bank->setName($attributes['name']);
        }
        if (isset($attributes['logo'])) {
            $bank->setLogo($attributes['logo']);
        }
        if (isset($attributes['image'])) {
            $bank->setImage($attributes['image']);
        }
        if (isset($attributes['createTime'])) {
            $bank->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $bank->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $bank->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $bank->setStatusTime($attributes['statusTime']);
        }

        return $bank;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($bank, array $keys = array())
    {
        $expression = array();

        if (!$bank instanceof Bank) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'logo',
                'image'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'banks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $bank->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $bank->getName();
        }
        if (in_array('logo', $keys)) {
            $attributes['logo'] = $bank->getLogo();
        }
        if (in_array('image', $keys)) {
            $attributes['image'] = $bank->getImage();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
