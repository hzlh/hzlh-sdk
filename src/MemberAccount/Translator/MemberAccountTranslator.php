<?php
namespace Sdk\MemberAccount\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\MemberAccount\Model\NullMemberAccount;

use Sdk\Member\Translator\MemberTranslator;

class MemberAccountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $memberAccount = null)
    {
        unset($expression);
        unset($memberAccount);
        return NullMemberAccount::getInstance();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($memberAccount, array $keys = array())
    {
        if (!$memberAccount instanceof MemberAccount) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'accountBalance',
                'frozenAccountBalance',
                'status',
                'createTime',
                'updateTime',
                'member'=>[]
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($memberAccount->getId());
        }
        if (in_array('accountBalance', $keys)) {
            $expression['accountBalance'] = empty($memberAccount->getAccountBalance()) ?
            '0.00' :
            number_format($memberAccount->getAccountBalance(), 2, '.', '');
        }
        if (in_array('frozenAccountBalance', $keys)) {
            $expression['frozenAccountBalance'] = empty($memberAccount->getFrozenAccountBalance()) ?
            '0.00' :
            number_format($memberAccount->getFrozenAccountBalance(), 2, '.', '');
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $memberAccount->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $memberAccount->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $memberAccount->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $expression['updateTime']);
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $memberAccount->getMember(),
                $keys['member']
            );
        }

        return $expression;
    }
}
