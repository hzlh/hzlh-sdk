<?php
namespace Sdk\TradeRecord\Translator;

use Sdk\TradeRecord\Model\TradeRecord;

use Marmot\Interfaces\IRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT'] =>
        'Sdk\Deposit\Translator\DepositRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM'] =>
        'Sdk\Deposit\Translator\DepositRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'] =>
        'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM'] =>
        'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE'] =>
        'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE'] =>
        'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'] =>
        'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL'] =>
        'Sdk\Withdrawal\Translator\WithdrawalRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM'] =>
        'Sdk\Withdrawal\Translator\WithdrawalRestfulTranslator',
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
