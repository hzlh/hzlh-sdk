<?php
namespace Sdk\TradeRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\Withdrawal\Model\NullWithdrawal;

class TradeRecord implements IObject
{
    use Object;

    const STATUS_NORMAL = 0; //正常
    
    const TRADE_RECORD_TYPES = array(
        'NULL' => 0,
        'DEPOSIT' => 1, //用户充值
        'DEPOSIT_PLATFORM' => 2, //平台转入(充值)
        'ORDER_PAY_ENTERPRISE' => 10, //支付(第三方支付）
        'ORDER_PAY_PLATFORM' => 12, //转入(第三方支付)
        'ORDER_CONFIRMATION_BUYER_ENTERPRISE' => 14, //支付(订单）
        'ORDER_CONFIRMATION_SELLER_ENTERPRISE' => 16, //收入(卖家)
        'ORDER_CONFIRMATION_PLATFORM' => 18, //支付(交易服务费）
        'WITHDRAWAL' => 20, //用户提现
        'WITHDRAWAL_PLATFORM' => 22, //平台转出(提现)
    );

    private $id;
    
    private $referenceId;

    private $reference;

    private $memberAccount;

    private $tradeTime;

    private $type;

    private $tradeMoney;

    private $debtor;

    private $creditor;

    private $balance;

    private $comment;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->referenceId = 0;
        $this->reference = new NullWithdrawal();
        $this->memberAccount = new MemberAccount();
        $this->tradeTime = 0;
        $this->type = self::TRADE_RECORD_TYPES['NULL'];
        $this->tradeMoney = 0.0;
        $this->debtor = '';
        $this->creditor = '';
        $this->balance = 0.0;
        $this->comment = '';
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->referenceId);
        unset($this->reference);
        unset($this->memberAccount);
        unset($this->tradeTime);
        unset($this->type);
        unset($this->tradeMoney);
        unset($this->debtor);
        unset($this->creditor);
        unset($this->balance);
        unset($this->comment);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setReferenceId(int $referenceId) : void
    {
        $this->referenceId = $referenceId;
    }

    public function getReferenceId() : int
    {
        return $this->referenceId;
    }

    public function setReference(ITradeAble $reference)
    {
        $this->reference = $reference;
    }

    public function getReference() : ITradeAble
    {
        return $this->reference;
    }

    public function setMemberAccount(MemberAccount $memberAccount)
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }
    
    public function setTradeTime(int $tradeTime) : void
    {
        $this->tradeTime = $tradeTime;
    }

    public function getTradeTime() : int
    {
        return $this->tradeTime;
    }
    
    public function setType(int $type) : void
    {
        $this->type = in_array($type, self::TRADE_RECORD_TYPES) ?
                    $type :
                    self::TRADE_RECORD_TYPES['NULL'];
    }

    public function getType() : int
    {
        return $this->type;
    }
    
    public function setTradeMoney(float $tradeMoney) : void
    {
        $this->tradeMoney = $tradeMoney;
    }

    public function getTradeMoney() : float
    {
        return $this->tradeMoney;
    }
    
    public function setDebtor(string $debtor) : void
    {
        $this->debtor = $debtor;
    }

    public function getDebtor() : string
    {
        return $this->debtor;
    }

    public function setCreditor(string $creditor) : void
    {
        $this->creditor = $creditor;
    }

    public function getCreditor() : string
    {
        return $this->creditor;
    }
    
    public function setBalance(float $balance) : void
    {
        $this->balance = $balance;
    }

    public function getBalance() : float
    {
        return $this->balance;
    }
    
    public function setComment(string $comment) : void
    {
        $this->comment = $comment;
    }

    public function getComment() : string
    {
        return $this->comment;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
