<?php
namespace Sdk\TradeRecord\Adapter\TradeRecord;

use Sdk\TradeRecord\Model\TradeRecord;
use Sdk\TradeRecord\Utils\MockFactory;

class TradeRecordMockAdapter implements ITradeRecordAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateTradeRecordObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $tradeRecordList = array();

        foreach ($ids as $id) {
            $tradeRecordList[] = MockFactory::generateTradeRecordObject($id);
        }

        return $tradeRecordList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateTradeRecordObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $tradeRecordList = array();

        foreach ($ids as $id) {
            $tradeRecordList[] = MockFactory::generateTradeRecordObject($id);
        }

        return $tradeRecordList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
