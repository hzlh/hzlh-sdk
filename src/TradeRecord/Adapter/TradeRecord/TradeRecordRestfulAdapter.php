<?php
namespace Sdk\TradeRecord\Adapter\TradeRecord;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\TradeRecord\Model\TradeRecord;
use Sdk\TradeRecord\Model\NullTradeRecord;
use Sdk\TradeRecord\Translator\TradeRecordRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class TradeRecordRestfulAdapter extends GuzzleAdapter implements ITradeRecordAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'OA_TRADE_RECORD_LIST'=>[
            'fields'=>[
                'tradeRecords'=>
                'tradeTime,tradeType,tradeMoney,balance,reference,debtor,creditor,comment,status,memberAccount',
                'deposits'=>'paymentType,number,transactionNumber',
                'serviceOrders'=>'orderno,paymentType,transactionNumber',
                'withdrawals'=>'amount,number,transactionNumber'
            ],
            'include'=> 'memberAccount,reference,reference.bankCard,reference.bankCard.bank'
        ],
        'PORTAL_TRADE_RECORD_LIST'=>[
            'fields'=>[],
            'include'=> 'memberAccount,reference,reference.bankCard,reference.bankCard.bank,memberAccount.member,reference.sellerEnterprise,reference.orderCommodities.commodity'//phpcs:ignore
        ],
        'TRADE_RECORD_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'memberAccount,reference,reference.bankCard,reference.bankCard.bank,memberAccount.member,reference.sellerEnterprise,reference.orderCommodities.commodity,reference.memberAccount,reference.memberAccount.member'//phpcs:ignore
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TradeRecordRestfulTranslator();
        $this->resource = 'tradeRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullTradeRecord::getInstance());
    }
}
