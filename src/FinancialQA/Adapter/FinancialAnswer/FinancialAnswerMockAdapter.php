<?php
namespace Sdk\FinancialQA\Adapter\FinancialAnswer;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

use Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory;

class FinancialAnswerMockAdapter implements IFinancialAnswerAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateAnswerObject($id);
    }

    public function fetchList(array $ids): array
    {
        $financialAnswerList = array();

        foreach ($ids as $id) {
            $financialAnswerList[] = MockFactory::generateAnswerObject($id);
        }

        return $financialAnswerList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateAnswerObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $financialAnswerList = array();

        foreach ($ids as $id) {
            $financialAnswerList[] = MockFactory::generateAnswerObject($id);
        }

        return $financialAnswerList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
