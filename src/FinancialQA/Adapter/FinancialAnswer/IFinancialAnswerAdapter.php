<?php
namespace Sdk\FinancialQA\Adapter\FinancialAnswer;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IFinancialAnswerAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
