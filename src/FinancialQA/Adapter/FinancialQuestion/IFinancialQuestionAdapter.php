<?php
namespace Sdk\FinancialQA\Adapter\FinancialQuestion;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IFinancialQuestionAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
