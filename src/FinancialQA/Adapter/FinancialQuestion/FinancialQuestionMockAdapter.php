<?php
namespace Sdk\FinancialQA\Adapter\FinancialQuestion;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

use Sdk\FinancialQA\Utils\FinancialQuestion\MockFactory;

class FinancialQuestionMockAdapter implements IFinancialQuestionAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateQuestionObject($id);
    }

    public function fetchList(array $ids): array
    {
        $financialQuestionList = array();

        foreach ($ids as $id) {
            $financialQuestionList[] = MockFactory::generateQuestionObject($id);
        }

        return $financialQuestionList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateQuestionObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $financialQuestionList = array();

        foreach ($ids as $id) {
            $financialQuestionList[] = MockFactory::generateQuestionObject($id);
        }

        return $financialQuestionList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
