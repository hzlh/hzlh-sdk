<?php
namespace Sdk\FinancialQA\Model;

use Marmot\Core;

use Sdk\Member\Model\Member;

class Answer
{
    private $id;

    private $member;

    private $content;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->content = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->content);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }
}
