<?php
namespace Sdk\FinancialQA\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Member\Model\Member;
use Sdk\Dictionary\Model\Dictionary;
use Sdk\FinancialQA\Model\Answer;

use Sdk\FinancialQA\Repository\FinancialQuestionRepository;

class FinancialQuestion implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    private $id;

    private $number;

    private $category;

    private $content;

    private $pageViews;

    private $commentCount;

    private $attentionDegree;

    private $member;

    private $answer;

    private $deleteInfo;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->number = '';
        $this->category = new Dictionary();
        $this->content = '';
        $this->pageViews = 0;
        $this->commentCount = 0;
        $this->attentionDegree = 0;
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->answer = new Answer();
        $this->deleteInfo = new DeleteInfo();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->repository = new FinancialQuestionRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->category);
        unset($this->content);
        unset($this->pageViews);
        unset($this->commentCount);
        unset($this->attentionDegree);
        unset($this->member);
        unset($this->answer);
        unset($this->deleteInfo);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setCategory(Dictionary $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : Dictionary
    {
        return $this->category;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setPageViews(int $pageViews) : void
    {
        $this->pageViews = $pageViews;
    }

    public function getPageViews() : int
    {
        return $this->pageViews;
    }

    public function setCommentCount(int $commentCount) : void
    {
        $this->commentCount = $commentCount;
    }

    public function getCommentCount() : int
    {
        return $this->commentCount;
    }

    public function setAttentionDegree(int $attentionDegree) : void
    {
        $this->attentionDegree = $attentionDegree;
    }

    public function getAttentionDegree() : int
    {
        return $this->attentionDegree;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setAnswer(Answer $answer) : void
    {
        $this->answer = $answer;
    }

    public function getAnswer() : Answer
    {
        return $this->answer;
    }

    public function setDeleteInfo(DeleteInfo $deleteInfo) : void
    {
        $this->deleteInfo = $deleteInfo;
    }

    public function getDeleteInfo() : DeleteInfo
    {
        return $this->deleteInfo;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, array_values(self::STATUS)) ?
            $status : self::STATUS['DELETED'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    protected function getRepository() : FinancialQuestionRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * [platformDelete 平台删除]
     * @return [type] [bool]
     */
    public function platformDelete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getRepository()->platformDelete($this);
    }
    /**
     * [deletes 本人删除]
     * @return [type] [bool]
     */
    public function deletes() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getRepository()->deletes($this);
    }
}
