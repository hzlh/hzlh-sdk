<?php
namespace Sdk\FinancialQA\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullAnswer extends Answer implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
