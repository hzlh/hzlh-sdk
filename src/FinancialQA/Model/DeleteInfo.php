<?php
namespace Sdk\FinancialQA\Model;

use Sdk\Common\Model\IDeleteAble;

/**
 * 身份信息,包含:
 * 删除类型 $type
 * 删除原因 $reason
 * 删除该信息的人 $deletedBy
 */
class DeleteInfo
{
    const TYPE = array(
        'NULL' => 0, //正常
        'DELETED' => 1, //本人删除
        'PLATFORM_DELETED' => 2, //平台删除
        'QUESTIONER_DELETED' => 3 //提问者删除
    );

    private $type;

    private $reason;

    private $deletedBy;

    public function __construct(
        int $type = 0,
        string $reason = '',
        IDeleteAble $deletedBy = null
    ) {
        $this->type = $type;
        $this->reason = $reason;
        $this->deletedBy = $deletedBy;
    }

    public function __destruct()
    {
        unset($this->type);
        unset($this->reason);
        unset($this->deletedBy);
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function getDeleteBy() : IDeleteAble
    {
        return $this->deletedBy;
    }
}
