<?php
namespace Sdk\FinancialQA\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Member\Model\Member;
use Sdk\FinancialQA\Model\FinancialQuestion;

use Sdk\FinancialQA\Repository\FinancialAnswerRepository;

class FinancialAnswer implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2 //删除
    );

    private $id;
    
    private $question;

    private $member;

    private $content;

    private $replyCount;

    private $likesCount;

    private $deleteInfo;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->question = new NullFinancialQuestion();
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->content = '';
        $this->replyCount = 0;
        $this->likesCount = 0;
        $this->deleteInfo = new DeleteInfo();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->repository = new FinancialAnswerRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->question);
        unset($this->member);
        unset($this->content);
        unset($this->replyCount);
        unset($this->likesCount);
        unset($this->deleteInfo);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setQuestion(FinancialQuestion $question) : void
    {
        $this->question = $question;
    }

    public function getQuestion() : FinancialQuestion
    {
        return $this->question;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setReplyCount(int $replyCount) : void
    {
        $this->replyCount = $replyCount;
    }

    public function getReplyCount() : int
    {
        return $this->replyCount;
    }

    public function setLikesCount(int $likesCount) : void
    {
        $this->likesCount = $likesCount;
    }

    public function getLikesCount() : int
    {
        return $this->likesCount;
    }

    public function setDeleteInfo(DeleteInfo $deleteInfo) : void
    {
        $this->deleteInfo = $deleteInfo;
    }

    public function getDeleteInfo() : DeleteInfo
    {
        return $this->deleteInfo;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, array_values(self::STATUS)) ?
            $status : self::STATUS['DELETED'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    protected function getRepository() : FinancialAnswerRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * [platformDelete 平台删除]
     * @return [type] [bool]
     */
    public function platformDelete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getRepository()->platformDelete($this);
    }
    /**
     * [questionerDelete 提问者删除]
     * @return [type] [bool]
     */
    public function questionerDelete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }
        
        return $this->getRepository()->questionerDelete($this);
    }
    /**
     * [deletes 本人删除]
     * @return [type] [bool]
     */
    public function deletes() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }
        
        return $this->getRepository()->deletes($this);
    }
}
