<?php
namespace Sdk\FinancialQA\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\FinancialQA\Model\Answer;
use Sdk\FinancialQA\Model\NullAnswer;

use Sdk\Member\Translator\MemberRestfulTranslator;

class AnswerRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $answer = null)
    {
        return $this->translateToObject($expression, $answer);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $answer = null)
    {
        if (empty($expression)) {
            return NullAnswer::getInstance();
        }

        if ($answer == null) {
            $answer = new Answer();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $answer->setId($data['id']);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['content'])) {
            $answer->setContent($attributes['content']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $answer->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        return $answer;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($answer, array $keys = array())
    {
        $expression = array();

        if (!$answer instanceof Answer) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'member',
                'content'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'answers'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $answer->getId();
        }

        $attributes = array();

        if (in_array('content', $keys)) {
            $attributes['content'] = $answer->getContent();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $answer->getMember()->getId()
                )
            );
        }

        return $expression;
    }
}
