<?php
namespace Sdk\FinancialQA\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\FinancialQA\Model\DeleteInfo;
use Sdk\FinancialQA\Model\FinancialAnswer;
use Sdk\FinancialQA\Model\NullFinancialAnswer;

use Sdk\Member\Translator\MemberRestfulTranslator;

class FinancialAnswerRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getFinancialQuestionRestfulTranslator()
    {
        return new FinancialQuestionRestfulTranslator();
    }

    public function arrayToObject(array $expression, $financialAnswer = null)
    {
        return $this->translateToObject($expression, $financialAnswer);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $financialAnswer = null)
    {
        if (empty($expression)) {
            return NullFinancialAnswer::getInstance();
        }

        if ($financialAnswer == null) {
            $financialAnswer = new FinancialAnswer();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $financialAnswer->setId($data['id']);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['content'])) {
            $financialAnswer->setContent($attributes['content']);
        }
        if (isset($attributes['replyCount'])) {
            $financialAnswer->setReplyCount($attributes['replyCount']);
        }
        if (isset($attributes['likesCount'])) {
            $financialAnswer->setLikesCount($attributes['likesCount']);
        }
        if (isset($attributes['createTime'])) {
            $financialAnswer->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $financialAnswer->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $financialAnswer->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $financialAnswer->setStatusTime($attributes['statusTime']);
        }
        $deleteType = isset($attributes['deleteType'])
        ? $attributes['deleteType']
        : 0;
        $deleteReason = isset($attributes['deleteReason'])
        ? $attributes['deleteReason']
        : '';
        $deleteBy = isset($attributes['deleteBy'])
        ? $attributes['deleteBy']
        : null;

        $financialAnswer->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['financialQuestion']['data'])) {
            $financialQuestion = $this->changeArrayFormat($relationships['financialQuestion']['data']);
            $financialAnswer->setQuestion(
                $this->getFinancialQuestionRestfulTranslator()->arrayToObject($financialQuestion)
            );
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $financialAnswer->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        
        return $financialAnswer;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($financialAnswer, array $keys = array())
    {
        $expression = array();

        if (!$financialAnswer instanceof FinancialAnswer) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'question',
                'member',
                'content',
                'deleteType',
                'deleteReason',
                'deleteBy'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'financialAnswers'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $financialAnswer->getId();
        }

        $attributes = array();

        if (in_array('content', $keys)) {
            $attributes['content'] = $financialAnswer->getContent();
        }
        if (in_array('deleteType', $keys)) {
            $attributes['deleteType'] = $financialAnswer->getDeleteInfo()->getType();
        }
        if (in_array('deleteReason', $keys)) {
            $attributes['deleteReason'] = $financialAnswer->getDeleteInfo()->getReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('question', $keys)) {
            $expression['data']['relationships']['financialQuestion']['data'] = array(
                array(
                    'type' => 'financialQuestions',
                    'id' => $financialAnswer->getQuestion()->getId()
                )
            );
        }
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $financialAnswer->getMember()->getId()
                )
            );
        }
        if (in_array('deleteBy', $keys)) {
            $expression['data']['relationships']['deleteBy']['data'] = array(
                array(
                    'type' => 'deleteBys',
                    'id' => $financialAnswer->getDeleteInfo()->getDeleteBy()->getId()
                )
            );
        }

        return $expression;
    }
}
