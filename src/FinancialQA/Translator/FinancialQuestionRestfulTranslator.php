<?php
namespace Sdk\FinancialQA\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\FinancialQA\Model\DeleteInfo;
use Sdk\FinancialQA\Model\FinancialQuestion;
use Sdk\FinancialQA\Model\NullFinancialQuestion;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

class FinancialQuestionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    public function getAnswerRestfulTranslator()
    {
        return new AnswerRestfulTranslator();
    }

    public function arrayToObject(array $expression, $financialQuestion = null)
    {
        return $this->translateToObject($expression, $financialQuestion);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $financialQuestion = null)
    {
        if (empty($expression)) {
            return NullFinancialQuestion::getInstance();
        }

        if ($financialQuestion == null) {
            $financialQuestion = new FinancialQuestion();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $financialQuestion->setId($data['id']);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $financialQuestion->setNumber($attributes['number']);
        }
        if (isset($attributes['content'])) {
            $financialQuestion->setContent($attributes['content']);
        }
        if (isset($attributes['pageViews'])) {
            $financialQuestion->setPageViews($attributes['pageViews']);
        }
        if (isset($attributes['commentCount'])) {
            $financialQuestion->setCommentCount($attributes['commentCount']);
        }
        if (isset($attributes['attentionDegree'])) {
            $financialQuestion->setAttentionDegree($attributes['attentionDegree']);
        }
        if (isset($attributes['createTime'])) {
            $financialQuestion->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $financialQuestion->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $financialQuestion->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $financialQuestion->setStatusTime($attributes['statusTime']);
        }
        $deleteType = isset($attributes['deleteType'])
        ? $attributes['deleteType']
        : 0;
        $deleteReason = isset($attributes['deleteReason'])
        ? $attributes['deleteReason']
        : '';
        $deleteBy = isset($attributes['deleteBy'])
        ? $attributes['deleteBy']
        : null;

        $financialQuestion->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['category']['data'])) {
            $category = $this->changeArrayFormat($relationships['category']['data']);
            $financialQuestion->setCategory($this->getDictionaryRestfulTranslator()->arrayToObject($category));
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $financialQuestion->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        if (isset($relationships['answer']['data'])) {
            $answer = $this->changeArrayFormat($relationships['answer']['data']);
            $financialQuestion->setAnswer(
                $this->getAnswerRestfulTranslator()->arrayToObject($answer)
            );
        }

        return $financialQuestion;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($financialQuestion, array $keys = array())
    {
        $expression = array();

        if (!$financialQuestion instanceof FinancialQuestion) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'category',
                'content',
                'member',
                'deleteType',
                'deleteReason',
                'deleteBy'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'financialQuestions'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $financialQuestion->getId();
        }

        $attributes = array();

        if (in_array('content', $keys)) {
            $attributes['content'] = $financialQuestion->getContent();
        }
        if (in_array('deleteType', $keys)) {
            $attributes['deleteType'] = $financialQuestion->getDeleteInfo()->getType();
        }
        if (in_array('deleteReason', $keys)) {
            $attributes['deleteReason'] = $financialQuestion->getDeleteInfo()->getReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('category', $keys)) {
            $expression['data']['relationships']['category']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $financialQuestion->getCategory()->getId()
                )
            );
        }
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $financialQuestion->getMember()->getId()
                )
            );
        }
        if (in_array('deleteBy', $keys)) {
            $expression['data']['relationships']['deleteBy']['data'] = array(
                array(
                    'type' => 'deleteBys',
                    'id' => $financialQuestion->getDeleteInfo()->getDeleteBy()->getId()
                )
            );
        }

        return $expression;
    }
}
