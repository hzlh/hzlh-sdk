<?php
namespace Sdk\Appointment\Adapter\Appointment;

use Sdk\Common\Adapter\ApplyAbleMockAdapterTrait;
use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\ModifyStatusAbleMockAdapterTrait;
use Sdk\Common\Adapter\ResubmitAbleMockAdapterTrait;

use Sdk\Appointment\Utils\MockFactory;

class AppointmentMockAdapter implements IAppointmentAdapter
{
    use OperatAbleMockAdapterTrait,
        ApplyAbleMockAdapterTrait,
        ModifyStatusAbleMockAdapterTrait,
        ResubmitAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateAppointmentObject($id);
    }

    public function fetchList(array $ids): array
    {
        $appointmentList = array();

        foreach ($ids as $id) {
            $appointmentList[] = MockFactory::generateAppointmentObject($id);
        }

        return $appointmentList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateAppointmentObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $appointmentList = array();

        foreach ($ids as $id) {
            $appointmentList[] = MockFactory::generateAppointmentObject($id);
        }

        return $appointmentList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
