<?php
namespace Sdk\Appointment\Adapter\Appointment;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IAppointmentAdapter extends IAsyncAdapter, IFetchAbleAdapter, IAppointmentOperatAdapter, IModifyStatusAbleAdapter//phpcs:ignore
{
}
