<?php
namespace Sdk\Appointment\Adapter\Appointment;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Sdk\Appointment\Model\Appointment;
use Sdk\Appointment\Model\NullAppointment;

use Sdk\Appointment\Translator\AppointmentRestfulTranslator;

class AppointmentRestfulAdapter extends GuzzleAdapter implements IAppointmentAdapter
{
    use CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'APPOINTMENT_LIST' => [
            'fields' => [
                'appointments'=> 'number,loanObject,loanAmount,loanProductTitle,enterpriseName,contactsName,contactsCellphone,status,loanStatus,createTime,updateTime'//phpcs:ignore
            ],
            'include' => 'loanProduct.labels,loanProductSnapshot,applicant,sellerEnterprise'
        ],
        'APPOINTMENT_FETCH_ONE' => [
            'fields' => [],
            'include' => 'loanProduct.labels,loanProductSnapshot,member,applicant,sellerEnterprise'
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AppointmentRestfulTranslator();
        $this->resource = 'appointments';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            111 => APPOINTMENT_NEED_NATURAL_PERSON_AUTHENTICATION,
            112 => APPOINTMENT_NEED_ENTERPRISE_AUTHENTICATION,
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAppointment::getInstance());
    }

    /**
     * [addAction 在线申请]
     * @param Appointment $appointment [object]
     * @return [type][bool]
     */
    protected function addAction(Appointment $appointment): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'attachments',
                'loanProductSnapshot',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
    * [resubmitAction 重新提交申请]
    * @param  Appointment $appointment [object]
    * @return [type][bool]
    */
    protected function resubmitAction(Appointment $appointment): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'attachments'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
     * [approve 审核通过]
     * @param  Appointment $appointment [object]
     * @return [type][bool]
     */
    protected function approveAction(Appointment $appointment) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array('remark')
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/approve',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
     * [completed 完成]
     * @param  Appointment $appointment [object]
     * @return [type][bool]
     */
    public function completed(Appointment $appointment) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'loanFailReason',
                'loanContractNumber',
                'loanStatus'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/completed',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    protected function editAction(Appointment $appointment): bool
    {
        unset($appointment);
        return false;
    }
}
