<?php
namespace Sdk\Appointment\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Appointment\Model\Appointment;
use Sdk\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        Appointment::LOAN_OBJECT['ENTERPRISE'] =>
        'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
        Appointment::LOAN_OBJECT['NATURAL_PERSON'] =>
        'Sdk\NaturalPerson\Translator\NaturalPersonRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
