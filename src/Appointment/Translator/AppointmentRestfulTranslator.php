<?php
namespace Sdk\Appointment\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\LoanProduct\Translator\LoanProductRestfulTranslator;
use Sdk\Appointment\Model\ContactsInfo;
use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;

use Sdk\Appointment\Model\Appointment;
use Sdk\Appointment\Model\NullAppointment;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class AppointmentRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getLoanProductRestfulTranslator()
    {
        return new LoanProductRestfulTranslator();
    }

    public function getSnapshotRestfulTranslator()
    {
        return new SnapshotRestfulTranslator();
    }

    public function arrayToObject(array $expression, $appointment = null)
    {
        return $this->translateToObject($expression, $appointment);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $appointment = null)
    {
        if (empty($expression)) {
            return NullAppointment::getInstance();
        }

        if ($appointment == null) {
            $appointment = new Appointment();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $appointment->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $appointment->setNumber($attributes['number']);
        }
        if (isset($attributes['loanObject'])) {
            $appointment->setLoanObject($attributes['loanObject']);
        }
        if (isset($attributes['loanProductTitle'])) {
            $appointment->setLoanProductTitle($attributes['loanProductTitle']);
        }
        if (isset($attributes['enterpriseName'])) {
            $appointment->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['loanAmount'])) {
            $appointment->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $appointment->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['loanTermUnit'])) {
            $appointment->setLoanTermUnit($attributes['loanTermUnit']);
        }
        if (isset($attributes['attachments'])) {
            $appointment->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['loanStatus'])) {
            $appointment->setLoanStatus($attributes['loanStatus']);
        }
        if (isset($attributes['loanContractNumber'])) {
            $appointment->setLoanContractNumber($attributes['loanContractNumber']);
        }
        if (isset($attributes['loanFailReason'])) {
            $appointment->setLoanFailReason($attributes['loanFailReason']);
        }
        if (isset($attributes['status'])) {
            $appointment->setStatus($attributes['status']);
        }
        if (isset($attributes['remark'])) {
            $appointment->setRemark($attributes['remark']);
        }
        if (isset($attributes['rejectReason'])) {
            $appointment->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $appointment->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $appointment->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $appointment->setStatusTime($attributes['statusTime']);
        }
        //联系人信息
        $contactsName = isset($attributes['contactsName']) ? $attributes['contactsName'] : '';
        $contactsCellphone = isset($attributes['contactsCellphone']) ? $attributes['contactsCellphone'] : '';
        $contactsArea = isset($attributes['contactsArea']) ? $attributes['contactsArea'] : '';
        $contactsAddress = isset($attributes['contactsAddress']) ? $attributes['contactsAddress'] : '';
        $appointment->setContactsInfo(
            new ContactsInfo(
                $contactsName,
                $contactsCellphone,
                $contactsArea,
                $contactsAddress
            )
        );

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['loanProductSnapshot']['data'])) {
            $loanProductSnapshot = $this->changeArrayFormat($relationships['loanProductSnapshot']['data']);
            $appointment->setLoanProductSnapshot(
                $this->getSnapshotRestfulTranslator()->arrayToObject($loanProductSnapshot)
            );
        }
        if (isset($relationships['loanProduct']['data'])) {
            $loanProduct = $this->changeArrayFormat($relationships['loanProduct']['data']);
            $appointment->setLoanProduct(
                $this->getLoanProductRestfulTranslator()->arrayToObject($loanProduct)
            );
        }
        if (isset($relationships['sellerEnterprise']['data'])) {
            $sellerEnterprise = $this->changeArrayFormat($relationships['sellerEnterprise']['data']);
            $appointment->setSellerEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($sellerEnterprise)
            );
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $appointment->setMember(
                $this->getMemberRestfulTranslator()->arrayToObject($member)
            );
        }
        if (isset($relationships['applicant']['data'])) {
            if (isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data']);
            }

            $applicantRestfulTranslator = $this->getTranslatorFactory()->getTranslator($attributes['loanObject']);
            $appointment->setApplicant($applicantRestfulTranslator->arrayToObject($applicant));
        }

        return $appointment;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($appointment, array $keys = array())
    {
        if (!$appointment instanceof Appointment) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'loanProductSnapshot',
                'loanProduct',
                'sellerEnterprise',
                'applicant',
                'member',
                'loanObject',
                'loanAmount',
                'loanTerm',
                'loanTermUnit',
                'attachments',
                'remark',
                'loanStatus',
                'loanFailReason',
                'loanContractNumber',
                'rejectReason',
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'appointments'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $appointment->getId();
        }

        $attributes = array();

        if (in_array('contactsName', $keys)) {
            $attributes['contactsName'] = $appointment->getContactsInfo()->getName();
        }
        if (in_array('contactsCellphone', $keys)) {
            $attributes['contactsCellphone'] = $appointment->getContactsInfo()->getCellphone();
        }
        if (in_array('contactsArea', $keys)) {
            $attributes['contactsArea'] = $appointment->getContactsInfo()->getArea();
        }
        if (in_array('contactsAddress', $keys)) {
            $attributes['contactsAddress'] = $appointment->getContactsInfo()->getAddress();
        }
        if (in_array('loanAmount', $keys)) {
            $attributes['loanAmount'] = $appointment->getLoanAmount();
        }
        if (in_array('loanTerm', $keys)) {
            $attributes['loanTerm'] = $appointment->getLoanTerm();
        }
        if (in_array('loanObject', $keys)) {
            $attributes['loanObject'] = $appointment->getLoanObject();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $appointment->getAttachments();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $appointment->getRemark();
        }
        if (in_array('loanStatus', $keys)) {
            $attributes['loanStatus'] = $appointment->getLoanStatus();
        }
        if (in_array('loanFailReason', $keys)) {
            $attributes['loanFailReason'] = $appointment->getLoanFailReason();
        }
        if (in_array('loanContractNumber', $keys)) {
            $attributes['loanContractNumber'] = $appointment->getloanContractNumber();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $appointment->getRejectReason();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('loanProductSnapshot', $keys)) {
            $expression['data']['relationships']['snapshot']['data'] = array(
                array(
                    'type'=>'snapshots',
                    'id'=>$appointment->getLoanProductSnapshot()->getId()
                )
            );
        }

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type'=>'members',
                    'id'=>$appointment->getMember()->getId()
                )
            );
        }

        return $expression;
    }
}
