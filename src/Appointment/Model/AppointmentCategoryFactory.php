<?php
namespace Sdk\Appointment\Model;

class AppointmentCategoryFactory
{
    const APPOINTMENT_STATUS_CN = array(
        Appointment::APPOINTMENT_STATUS['NORMAL'] =>'待审核',
        Appointment::APPOINTMENT_STATUS['REJECT'] =>'已驳回',
        Appointment::APPOINTMENT_STATUS['APPROVE'] =>'已通过',
        Appointment::APPOINTMENT_STATUS['REVOKED'] =>'已撤销',
        Appointment::APPOINTMENT_STATUS['COMPLETED'] =>'已完成',
        Appointment::APPOINTMENT_STATUS['DELETED'] =>'已删除',
        Appointment::APPOINTMENT_STATUS['REVOKED_DELETED'] =>'已删除'
    );

    const LOAN_STATUS_CN = array(
        Appointment::LOAN_STATUS['PENDING'] => '待处理',
        Appointment::LOAN_STATUS['SUCCESS'] => '成功',
        Appointment::LOAN_STATUS['FAIL'] => '失败'
    );

    const LOAN_TERM_UNIT_CN = array(
        Appointment::LOAN_TERM_UNIT['MONTH'] =>'月',
        Appointment::LOAN_TERM_UNIT['DAY'] =>'天'
    );

    const LOAN_OBJECT_CN = array(
        Appointment::LOAN_OBJECT['ENTERPRISE'] => '企业',
        Appointment::LOAN_OBJECT['NATURAL_PERSON'] => '个人'
    );
}
