<?php
namespace Sdk\Appointment\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IResubmitAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\ResubmitAbleTrait;
use Sdk\Common\Model\ModifyStatusAbleTrait;

use Sdk\Snapshot\Model\Snapshot;
use Sdk\Snapshot\Model\ISnapshotAble;

use Sdk\Member\Model\Member;
use Sdk\Enterprise\Model\NullEnterprise;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\Appointment\Repository\AppointmentRepository;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class Appointment implements IObject, IApplyAble, IOperatAble, IResubmitAble, IModifyStatusAble, ISnapshotAble
{
    use Object, ValidateTrait, ApplyAbleTrait, OperatAbleTrait, ResubmitAbleTrait, ModifyStatusAbleTrait;

    //状态
    const APPOINTMENT_STATUS = array(
        'NORMAL' => 0,     // 正常
        'REJECT' => -2,    // 已驳回
        'APPROVE' => 2,    // 已通过
        'REVOKED' => -4,   // 撤销
        'COMPLETED' => 4,  // 完成
        'DELETED' => -6,    // 驳回/完成删除
        'REVOKED_DELETED' => -8  // 撤销删除
    );

    //贷款状态
    const LOAN_STATUS = array(
        'PENDING' => 0,  // 待处理
        'SUCCESS' => 2,  // 成功
        'FAIL' => -2     // 失败
    );

    //贷款期限单位
    const LOAN_TERM_UNIT = array(
        'MONTH' => 1,  // 月
        'DAY' => 2     // 天
    );

    //贷款对象
    const LOAN_OBJECT = array(
        'ENTERPRISE' => 1,     // 企业
        'NATURAL_PERSON' => 2  // 个人
    );

    private $id;

    private $number;

    private $loanProductSnapshot;

    private $loanProduct;

    private $loanProductTitle;

    private $contactsInfo;

    private $enterpriseName;

    private $sellerEnterprise;

    private $applicant;

    private $loanObject;

    private $loanAmount;

    private $loanTerm;

    private $loanTermUnit;

    private $attachments;

    private $remark;

    private $loanStatus;

    private $rejectReason;

    private $loanFailReason;

    private $loanContractNumber;

    private $member;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->loanProductSnapshot = new Snapshot();
        $this->loanProduct = new LoanProduct();
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->sellerEnterprise = new Enterprise();
        $this->applicant = new NullEnterprise();
        $this->loanProductTitle = '';
        $this->enterpriseName = '';
        $this->loanObject = self::LOAN_OBJECT['ENTERPRISE'];
        $this->loanAmount = 0.00;
        $this->loanTerm = 0;
        $this->loanTermUnit = self::LOAN_TERM_UNIT['MONTH'];
        $this->attachments = array();
        $this->contactsInfo = new ContactsInfo();
        $this->loanStatus = self::LOAN_STATUS['PENDING'];
        $this->loanContractNumber = '';
        $this->loanFailReason = '';
        $this->rejectReason = '';
        $this->remark = '';
        $this->status = self::APPOINTMENT_STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new AppointmentRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->loanProductSnapshot);
        unset($this->loanProduct);
        unset($this->member);
        unset($this->sellerEnterprise);
        unset($this->applicant);
        unset($this->loanProductTitle);
        unset($this->enterpriseName);
        unset($this->loanObject);
        unset($this->loanAmount);
        unset($this->loanTerm);
        unset($this->loanTermUnit);
        unset($this->attachments);
        unset($this->contactsInfo);
        unset($this->loanStatus);
        unset($this->loanContractNumber);
        unset($this->loanFailReason);
        unset($this->rejectReason);
        unset($this->remark);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setLoanProductSnapshot(Snapshot $loanProductSnapshot) : void
    {
        $this->loanProductSnapshot = $loanProductSnapshot;
    }

    public function getLoanProductSnapshot() : Snapshot
    {
        return $this->loanProductSnapshot;
    }

    public function setContactsInfo(ContactsInfo $contactsInfo) : void
    {
        $this->contactsInfo = $contactsInfo;
    }

    public function getContactsInfo() : ContactsInfo
    {
        return $this->contactsInfo;
    }

    public function setLoanProduct(LoanProduct $loanProduct) : void
    {
        $this->loanProduct = $loanProduct;
    }

    public function getLoanProduct() : LoanProduct
    {
        return $this->loanProduct;
    }

    public function setLoanProductTitle(string $loanProductTitle) : void
    {
        $this->loanProductTitle = $loanProductTitle;
    }
    
    public function getLoanProductTitle() : string
    {
        return $this->loanProductTitle;
    }

    public function setLoanContractNumber(string $loanContractNumber) : void
    {
        $this->loanContractNumber = $loanContractNumber;
    }
    
    public function getLoanContractNumber() : string
    {
        return $this->loanContractNumber;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setSellerEnterprise(Enterprise $sellerEnterprise) : void
    {
        $this->sellerEnterprise = $sellerEnterprise;
    }

    public function getSellerEnterprise() : Enterprise
    {
        return $this->sellerEnterprise;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setApplicant(IAppointmentAble $applicant)
    {
        $this->applicant = $applicant;
    }

    public function getApplicant() : IAppointmentAble
    {
        return $this->applicant;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setLoanObject(int $loanObject) : void
    {
        $this->loanObject = $loanObject;
    }

    public function getLoanObject() : int
    {
        return $this->loanObject;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setLoanAmount(float $loanAmount) : void
    {
        $this->loanAmount = $loanAmount;
    }

    public function getLoanAmount() : float
    {
        return $this->loanAmount;
    }

    public function setLoanTerm(int $loanTerm) : void
    {
        $this->loanTerm = $loanTerm;
    }

    public function getLoanTerm() : int
    {
        return $this->loanTerm;
    }

    public function setLoanTermUnit(int $loanTermUnit) : void
    {
        $this->loanTermUnit = $loanTermUnit;
    }

    public function getLoanTermUnit() : int
    {
        return $this->loanTermUnit;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setLoanStatus(int $loanStatus) : void
    {
        $this->loanStatus = $loanStatus;
    }

    public function getLoanStatus() : int
    {
        return $this->loanStatus;
    }

    public function setLoanFailReason(string $loanFailReason) : void
    {
        $this->loanFailReason = $loanFailReason;
    }

    public function getLoanFailReason() : string
    {
        return $this->loanFailReason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 用户提交申请
     * @return bool
     */
    public function add() : bool
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->getIOperatAbleAdapter()->add($this);
    }

    /**
     * 用户撤销申请
     * @return bool
     */
    public function revoke() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getIModifyStatusAbleAdapter()->revoke($this);
    }

    /**
     * 用户删除申请
     * @return bool
     */
    public function deletes() : bool
    {
        if ($this->isReject() || $this->isRevoked()) {
            return $this->getIModifyStatusAbleAdapter()->deletes($this);
        }

        Core::setLastError(RESOURCE_STATUS_NOT_REJECT_OR_REVOKED);
        return false;
    }

    /**
     * 商家完成申请
     * @return bool
     */
    public function completed() : bool
    {
        if (!$this->isApprove()) {
            Core::setLastError(RESOURCE_STATUS_NOT_APPROVE);
            return false;
        }

        return $this->getRepository()->completed($this);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['NORMAL'];
    }

    protected function isReject() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['REJECT'];
    }

    protected function isRevoked() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['REVOKED'];
    }

    protected function isApprove() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['APPROVE'];
    }
}
