<?php
namespace Sdk\Appointment\Model;

class ContactsInfo
{
    private $name;

    private $cellphone;

    private $area;

    private $address;

    public function __construct(
        string $name = '',
        string $cellphone = '',
        string $area = '',
        string $address = ''
    ) {
        $this->name = $name;
        $this->area = $area;
        $this->cellphone = $cellphone;
        $this->address = $address;
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->cellphone);
        unset($this->area);
        unset($this->address);
    }

    public function getCellphone(): string
    {
        return $this->cellphone;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getArea() : string
    {
        return $this->area;
    }
}
