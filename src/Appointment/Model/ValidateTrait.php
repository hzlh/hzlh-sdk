<?php
namespace Sdk\Appointment\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Sdk\Common\Model\IApplyAble;

use Sdk\NaturalPerson\Repository\NaturalPersonRepository;
use Sdk\Enterprise\Repository\EnterpriseRepository;

trait ValidateTrait
{
    protected function getNaturalPersonRepository() : NaturalPersonRepository
    {
        return new NaturalPersonRepository();
    }
    
    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function validate()
    {
        if ($this->getLoanProductSnapshot() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY);
            return false;
        }

        if (!$this->isAuthenticate()) {
            return false;
        }

        return true;
    }

    protected function isAuthenticate() : bool
    {
        if ($this->getLoanObject() == Appointment::LOAN_OBJECT['ENTERPRISE']) {
            return $this->isBindEnterprise();
        }

        if ($this->getLoanObject() == Appointment::LOAN_OBJECT['NATURAL_PERSON']) {
            return $this->isBindNaturalPerson();
        }

        return false;
    }

    protected function isBindNaturalPerson() : bool
    {
        $filter['member'] = $this->getMember()->getId();
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        list($count, $naturalPersonList) = $this->getNaturalPersonRepository()
            ->scenario(NaturalPersonRepository::LIST_MODEL_UN)
            ->search($filter, ['-updateTime'], 1, 1);
        unset($naturalPersonList);

        //因trait里面没办法定义常量,所以此处用了魔术数字,此处的1代表的是正常的实名认证的数据数量
        if ($count != 1) {
            Core::setLastError(USER_NOT_NATURAL_PERSON_AUTHENTICATION);
            return false;
        }

        return true;
    }

    protected function isBindEnterprise() : bool
    {
        $enterpriseId = $this->getMember()->getId();
        $enterprise = $this->getEnterpriseRepository()->fetchOne($enterpriseId);

        if ($enterprise instanceof INull) {
            Core::setLastError(USER_NOT_ENTERPRISE_AUTHENTICATION);
            return false;
        }

        return true;
    }
}
