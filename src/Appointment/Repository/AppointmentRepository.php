<?php
namespace Sdk\Appointment\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Sdk\Appointment\Adapter\Appointment\IAppointmentAdapter;
use Sdk\Appointment\Adapter\Appointment\AppointmentMockAdapter;
use Sdk\Appointment\Adapter\Appointment\AppointmentRestfulAdapter;

use Sdk\Appointment\Model\Appointment;

class AppointmentRepository extends Repository implements IAppointmentAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'APPOINTMENT_LIST';
    const PORTAL_LIST_MODEL_UN = 'APPOINTMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'APPOINTMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AppointmentRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IAppointmentAdapter
    {
        return new AppointmentMockAdapter();
    }

    public function getActualAdapter(): IAppointmentAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function completed(Appointment $appointment) : bool
    {
        return $this->getAdapter()->completed($appointment);
    }
}
