<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();
$crew = new Sdk\Crew\Model\Crew($id = 1);

// policy 测试
$policy = new Sdk\Policy\Model\Policy($id = 1);
// $labels = new Sdk\Label\Model\Label(1);
// $dispatchDepartment = new Sdk\DispatchDepartment\Model\DispatchDepartment(1);

$policyRepository = new Sdk\Policy\Repository\PolicyRepository();
$policyProductRepository = new Sdk\PolicyProduct\Repository\PolicyProductRepository();
$dispatchDepartmentRepository = new Sdk\DispatchDepartment\Repository\DispatchDepartmentRepository();
$labelsRepository = new Sdk\Label\Repository\LabelRepository();
$dictionaryRepository = new Sdk\Dictionary\Repository\DictionaryRepository();


// $policy->setId(2);
// $policy->setStatus('-2');
$policy->setTitle('政策标题dsadsadsa');
foreach ($dictionaryRepository->fetchList(array(1, 2, 3))[1] as $value) {
	$policy->addApplicableObject($value);
}
foreach ($dispatchDepartmentRepository->fetchList(array(1, 2, 3, 4))[1] as $value) {
	$policy->addDispatchDepartment($value);
}
foreach ($dictionaryRepository->fetchList(array(1, 2, 3))[1] as $value) {
	$policy->addApplicableIndustry($value);
}
$policy->setLevel($dictionaryRepository->fetchOne(1));
foreach ($dictionaryRepository->fetchList(array(1, 2, 3))[1] as $value) {
	$policy->addClassify($value);
}
$policy->setDetail(array(array("type"=>"text", "value"=>"sss")));
$policy->setDescription('政策描述---description--政策描述---description政策描述---description');
$policy->setImage(array("neme"=>"封面图","identify"=>'ewqqweqw.jpg'));
$policy->setAttachments(array(array("name"=>"政策附件","identify"=>"identify.zip")));
foreach ($labelsRepository->fetchList(array(1, 2, 3))[1] as $value) {
	$policy->addLabel($value);
}
$policy->setAdmissibleAddress(array(
					array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
					array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
					array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4")
				));
$policy->setProcessingFlow(array(array("type"=>"text", "value"=>"办理流程")));
$policy->setCrew($crew);

// $serviceRepository = new Sdk\Service\Repository\ServiceRepository();
// $service = $serviceRepository->scenario($serviceRepository::FETCH_ONE_MODEL_UN)->fetchOne(1);

// $policyProduct = new Sdk\PolicyProduct\Model\PolicyProduct();
// $policyProduct->setCategory(1);
// $policyProduct->setProduct($service);

// $policyProducts[] = $policyProduct;

// foreach ($policyProducts as $policyProduct) {
//     $policy->addPolicyProduct($policyProduct);
// }

// 取消关联
// $policyProduct = new Sdk\PolicyProduct\Model\PolicyProduct($id = 1);
// $policy->addPolicyProduct($policyProduct);

// var_dump($policy->add());//done
// var_dump($policy->edit());//done
// var_dump($policy->offStock());//done
// var_dump($policy->onShelf());//done
var_dump($policyRepository->scenario($policyRepository::FETCH_ONE_MODEL_UN)->fetchList([1,2,3])[1][1]);//done
// var_dump($policyRepository->fetchList(array(1,2)));//done
// var_dump($policy->relationProduct());//done
// var_dump($policy->cancelRelationProduct());//done
// var_dump($policyProductRepository->scenario($policyProductRepository::FETCH_ONE_MODEL_UN)->fetchOne(2));//done

// PolicyInterpretation 测试
// $policyInterpretation = new Sdk\PolicyInterpretation\Model\PolicyInterpretation();
// $policyInterpretationRepository = new Sdk\PolicyInterpretation\Repository\PolicyInterpretationRepository();

// $policyInterpretation->setId(1);
// $policyInterpretation->setStatus('-2');
// $policyInterpretation->setPolicy($policy);
// $policyInterpretation->setCover(array("neme"=>"封面图","identify"=>"ewqqweqw.jpg"));
// $policyInterpretation->setTitle('政策解读标题dasdadasd');
// $policyInterpretation->setDetail(array(array("type"=>"text", "value"=>"文本内容")));
// $policyInterpretation->setDescription('政策描述---description--政策描述---description政策描述---description');
// $policyInterpretation->setAttachments(array(array("name"=>"政策附件","identify"=>"identify.zip")));
// $policyInterpretation->setCrew($crew);

// var_dump($policyInterpretation->add());//done
// var_dump($policyInterpretation->edit());//done
// var_dump($policyInterpretation->offStock());//done
// var_dump($policyInterpretation->onShelf());//done
// var_dump($policyInterpretationRepository->fetchOne(1));//done
// var_dump($policyInterpretationRepository->fetchList(array(1,2,3)));//done


