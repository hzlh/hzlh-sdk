<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Staff\Model\Staff;
use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Repository\DictionaryRepository;
use Sdk\ProfessionalTitle\Model\ProfessionalTitle;
use Sdk\ProfessionalTitle\Repository\ProfessionalTitleRepository;

$staff = new Staff(1);
$parentCategory = new Dictionary(3);
$category = new Dictionary(4);
$type = new Dictionary(5);

$professionalTitle = new ProfessionalTitle();
$professionalTitleRepository = new ProfessionalTitleRepository();

// $professionalTitle->setParentCategory($parentCategory);
// $professionalTitle->setCategory($category);
// $professionalTitle->setType($type);
// $professionalTitle->setCertificates(array(array("name"=>"证书新", "identify"=>"证书1.jpg")));
// $professionalTitle->setStaff($staff);

// $professionalTitle->setId(1);

var_dump($professionalTitle->batchAdd(array(array($type,array(array("name"=>"证书1", "identify"=>"证书1.jpg")),$staff))));
// var_dump($professionalTitle->edit());

exit();