<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

/*
 * 进入容器运行测试
 */
// paymentPassword 测试
$paymentPassword = new Sdk\PaymentPassword\Model\PaymentPassword;
// $memberAccount = new Sdk\MemberAccount\Model\MemberAccount;
$memberAccountRepository = new Sdk\MemberAccount\Repository\MemberAccountRepository;

$memberAccount = $memberAccountRepository->fetchOne(2);

$paymentPassword->setId(2);
$paymentPassword->setCellphone('15229385743');
$paymentPassword->setPassword('314159');
$paymentPassword->setOldPassword('314158');
$paymentPassword->setMemberAccount($memberAccount);

// var_dump($paymentPassword->add($paymentPassword));//done
// var_dump($paymentPassword->edit($paymentPassword));//done
// var_dump($paymentPassword->resetPaymentPassword($paymentPassword));//done
// var_dump($paymentPassword->validate($paymentPassword));//done

