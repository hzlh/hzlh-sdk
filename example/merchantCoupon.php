<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

// coupon 测试
$coupon = new Sdk\MerchantCoupon\Model\MerchantCoupon;
$couponRepository = new Sdk\MerchantCoupon\Repository\MerchantCouponRepository();
$crew = new Sdk\Crew\Model\Crew($id = 2);
$enterprise = new Sdk\Enterprise\Model\Enterprise($id = 1);
$enterpriseId = [1];

// $coupon->setId(1);
// $coupon->setName('下单送优惠劵');
// $coupon->setCouponType(2);   // 0 ：满减券,2 ：折扣券
// $coupon->setDenomination(10);    //优惠面额,根据type传，type为0时传
// $coupon->setDiscount(30);     //优惠券折扣值,根据type传，type为2时传
// $coupon->setUseStandard(300);    //优惠券使用门槛 满300
// $coupon->setIssueTotal(1000);    //优惠券总发行量
// $coupon->setPerQuota(1);     //优惠券每人限领
// $coupon->setValidityStartTime(0);
// $coupon->setValidityEndTime(0);
// $coupon->setUseScenario(2);   //领取场景（0 : 无限制，1:注册送，2 ：下单送）
// $coupon->setReceivingMode(2);    //优惠券领取方式（0 : 手动领取，2 ：平台自动发放）
// $coupon->setReceivingUsers(0);   //优惠券可领取用户（全部用户 ：0，新人用户 ：2，vip用户 ：4）
// $coupon->setIsSuperposition(0);   //优惠券是否和店铺优惠券叠加使用（0 ：No，2 ：yes）
// $coupon->setApplyScope(1);    //优惠券使用范围（1 ：平台通用，2 ：店铺通用)）
// $coupon->setApplySituation($enterpriseId);  //平台,applyScope:1 : []；店铺，useScope:2 ：[enterpriseId])
// $coupon->setStatus(0);    //状态 （0 ：正常，-2 ：已过期，-4 ：已停用，-6 ：已删除）
// $coupon->setReleaseType(0);   // (0: 平台，2: 店铺）
// $coupon->setReference($enterprise);

// var_dump($coupon->add()); //done
// var_dump($coupon->discontinue()); //done
// var_dump($coupon->deletes()); //done
// var_dump($couponRepository->fetchOne(1)); //done
// var_dump($couponRepository->fetchList(array(1, 2))); //done