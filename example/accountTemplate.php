<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Dictionary\Model\Dictionary;
use Sdk\AccountTemplate\Repository\AccountTemplateRepository;

$accountTemplate = new Sdk\AccountTemplate\Model\AccountTemplate;

$accountTemplate->setId(1);
$accountTemplate->setName('西安分公司内账');
$accountTemplate->setTypeVat(1);
$accountTemplate->setActivationDate(1542174930);
$accountTemplate->setAccountingStandard(1);
$accountTemplate->setVoucherApproval(0);
$accountTemplate->setIndustry(new Dictionary(451));
$accountTemplate->setEnterprise(new Enterprise(1));

var_dump($accountTemplate->add()); //done
//var_dump($accountTemplate->edit()); //done

// $repository = new AccountTemplateRepository();

// var_dump($repository->scenario(AccountTemplateRepository::FETCH_ONE_MODEL_UN)->fetchOne(1)); exit();//done

exit();