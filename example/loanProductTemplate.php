<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Enterprise\Model\Enterprise;
use Sdk\LoanProductTemplate\Repository\LoanProductTemplateRepository;

$loanProductTemplate = new Sdk\LoanProductTemplate\Model\LoanProductTemplate;

$loanProductTemplate->setId(1);
$loanProductTemplate->setName('西安分公司内账');
$loanProductTemplate->setFileType(1);
$loanProductTemplate->setFile(array("name"=>"模板","identify"=>'template.jpg'));
$loanProductTemplate->setEnterprise(new Enterprise(1));

var_dump($loanProductTemplate->add()); //done
//var_dump($loanProductTemplate->edit()); //done

// $repository = new LoanProductTemplateRepository();

// var_dump($repository->scenario(LoanProductTemplateRepository::FETCH_ONE_MODEL_UN)->fetchOne(1)); exit();//done

exit();